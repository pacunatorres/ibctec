INSERT INTO TB_CONFIGURATION VALUES ('ABOUT_STORE', 'ACERCA DE LA TIENDA', 'Bienvenidos a su portal Favorito, Buses 3d Perú, Portal Busologo dedicado a la reproducción de ómnibuses interprovinciales y urbanos de todos los tiempos de nuestro país y latino américa, Buses 3D Perú es un equipo que desarrolla MODS a base de modelos 3D de ómnibuses para juegos de la saga 18 Wheels Of Steel ®, ETS2 ® y mas.', '1', null, null, null, null, '1');
INSERT INTO TB_CONFIGURATION VALUES ('AUTHENTICATION_FACEBOOK', 'CLIENT_ID', '123153357851610', '1', 'Login con Facebook', null, null, null, '1');
INSERT INTO TB_CONFIGURATION VALUES ('AUTHENTICATION_FACEBOOK', 'REDIRECT_URI', 'http://localhost:8086/public/oauth2fb', '1', 'Login con Facebook', null, null, null, '3');
INSERT INTO TB_CONFIGURATION VALUES ('AUTHENTICATION_FACEBOOK', 'SECRET_ID', '2ab5b2f62b52f8a693c6e2d7a838226e', '1', 'Login con Facebook', null, null, null, '2');
INSERT INTO TB_CONFIGURATION VALUES ('AUTHENTICATION_GOOGLE', 'CLIENT_ID', '11425588943-kftshokqqni2h138jtdlbpsfh6ndtc96.apps.googleusercontent.com', '1', 'Login con Google', null, null, null, '1');
INSERT INTO TB_CONFIGURATION VALUES ('AUTHENTICATION_GOOGLE', 'REDIRECT_URI', 'http://localhost:8086/public/oauth2callback', '1', 'Login con Google', null, null, null, '3');
INSERT INTO TB_CONFIGURATION VALUES ('AUTHENTICATION_GOOGLE', 'SECRET_ID', 'O_HTbY_76BjgOxuPBRO6qDyV', '1', 'Login con Google', null, null, null, '2');
INSERT INTO TB_CONFIGURATION VALUES ('BANNER_PRICIPAL', 'DESTINOS', 'Variedad de destinos', '1', 'Ver destinos', 'resources/commons/images/hotel-slide.jpg', '#', null, '2');
INSERT INTO TB_CONFIGURATION VALUES ('BANNER_PRICIPAL', 'SERVICIOS', 'Variedad de servicios', '1', 'Ver Servicios', 'resources/commons/images/slide15.jpg', '#', null, '1');
INSERT INTO TB_CONFIGURATION VALUES ('COPYRIGHT', 'Pasajon Copyright © 2017', null, '1', 'Buses3DPeru', null, '#', null, '1');
INSERT INTO TB_CONFIGURATION VALUES ('DOCUMENT_TYPE', 'C', 'Carnet de extranjería ', '1', null, null, null, null, '2');
INSERT INTO TB_CONFIGURATION VALUES ('DOCUMENT_TYPE', 'D', 'Dni', '1', null, null, null, null, '1');
INSERT INTO TB_CONFIGURATION VALUES ('DOCUMENT_TYPE', 'P', 'Pasaporte', '1', null, null, null, null, '3');
INSERT INTO TB_CONFIGURATION VALUES ('EMAIL_CONTACT', 'EMAIL', 'ventas@pasajon.pe', '1', null, null, null, null, '2');
INSERT INTO TB_CONFIGURATION VALUES ('GENDER', 'F', 'Femenino', '1', null, null, null, null, '2');
INSERT INTO TB_CONFIGURATION VALUES ('GENDER', 'M', 'Masculino', '1', null, null, null, null, '1');
INSERT INTO TB_CONFIGURATION VALUES ('GENDER', 'O', 'Otro', '1', null, null, null, null, '3');
INSERT INTO TB_CONFIGURATION VALUES ('LIMIT_AGE', 'END_DATE', '18', '1', null, null, null, null, '1');
INSERT INTO TB_CONFIGURATION VALUES ('PAYMENT_METHODS', 'PAYPAL', '2997D8', '1', 'Paypal', 'fa fa-cc-paypal', '#', null, '1');
INSERT INTO TB_CONFIGURATION VALUES ('PAYMENT_METHODS', 'VISA', '183391', '1', 'Visa', 'fa fa-cc-visa', '#', null, '2');
INSERT INTO TB_CONFIGURATION VALUES ('PHONE_CONTACT', 'PHONE', '(+51) 940400853', '1', null, null, null, null, '1');
INSERT INTO TB_CONFIGURATION VALUES ('PRINCIPAL_ADDRESS', 'ADDRESS', 'Av. Luna Pizarro 1134 - La Victoria', '1', null, null, null, null, '1');
INSERT INTO TB_CONFIGURATION VALUES ('SOCIAL_NETWORKS', 'FACEBOOK', null, '1', 'Facebook', 'fa fa-facebook', '#', null, '1');
INSERT INTO TB_CONFIGURATION VALUES ('SOCIAL_NETWORKS', 'INSTAGRAM', null, '0', 'Instagram', 'fa fa-instagram', '#', null, '3');
INSERT INTO TB_CONFIGURATION VALUES ('SOCIAL_NETWORKS', 'PINTEREST', null, '0', 'Pinterest', 'fa fa-pinterest', '#', null, '4');
INSERT INTO TB_CONFIGURATION VALUES ('SOCIAL_NETWORKS', 'TWITTER', null, '1', 'Twitter', 'fa fa-twitter', '#', null, '2');

INSERT INTO TB_OPTION_MENU VALUES (SQ_MENU.NEXTVAL, 'PRIVATE', '1', '0', 'Principal', '/secure/admin/dashboard', '1', '1', '1', '1');
INSERT INTO TB_OPTION_MENU VALUES (SQ_MENU.NEXTVAL, 'PRIVATE', '2', '0', 'Usuarios', '#', '1', '2', '1', '1');
INSERT INTO TB_OPTION_MENU VALUES (SQ_MENU.NEXTVAL, 'PRIVATE', '201', '2', 'Registro', '/secure/admin/dashboard/users', '2', '3', '1', '1');
INSERT INTO TB_OPTION_MENU VALUES (SQ_MENU.NEXTVAL, 'PRIVATE', '202', '2', 'Asignar Roles', '/secure/admin/dashboard/assign-roles', '2', '4', '1', '1');
INSERT INTO TB_OPTION_MENU VALUES (SQ_MENU.NEXTVAL, 'PRIVATE', '3', '0', 'Anuncios', '#', '1', '5', '1', '1');
INSERT INTO TB_OPTION_MENU VALUES (SQ_MENU.NEXTVAL, 'PRIVATE', '303', '3', 'Mis Anuncios', '/secure/admin/dashboard/advertisements', '2', '6', '1', '1');

INSERT INTO TB_ROLE VALUES (SQ_ROLE.NEXTVAL, 'ROLE_TEACHER', 'Docente');
INSERT INTO TB_ROLE VALUES (SQ_ROLE.NEXTVAL, 'ROLE_ADMIN', 'Administrador');

INSERT INTO TB_ROLE_OPTION_MENU VALUES ('2','1','1');
INSERT INTO TB_ROLE_OPTION_MENU VALUES ('2','2','1');
INSERT INTO TB_ROLE_OPTION_MENU VALUES ('2','3','1');
INSERT INTO TB_ROLE_OPTION_MENU VALUES ('2','4','1');
INSERT INTO TB_ROLE_OPTION_MENU VALUES ('2','5','1');
INSERT INTO TB_ROLE_OPTION_MENU VALUES ('2','6','1');

INSERT INTO TB_USER VALUES (SQ_USER.NEXTVAL, 42722316, 'JOSE ANTONIO', 'HURTADO VERGARA', 'joanhuve@gmail.com', 'M', 'nyeerlGUUOkBx9NluEd4hA==', '940400852', '1', 'JHURTADO', sysdate, '127.0.0.1', null, null, null);

INSERT INTO TB_USER_ROLE VALUES ('4', '2', '1');