/*==============================================================*/
/* DBMS name:      ORACLE Version 11g                           */
/* Created on:     5/22/2018 3:40:36 PM                         */
/*==============================================================*/


ALTER TABLE TB_ROLE_OPTION_MENU
   DROP CONSTRAINT FK_TB_ROLE__FK_MENU_TB_OPTIO;

ALTER TABLE TB_ROLE_OPTION_MENU
   DROP CONSTRAINT FK_TB_ROLE__FK_MENU_R_TB_ROLE;

ALTER TABLE TB_USER_ROLE
   DROP CONSTRAINT FK_TB_USER__FK_USER_TB_USER;

ALTER TABLE TB_USER_ROLE
   DROP CONSTRAINT FK_TB_USER__FK_USER_R_TB_ROLE;

ALTER TABLE TB_ACADEMIC_CONFIGURATION
   DROP PRIMARY KEY CASCADE;

DROP TABLE TB_ACADEMIC_CONFIGURATION CASCADE CONSTRAINTS;

ALTER TABLE TB_CLASSROOM
   DROP PRIMARY KEY CASCADE;

DROP TABLE TB_CLASSROOM CASCADE CONSTRAINTS;

ALTER TABLE TB_CONFIGURATION
   DROP PRIMARY KEY CASCADE;

DROP TABLE TB_CONFIGURATION CASCADE CONSTRAINTS;

ALTER TABLE TB_DEPARTMENT
   DROP PRIMARY KEY CASCADE;

DROP TABLE TB_DEPARTMENT CASCADE CONSTRAINTS;

ALTER TABLE TB_EXPLOITATION_UNIT
   DROP PRIMARY KEY CASCADE;

DROP TABLE TB_EXPLOITATION_UNIT CASCADE CONSTRAINTS;

ALTER TABLE TB_INTERVAL
   DROP PRIMARY KEY CASCADE;

DROP TABLE TB_INTERVAL CASCADE CONSTRAINTS;

ALTER TABLE TB_OPTION_MENU
   DROP PRIMARY KEY CASCADE;

DROP TABLE TB_OPTION_MENU CASCADE CONSTRAINTS;

ALTER TABLE TB_PRODUCT
   DROP PRIMARY KEY CASCADE;

DROP TABLE TB_PRODUCT CASCADE CONSTRAINTS;

ALTER TABLE USR_TEACHING_MARKING.TB_REGISTRY
   DROP PRIMARY KEY CASCADE;

DROP TABLE USR_TEACHING_MARKING.TB_REGISTRY CASCADE CONSTRAINTS;

ALTER TABLE TB_ROLE
   DROP PRIMARY KEY CASCADE;

DROP TABLE TB_ROLE CASCADE CONSTRAINTS;

DROP TABLE TB_ROLE_OPTION_MENU CASCADE CONSTRAINTS;

ALTER TABLE TB_USER
   DROP PRIMARY KEY CASCADE;

DROP TABLE TB_USER CASCADE CONSTRAINTS;

DROP TABLE TB_USER_ROLE CASCADE CONSTRAINTS;

DROP SEQUENCE SQ_ACA_CONF;

DROP SEQUENCE SQ_CLASSROOM;

DROP SEQUENCE SQ_DEPARTMENT;

DROP SEQUENCE SQ_EXP_UNIT;

DROP SEQUENCE SQ_INTERVAL;

DROP SEQUENCE SQ_MENU;

DROP SEQUENCE SQ_PRODUCT;

DROP SEQUENCE SQ_REGISTRY;

DROP SEQUENCE SQ_ROLE;

DROP SEQUENCE SQ_USER;

CREATE SEQUENCE SQ_ACA_CONF
INCREMENT BY 1
 MAXVALUE 9999999999999999999999999999
 MINVALUE 1;

CREATE SEQUENCE SQ_CLASSROOM
INCREMENT BY 1
 MAXVALUE 9999999999999999999999999999
 MINVALUE 1;

CREATE SEQUENCE SQ_DEPARTMENT
INCREMENT BY 1
 MAXVALUE 9999999999999999999999999999
 MINVALUE 1;

CREATE SEQUENCE SQ_EXP_UNIT
INCREMENT BY 1
 MAXVALUE 9999999999999999999999999999
 MINVALUE 1;

CREATE SEQUENCE SQ_INTERVAL
INCREMENT BY 1
 MAXVALUE 9999999999999999999999999999
 MINVALUE 1;

CREATE SEQUENCE SQ_MENU
INCREMENT BY 1
 MAXVALUE 9999999999999999999999999999
 MINVALUE 1;

CREATE SEQUENCE SQ_PRODUCT
INCREMENT BY 1
 MAXVALUE 9999999999999999999999999999
 MINVALUE 1;

CREATE SEQUENCE SQ_REGISTRY
INCREMENT BY 1
 MAXVALUE 9999999999999999999999999999
 MINVALUE 1;

CREATE SEQUENCE SQ_ROLE
INCREMENT BY 1
 MAXVALUE 9999999999999999999999999999
 MINVALUE 1;

CREATE SEQUENCE SQ_USER
INCREMENT BY 1
 MAXVALUE 9999999999999999999999999999
 MINVALUE 1;

/*==============================================================*/
/* Table: TB_ACADEMIC_CONFIGURATION                             */
/*==============================================================*/
CREATE TABLE TB_ACADEMIC_CONFIGURATION 
(
   ID_ACA_CONF          NUMBER               NOT NULL,
   "LEVEL"              VARCHAR2(50 BYTE),
   IP_REGISTRATION      CHAR(1 BYTE),
   ACCOUNTING           NUMBER,
   INCOME_TOLERANCE     NUMBER,
   EXIT_TOLERANCE       NUMBER,
   PREVIOUS             NUMBER,
   AFTER                NUMBER,
   ID_USER              NUMBER,
   STATE                CHAR(1 BYTE),
   COD_USER_CREA        VARCHAR2(30 BYTE)    NOT NULL,
   DATE_USER_CREA       DATE                 NOT NULL,
   DE_TERM_CREA         VARCHAR2(40 BYTE)    NOT NULL,
   COD_USER_MODI        VARCHAR2(30 BYTE),
   DATE_USER_MODI       DATE,
   DE_TERM_MODI         VARCHAR2(40 BYTE)
);

COMMENT ON TABLE TB_ACADEMIC_CONFIGURATION IS
'Tabla de configuracion academica';

ALTER TABLE TB_ACADEMIC_CONFIGURATION
   ADD CONSTRAINT PK_TB_ACADEMIC_CONFIGURATION PRIMARY KEY (ID_ACA_CONF);

/*==============================================================*/
/* Table: TB_CLASSROOM                                          */
/*==============================================================*/
CREATE TABLE TB_CLASSROOM 
(
   ID_CLASSROOM         NUMBER               NOT NULL,
   CAMPUS               VARCHAR2(15 BYTE),
   BUILDING             VARCHAR2(15 BYTE),
   CLASSROOM            VARCHAR2(15 BYTE),
   IP_ADRESS            VARCHAR2(40 BYTE),
   MAC_ADRESS           VARCHAR2(45 BYTE),
   ID_USER              NUMBER,
   STATE                CHAR(1 BYTE),
   COD_USER_CREA        VARCHAR2(30 BYTE)    NOT NULL,
   DATE_USER_CREA       DATE                 NOT NULL,
   DE_TERM_CREA         VARCHAR2(40 BYTE)    NOT NULL,
   COD_USER_MODI        VARCHAR2(30 BYTE),
   DATE_USER_MODI       DATE,
   DE_TERM_MODI         VARCHAR2(40 BYTE)
);

ALTER TABLE TB_CLASSROOM
   ADD CONSTRAINT PK_TB_CLASSROOM PRIMARY KEY (ID_CLASSROOM);

/*==============================================================*/
/* Table: TB_CONFIGURATION                                      */
/*==============================================================*/
CREATE TABLE TB_CONFIGURATION 
(
   CTABLE               VARCHAR2(30 BYTE)    NOT NULL,
   CVALUE               VARCHAR2(50 BYTE)    NOT NULL,
   CVALUEDESC           VARCHAR2(4000 BYTE),
   CSTATUS              VARCHAR2(2 BYTE),
   CTAG1                VARCHAR2(80 BYTE),
   CTAG2                VARCHAR2(80 BYTE),
   CTAG3                VARCHAR2(80 BYTE),
   CTAG4                NUMBER(40,2),
   "INDEX"              NUMBER(10,2)
);

ALTER TABLE TB_CONFIGURATION
   ADD CONSTRAINT PK_TB_CONFIGURATION PRIMARY KEY (CTABLE, CVALUE);

/*==============================================================*/
/* Table: TB_DEPARTMENT                                         */
/*==============================================================*/
CREATE TABLE TB_DEPARTMENT 
(
   ID_DEPARTMENT        NUMBER               NOT NULL,
   COD_LEVEL            CHAR(2 BYTE),
   DESC_LEVEL           VARCHAR2(60 BYTE),
   COD_DEPT             CHAR(3 BYTE),
   DESC_DEPT            VARCHAR2(60 BYTE),
   ID_USER              NUMBER,
   STATE                CHAR(1 BYTE),
   COD_USER_CREA        VARCHAR2(30 BYTE)    NOT NULL,
   DATE_USER_CREA       DATE                 NOT NULL,
   DE_TERM_CREA         VARCHAR2(40 BYTE)    NOT NULL,
   COD_USER_MODI        VARCHAR2(30 BYTE),
   DATE_USER_MODI       DATE,
   DE_TERM_MODI         VARCHAR2(40 BYTE)
);

ALTER TABLE TB_DEPARTMENT
   ADD CONSTRAINT PK_TB_DEPARTMENT PRIMARY KEY (ID_DEPARTMENT);

/*==============================================================*/
/* Table: TB_EXPLOITATION_UNIT                                  */
/*==============================================================*/
CREATE TABLE TB_EXPLOITATION_UNIT 
(
   ID_EXP_UNIT          NUMBER               NOT NULL,
   COD_CAMPUS           CHAR(3 BYTE),
   DESC_CAMPUS          VARCHAR2(40 BYTE),
   UNIT_EXP             CHAR(2 BYTE),
   DESC_UNIT_EXP        VARCHAR2(40 BYTE),
   ID_USER              NUMBER,
   STATE                CHAR(1 BYTE),
   COD_USER_CREA        VARCHAR2(30 BYTE)    NOT NULL,
   DATE_USER_CREA       DATE                 NOT NULL,
   DE_TERM_CREA         VARCHAR2(40 BYTE)    NOT NULL,
   COD_USER_MODI        VARCHAR2(30 BYTE),
   DATE_USER_MODI       DATE,
   DE_TERM_MODI         VARCHAR2(40 BYTE)
);

ALTER TABLE TB_EXPLOITATION_UNIT
   ADD CONSTRAINT PK_TB_EXPLOITATION_UNIT PRIMARY KEY (ID_EXP_UNIT);

/*==============================================================*/
/* Table: TB_INTERVAL                                           */
/*==============================================================*/
CREATE TABLE TB_INTERVAL 
(
   ID_INTERVAL          NUMBER               NOT NULL,
   YEAR                 NUMBER,
   MONTH                CHAR(3 BYTE),
   "START"              TIMESTAMP,
   END                  TIMESTAMP,
   ID_USER              NUMBER,
   STATE                CHAR(1 BYTE),
   COD_USER_CREA        VARCHAR2(30 BYTE)    NOT NULL,
   DATE_USER_CREA       DATE                 NOT NULL,
   DE_TERM_CREA         VARCHAR2(40 BYTE)    NOT NULL,
   COD_USER_MODI        VARCHAR2(30 BYTE),
   DATE_USER_MODI       DATE,
   DE_TERM_MODI         VARCHAR2(40 BYTE)
);

ALTER TABLE TB_INTERVAL
   ADD CONSTRAINT PK_TB_INTERVAL PRIMARY KEY (ID_INTERVAL);

/*==============================================================*/
/* Table: TB_OPTION_MENU                                        */
/*==============================================================*/
CREATE TABLE TB_OPTION_MENU 
(
   ID_MENU              NUMBER               NOT NULL,
   TYPE_MENU            VARCHAR2(45 BYTE)    NOT NULL,
   COD_OPTION           VARCHAR2(45 BYTE),
   COD_PARENT           VARCHAR2(45 BYTE),
   DESCRIPTION          VARCHAR2(45 BYTE),
   URL                  VARCHAR2(45 BYTE),
   "LEVEL"              VARCHAR2(45 BYTE),
   "ORDER"              VARCHAR2(45 BYTE),
   STATE                CHAR(1 BYTE),
   FLAG_SESSION         CHAR(1 BYTE)
);

ALTER TABLE TB_OPTION_MENU
   ADD CONSTRAINT PK_TB_OPTION_MENU PRIMARY KEY (ID_MENU);

/*==============================================================*/
/* Table: TB_PRODUCT                                            */
/*==============================================================*/
CREATE TABLE TB_PRODUCT 
(
   ID_PRODUCT           NUMBER               NOT NULL,
   COD_PROG             CHAR(3 BYTE),
   DESC_PROG            VARCHAR2(60 BYTE),
   COD_PROD             CHAR(3 BYTE),
   DESC_PROD            VARCHAR2(60 BYTE),
   ID_USER              NUMBER,
   STATE                CHAR(1 BYTE),
   COD_USER_CREA        VARCHAR2(30 BYTE)    NOT NULL,
   DATE_USER_CREA       DATE                 NOT NULL,
   DE_TERM_CREA         VARCHAR2(40 BYTE)    NOT NULL,
   COD_USER_MODI        VARCHAR2(30 BYTE),
   DATE_USER_MODI       DATE,
   DE_TERM_MODI         VARCHAR2(40 BYTE)
);

ALTER TABLE TB_PRODUCT
   ADD CONSTRAINT PK_TB_PRODUCT PRIMARY KEY (ID_PRODUCT);

/*==============================================================*/
/* Table: TB_REGISTRY                                           */
/*==============================================================*/
CREATE TABLE TB_REGISTRY
(
   ID_REGISTRY          NUMBER               NOT NULL,
   INTERVAL             CHAR(6 BYTE),
   NRC                  CHAR(5 BYTE),
   COD_COURSE           CHAR(5 BYTE),
   DESC_COURSE          VARCHAR2(30 BYTE),
   COD_CAMPUS           VARCHAR2(30 BYTE),
   DESC_CAMPUS          VARCHAR2(30 BYTE),
   COD_CLASSROOM        VARCHAR2(30 BYTE),
   "DATE"               DATE,
   INCOME_TIME          TIMESTAMP,
   DEPARTURE_TIME       TIMESTAMP,
   START_TIME           TIMESTAMP,
   END_TIME             TIMESTAMP,
   COURSE_TYPE          VARCHAR2(35 BYTE),
   ID_USER              NUMBER,
   STATE                CHAR(1 BYTE),
   COD_USER_CREA        VARCHAR2(30 BYTE),
   DATE_USER_CREA         DATE,
   DE_TERM_CREA         VARCHAR2(40 BYTE),
   COD_USER_MODI        VARCHAR2(30 BYTE),
   DATE_USER_MODI         DATE,
   DE_TERM_MODI         VARCHAR2(40 BYTE)
);

ALTER TABLE TB_REGISTRY
   ADD CONSTRAINT PK_TB_REGISTRY PRIMARY KEY (ID_REGISTRY);

/*==============================================================*/
/* Table: TB_ROLE                                               */
/*==============================================================*/
CREATE TABLE TB_ROLE 
(
   ID_ROLE              NUMBER               NOT NULL,
   ROLE                 VARCHAR2(12 BYTE)    NOT NULL,
   DESCRIPTION          VARCHAR2(20 BYTE)    NOT NULL
);

ALTER TABLE TB_ROLE
   ADD CONSTRAINT PK_TB_ROLE PRIMARY KEY (ID_ROLE);

/*==============================================================*/
/* Table: TB_ROLE_OPTION_MENU                                   */
/*==============================================================*/
CREATE TABLE TB_ROLE_OPTION_MENU 
(
   ID_ROLE              NUMBER               NOT NULL,
   ID_MENU              NUMBER               NOT NULL,
   STATE                CHAR(1 BYTE)         NOT NULL
);

/*==============================================================*/
/* Table: TB_USER                                               */
/*==============================================================*/
CREATE TABLE TB_USER 
(
   ID_USER              NUMBER               NOT NULL,
   CODE_AD              VARCHAR2(20 BYTE)    NOT NULL,
   NAME                 VARCHAR2(60 BYTE)    NOT NULL,
   LAST_NAME            VARCHAR2(60 BYTE),
   EMAIL                VARCHAR2(50 BYTE),
   GENDER               CHAR(1 BYTE),
   PASSWORD             VARCHAR2(80 BYTE),
   PHONE                NUMBER,
   STATE                CHAR(1 BYTE),
   COD_USER_CREA        VARCHAR2(30 BYTE)    NOT NULL,
   DATE_USER_CREA       DATE                 NOT NULL,
   DE_TERM_CREA         VARCHAR2(40 BYTE)    NOT NULL,
   COD_USER_MODI        VARCHAR2(30 BYTE),
   DATE_USER_MODI       DATE,
   DE_TERM_MODI         VARCHAR2(40 BYTE)
);

ALTER TABLE TB_USER
   ADD CONSTRAINT PK_TB_USER PRIMARY KEY (ID_USER);

/*==============================================================*/
/* Table: TB_USER_ROLE                                          */
/*==============================================================*/
CREATE TABLE TB_USER_ROLE 
(
   ID_USER              NUMBER               NOT NULL,
   ID_ROLE              NUMBER               NOT NULL,
   STATE                CHAR(1 BYTE)         NOT NULL
);

ALTER TABLE TB_ROLE_OPTION_MENU
   ADD CONSTRAINT FK_TB_ROLE__FK_MENU_TB_OPTIO FOREIGN KEY (ID_MENU)
      REFERENCES TB_OPTION_MENU (ID_MENU);

ALTER TABLE TB_ROLE_OPTION_MENU
   ADD CONSTRAINT FK_TB_ROLE__FK_MENU_R_TB_ROLE FOREIGN KEY (ID_ROLE)
      REFERENCES TB_ROLE (ID_ROLE);

ALTER TABLE TB_USER_ROLE
   ADD CONSTRAINT FK_TB_USER__FK_USER_TB_USER FOREIGN KEY (ID_USER)
      REFERENCES TB_USER (ID_USER);

ALTER TABLE TB_USER_ROLE
   ADD CONSTRAINT FK_TB_USER__FK_USER_R_TB_ROLE FOREIGN KEY (ID_ROLE)
      REFERENCES TB_ROLE (ID_ROLE);

