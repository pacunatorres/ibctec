/*
Navicat Oracle Data Transfer
Oracle Client Version : 10.2.0.5.0

Source Server         : DEVELOP-CERTUS
Source Server Version : 110200
Source Host           : LOCALHOST:1521
Source Schema         : USR_CERTUS

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2018-05-29 17:43:23
*/


-- ----------------------------
-- Table structure for TB_ACADEMIC_CONFIGURATION
-- ----------------------------
DROP TABLE "USR_CERTUS"."TB_ACADEMIC_CONFIGURATION";
CREATE TABLE "USR_CERTUS"."TB_ACADEMIC_CONFIGURATION" (
"ID_ACA_CONF" NUMBER NOT NULL ,
"LEVEL" VARCHAR2(50 BYTE) NULL ,
"IP_REGISTRATION" CHAR(1 BYTE) NULL ,
"ACCOUNTING" NUMBER NULL ,
"INCOME_TOLERANCE" NUMBER NULL ,
"EXIT_TOLERANCE" NUMBER NULL ,
"PREVIOUS" NUMBER NULL ,
"AFTER" NUMBER NULL ,
"ID_USER" NUMBER NULL ,
"STATE" CHAR(1 BYTE) NULL ,
"COD_USER_CREA" VARCHAR2(30 BYTE) NOT NULL ,
"DATE_USER_CREA" DATE NOT NULL ,
"DE_TERM_CREA" VARCHAR2(40 BYTE) NOT NULL ,
"COD_USER_MODI" VARCHAR2(30 BYTE) NULL ,
"DATE_USER_MODI" DATE NULL ,
"DE_TERM_MODI" VARCHAR2(40 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;
COMMENT ON TABLE "USR_CERTUS"."TB_ACADEMIC_CONFIGURATION" IS 'Tabla de configuracion academica';

-- ----------------------------
-- Records of TB_ACADEMIC_CONFIGURATION
-- ----------------------------
INSERT INTO "USR_CERTUS"."TB_ACADEMIC_CONFIGURATION" VALUES ('2', 'TECNICO', '1', '1', '5', '5', '5', '5', '4', '1', '4', TO_DATE('2018-05-25 17:12:27', 'YYYY-MM-DD HH24:MI:SS'), '127.1.1.0', null, null, null);
INSERT INTO "USR_CERTUS"."TB_ACADEMIC_CONFIGURATION" VALUES ('4', 'TECNICO', '1', '1', '1', '1', '1', '1', '4', '1', '4', TO_DATE('2018-05-25 17:27:07', 'YYYY-MM-DD HH24:MI:SS'), '186.160.127.57', null, null, null);
INSERT INTO "USR_CERTUS"."TB_ACADEMIC_CONFIGURATION" VALUES ('5', 'TECNICO', '1', '1', '1', '1', '1', '1', '4', '1', '4', TO_DATE('2018-05-25 17:29:03', 'YYYY-MM-DD HH24:MI:SS'), '186.160.127.57', null, null, null);
INSERT INTO "USR_CERTUS"."TB_ACADEMIC_CONFIGURATION" VALUES ('6', 'CERTIFICACION', '1', '1', '2', '2', '2', '2', '4', '1', '4', TO_DATE('2018-05-25 17:29:12', 'YYYY-MM-DD HH24:MI:SS'), '186.160.127.57', null, null, null);
INSERT INTO "USR_CERTUS"."TB_ACADEMIC_CONFIGURATION" VALUES ('7', 'CERTIFICACION', '1', '1', '2', '2', '2', '2', '4', '1', '4', TO_DATE('2018-05-25 18:16:15', 'YYYY-MM-DD HH24:MI:SS'), '186.160.127.57', null, null, null);
INSERT INTO "USR_CERTUS"."TB_ACADEMIC_CONFIGURATION" VALUES ('8', 'CERTIFICACION', '1', '1', '2', '2', '2', '2', '4', '1', '4', TO_DATE('2018-05-25 18:19:16', 'YYYY-MM-DD HH24:MI:SS'), '186.160.127.57', null, null, null);
INSERT INTO "USR_CERTUS"."TB_ACADEMIC_CONFIGURATION" VALUES ('9', 'IN HOUSE', '1', '1', '1', '1', '1', '1', '4', '1', '4', TO_DATE('2018-05-25 18:19:25', 'YYYY-MM-DD HH24:MI:SS'), '186.160.127.57', null, null, null);
INSERT INTO "USR_CERTUS"."TB_ACADEMIC_CONFIGURATION" VALUES ('10', 'CERTIFICACION', '1', '1', '2', '2', '8', '8', '4', '1', '4', TO_DATE('2018-05-25 18:23:18', 'YYYY-MM-DD HH24:MI:SS'), '186.160.127.57', null, null, null);
INSERT INTO "USR_CERTUS"."TB_ACADEMIC_CONFIGURATION" VALUES ('11', 'CERTIFICACION', '1', '1', '8', '8', '8', '8', '4', '1', '4', TO_DATE('2018-05-25 18:25:25', 'YYYY-MM-DD HH24:MI:SS'), '186.160.127.57', null, null, null);

-- ----------------------------
-- Table structure for TB_CLASSROOM
-- ----------------------------
DROP TABLE "USR_CERTUS"."TB_CLASSROOM";
CREATE TABLE "USR_CERTUS"."TB_CLASSROOM" (
"ID_CLASSROOM" NUMBER NOT NULL ,
"CAMPUS" VARCHAR2(15 BYTE) NULL ,
"BUILDING" VARCHAR2(15 BYTE) NULL ,
"CLASSROOM" VARCHAR2(15 BYTE) NULL ,
"IP_ADDRESS" VARCHAR2(40 BYTE) NULL ,
"MAC_ADDRESS" VARCHAR2(45 BYTE) NULL ,
"ID_USER" NUMBER NULL ,
"STATE" CHAR(1 BYTE) NULL ,
"COD_USER_CREA" VARCHAR2(30 BYTE) NOT NULL ,
"DATE_USER_CREA" DATE NOT NULL ,
"DE_TERM_CREA" VARCHAR2(40 BYTE) NOT NULL ,
"COD_USER_MODI" VARCHAR2(30 BYTE) NULL ,
"DATE_USER_MODI" DATE NULL ,
"DE_TERM_MODI" VARCHAR2(40 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TB_CLASSROOM
-- ----------------------------

-- ----------------------------
-- Table structure for TB_CONFIGURATION
-- ----------------------------
DROP TABLE "USR_CERTUS"."TB_CONFIGURATION";
CREATE TABLE "USR_CERTUS"."TB_CONFIGURATION" (
"CTABLE" VARCHAR2(30 BYTE) NOT NULL ,
"CVALUE" VARCHAR2(50 BYTE) NOT NULL ,
"CVALUEDESC" VARCHAR2(4000 BYTE) NULL ,
"CSTATUS" VARCHAR2(2 BYTE) NULL ,
"CTAG1" VARCHAR2(80 BYTE) NULL ,
"CTAG2" VARCHAR2(80 BYTE) NULL ,
"CTAG3" VARCHAR2(80 BYTE) NULL ,
"CTAG4" NUMBER(38,2) NULL ,
"INDEX" NUMBER(10,2) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TB_CONFIGURATION
-- ----------------------------
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('ABOUT_STORE', 'ACERCA DE LA TIENDA', 'Bienvenidos a su portal Favorito, Buses 3d Perú, Portal Busologo dedicado a la reproducción de ómnibuses interprovinciales y urbanos de todos los tiempos de nuestro país y latino américa, Buses 3D Perú es un equipo que desarrolla MODS a base de modelos 3D de ómnibuses para juegos de la saga 18 Wheels Of Steel ®, ETS2 ® y mas.', '1', null, null, null, null, '1');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('AUTHENTICATION_FACEBOOK', 'CLIENT_ID', '123153357851610', '1', 'Login con Facebook', null, null, null, '1');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('AUTHENTICATION_FACEBOOK', 'REDIRECT_URI', 'http://localhost:8086/public/oauth2fb', '1', 'Login con Facebook', null, null, null, '3');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('AUTHENTICATION_FACEBOOK', 'SECRET_ID', '2ab5b2f62b52f8a693c6e2d7a838226e', '1', 'Login con Facebook', null, null, null, '2');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('AUTHENTICATION_GOOGLE', 'CLIENT_ID', '11425588943-kftshokqqni2h138jtdlbpsfh6ndtc96.apps.googleusercontent.com', '1', 'Login con Google', null, null, null, '1');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('AUTHENTICATION_GOOGLE', 'REDIRECT_URI', 'http://localhost:8086/public/oauth2callback', '1', 'Login con Google', null, null, null, '3');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('AUTHENTICATION_GOOGLE', 'SECRET_ID', 'O_HTbY_76BjgOxuPBRO6qDyV', '1', 'Login con Google', null, null, null, '2');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('BANNER_PRICIPAL', 'DESTINOS', 'Variedad de destinos', '1', 'Ver destinos', 'resources/commons/images/hotel-slide.jpg', '#', null, '2');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('BANNER_PRICIPAL', 'SERVICIOS', 'Variedad de servicios', '1', 'Ver Servicios', 'resources/commons/images/slide15.jpg', '#', null, '1');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('COPYRIGHT', 'Pasajon Copyright © 2017', null, '1', 'Buses3DPeru', null, '#', null, '1');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('DOCUMENT_TYPE', 'C', 'Carnet de extranjería ', '1', null, null, null, null, '2');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('DOCUMENT_TYPE', 'D', 'Dni', '1', null, null, null, null, '1');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('DOCUMENT_TYPE', 'P', 'Pasaporte', '1', null, null, null, null, '3');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('EMAIL_CONTACT', 'EMAIL', 'ventas@pasajon.pe', '1', null, null, null, null, '2');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('GENDER', 'F', 'Femenino', '1', null, null, null, null, '2');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('GENDER', 'M', 'Masculino', '1', null, null, null, null, '1');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('GENDER', 'O', 'Otro', '1', null, null, null, null, '3');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('LIMIT_AGE', 'END_DATE', '18', '1', null, null, null, null, '1');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('PAYMENT_METHODS', 'PAYPAL', '2997D8', '1', 'Paypal', 'fa fa-cc-paypal', '#', null, '1');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('PAYMENT_METHODS', 'VISA', '183391', '1', 'Visa', 'fa fa-cc-visa', '#', null, '2');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('PHONE_CONTACT', 'PHONE', '(+51) 940400853', '1', null, null, null, null, '1');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('PRINCIPAL_ADDRESS', 'ADDRESS', 'Av. Luna Pizarro 1134 - La Victoria', '1', null, null, null, null, '1');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('SOCIAL_NETWORKS', 'FACEBOOK', null, '1', 'Facebook', 'fa fa-facebook', '#', null, '1');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('SOCIAL_NETWORKS', 'INSTAGRAM', null, '0', 'Instagram', 'fa fa-instagram', '#', null, '3');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('SOCIAL_NETWORKS', 'PINTEREST', null, '0', 'Pinterest', 'fa fa-pinterest', '#', null, '4');
INSERT INTO "USR_CERTUS"."TB_CONFIGURATION" VALUES ('SOCIAL_NETWORKS', 'TWITTER', null, '1', 'Twitter', 'fa fa-twitter', '#', null, '2');

-- ----------------------------
-- Table structure for TB_DEPARTMENT
-- ----------------------------
DROP TABLE "USR_CERTUS"."TB_DEPARTMENT";
CREATE TABLE "USR_CERTUS"."TB_DEPARTMENT" (
"ID_DEPARTMENT" NUMBER NOT NULL ,
"COD_LEVEL" CHAR(2 BYTE) NULL ,
"DESC_LEVEL" VARCHAR2(60 BYTE) NULL ,
"COD_DEPT" CHAR(3 BYTE) NULL ,
"DESC_DEPT" VARCHAR2(60 BYTE) NULL ,
"ID_USER" NUMBER NULL ,
"STATE" CHAR(1 BYTE) NULL ,
"COD_USER_CREA" VARCHAR2(30 BYTE) NOT NULL ,
"DATE_USER_CREA" DATE NOT NULL ,
"DE_TERM_CREA" VARCHAR2(40 BYTE) NOT NULL ,
"COD_USER_MODI" VARCHAR2(30 BYTE) NULL ,
"DATE_USER_MODI" DATE NULL ,
"DE_TERM_MODI" VARCHAR2(40 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TB_DEPARTMENT
-- ----------------------------

-- ----------------------------
-- Table structure for TB_EXPLOITATION_UNIT
-- ----------------------------
DROP TABLE "USR_CERTUS"."TB_EXPLOITATION_UNIT";
CREATE TABLE "USR_CERTUS"."TB_EXPLOITATION_UNIT" (
"ID_EXP_UNIT" NUMBER NOT NULL ,
"COD_CAMPUS" CHAR(3 BYTE) NULL ,
"DESC_CAMPUS" VARCHAR2(40 BYTE) NULL ,
"UNIT_EXP" CHAR(2 BYTE) NULL ,
"DESC_UNIT_EXP" VARCHAR2(40 BYTE) NULL ,
"ID_USER" NUMBER NULL ,
"STATE" CHAR(1 BYTE) NULL ,
"COD_USER_CREA" VARCHAR2(30 BYTE) NOT NULL ,
"DATE_USER_CREA" DATE NOT NULL ,
"DE_TERM_CREA" VARCHAR2(40 BYTE) NOT NULL ,
"COD_USER_MODI" VARCHAR2(30 BYTE) NULL ,
"DATE_USER_MODI" DATE NULL ,
"DE_TERM_MODI" VARCHAR2(40 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TB_EXPLOITATION_UNIT
-- ----------------------------

-- ----------------------------
-- Table structure for TB_INTERVAL
-- ----------------------------
DROP TABLE "USR_CERTUS"."TB_INTERVAL";
CREATE TABLE "USR_CERTUS"."TB_INTERVAL" (
"ID_INTERVAL" NUMBER NOT NULL ,
"YEAR" NUMBER NULL ,
"MONTH" VARCHAR2(15 BYTE) NULL ,
"START" TIMESTAMP(6)  NULL ,
"END" TIMESTAMP(6)  NULL ,
"ID_USER" NUMBER NULL ,
"DESC_STATE" VARCHAR2(20 BYTE) NULL ,
"STATE" CHAR(1 BYTE) NULL ,
"COD_USER_CREA" VARCHAR2(30 BYTE) NOT NULL ,
"DATE_USER_CREA" DATE NOT NULL ,
"DE_TERM_CREA" VARCHAR2(40 BYTE) NOT NULL ,
"COD_USER_MODI" VARCHAR2(30 BYTE) NULL ,
"DATE_USER_MODI" DATE NULL ,
"DE_TERM_MODI" VARCHAR2(40 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TB_INTERVAL
-- ----------------------------
INSERT INTO "USR_CERTUS"."TB_INTERVAL" VALUES ('9', '2018', 'MAYO', TO_TIMESTAMP(' 2018-05-29 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), TO_TIMESTAMP(' 2018-06-07 00:00:00:000000', 'YYYY-MM-DD HH24:MI:SS:FF6'), '4', null, '1', '4', TO_DATE('2018-05-29 17:40:21', 'YYYY-MM-DD HH24:MI:SS'), '186.160.127.57', null, null, null);

-- ----------------------------
-- Table structure for TB_OPTION_MENU
-- ----------------------------
DROP TABLE "USR_CERTUS"."TB_OPTION_MENU";
CREATE TABLE "USR_CERTUS"."TB_OPTION_MENU" (
"ID_MENU" NUMBER NOT NULL ,
"TYPE_MENU" VARCHAR2(45 BYTE) NOT NULL ,
"COD_OPTION" VARCHAR2(45 BYTE) NULL ,
"COD_PARENT" VARCHAR2(45 BYTE) NULL ,
"DESCRIPTION" VARCHAR2(45 BYTE) NULL ,
"URL" VARCHAR2(45 BYTE) NULL ,
"LEVEL" VARCHAR2(45 BYTE) NULL ,
"ORDER" VARCHAR2(45 BYTE) NULL ,
"STATE" CHAR(1 BYTE) NULL ,
"FLAG_SESSION" CHAR(1 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TB_OPTION_MENU
-- ----------------------------
INSERT INTO "USR_CERTUS"."TB_OPTION_MENU" VALUES ('1', 'PRIVATE', '1', '0', 'Principal', '/secure/admin/dashboard', '1', '1', '1', '1');
INSERT INTO "USR_CERTUS"."TB_OPTION_MENU" VALUES ('2', 'PRIVATE', '2', '0', 'Configuración', '#', '1', '2', '1', '1');
INSERT INTO "USR_CERTUS"."TB_OPTION_MENU" VALUES ('3', 'PRIVATE', '201', '2', 'Configuración académica', '/secure/admin/dashboard/academic', '2', '3', '1', '1');
INSERT INTO "USR_CERTUS"."TB_OPTION_MENU" VALUES ('4', 'PRIVATE', '202', '2', 'Configuración de sede', '/secure/admin/dashboard/site', '2', '4', '1', '1');
INSERT INTO "USR_CERTUS"."TB_OPTION_MENU" VALUES ('5', 'PRIVATE', '3', '0', 'Configuración GTH', '#', '1', '5', '1', '1');
INSERT INTO "USR_CERTUS"."TB_OPTION_MENU" VALUES ('6', 'PRIVATE', '301', '3', 'Periodos', '/secure/admin/dashboard/interval', '2', '6', '1', '1');
INSERT INTO "USR_CERTUS"."TB_OPTION_MENU" VALUES ('7', 'PRIVATE', '4', '0', 'Centro de costos', '#', '1', '7', '1', '1');
INSERT INTO "USR_CERTUS"."TB_OPTION_MENU" VALUES ('8', 'PRIVATE', '401', '4', 'Unidad de explotación', '/secure/admin/dashboard/unitexplot', '2', '8', '1', '1');
INSERT INTO "USR_CERTUS"."TB_OPTION_MENU" VALUES ('9', 'PRIVATE', '402', '4', 'Configuración de producto', '/secure/admin/dashboard/product', '2', '9', '1', '1');
INSERT INTO "USR_CERTUS"."TB_OPTION_MENU" VALUES ('10', 'PRIVATE', '403', '4', 'Configuración de departamento', '/secure/admin/dashboard/department', '2', '10', '1', '1');
INSERT INTO "USR_CERTUS"."TB_OPTION_MENU" VALUES ('11', 'PRIVATE', '5', '0', 'Registro', '#', '1', '11', '1', '1');
INSERT INTO "USR_CERTUS"."TB_OPTION_MENU" VALUES ('12', 'PRIVATE', '501', '5', 'Registrar horario', '/secure/admin/dashboard/registry', '2', '12', '1', '1');

-- ----------------------------
-- Table structure for TB_PRODUCT
-- ----------------------------
DROP TABLE "USR_CERTUS"."TB_PRODUCT";
CREATE TABLE "USR_CERTUS"."TB_PRODUCT" (
"ID_PRODUCT" NUMBER NOT NULL ,
"COD_PROG" CHAR(3 BYTE) NULL ,
"DESC_PROG" VARCHAR2(60 BYTE) NULL ,
"COD_PROD" CHAR(3 BYTE) NULL ,
"DESC_PROD" VARCHAR2(60 BYTE) NULL ,
"ID_USER" NUMBER NULL ,
"STATE" CHAR(1 BYTE) NULL ,
"COD_USER_CREA" VARCHAR2(30 BYTE) NOT NULL ,
"DATE_USER_CREA" DATE NOT NULL ,
"DE_TERM_CREA" VARCHAR2(40 BYTE) NOT NULL ,
"COD_USER_MODI" VARCHAR2(30 BYTE) NULL ,
"DATE_USER_MODI" DATE NULL ,
"DE_TERM_MODI" VARCHAR2(40 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TB_PRODUCT
-- ----------------------------

-- ----------------------------
-- Table structure for TB_REGISTRY
-- ----------------------------
DROP TABLE "USR_CERTUS"."TB_REGISTRY";
CREATE TABLE "USR_CERTUS"."TB_REGISTRY" (
"ID_REGISTRY" NUMBER NOT NULL ,
"INTERVAL" CHAR(6 BYTE) NULL ,
"NRC" CHAR(5 BYTE) NULL ,
"COD_COURSE" CHAR(5 BYTE) NULL ,
"DESC_COURSE" VARCHAR2(30 BYTE) NULL ,
"COD_CAMPUS" VARCHAR2(30 BYTE) NULL ,
"DESC_CAMPUS" VARCHAR2(30 BYTE) NULL ,
"COD_CLASSROOM" VARCHAR2(30 BYTE) NULL ,
"DATE" DATE NULL ,
"INCOME_TIME" TIMESTAMP(6)  NULL ,
"DEPARTURE_TIME" TIMESTAMP(6)  NULL ,
"START_TIME" TIMESTAMP(6)  NULL ,
"END_TIME" TIMESTAMP(6)  NULL ,
"COURSE_TYPE" VARCHAR2(35 BYTE) NULL ,
"ID_USER" NUMBER NULL ,
"STATE" CHAR(1 BYTE) NULL ,
"COD_USER_CREA" VARCHAR2(30 BYTE) NULL ,
"DATE_USER_CREA" DATE NULL ,
"DE_TERM_CREA" VARCHAR2(40 BYTE) NULL ,
"COD_USER_MODI" VARCHAR2(30 BYTE) NULL ,
"DATE_USER_MODI" DATE NULL ,
"DE_TERM_MODI" VARCHAR2(40 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TB_REGISTRY
-- ----------------------------

-- ----------------------------
-- Table structure for TB_ROLE
-- ----------------------------
DROP TABLE "USR_CERTUS"."TB_ROLE";
CREATE TABLE "USR_CERTUS"."TB_ROLE" (
"ID_ROLE" NUMBER NOT NULL ,
"ROLE" VARCHAR2(12 BYTE) NOT NULL ,
"DESCRIPTION" VARCHAR2(20 BYTE) NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TB_ROLE
-- ----------------------------
INSERT INTO "USR_CERTUS"."TB_ROLE" VALUES ('1', 'ROLE_TEACHER', 'Docente');
INSERT INTO "USR_CERTUS"."TB_ROLE" VALUES ('2', 'ROLE_ADMIN', 'Administrador');

-- ----------------------------
-- Table structure for TB_ROLE_OPTION_MENU
-- ----------------------------
DROP TABLE "USR_CERTUS"."TB_ROLE_OPTION_MENU";
CREATE TABLE "USR_CERTUS"."TB_ROLE_OPTION_MENU" (
"ID_ROLE" NUMBER NOT NULL ,
"ID_MENU" NUMBER NOT NULL ,
"STATE" CHAR(1 BYTE) NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TB_ROLE_OPTION_MENU
-- ----------------------------
INSERT INTO "USR_CERTUS"."TB_ROLE_OPTION_MENU" VALUES ('2', '1', '1');
INSERT INTO "USR_CERTUS"."TB_ROLE_OPTION_MENU" VALUES ('2', '2', '1');
INSERT INTO "USR_CERTUS"."TB_ROLE_OPTION_MENU" VALUES ('2', '3', '1');
INSERT INTO "USR_CERTUS"."TB_ROLE_OPTION_MENU" VALUES ('2', '4', '1');
INSERT INTO "USR_CERTUS"."TB_ROLE_OPTION_MENU" VALUES ('2', '5', '1');
INSERT INTO "USR_CERTUS"."TB_ROLE_OPTION_MENU" VALUES ('2', '6', '1');
INSERT INTO "USR_CERTUS"."TB_ROLE_OPTION_MENU" VALUES ('2', '7', '1');
INSERT INTO "USR_CERTUS"."TB_ROLE_OPTION_MENU" VALUES ('2', '8', '1');
INSERT INTO "USR_CERTUS"."TB_ROLE_OPTION_MENU" VALUES ('2', '9', '1');
INSERT INTO "USR_CERTUS"."TB_ROLE_OPTION_MENU" VALUES ('2', '10', '1');
INSERT INTO "USR_CERTUS"."TB_ROLE_OPTION_MENU" VALUES ('2', '11', '1');
INSERT INTO "USR_CERTUS"."TB_ROLE_OPTION_MENU" VALUES ('2', '12', '1');

-- ----------------------------
-- Table structure for TB_USER
-- ----------------------------
DROP TABLE "USR_CERTUS"."TB_USER";
CREATE TABLE "USR_CERTUS"."TB_USER" (
"ID_USER" NUMBER NOT NULL ,
"CODE_AD" VARCHAR2(20 BYTE) NOT NULL ,
"NAME" VARCHAR2(60 BYTE) NOT NULL ,
"LAST_NAME" VARCHAR2(60 BYTE) NULL ,
"EMAIL" VARCHAR2(50 BYTE) NULL ,
"GENDER" CHAR(1 BYTE) NULL ,
"PASSWORD" VARCHAR2(80 BYTE) NULL ,
"PHONE" NUMBER NULL ,
"STATE" CHAR(1 BYTE) NULL ,
"COD_USER_CREA" VARCHAR2(30 BYTE) NOT NULL ,
"DATE_USER_CREA" DATE NOT NULL ,
"DE_TERM_CREA" VARCHAR2(40 BYTE) NOT NULL ,
"COD_USER_MODI" VARCHAR2(30 BYTE) NULL ,
"DATE_USER_MODI" DATE NULL ,
"DE_TERM_MODI" VARCHAR2(40 BYTE) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TB_USER
-- ----------------------------
INSERT INTO "USR_CERTUS"."TB_USER" VALUES ('4', '42722316', 'JOSE ANTONIO', 'HURTADO VERGARA', 'joanhuve@gmail.com', 'M', 'nyeerlGUUOkBx9NluEd4hA==', '940400852', '1', 'JHURTADO', TO_DATE('2018-05-22 17:12:12', 'YYYY-MM-DD HH24:MI:SS'), '127.0.0.1', null, null, null);

-- ----------------------------
-- Table structure for TB_USER_ROLE
-- ----------------------------
DROP TABLE "USR_CERTUS"."TB_USER_ROLE";
CREATE TABLE "USR_CERTUS"."TB_USER_ROLE" (
"ID_USER" NUMBER NOT NULL ,
"ID_ROLE" NUMBER NOT NULL ,
"STATE" CHAR(1 BYTE) NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of TB_USER_ROLE
-- ----------------------------
INSERT INTO "USR_CERTUS"."TB_USER_ROLE" VALUES ('4', '2', '1');

-- ----------------------------
-- Sequence structure for SQ_ACA_CONF
-- ----------------------------
DROP SEQUENCE "USR_CERTUS"."SQ_ACA_CONF";
CREATE SEQUENCE "USR_CERTUS"."SQ_ACA_CONF"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 21
 CACHE 20;

-- ----------------------------
-- Sequence structure for SQ_CLASSROOM
-- ----------------------------
DROP SEQUENCE "USR_CERTUS"."SQ_CLASSROOM";
CREATE SEQUENCE "USR_CERTUS"."SQ_CLASSROOM"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 41
 CACHE 20;

-- ----------------------------
-- Sequence structure for SQ_DEPARTMENT
-- ----------------------------
DROP SEQUENCE "USR_CERTUS"."SQ_DEPARTMENT";
CREATE SEQUENCE "USR_CERTUS"."SQ_DEPARTMENT"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for SQ_EXP_UNIT
-- ----------------------------
DROP SEQUENCE "USR_CERTUS"."SQ_EXP_UNIT";
CREATE SEQUENCE "USR_CERTUS"."SQ_EXP_UNIT"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for SQ_INTERVAL
-- ----------------------------
DROP SEQUENCE "USR_CERTUS"."SQ_INTERVAL";
CREATE SEQUENCE "USR_CERTUS"."SQ_INTERVAL"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 21
 CACHE 20;

-- ----------------------------
-- Sequence structure for SQ_MENU
-- ----------------------------
DROP SEQUENCE "USR_CERTUS"."SQ_MENU";
CREATE SEQUENCE "USR_CERTUS"."SQ_MENU"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 21
 CACHE 20;

-- ----------------------------
-- Sequence structure for SQ_PRODUCT
-- ----------------------------
DROP SEQUENCE "USR_CERTUS"."SQ_PRODUCT";
CREATE SEQUENCE "USR_CERTUS"."SQ_PRODUCT"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for SQ_REGISTRY
-- ----------------------------
DROP SEQUENCE "USR_CERTUS"."SQ_REGISTRY";
CREATE SEQUENCE "USR_CERTUS"."SQ_REGISTRY"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 1
 CACHE 20;

-- ----------------------------
-- Sequence structure for SQ_ROLE
-- ----------------------------
DROP SEQUENCE "USR_CERTUS"."SQ_ROLE";
CREATE SEQUENCE "USR_CERTUS"."SQ_ROLE"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 21
 CACHE 20;

-- ----------------------------
-- Sequence structure for SQ_USER
-- ----------------------------
DROP SEQUENCE "USR_CERTUS"."SQ_USER";
CREATE SEQUENCE "USR_CERTUS"."SQ_USER"
 INCREMENT BY 1
 MINVALUE 1
 MAXVALUE 9999999999999999999999999999
 START WITH 21
 CACHE 20;

-- ----------------------------
-- Indexes structure for table TB_ACADEMIC_CONFIGURATION
-- ----------------------------

-- ----------------------------
-- Checks structure for table TB_ACADEMIC_CONFIGURATION
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_ACADEMIC_CONFIGURATION" ADD CHECK ("ID_ACA_CONF" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_ACADEMIC_CONFIGURATION" ADD CHECK ("COD_USER_CREA" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_ACADEMIC_CONFIGURATION" ADD CHECK ("DATE_USER_CREA" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_ACADEMIC_CONFIGURATION" ADD CHECK ("DE_TERM_CREA" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TB_ACADEMIC_CONFIGURATION
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_ACADEMIC_CONFIGURATION" ADD PRIMARY KEY ("ID_ACA_CONF");

-- ----------------------------
-- Checks structure for table TB_CLASSROOM
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_CLASSROOM" ADD CHECK ("ID_CLASSROOM" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_CLASSROOM" ADD CHECK ("COD_USER_CREA" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_CLASSROOM" ADD CHECK ("DATE_USER_CREA" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_CLASSROOM" ADD CHECK ("DE_TERM_CREA" IS NOT NULL);

-- ----------------------------
-- Indexes structure for table TB_CONFIGURATION
-- ----------------------------

-- ----------------------------
-- Checks structure for table TB_CONFIGURATION
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_CONFIGURATION" ADD CHECK ("CTABLE" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_CONFIGURATION" ADD CHECK ("CVALUE" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TB_CONFIGURATION
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_CONFIGURATION" ADD PRIMARY KEY ("CTABLE", "CVALUE");

-- ----------------------------
-- Indexes structure for table TB_DEPARTMENT
-- ----------------------------

-- ----------------------------
-- Checks structure for table TB_DEPARTMENT
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_DEPARTMENT" ADD CHECK ("ID_DEPARTMENT" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_DEPARTMENT" ADD CHECK ("COD_USER_CREA" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_DEPARTMENT" ADD CHECK ("DATE_USER_CREA" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_DEPARTMENT" ADD CHECK ("DE_TERM_CREA" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TB_DEPARTMENT
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_DEPARTMENT" ADD PRIMARY KEY ("ID_DEPARTMENT");

-- ----------------------------
-- Indexes structure for table TB_EXPLOITATION_UNIT
-- ----------------------------

-- ----------------------------
-- Checks structure for table TB_EXPLOITATION_UNIT
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_EXPLOITATION_UNIT" ADD CHECK ("ID_EXP_UNIT" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_EXPLOITATION_UNIT" ADD CHECK ("COD_USER_CREA" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_EXPLOITATION_UNIT" ADD CHECK ("DATE_USER_CREA" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_EXPLOITATION_UNIT" ADD CHECK ("DE_TERM_CREA" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TB_EXPLOITATION_UNIT
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_EXPLOITATION_UNIT" ADD PRIMARY KEY ("ID_EXP_UNIT");

-- ----------------------------
-- Checks structure for table TB_INTERVAL
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_INTERVAL" ADD CHECK ("ID_INTERVAL" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_INTERVAL" ADD CHECK ("COD_USER_CREA" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_INTERVAL" ADD CHECK ("DATE_USER_CREA" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_INTERVAL" ADD CHECK ("DE_TERM_CREA" IS NOT NULL);

-- ----------------------------
-- Indexes structure for table TB_OPTION_MENU
-- ----------------------------

-- ----------------------------
-- Checks structure for table TB_OPTION_MENU
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_OPTION_MENU" ADD CHECK ("ID_MENU" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_OPTION_MENU" ADD CHECK ("TYPE_MENU" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TB_OPTION_MENU
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_OPTION_MENU" ADD PRIMARY KEY ("ID_MENU");

-- ----------------------------
-- Indexes structure for table TB_PRODUCT
-- ----------------------------

-- ----------------------------
-- Checks structure for table TB_PRODUCT
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_PRODUCT" ADD CHECK ("ID_PRODUCT" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_PRODUCT" ADD CHECK ("COD_USER_CREA" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_PRODUCT" ADD CHECK ("DATE_USER_CREA" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_PRODUCT" ADD CHECK ("DE_TERM_CREA" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TB_PRODUCT
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_PRODUCT" ADD PRIMARY KEY ("ID_PRODUCT");

-- ----------------------------
-- Indexes structure for table TB_REGISTRY
-- ----------------------------

-- ----------------------------
-- Checks structure for table TB_REGISTRY
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_REGISTRY" ADD CHECK ("ID_REGISTRY" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TB_REGISTRY
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_REGISTRY" ADD PRIMARY KEY ("ID_REGISTRY");

-- ----------------------------
-- Indexes structure for table TB_ROLE
-- ----------------------------

-- ----------------------------
-- Checks structure for table TB_ROLE
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_ROLE" ADD CHECK ("ID_ROLE" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_ROLE" ADD CHECK ("ROLE" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_ROLE" ADD CHECK ("DESCRIPTION" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TB_ROLE
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_ROLE" ADD PRIMARY KEY ("ID_ROLE");

-- ----------------------------
-- Checks structure for table TB_ROLE_OPTION_MENU
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_ROLE_OPTION_MENU" ADD CHECK ("ID_ROLE" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_ROLE_OPTION_MENU" ADD CHECK ("ID_MENU" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_ROLE_OPTION_MENU" ADD CHECK ("STATE" IS NOT NULL);

-- ----------------------------
-- Indexes structure for table TB_USER
-- ----------------------------

-- ----------------------------
-- Checks structure for table TB_USER
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_USER" ADD CHECK ("ID_USER" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_USER" ADD CHECK ("CODE_AD" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_USER" ADD CHECK ("NAME" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_USER" ADD CHECK ("COD_USER_CREA" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_USER" ADD CHECK ("DATE_USER_CREA" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_USER" ADD CHECK ("DE_TERM_CREA" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TB_USER
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_USER" ADD PRIMARY KEY ("ID_USER");

-- ----------------------------
-- Checks structure for table TB_USER_ROLE
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_USER_ROLE" ADD CHECK ("ID_USER" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_USER_ROLE" ADD CHECK ("ID_ROLE" IS NOT NULL);
ALTER TABLE "USR_CERTUS"."TB_USER_ROLE" ADD CHECK ("STATE" IS NOT NULL);

-- ----------------------------
-- Foreign Key structure for table "USR_CERTUS"."TB_ROLE_OPTION_MENU"
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_ROLE_OPTION_MENU" ADD FOREIGN KEY ("ID_ROLE") REFERENCES "USR_CERTUS"."TB_ROLE" ("ID_ROLE");
ALTER TABLE "USR_CERTUS"."TB_ROLE_OPTION_MENU" ADD FOREIGN KEY ("ID_MENU") REFERENCES "USR_CERTUS"."TB_OPTION_MENU" ("ID_MENU");

-- ----------------------------
-- Foreign Key structure for table "USR_CERTUS"."TB_USER_ROLE"
-- ----------------------------
ALTER TABLE "USR_CERTUS"."TB_USER_ROLE" ADD FOREIGN KEY ("ID_ROLE") REFERENCES "USR_CERTUS"."TB_ROLE" ("ID_ROLE");
ALTER TABLE "USR_CERTUS"."TB_USER_ROLE" ADD FOREIGN KEY ("ID_USER") REFERENCES "USR_CERTUS"."TB_USER" ("ID_USER");
