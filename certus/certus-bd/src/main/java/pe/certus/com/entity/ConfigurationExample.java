package pe.certus.com.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class ConfigurationExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table TB_CONFIGURATION
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table TB_CONFIGURATION
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table TB_CONFIGURATION
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    protected List<Criteria> oredCriteria;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_CONFIGURATION
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public ConfigurationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_CONFIGURATION
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_CONFIGURATION
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_CONFIGURATION
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_CONFIGURATION
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_CONFIGURATION
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_CONFIGURATION
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_CONFIGURATION
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_CONFIGURATION
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_CONFIGURATION
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_CONFIGURATION
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table TB_CONFIGURATION
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCtableIsNull() {
            addCriterion("CTABLE is null");
            return (Criteria) this;
        }

        public Criteria andCtableIsNotNull() {
            addCriterion("CTABLE is not null");
            return (Criteria) this;
        }

        public Criteria andCtableEqualTo(String value) {
            addCriterion("CTABLE =", value, "ctable");
            return (Criteria) this;
        }

        public Criteria andCtableNotEqualTo(String value) {
            addCriterion("CTABLE <>", value, "ctable");
            return (Criteria) this;
        }

        public Criteria andCtableGreaterThan(String value) {
            addCriterion("CTABLE >", value, "ctable");
            return (Criteria) this;
        }

        public Criteria andCtableGreaterThanOrEqualTo(String value) {
            addCriterion("CTABLE >=", value, "ctable");
            return (Criteria) this;
        }

        public Criteria andCtableLessThan(String value) {
            addCriterion("CTABLE <", value, "ctable");
            return (Criteria) this;
        }

        public Criteria andCtableLessThanOrEqualTo(String value) {
            addCriterion("CTABLE <=", value, "ctable");
            return (Criteria) this;
        }

        public Criteria andCtableLike(String value) {
            addCriterion("CTABLE like", value, "ctable");
            return (Criteria) this;
        }

        public Criteria andCtableNotLike(String value) {
            addCriterion("CTABLE not like", value, "ctable");
            return (Criteria) this;
        }

        public Criteria andCtableIn(List<String> values) {
            addCriterion("CTABLE in", values, "ctable");
            return (Criteria) this;
        }

        public Criteria andCtableNotIn(List<String> values) {
            addCriterion("CTABLE not in", values, "ctable");
            return (Criteria) this;
        }

        public Criteria andCtableBetween(String value1, String value2) {
            addCriterion("CTABLE between", value1, value2, "ctable");
            return (Criteria) this;
        }

        public Criteria andCtableNotBetween(String value1, String value2) {
            addCriterion("CTABLE not between", value1, value2, "ctable");
            return (Criteria) this;
        }

        public Criteria andCvalueIsNull() {
            addCriterion("CVALUE is null");
            return (Criteria) this;
        }

        public Criteria andCvalueIsNotNull() {
            addCriterion("CVALUE is not null");
            return (Criteria) this;
        }

        public Criteria andCvalueEqualTo(String value) {
            addCriterion("CVALUE =", value, "cvalue");
            return (Criteria) this;
        }

        public Criteria andCvalueNotEqualTo(String value) {
            addCriterion("CVALUE <>", value, "cvalue");
            return (Criteria) this;
        }

        public Criteria andCvalueGreaterThan(String value) {
            addCriterion("CVALUE >", value, "cvalue");
            return (Criteria) this;
        }

        public Criteria andCvalueGreaterThanOrEqualTo(String value) {
            addCriterion("CVALUE >=", value, "cvalue");
            return (Criteria) this;
        }

        public Criteria andCvalueLessThan(String value) {
            addCriterion("CVALUE <", value, "cvalue");
            return (Criteria) this;
        }

        public Criteria andCvalueLessThanOrEqualTo(String value) {
            addCriterion("CVALUE <=", value, "cvalue");
            return (Criteria) this;
        }

        public Criteria andCvalueLike(String value) {
            addCriterion("CVALUE like", value, "cvalue");
            return (Criteria) this;
        }

        public Criteria andCvalueNotLike(String value) {
            addCriterion("CVALUE not like", value, "cvalue");
            return (Criteria) this;
        }

        public Criteria andCvalueIn(List<String> values) {
            addCriterion("CVALUE in", values, "cvalue");
            return (Criteria) this;
        }

        public Criteria andCvalueNotIn(List<String> values) {
            addCriterion("CVALUE not in", values, "cvalue");
            return (Criteria) this;
        }

        public Criteria andCvalueBetween(String value1, String value2) {
            addCriterion("CVALUE between", value1, value2, "cvalue");
            return (Criteria) this;
        }

        public Criteria andCvalueNotBetween(String value1, String value2) {
            addCriterion("CVALUE not between", value1, value2, "cvalue");
            return (Criteria) this;
        }

        public Criteria andCvaluedescIsNull() {
            addCriterion("CVALUEDESC is null");
            return (Criteria) this;
        }

        public Criteria andCvaluedescIsNotNull() {
            addCriterion("CVALUEDESC is not null");
            return (Criteria) this;
        }

        public Criteria andCvaluedescEqualTo(String value) {
            addCriterion("CVALUEDESC =", value, "cvaluedesc");
            return (Criteria) this;
        }

        public Criteria andCvaluedescNotEqualTo(String value) {
            addCriterion("CVALUEDESC <>", value, "cvaluedesc");
            return (Criteria) this;
        }

        public Criteria andCvaluedescGreaterThan(String value) {
            addCriterion("CVALUEDESC >", value, "cvaluedesc");
            return (Criteria) this;
        }

        public Criteria andCvaluedescGreaterThanOrEqualTo(String value) {
            addCriterion("CVALUEDESC >=", value, "cvaluedesc");
            return (Criteria) this;
        }

        public Criteria andCvaluedescLessThan(String value) {
            addCriterion("CVALUEDESC <", value, "cvaluedesc");
            return (Criteria) this;
        }

        public Criteria andCvaluedescLessThanOrEqualTo(String value) {
            addCriterion("CVALUEDESC <=", value, "cvaluedesc");
            return (Criteria) this;
        }

        public Criteria andCvaluedescLike(String value) {
            addCriterion("CVALUEDESC like", value, "cvaluedesc");
            return (Criteria) this;
        }

        public Criteria andCvaluedescNotLike(String value) {
            addCriterion("CVALUEDESC not like", value, "cvaluedesc");
            return (Criteria) this;
        }

        public Criteria andCvaluedescIn(List<String> values) {
            addCriterion("CVALUEDESC in", values, "cvaluedesc");
            return (Criteria) this;
        }

        public Criteria andCvaluedescNotIn(List<String> values) {
            addCriterion("CVALUEDESC not in", values, "cvaluedesc");
            return (Criteria) this;
        }

        public Criteria andCvaluedescBetween(String value1, String value2) {
            addCriterion("CVALUEDESC between", value1, value2, "cvaluedesc");
            return (Criteria) this;
        }

        public Criteria andCvaluedescNotBetween(String value1, String value2) {
            addCriterion("CVALUEDESC not between", value1, value2, "cvaluedesc");
            return (Criteria) this;
        }

        public Criteria andCstatusIsNull() {
            addCriterion("CSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andCstatusIsNotNull() {
            addCriterion("CSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andCstatusEqualTo(String value) {
            addCriterion("CSTATUS =", value, "cstatus");
            return (Criteria) this;
        }

        public Criteria andCstatusNotEqualTo(String value) {
            addCriterion("CSTATUS <>", value, "cstatus");
            return (Criteria) this;
        }

        public Criteria andCstatusGreaterThan(String value) {
            addCriterion("CSTATUS >", value, "cstatus");
            return (Criteria) this;
        }

        public Criteria andCstatusGreaterThanOrEqualTo(String value) {
            addCriterion("CSTATUS >=", value, "cstatus");
            return (Criteria) this;
        }

        public Criteria andCstatusLessThan(String value) {
            addCriterion("CSTATUS <", value, "cstatus");
            return (Criteria) this;
        }

        public Criteria andCstatusLessThanOrEqualTo(String value) {
            addCriterion("CSTATUS <=", value, "cstatus");
            return (Criteria) this;
        }

        public Criteria andCstatusLike(String value) {
            addCriterion("CSTATUS like", value, "cstatus");
            return (Criteria) this;
        }

        public Criteria andCstatusNotLike(String value) {
            addCriterion("CSTATUS not like", value, "cstatus");
            return (Criteria) this;
        }

        public Criteria andCstatusIn(List<String> values) {
            addCriterion("CSTATUS in", values, "cstatus");
            return (Criteria) this;
        }

        public Criteria andCstatusNotIn(List<String> values) {
            addCriterion("CSTATUS not in", values, "cstatus");
            return (Criteria) this;
        }

        public Criteria andCstatusBetween(String value1, String value2) {
            addCriterion("CSTATUS between", value1, value2, "cstatus");
            return (Criteria) this;
        }

        public Criteria andCstatusNotBetween(String value1, String value2) {
            addCriterion("CSTATUS not between", value1, value2, "cstatus");
            return (Criteria) this;
        }

        public Criteria andCtag1IsNull() {
            addCriterion("CTAG1 is null");
            return (Criteria) this;
        }

        public Criteria andCtag1IsNotNull() {
            addCriterion("CTAG1 is not null");
            return (Criteria) this;
        }

        public Criteria andCtag1EqualTo(String value) {
            addCriterion("CTAG1 =", value, "ctag1");
            return (Criteria) this;
        }

        public Criteria andCtag1NotEqualTo(String value) {
            addCriterion("CTAG1 <>", value, "ctag1");
            return (Criteria) this;
        }

        public Criteria andCtag1GreaterThan(String value) {
            addCriterion("CTAG1 >", value, "ctag1");
            return (Criteria) this;
        }

        public Criteria andCtag1GreaterThanOrEqualTo(String value) {
            addCriterion("CTAG1 >=", value, "ctag1");
            return (Criteria) this;
        }

        public Criteria andCtag1LessThan(String value) {
            addCriterion("CTAG1 <", value, "ctag1");
            return (Criteria) this;
        }

        public Criteria andCtag1LessThanOrEqualTo(String value) {
            addCriterion("CTAG1 <=", value, "ctag1");
            return (Criteria) this;
        }

        public Criteria andCtag1Like(String value) {
            addCriterion("CTAG1 like", value, "ctag1");
            return (Criteria) this;
        }

        public Criteria andCtag1NotLike(String value) {
            addCriterion("CTAG1 not like", value, "ctag1");
            return (Criteria) this;
        }

        public Criteria andCtag1In(List<String> values) {
            addCriterion("CTAG1 in", values, "ctag1");
            return (Criteria) this;
        }

        public Criteria andCtag1NotIn(List<String> values) {
            addCriterion("CTAG1 not in", values, "ctag1");
            return (Criteria) this;
        }

        public Criteria andCtag1Between(String value1, String value2) {
            addCriterion("CTAG1 between", value1, value2, "ctag1");
            return (Criteria) this;
        }

        public Criteria andCtag1NotBetween(String value1, String value2) {
            addCriterion("CTAG1 not between", value1, value2, "ctag1");
            return (Criteria) this;
        }

        public Criteria andCtag2IsNull() {
            addCriterion("CTAG2 is null");
            return (Criteria) this;
        }

        public Criteria andCtag2IsNotNull() {
            addCriterion("CTAG2 is not null");
            return (Criteria) this;
        }

        public Criteria andCtag2EqualTo(String value) {
            addCriterion("CTAG2 =", value, "ctag2");
            return (Criteria) this;
        }

        public Criteria andCtag2NotEqualTo(String value) {
            addCriterion("CTAG2 <>", value, "ctag2");
            return (Criteria) this;
        }

        public Criteria andCtag2GreaterThan(String value) {
            addCriterion("CTAG2 >", value, "ctag2");
            return (Criteria) this;
        }

        public Criteria andCtag2GreaterThanOrEqualTo(String value) {
            addCriterion("CTAG2 >=", value, "ctag2");
            return (Criteria) this;
        }

        public Criteria andCtag2LessThan(String value) {
            addCriterion("CTAG2 <", value, "ctag2");
            return (Criteria) this;
        }

        public Criteria andCtag2LessThanOrEqualTo(String value) {
            addCriterion("CTAG2 <=", value, "ctag2");
            return (Criteria) this;
        }

        public Criteria andCtag2Like(String value) {
            addCriterion("CTAG2 like", value, "ctag2");
            return (Criteria) this;
        }

        public Criteria andCtag2NotLike(String value) {
            addCriterion("CTAG2 not like", value, "ctag2");
            return (Criteria) this;
        }

        public Criteria andCtag2In(List<String> values) {
            addCriterion("CTAG2 in", values, "ctag2");
            return (Criteria) this;
        }

        public Criteria andCtag2NotIn(List<String> values) {
            addCriterion("CTAG2 not in", values, "ctag2");
            return (Criteria) this;
        }

        public Criteria andCtag2Between(String value1, String value2) {
            addCriterion("CTAG2 between", value1, value2, "ctag2");
            return (Criteria) this;
        }

        public Criteria andCtag2NotBetween(String value1, String value2) {
            addCriterion("CTAG2 not between", value1, value2, "ctag2");
            return (Criteria) this;
        }

        public Criteria andCtag3IsNull() {
            addCriterion("CTAG3 is null");
            return (Criteria) this;
        }

        public Criteria andCtag3IsNotNull() {
            addCriterion("CTAG3 is not null");
            return (Criteria) this;
        }

        public Criteria andCtag3EqualTo(String value) {
            addCriterion("CTAG3 =", value, "ctag3");
            return (Criteria) this;
        }

        public Criteria andCtag3NotEqualTo(String value) {
            addCriterion("CTAG3 <>", value, "ctag3");
            return (Criteria) this;
        }

        public Criteria andCtag3GreaterThan(String value) {
            addCriterion("CTAG3 >", value, "ctag3");
            return (Criteria) this;
        }

        public Criteria andCtag3GreaterThanOrEqualTo(String value) {
            addCriterion("CTAG3 >=", value, "ctag3");
            return (Criteria) this;
        }

        public Criteria andCtag3LessThan(String value) {
            addCriterion("CTAG3 <", value, "ctag3");
            return (Criteria) this;
        }

        public Criteria andCtag3LessThanOrEqualTo(String value) {
            addCriterion("CTAG3 <=", value, "ctag3");
            return (Criteria) this;
        }

        public Criteria andCtag3Like(String value) {
            addCriterion("CTAG3 like", value, "ctag3");
            return (Criteria) this;
        }

        public Criteria andCtag3NotLike(String value) {
            addCriterion("CTAG3 not like", value, "ctag3");
            return (Criteria) this;
        }

        public Criteria andCtag3In(List<String> values) {
            addCriterion("CTAG3 in", values, "ctag3");
            return (Criteria) this;
        }

        public Criteria andCtag3NotIn(List<String> values) {
            addCriterion("CTAG3 not in", values, "ctag3");
            return (Criteria) this;
        }

        public Criteria andCtag3Between(String value1, String value2) {
            addCriterion("CTAG3 between", value1, value2, "ctag3");
            return (Criteria) this;
        }

        public Criteria andCtag3NotBetween(String value1, String value2) {
            addCriterion("CTAG3 not between", value1, value2, "ctag3");
            return (Criteria) this;
        }

        public Criteria andCtag4IsNull() {
            addCriterion("CTAG4 is null");
            return (Criteria) this;
        }

        public Criteria andCtag4IsNotNull() {
            addCriterion("CTAG4 is not null");
            return (Criteria) this;
        }

        public Criteria andCtag4EqualTo(BigDecimal value) {
            addCriterion("CTAG4 =", value, "ctag4");
            return (Criteria) this;
        }

        public Criteria andCtag4NotEqualTo(BigDecimal value) {
            addCriterion("CTAG4 <>", value, "ctag4");
            return (Criteria) this;
        }

        public Criteria andCtag4GreaterThan(BigDecimal value) {
            addCriterion("CTAG4 >", value, "ctag4");
            return (Criteria) this;
        }

        public Criteria andCtag4GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("CTAG4 >=", value, "ctag4");
            return (Criteria) this;
        }

        public Criteria andCtag4LessThan(BigDecimal value) {
            addCriterion("CTAG4 <", value, "ctag4");
            return (Criteria) this;
        }

        public Criteria andCtag4LessThanOrEqualTo(BigDecimal value) {
            addCriterion("CTAG4 <=", value, "ctag4");
            return (Criteria) this;
        }

        public Criteria andCtag4In(List<BigDecimal> values) {
            addCriterion("CTAG4 in", values, "ctag4");
            return (Criteria) this;
        }

        public Criteria andCtag4NotIn(List<BigDecimal> values) {
            addCriterion("CTAG4 not in", values, "ctag4");
            return (Criteria) this;
        }

        public Criteria andCtag4Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("CTAG4 between", value1, value2, "ctag4");
            return (Criteria) this;
        }

        public Criteria andCtag4NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("CTAG4 not between", value1, value2, "ctag4");
            return (Criteria) this;
        }

        public Criteria andIndexIsNull() {
            addCriterion("INDEX is null");
            return (Criteria) this;
        }

        public Criteria andIndexIsNotNull() {
            addCriterion("INDEX is not null");
            return (Criteria) this;
        }

        public Criteria andIndexEqualTo(BigDecimal value) {
            addCriterion("INDEX =", value, "index");
            return (Criteria) this;
        }

        public Criteria andIndexNotEqualTo(BigDecimal value) {
            addCriterion("INDEX <>", value, "index");
            return (Criteria) this;
        }

        public Criteria andIndexGreaterThan(BigDecimal value) {
            addCriterion("INDEX >", value, "index");
            return (Criteria) this;
        }

        public Criteria andIndexGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("INDEX >=", value, "index");
            return (Criteria) this;
        }

        public Criteria andIndexLessThan(BigDecimal value) {
            addCriterion("INDEX <", value, "index");
            return (Criteria) this;
        }

        public Criteria andIndexLessThanOrEqualTo(BigDecimal value) {
            addCriterion("INDEX <=", value, "index");
            return (Criteria) this;
        }

        public Criteria andIndexIn(List<BigDecimal> values) {
            addCriterion("INDEX in", values, "index");
            return (Criteria) this;
        }

        public Criteria andIndexNotIn(List<BigDecimal> values) {
            addCriterion("INDEX not in", values, "index");
            return (Criteria) this;
        }

        public Criteria andIndexBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("INDEX between", value1, value2, "index");
            return (Criteria) this;
        }

        public Criteria andIndexNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("INDEX not between", value1, value2, "index");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table TB_CONFIGURATION
     *
     * @mbggenerated do_not_delete_during_merge Wed May 23 12:07:48 COT 2018
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table TB_CONFIGURATION
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}