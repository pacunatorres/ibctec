package pe.certus.com.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class OptionMenuExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table TB_OPTION_MENU
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table TB_OPTION_MENU
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table TB_OPTION_MENU
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    protected List<Criteria> oredCriteria;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_OPTION_MENU
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public OptionMenuExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_OPTION_MENU
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_OPTION_MENU
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_OPTION_MENU
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_OPTION_MENU
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_OPTION_MENU
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_OPTION_MENU
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_OPTION_MENU
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_OPTION_MENU
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_OPTION_MENU
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_OPTION_MENU
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table TB_OPTION_MENU
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdMenuIsNull() {
            addCriterion("ID_MENU is null");
            return (Criteria) this;
        }

        public Criteria andIdMenuIsNotNull() {
            addCriterion("ID_MENU is not null");
            return (Criteria) this;
        }

        public Criteria andIdMenuEqualTo(BigDecimal value) {
            addCriterion("ID_MENU =", value, "idMenu");
            return (Criteria) this;
        }

        public Criteria andIdMenuNotEqualTo(BigDecimal value) {
            addCriterion("ID_MENU <>", value, "idMenu");
            return (Criteria) this;
        }

        public Criteria andIdMenuGreaterThan(BigDecimal value) {
            addCriterion("ID_MENU >", value, "idMenu");
            return (Criteria) this;
        }

        public Criteria andIdMenuGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("ID_MENU >=", value, "idMenu");
            return (Criteria) this;
        }

        public Criteria andIdMenuLessThan(BigDecimal value) {
            addCriterion("ID_MENU <", value, "idMenu");
            return (Criteria) this;
        }

        public Criteria andIdMenuLessThanOrEqualTo(BigDecimal value) {
            addCriterion("ID_MENU <=", value, "idMenu");
            return (Criteria) this;
        }

        public Criteria andIdMenuIn(List<BigDecimal> values) {
            addCriterion("ID_MENU in", values, "idMenu");
            return (Criteria) this;
        }

        public Criteria andIdMenuNotIn(List<BigDecimal> values) {
            addCriterion("ID_MENU not in", values, "idMenu");
            return (Criteria) this;
        }

        public Criteria andIdMenuBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ID_MENU between", value1, value2, "idMenu");
            return (Criteria) this;
        }

        public Criteria andIdMenuNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ID_MENU not between", value1, value2, "idMenu");
            return (Criteria) this;
        }

        public Criteria andTypeMenuIsNull() {
            addCriterion("TYPE_MENU is null");
            return (Criteria) this;
        }

        public Criteria andTypeMenuIsNotNull() {
            addCriterion("TYPE_MENU is not null");
            return (Criteria) this;
        }

        public Criteria andTypeMenuEqualTo(String value) {
            addCriterion("TYPE_MENU =", value, "typeMenu");
            return (Criteria) this;
        }

        public Criteria andTypeMenuNotEqualTo(String value) {
            addCriterion("TYPE_MENU <>", value, "typeMenu");
            return (Criteria) this;
        }

        public Criteria andTypeMenuGreaterThan(String value) {
            addCriterion("TYPE_MENU >", value, "typeMenu");
            return (Criteria) this;
        }

        public Criteria andTypeMenuGreaterThanOrEqualTo(String value) {
            addCriterion("TYPE_MENU >=", value, "typeMenu");
            return (Criteria) this;
        }

        public Criteria andTypeMenuLessThan(String value) {
            addCriterion("TYPE_MENU <", value, "typeMenu");
            return (Criteria) this;
        }

        public Criteria andTypeMenuLessThanOrEqualTo(String value) {
            addCriterion("TYPE_MENU <=", value, "typeMenu");
            return (Criteria) this;
        }

        public Criteria andTypeMenuLike(String value) {
            addCriterion("TYPE_MENU like", value, "typeMenu");
            return (Criteria) this;
        }

        public Criteria andTypeMenuNotLike(String value) {
            addCriterion("TYPE_MENU not like", value, "typeMenu");
            return (Criteria) this;
        }

        public Criteria andTypeMenuIn(List<String> values) {
            addCriterion("TYPE_MENU in", values, "typeMenu");
            return (Criteria) this;
        }

        public Criteria andTypeMenuNotIn(List<String> values) {
            addCriterion("TYPE_MENU not in", values, "typeMenu");
            return (Criteria) this;
        }

        public Criteria andTypeMenuBetween(String value1, String value2) {
            addCriterion("TYPE_MENU between", value1, value2, "typeMenu");
            return (Criteria) this;
        }

        public Criteria andTypeMenuNotBetween(String value1, String value2) {
            addCriterion("TYPE_MENU not between", value1, value2, "typeMenu");
            return (Criteria) this;
        }

        public Criteria andCodOptionIsNull() {
            addCriterion("COD_OPTION is null");
            return (Criteria) this;
        }

        public Criteria andCodOptionIsNotNull() {
            addCriterion("COD_OPTION is not null");
            return (Criteria) this;
        }

        public Criteria andCodOptionEqualTo(String value) {
            addCriterion("COD_OPTION =", value, "codOption");
            return (Criteria) this;
        }

        public Criteria andCodOptionNotEqualTo(String value) {
            addCriterion("COD_OPTION <>", value, "codOption");
            return (Criteria) this;
        }

        public Criteria andCodOptionGreaterThan(String value) {
            addCriterion("COD_OPTION >", value, "codOption");
            return (Criteria) this;
        }

        public Criteria andCodOptionGreaterThanOrEqualTo(String value) {
            addCriterion("COD_OPTION >=", value, "codOption");
            return (Criteria) this;
        }

        public Criteria andCodOptionLessThan(String value) {
            addCriterion("COD_OPTION <", value, "codOption");
            return (Criteria) this;
        }

        public Criteria andCodOptionLessThanOrEqualTo(String value) {
            addCriterion("COD_OPTION <=", value, "codOption");
            return (Criteria) this;
        }

        public Criteria andCodOptionLike(String value) {
            addCriterion("COD_OPTION like", value, "codOption");
            return (Criteria) this;
        }

        public Criteria andCodOptionNotLike(String value) {
            addCriterion("COD_OPTION not like", value, "codOption");
            return (Criteria) this;
        }

        public Criteria andCodOptionIn(List<String> values) {
            addCriterion("COD_OPTION in", values, "codOption");
            return (Criteria) this;
        }

        public Criteria andCodOptionNotIn(List<String> values) {
            addCriterion("COD_OPTION not in", values, "codOption");
            return (Criteria) this;
        }

        public Criteria andCodOptionBetween(String value1, String value2) {
            addCriterion("COD_OPTION between", value1, value2, "codOption");
            return (Criteria) this;
        }

        public Criteria andCodOptionNotBetween(String value1, String value2) {
            addCriterion("COD_OPTION not between", value1, value2, "codOption");
            return (Criteria) this;
        }

        public Criteria andCodParentIsNull() {
            addCriterion("COD_PARENT is null");
            return (Criteria) this;
        }

        public Criteria andCodParentIsNotNull() {
            addCriterion("COD_PARENT is not null");
            return (Criteria) this;
        }

        public Criteria andCodParentEqualTo(String value) {
            addCriterion("COD_PARENT =", value, "codParent");
            return (Criteria) this;
        }

        public Criteria andCodParentNotEqualTo(String value) {
            addCriterion("COD_PARENT <>", value, "codParent");
            return (Criteria) this;
        }

        public Criteria andCodParentGreaterThan(String value) {
            addCriterion("COD_PARENT >", value, "codParent");
            return (Criteria) this;
        }

        public Criteria andCodParentGreaterThanOrEqualTo(String value) {
            addCriterion("COD_PARENT >=", value, "codParent");
            return (Criteria) this;
        }

        public Criteria andCodParentLessThan(String value) {
            addCriterion("COD_PARENT <", value, "codParent");
            return (Criteria) this;
        }

        public Criteria andCodParentLessThanOrEqualTo(String value) {
            addCriterion("COD_PARENT <=", value, "codParent");
            return (Criteria) this;
        }

        public Criteria andCodParentLike(String value) {
            addCriterion("COD_PARENT like", value, "codParent");
            return (Criteria) this;
        }

        public Criteria andCodParentNotLike(String value) {
            addCriterion("COD_PARENT not like", value, "codParent");
            return (Criteria) this;
        }

        public Criteria andCodParentIn(List<String> values) {
            addCriterion("COD_PARENT in", values, "codParent");
            return (Criteria) this;
        }

        public Criteria andCodParentNotIn(List<String> values) {
            addCriterion("COD_PARENT not in", values, "codParent");
            return (Criteria) this;
        }

        public Criteria andCodParentBetween(String value1, String value2) {
            addCriterion("COD_PARENT between", value1, value2, "codParent");
            return (Criteria) this;
        }

        public Criteria andCodParentNotBetween(String value1, String value2) {
            addCriterion("COD_PARENT not between", value1, value2, "codParent");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNull() {
            addCriterion("DESCRIPTION is null");
            return (Criteria) this;
        }

        public Criteria andDescriptionIsNotNull() {
            addCriterion("DESCRIPTION is not null");
            return (Criteria) this;
        }

        public Criteria andDescriptionEqualTo(String value) {
            addCriterion("DESCRIPTION =", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotEqualTo(String value) {
            addCriterion("DESCRIPTION <>", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThan(String value) {
            addCriterion("DESCRIPTION >", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionGreaterThanOrEqualTo(String value) {
            addCriterion("DESCRIPTION >=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThan(String value) {
            addCriterion("DESCRIPTION <", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLessThanOrEqualTo(String value) {
            addCriterion("DESCRIPTION <=", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionLike(String value) {
            addCriterion("DESCRIPTION like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotLike(String value) {
            addCriterion("DESCRIPTION not like", value, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionIn(List<String> values) {
            addCriterion("DESCRIPTION in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotIn(List<String> values) {
            addCriterion("DESCRIPTION not in", values, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionBetween(String value1, String value2) {
            addCriterion("DESCRIPTION between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andDescriptionNotBetween(String value1, String value2) {
            addCriterion("DESCRIPTION not between", value1, value2, "description");
            return (Criteria) this;
        }

        public Criteria andUrlIsNull() {
            addCriterion("URL is null");
            return (Criteria) this;
        }

        public Criteria andUrlIsNotNull() {
            addCriterion("URL is not null");
            return (Criteria) this;
        }

        public Criteria andUrlEqualTo(String value) {
            addCriterion("URL =", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlNotEqualTo(String value) {
            addCriterion("URL <>", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlGreaterThan(String value) {
            addCriterion("URL >", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlGreaterThanOrEqualTo(String value) {
            addCriterion("URL >=", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlLessThan(String value) {
            addCriterion("URL <", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlLessThanOrEqualTo(String value) {
            addCriterion("URL <=", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlLike(String value) {
            addCriterion("URL like", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlNotLike(String value) {
            addCriterion("URL not like", value, "url");
            return (Criteria) this;
        }

        public Criteria andUrlIn(List<String> values) {
            addCriterion("URL in", values, "url");
            return (Criteria) this;
        }

        public Criteria andUrlNotIn(List<String> values) {
            addCriterion("URL not in", values, "url");
            return (Criteria) this;
        }

        public Criteria andUrlBetween(String value1, String value2) {
            addCriterion("URL between", value1, value2, "url");
            return (Criteria) this;
        }

        public Criteria andUrlNotBetween(String value1, String value2) {
            addCriterion("URL not between", value1, value2, "url");
            return (Criteria) this;
        }

        public Criteria andLevelIsNull() {
            addCriterion("LEVEL is null");
            return (Criteria) this;
        }

        public Criteria andLevelIsNotNull() {
            addCriterion("LEVEL is not null");
            return (Criteria) this;
        }

        public Criteria andLevelEqualTo(String value) {
            addCriterion("LEVEL =", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotEqualTo(String value) {
            addCriterion("LEVEL <>", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelGreaterThan(String value) {
            addCriterion("LEVEL >", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelGreaterThanOrEqualTo(String value) {
            addCriterion("LEVEL >=", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelLessThan(String value) {
            addCriterion("LEVEL <", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelLessThanOrEqualTo(String value) {
            addCriterion("LEVEL <=", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelLike(String value) {
            addCriterion("LEVEL like", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotLike(String value) {
            addCriterion("LEVEL not like", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelIn(List<String> values) {
            addCriterion("LEVEL in", values, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotIn(List<String> values) {
            addCriterion("LEVEL not in", values, "level");
            return (Criteria) this;
        }

        public Criteria andLevelBetween(String value1, String value2) {
            addCriterion("LEVEL between", value1, value2, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotBetween(String value1, String value2) {
            addCriterion("LEVEL not between", value1, value2, "level");
            return (Criteria) this;
        }

        public Criteria andOrderIsNull() {
            addCriterion("ORDER is null");
            return (Criteria) this;
        }

        public Criteria andOrderIsNotNull() {
            addCriterion("ORDER is not null");
            return (Criteria) this;
        }

        public Criteria andOrderEqualTo(String value) {
            addCriterion("ORDER =", value, "order");
            return (Criteria) this;
        }

        public Criteria andOrderNotEqualTo(String value) {
            addCriterion("ORDER <>", value, "order");
            return (Criteria) this;
        }

        public Criteria andOrderGreaterThan(String value) {
            addCriterion("ORDER >", value, "order");
            return (Criteria) this;
        }

        public Criteria andOrderGreaterThanOrEqualTo(String value) {
            addCriterion("ORDER >=", value, "order");
            return (Criteria) this;
        }

        public Criteria andOrderLessThan(String value) {
            addCriterion("ORDER <", value, "order");
            return (Criteria) this;
        }

        public Criteria andOrderLessThanOrEqualTo(String value) {
            addCriterion("ORDER <=", value, "order");
            return (Criteria) this;
        }

        public Criteria andOrderLike(String value) {
            addCriterion("ORDER like", value, "order");
            return (Criteria) this;
        }

        public Criteria andOrderNotLike(String value) {
            addCriterion("ORDER not like", value, "order");
            return (Criteria) this;
        }

        public Criteria andOrderIn(List<String> values) {
            addCriterion("ORDER in", values, "order");
            return (Criteria) this;
        }

        public Criteria andOrderNotIn(List<String> values) {
            addCriterion("ORDER not in", values, "order");
            return (Criteria) this;
        }

        public Criteria andOrderBetween(String value1, String value2) {
            addCriterion("ORDER between", value1, value2, "order");
            return (Criteria) this;
        }

        public Criteria andOrderNotBetween(String value1, String value2) {
            addCriterion("ORDER not between", value1, value2, "order");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("STATE is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("STATE is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(String value) {
            addCriterion("STATE =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(String value) {
            addCriterion("STATE <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(String value) {
            addCriterion("STATE >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(String value) {
            addCriterion("STATE >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(String value) {
            addCriterion("STATE <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(String value) {
            addCriterion("STATE <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLike(String value) {
            addCriterion("STATE like", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotLike(String value) {
            addCriterion("STATE not like", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<String> values) {
            addCriterion("STATE in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<String> values) {
            addCriterion("STATE not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(String value1, String value2) {
            addCriterion("STATE between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(String value1, String value2) {
            addCriterion("STATE not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andFlagSessionIsNull() {
            addCriterion("FLAG_SESSION is null");
            return (Criteria) this;
        }

        public Criteria andFlagSessionIsNotNull() {
            addCriterion("FLAG_SESSION is not null");
            return (Criteria) this;
        }

        public Criteria andFlagSessionEqualTo(String value) {
            addCriterion("FLAG_SESSION =", value, "flagSession");
            return (Criteria) this;
        }

        public Criteria andFlagSessionNotEqualTo(String value) {
            addCriterion("FLAG_SESSION <>", value, "flagSession");
            return (Criteria) this;
        }

        public Criteria andFlagSessionGreaterThan(String value) {
            addCriterion("FLAG_SESSION >", value, "flagSession");
            return (Criteria) this;
        }

        public Criteria andFlagSessionGreaterThanOrEqualTo(String value) {
            addCriterion("FLAG_SESSION >=", value, "flagSession");
            return (Criteria) this;
        }

        public Criteria andFlagSessionLessThan(String value) {
            addCriterion("FLAG_SESSION <", value, "flagSession");
            return (Criteria) this;
        }

        public Criteria andFlagSessionLessThanOrEqualTo(String value) {
            addCriterion("FLAG_SESSION <=", value, "flagSession");
            return (Criteria) this;
        }

        public Criteria andFlagSessionLike(String value) {
            addCriterion("FLAG_SESSION like", value, "flagSession");
            return (Criteria) this;
        }

        public Criteria andFlagSessionNotLike(String value) {
            addCriterion("FLAG_SESSION not like", value, "flagSession");
            return (Criteria) this;
        }

        public Criteria andFlagSessionIn(List<String> values) {
            addCriterion("FLAG_SESSION in", values, "flagSession");
            return (Criteria) this;
        }

        public Criteria andFlagSessionNotIn(List<String> values) {
            addCriterion("FLAG_SESSION not in", values, "flagSession");
            return (Criteria) this;
        }

        public Criteria andFlagSessionBetween(String value1, String value2) {
            addCriterion("FLAG_SESSION between", value1, value2, "flagSession");
            return (Criteria) this;
        }

        public Criteria andFlagSessionNotBetween(String value1, String value2) {
            addCriterion("FLAG_SESSION not between", value1, value2, "flagSession");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table TB_OPTION_MENU
     *
     * @mbggenerated do_not_delete_during_merge Wed May 23 12:07:48 COT 2018
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table TB_OPTION_MENU
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}