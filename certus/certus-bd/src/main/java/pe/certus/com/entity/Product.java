package pe.certus.com.entity;

import java.math.BigDecimal;
import java.util.Date;

public class Product {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TB_PRODUCT.ID_PRODUCT
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    private BigDecimal idProduct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TB_PRODUCT.COD_PROG
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    private String codProg;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TB_PRODUCT.DESC_PROG
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    private String descProg;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TB_PRODUCT.COD_PROD
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    private String codProd;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TB_PRODUCT.DESC_PROD
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    private String descProd;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TB_PRODUCT.ID_USER
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    private BigDecimal idUser;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TB_PRODUCT.STATE
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    private String state;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TB_PRODUCT.COD_USER_CREA
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    private String codUserCrea;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TB_PRODUCT.DATE_USER_CREA
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    private Date dateUserCrea;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TB_PRODUCT.DE_TERM_CREA
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    private String deTermCrea;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TB_PRODUCT.COD_USER_MODI
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    private String codUserModi;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TB_PRODUCT.DATE_USER_MODI
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    private Date dateUserModi;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TB_PRODUCT.DE_TERM_MODI
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    private String deTermModi;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TB_PRODUCT.ID_PRODUCT
     *
     * @return the value of TB_PRODUCT.ID_PRODUCT
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public BigDecimal getIdProduct() {
        return idProduct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TB_PRODUCT.ID_PRODUCT
     *
     * @param idProduct the value for TB_PRODUCT.ID_PRODUCT
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setIdProduct(BigDecimal idProduct) {
        this.idProduct = idProduct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TB_PRODUCT.COD_PROG
     *
     * @return the value of TB_PRODUCT.COD_PROG
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public String getCodProg() {
        return codProg;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TB_PRODUCT.COD_PROG
     *
     * @param codProg the value for TB_PRODUCT.COD_PROG
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setCodProg(String codProg) {
        this.codProg = codProg == null ? null : codProg.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TB_PRODUCT.DESC_PROG
     *
     * @return the value of TB_PRODUCT.DESC_PROG
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public String getDescProg() {
        return descProg;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TB_PRODUCT.DESC_PROG
     *
     * @param descProg the value for TB_PRODUCT.DESC_PROG
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setDescProg(String descProg) {
        this.descProg = descProg == null ? null : descProg.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TB_PRODUCT.COD_PROD
     *
     * @return the value of TB_PRODUCT.COD_PROD
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public String getCodProd() {
        return codProd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TB_PRODUCT.COD_PROD
     *
     * @param codProd the value for TB_PRODUCT.COD_PROD
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setCodProd(String codProd) {
        this.codProd = codProd == null ? null : codProd.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TB_PRODUCT.DESC_PROD
     *
     * @return the value of TB_PRODUCT.DESC_PROD
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public String getDescProd() {
        return descProd;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TB_PRODUCT.DESC_PROD
     *
     * @param descProd the value for TB_PRODUCT.DESC_PROD
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setDescProd(String descProd) {
        this.descProd = descProd == null ? null : descProd.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TB_PRODUCT.ID_USER
     *
     * @return the value of TB_PRODUCT.ID_USER
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public BigDecimal getIdUser() {
        return idUser;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TB_PRODUCT.ID_USER
     *
     * @param idUser the value for TB_PRODUCT.ID_USER
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setIdUser(BigDecimal idUser) {
        this.idUser = idUser;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TB_PRODUCT.STATE
     *
     * @return the value of TB_PRODUCT.STATE
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public String getState() {
        return state;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TB_PRODUCT.STATE
     *
     * @param state the value for TB_PRODUCT.STATE
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TB_PRODUCT.COD_USER_CREA
     *
     * @return the value of TB_PRODUCT.COD_USER_CREA
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public String getCodUserCrea() {
        return codUserCrea;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TB_PRODUCT.COD_USER_CREA
     *
     * @param codUserCrea the value for TB_PRODUCT.COD_USER_CREA
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setCodUserCrea(String codUserCrea) {
        this.codUserCrea = codUserCrea == null ? null : codUserCrea.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TB_PRODUCT.DATE_USER_CREA
     *
     * @return the value of TB_PRODUCT.DATE_USER_CREA
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public Date getDateUserCrea() {
        return dateUserCrea;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TB_PRODUCT.DATE_USER_CREA
     *
     * @param dateUserCrea the value for TB_PRODUCT.DATE_USER_CREA
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setDateUserCrea(Date dateUserCrea) {
        this.dateUserCrea = dateUserCrea;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TB_PRODUCT.DE_TERM_CREA
     *
     * @return the value of TB_PRODUCT.DE_TERM_CREA
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public String getDeTermCrea() {
        return deTermCrea;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TB_PRODUCT.DE_TERM_CREA
     *
     * @param deTermCrea the value for TB_PRODUCT.DE_TERM_CREA
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setDeTermCrea(String deTermCrea) {
        this.deTermCrea = deTermCrea == null ? null : deTermCrea.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TB_PRODUCT.COD_USER_MODI
     *
     * @return the value of TB_PRODUCT.COD_USER_MODI
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public String getCodUserModi() {
        return codUserModi;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TB_PRODUCT.COD_USER_MODI
     *
     * @param codUserModi the value for TB_PRODUCT.COD_USER_MODI
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setCodUserModi(String codUserModi) {
        this.codUserModi = codUserModi == null ? null : codUserModi.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TB_PRODUCT.DATE_USER_MODI
     *
     * @return the value of TB_PRODUCT.DATE_USER_MODI
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public Date getDateUserModi() {
        return dateUserModi;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TB_PRODUCT.DATE_USER_MODI
     *
     * @param dateUserModi the value for TB_PRODUCT.DATE_USER_MODI
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setDateUserModi(Date dateUserModi) {
        this.dateUserModi = dateUserModi;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TB_PRODUCT.DE_TERM_MODI
     *
     * @return the value of TB_PRODUCT.DE_TERM_MODI
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public String getDeTermModi() {
        return deTermModi;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TB_PRODUCT.DE_TERM_MODI
     *
     * @param deTermModi the value for TB_PRODUCT.DE_TERM_MODI
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setDeTermModi(String deTermModi) {
        this.deTermModi = deTermModi == null ? null : deTermModi.trim();
    }
}