package pe.certus.com.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import pe.certus.com.entity.Classroom;
import pe.certus.com.entity.ClassroomExample;

import java.math.BigDecimal;
import java.util.List;

public interface ClassroomMapper {

    String GET_ALL_SITES = "SELECT\n" +
            "TC.ID_CLASSROOM,\n" +
            "TC.CAMPUS,\n" +
            "TC.BUILDING,\n" +
            "TC.CLASSROOM,\n" +
            "TC.IP_ADDRESS,\n" +
            "TC.MAC_ADDRESS,\n" +
            "TC.ID_USER,\n" +
            "\"TC\".STATE,\n" +
            "TC.COD_USER_CREA,\n" +
            "TC.DATE_USER_CREA,\n" +
            "TC.DE_TERM_CREA,\n" +
            "TC.COD_USER_MODI,\n" +
            "TC.DATE_USER_MODI,\n" +
            "TC.DE_TERM_MODI,\n" +
            "TC.DE_TERM_MODI\n" +
            "FROM \n" +
            "TB_CLASSROOM TC\n" +
            "WHERE \"TC\".\"STATE\" = #{state}";

    @Select(GET_ALL_SITES)
    @ResultMap("BaseResultMap")
    List<Classroom> getAllSitesByStatus(@Param("state") String state);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_CLASSROOM
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    int countByExample(ClassroomExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_CLASSROOM
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    int deleteByPrimaryKey(BigDecimal primaryKey);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_CLASSROOM
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */

    int deleteByExample(ClassroomExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_CLASSROOM
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    int insert(Classroom record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_CLASSROOM
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    int insertSelective(Classroom record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_CLASSROOM
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    List<Classroom> selectByExample(ClassroomExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_CLASSROOM
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    int updateByExampleSelective(@Param("record") Classroom record, @Param("example") ClassroomExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_CLASSROOM
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    int updateByExample(@Param("record") Classroom record, @Param("example") ClassroomExample example);
}