package pe.certus.com.entity;

import java.math.BigDecimal;

public class Configuration extends ConfigurationKey {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TB_CONFIGURATION.CVALUEDESC
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    private String cvaluedesc;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TB_CONFIGURATION.CSTATUS
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    private String cstatus;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TB_CONFIGURATION.CTAG1
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    private String ctag1;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TB_CONFIGURATION.CTAG2
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    private String ctag2;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TB_CONFIGURATION.CTAG3
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    private String ctag3;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TB_CONFIGURATION.CTAG4
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    private BigDecimal ctag4;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column TB_CONFIGURATION.INDEX
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    private BigDecimal index;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TB_CONFIGURATION.CVALUEDESC
     *
     * @return the value of TB_CONFIGURATION.CVALUEDESC
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public String getCvaluedesc() {
        return cvaluedesc;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TB_CONFIGURATION.CVALUEDESC
     *
     * @param cvaluedesc the value for TB_CONFIGURATION.CVALUEDESC
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setCvaluedesc(String cvaluedesc) {
        this.cvaluedesc = cvaluedesc == null ? null : cvaluedesc.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TB_CONFIGURATION.CSTATUS
     *
     * @return the value of TB_CONFIGURATION.CSTATUS
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public String getCstatus() {
        return cstatus;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TB_CONFIGURATION.CSTATUS
     *
     * @param cstatus the value for TB_CONFIGURATION.CSTATUS
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setCstatus(String cstatus) {
        this.cstatus = cstatus == null ? null : cstatus.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TB_CONFIGURATION.CTAG1
     *
     * @return the value of TB_CONFIGURATION.CTAG1
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public String getCtag1() {
        return ctag1;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TB_CONFIGURATION.CTAG1
     *
     * @param ctag1 the value for TB_CONFIGURATION.CTAG1
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setCtag1(String ctag1) {
        this.ctag1 = ctag1 == null ? null : ctag1.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TB_CONFIGURATION.CTAG2
     *
     * @return the value of TB_CONFIGURATION.CTAG2
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public String getCtag2() {
        return ctag2;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TB_CONFIGURATION.CTAG2
     *
     * @param ctag2 the value for TB_CONFIGURATION.CTAG2
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setCtag2(String ctag2) {
        this.ctag2 = ctag2 == null ? null : ctag2.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TB_CONFIGURATION.CTAG3
     *
     * @return the value of TB_CONFIGURATION.CTAG3
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public String getCtag3() {
        return ctag3;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TB_CONFIGURATION.CTAG3
     *
     * @param ctag3 the value for TB_CONFIGURATION.CTAG3
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setCtag3(String ctag3) {
        this.ctag3 = ctag3 == null ? null : ctag3.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TB_CONFIGURATION.CTAG4
     *
     * @return the value of TB_CONFIGURATION.CTAG4
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public BigDecimal getCtag4() {
        return ctag4;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TB_CONFIGURATION.CTAG4
     *
     * @param ctag4 the value for TB_CONFIGURATION.CTAG4
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setCtag4(BigDecimal ctag4) {
        this.ctag4 = ctag4;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column TB_CONFIGURATION.INDEX
     *
     * @return the value of TB_CONFIGURATION.INDEX
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public BigDecimal getIndex() {
        return index;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column TB_CONFIGURATION.INDEX
     *
     * @param index the value for TB_CONFIGURATION.INDEX
     *
     * @mbggenerated Wed May 23 12:07:48 COT 2018
     */
    public void setIndex(BigDecimal index) {
        this.index = index;
    }
}