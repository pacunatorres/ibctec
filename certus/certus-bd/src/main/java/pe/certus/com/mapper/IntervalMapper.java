package pe.certus.com.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import pe.certus.com.entity.Interval;
import pe.certus.com.entity.IntervalExample;

import java.math.BigDecimal;
import java.util.List;

public interface IntervalMapper {

    String GET_ALL_INTERVAL = "SELECT \n" +
            "TI.ID_INTERVAL,\n" +
            "\"TI\".\"YEAR\",\n" +
            "\"TI\".\"MONTH\",\n" +
            "\"TI\".\"START\",\n" +
            "\"TI\".\"END\",\n" +
            "TI.ID_USER,\n" +
            "TI.DESC_STATE,\n" +
            "TI.STATE,\n" +
            "TI.COD_USER_CREA,\n" +
            "TI.DATE_USER_CREA,\n" +
            "TI.DE_TERM_CREA,\n" +
            "TI.COD_USER_MODI,\n" +
            "TI.DATE_USER_MODI,\n" +
            "TI.DE_TERM_MODI,\n" +
            "TI.DE_TERM_MODI\n" +
            "FROM TB_INTERVAL TI\n" +
            "WHERE STATE = #{state}";

    @Select(GET_ALL_INTERVAL)
    @ResultMap("BaseResultMap")
    List<Interval> getAllIntervalByStatus(@Param("state") String state);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    int countByExample(IntervalExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    int deleteByPrimaryKey(BigDecimal primaryKey);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    int deleteByExample(IntervalExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    int insert(Interval record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    int insertSelective(Interval record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    List<Interval> selectByExample(IntervalExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    int updateByExampleSelective(@Param("record") Interval record, @Param("example") IntervalExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    int updateByExample(@Param("record") Interval record, @Param("example") IntervalExample example);
}