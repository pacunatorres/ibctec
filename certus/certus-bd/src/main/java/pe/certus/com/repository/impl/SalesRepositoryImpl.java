package pe.certus.com.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pe.certus.com.bean.RegistryBean;
import pe.certus.com.constants.Constants;
import pe.certus.com.entity.*;
import pe.certus.com.exception.SalesDBException;
import pe.certus.com.mapper.*;
import pe.certus.com.repository.SalesRepository;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jose.hurtado on 8/17/2017.
 */
@Component
public class SalesRepositoryImpl implements SalesRepository {

    @Autowired
    private OptionMenuMapper optionMenuMapper;

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Autowired
    private ConfigurationMapper configurationMapper;

    @Autowired
    private AcademicConfigurationMapper academicConfigurationMapper;

    @Autowired
    private ClassroomMapper classroomMapper;

    @Autowired
    private IntervalMapper intervalMapper;

    @Autowired
    private ExploitationUnitMapper exploitationUnitMapper;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private DepartmentMapper departmentMapper;

    @Autowired
    private RegistryMapper registryMapper;

    @Override
    public List<OptionMenu> getOptionMenu(List<String> profiles, List<String> typeMenus) throws SalesDBException {
        List<OptionMenu> listOptionMenu;
        try {

            Map<String, Object> params = new HashMap<String, Object>();
            params.put("state", Constants.FLAG_ACTIVE);
            params.put("typeMenus", typeMenus);
            params.put("profiles", profiles);
            listOptionMenu = optionMenuMapper.getOptionMenu(params);

        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
        return listOptionMenu;
    }

    @Override
    public List<Role> getProfileByUserName(String userName) throws SalesDBException {
        try {
            return roleMapper.getProfileByUserName(Constants.FLAG_ACTIVE, userName);
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public User getUserByUserName(String userName) throws SalesDBException {
        try {
            return userMapper.getUserByUserName(userName, Constants.FLAG_ACTIVE);
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public Long getUserByUserNameAndPassword(String userName, String password) throws SalesDBException {
        try {
            return userMapper.getUserByUserNameAndPassword(userName, password);
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public List<Configuration> getConfigurationByParams(Map<String, Object> params) throws SalesDBException {
        try {
            return configurationMapper.getConfigurationByParams(params);
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public Integer registerUser(User usuario) throws SalesDBException {
        Integer rowAffected;
        try {
            rowAffected = userMapper.insert(usuario);
            if (rowAffected > 0) {
                rowAffected = usuario.getIdUser().intValue();
            }
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
        return rowAffected;
    }

    @Override
    public Integer registerUserProfile(UserRole userRole) throws SalesDBException {
        try {
            return userRoleMapper.insert(userRole);
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public Integer getIdRoleByRole(String role) throws SalesDBException {
        try {
            return roleMapper.getIdRoleByRole(role);
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public User getUserByEmail(String email) throws SalesDBException {
        try {
            return userMapper.getUserByEmail(email);
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public List<Role> getAllRoles() {
        try {
            return roleMapper.getAllRoles();
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }


    @Override
    public Integer updateUserSelective(User user) throws SalesDBException {
        try {
            return userMapper.updateByPrimaryKeySelective(user);
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public User getUserByCode(Integer code) throws SalesDBException {
        try {
            return userMapper.selectByPrimaryKey(new BigDecimal(code));
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public Integer registerAcademicConfigurations(AcademicConfiguration academicConfiguration) {
        try {
            return academicConfigurationMapper.insertSelective(academicConfiguration);
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public List<AcademicConfiguration> getAllAcademicConfigurationByStatus(String status) {
        try {
            return academicConfigurationMapper.getAllAcademicConfigurationByStatus(status);
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public Integer deleteAcademicConfiguration(Integer code) {
        try {
            return academicConfigurationMapper.deleteByPrimaryKey(new BigDecimal(code));
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public Integer registerClassroom(Classroom classroom) {
        try {
            return classroomMapper.insertSelective(classroom);
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public List<Classroom> getAllSitesByStatus(String status) {
        try {
            return classroomMapper.getAllSitesByStatus(status);
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public Integer deleteSite(Integer code) {
        try {
            return classroomMapper.deleteByPrimaryKey(new BigDecimal(code));
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public Integer registerInterval(Interval interval) {
        try {
            return intervalMapper.insertSelective(interval);
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public List<Interval> getAllIntervalByStatus(String status) {
        try {
            return intervalMapper.getAllIntervalByStatus(status);
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public Integer deleteInterval(Integer code) {
        try {
            return intervalMapper.deleteByPrimaryKey(new BigDecimal(code));
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public Integer registerExploitationUnit(ExploitationUnit exploitationUnit) {
        try {
            return exploitationUnitMapper.insertSelective(exploitationUnit);
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public List<ExploitationUnit> getAllExploitationUnitByStatus(String status) {
        try {
            return exploitationUnitMapper.getAllExploitationUnits(status);
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public Integer deleteExploitationUnit(Integer code) {
        try {
            return exploitationUnitMapper.deleteByPrimaryKey(new BigDecimal(code));
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public Integer registerProduct(Product product) {
        try {
            return productMapper.insertSelective(product);
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public List<Product> getAllProductsByStatus(String status) {
        try {
            return productMapper.getAllProducts(status);
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public Integer deleteProduct(Integer code) {
        try {
            return productMapper.deleteByPrimaryKey(new BigDecimal(code));
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public Integer registerDepartment(Department department) {
        try {
            return departmentMapper.insertSelective(department);
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public List<Department> getAllDepartmentsByStatus(String status) {
        try {
            return departmentMapper.getAllDepartments(status);
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public Integer deleteDepartment(Integer code) {
        try {
            return departmentMapper.deleteByPrimaryKey(new BigDecimal(code));
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

    @Override
    public List<RegistryBean> getAllRegistry() {
        try {
            return registryMapper.getAllRegistry();
        } catch (Exception ex) {
            throw new SalesDBException(ex.getMessage(), Constants.ERROR_CODES.DATABASE_ERROR);
        }
    }

}
