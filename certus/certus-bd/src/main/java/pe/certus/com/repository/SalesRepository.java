package pe.certus.com.repository;

import pe.certus.com.bean.RegistryBean;
import pe.certus.com.entity.*;
import pe.certus.com.exception.SalesDBException;

import java.util.List;
import java.util.Map;

/**
 * Created by jose.hurtado on 8/17/2017.
 */
public interface SalesRepository {

    List<OptionMenu> getOptionMenu(List<String> profiles, List<String> typeMenus) throws SalesDBException;

    List<Role> getProfileByUserName(String userName) throws SalesDBException;

    User getUserByUserName(String userName) throws SalesDBException;

    Long getUserByUserNameAndPassword(String userName, String password) throws SalesDBException;

    List<Configuration> getConfigurationByParams(Map<String, Object> params) throws SalesDBException;

    Integer registerUser(User usuario) throws SalesDBException;

    Integer registerUserProfile(UserRole usuarioPerfil) throws SalesDBException;

    Integer getIdRoleByRole(String role) throws SalesDBException;

    User getUserByEmail(String email) throws SalesDBException;

    List<Role> getAllRoles();

    Integer updateUserSelective(User usuario) throws SalesDBException;

    User getUserByCode(Integer code) throws SalesDBException;

    Integer registerAcademicConfigurations(AcademicConfiguration academicConfiguration);

    List<AcademicConfiguration> getAllAcademicConfigurationByStatus(String status);

    Integer deleteAcademicConfiguration(Integer code);

    Integer registerClassroom(Classroom classroom);

    List<Classroom> getAllSitesByStatus(String status);

    Integer deleteSite(Integer code);

    Integer registerInterval(Interval interval);

    List<Interval> getAllIntervalByStatus(String status);

    Integer deleteInterval(Integer code);

    Integer registerExploitationUnit(ExploitationUnit exploitationUnit);

    List<ExploitationUnit> getAllExploitationUnitByStatus(String status);

    Integer deleteExploitationUnit(Integer code);

    Integer registerProduct(Product product);

    List<Product> getAllProductsByStatus(String status);

    Integer deleteProduct(Integer code);

    Integer registerDepartment(Department department);

    List<Department> getAllDepartmentsByStatus(String status);

    Integer deleteDepartment(Integer code);

    List<RegistryBean> getAllRegistry();

}
