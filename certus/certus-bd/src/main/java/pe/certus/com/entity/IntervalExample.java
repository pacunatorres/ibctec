package pe.certus.com.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class IntervalExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    protected List<Criteria> oredCriteria;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    public IntervalExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIntervalIsNull() {
            addCriterion("ID_INTERVAL is null");
            return (Criteria) this;
        }

        public Criteria andIdIntervalIsNotNull() {
            addCriterion("ID_INTERVAL is not null");
            return (Criteria) this;
        }

        public Criteria andIdIntervalEqualTo(BigDecimal value) {
            addCriterion("ID_INTERVAL =", value, "idInterval");
            return (Criteria) this;
        }

        public Criteria andIdIntervalNotEqualTo(BigDecimal value) {
            addCriterion("ID_INTERVAL <>", value, "idInterval");
            return (Criteria) this;
        }

        public Criteria andIdIntervalGreaterThan(BigDecimal value) {
            addCriterion("ID_INTERVAL >", value, "idInterval");
            return (Criteria) this;
        }

        public Criteria andIdIntervalGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("ID_INTERVAL >=", value, "idInterval");
            return (Criteria) this;
        }

        public Criteria andIdIntervalLessThan(BigDecimal value) {
            addCriterion("ID_INTERVAL <", value, "idInterval");
            return (Criteria) this;
        }

        public Criteria andIdIntervalLessThanOrEqualTo(BigDecimal value) {
            addCriterion("ID_INTERVAL <=", value, "idInterval");
            return (Criteria) this;
        }

        public Criteria andIdIntervalIn(List<BigDecimal> values) {
            addCriterion("ID_INTERVAL in", values, "idInterval");
            return (Criteria) this;
        }

        public Criteria andIdIntervalNotIn(List<BigDecimal> values) {
            addCriterion("ID_INTERVAL not in", values, "idInterval");
            return (Criteria) this;
        }

        public Criteria andIdIntervalBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ID_INTERVAL between", value1, value2, "idInterval");
            return (Criteria) this;
        }

        public Criteria andIdIntervalNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ID_INTERVAL not between", value1, value2, "idInterval");
            return (Criteria) this;
        }

        public Criteria andYearIsNull() {
            addCriterion("YEAR is null");
            return (Criteria) this;
        }

        public Criteria andYearIsNotNull() {
            addCriterion("YEAR is not null");
            return (Criteria) this;
        }

        public Criteria andYearEqualTo(BigDecimal value) {
            addCriterion("YEAR =", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearNotEqualTo(BigDecimal value) {
            addCriterion("YEAR <>", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearGreaterThan(BigDecimal value) {
            addCriterion("YEAR >", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("YEAR >=", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearLessThan(BigDecimal value) {
            addCriterion("YEAR <", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearLessThanOrEqualTo(BigDecimal value) {
            addCriterion("YEAR <=", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearIn(List<BigDecimal> values) {
            addCriterion("YEAR in", values, "year");
            return (Criteria) this;
        }

        public Criteria andYearNotIn(List<BigDecimal> values) {
            addCriterion("YEAR not in", values, "year");
            return (Criteria) this;
        }

        public Criteria andYearBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("YEAR between", value1, value2, "year");
            return (Criteria) this;
        }

        public Criteria andYearNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("YEAR not between", value1, value2, "year");
            return (Criteria) this;
        }

        public Criteria andMonthIsNull() {
            addCriterion("MONTH is null");
            return (Criteria) this;
        }

        public Criteria andMonthIsNotNull() {
            addCriterion("MONTH is not null");
            return (Criteria) this;
        }

        public Criteria andMonthEqualTo(String value) {
            addCriterion("MONTH =", value, "month");
            return (Criteria) this;
        }

        public Criteria andMonthNotEqualTo(String value) {
            addCriterion("MONTH <>", value, "month");
            return (Criteria) this;
        }

        public Criteria andMonthGreaterThan(String value) {
            addCriterion("MONTH >", value, "month");
            return (Criteria) this;
        }

        public Criteria andMonthGreaterThanOrEqualTo(String value) {
            addCriterion("MONTH >=", value, "month");
            return (Criteria) this;
        }

        public Criteria andMonthLessThan(String value) {
            addCriterion("MONTH <", value, "month");
            return (Criteria) this;
        }

        public Criteria andMonthLessThanOrEqualTo(String value) {
            addCriterion("MONTH <=", value, "month");
            return (Criteria) this;
        }

        public Criteria andMonthLike(String value) {
            addCriterion("MONTH like", value, "month");
            return (Criteria) this;
        }

        public Criteria andMonthNotLike(String value) {
            addCriterion("MONTH not like", value, "month");
            return (Criteria) this;
        }

        public Criteria andMonthIn(List<String> values) {
            addCriterion("MONTH in", values, "month");
            return (Criteria) this;
        }

        public Criteria andMonthNotIn(List<String> values) {
            addCriterion("MONTH not in", values, "month");
            return (Criteria) this;
        }

        public Criteria andMonthBetween(String value1, String value2) {
            addCriterion("MONTH between", value1, value2, "month");
            return (Criteria) this;
        }

        public Criteria andMonthNotBetween(String value1, String value2) {
            addCriterion("MONTH not between", value1, value2, "month");
            return (Criteria) this;
        }

        public Criteria andStartIsNull() {
            addCriterion("START is null");
            return (Criteria) this;
        }

        public Criteria andStartIsNotNull() {
            addCriterion("START is not null");
            return (Criteria) this;
        }

        public Criteria andStartEqualTo(Date value) {
            addCriterion("START =", value, "start");
            return (Criteria) this;
        }

        public Criteria andStartNotEqualTo(Date value) {
            addCriterion("START <>", value, "start");
            return (Criteria) this;
        }

        public Criteria andStartGreaterThan(Date value) {
            addCriterion("START >", value, "start");
            return (Criteria) this;
        }

        public Criteria andStartGreaterThanOrEqualTo(Date value) {
            addCriterion("START >=", value, "start");
            return (Criteria) this;
        }

        public Criteria andStartLessThan(Date value) {
            addCriterion("START <", value, "start");
            return (Criteria) this;
        }

        public Criteria andStartLessThanOrEqualTo(Date value) {
            addCriterion("START <=", value, "start");
            return (Criteria) this;
        }

        public Criteria andStartIn(List<Date> values) {
            addCriterion("START in", values, "start");
            return (Criteria) this;
        }

        public Criteria andStartNotIn(List<Date> values) {
            addCriterion("START not in", values, "start");
            return (Criteria) this;
        }

        public Criteria andStartBetween(Date value1, Date value2) {
            addCriterion("START between", value1, value2, "start");
            return (Criteria) this;
        }

        public Criteria andStartNotBetween(Date value1, Date value2) {
            addCriterion("START not between", value1, value2, "start");
            return (Criteria) this;
        }

        public Criteria andEndIsNull() {
            addCriterion("END is null");
            return (Criteria) this;
        }

        public Criteria andEndIsNotNull() {
            addCriterion("END is not null");
            return (Criteria) this;
        }

        public Criteria andEndEqualTo(Date value) {
            addCriterion("END =", value, "end");
            return (Criteria) this;
        }

        public Criteria andEndNotEqualTo(Date value) {
            addCriterion("END <>", value, "end");
            return (Criteria) this;
        }

        public Criteria andEndGreaterThan(Date value) {
            addCriterion("END >", value, "end");
            return (Criteria) this;
        }

        public Criteria andEndGreaterThanOrEqualTo(Date value) {
            addCriterion("END >=", value, "end");
            return (Criteria) this;
        }

        public Criteria andEndLessThan(Date value) {
            addCriterion("END <", value, "end");
            return (Criteria) this;
        }

        public Criteria andEndLessThanOrEqualTo(Date value) {
            addCriterion("END <=", value, "end");
            return (Criteria) this;
        }

        public Criteria andEndIn(List<Date> values) {
            addCriterion("END in", values, "end");
            return (Criteria) this;
        }

        public Criteria andEndNotIn(List<Date> values) {
            addCriterion("END not in", values, "end");
            return (Criteria) this;
        }

        public Criteria andEndBetween(Date value1, Date value2) {
            addCriterion("END between", value1, value2, "end");
            return (Criteria) this;
        }

        public Criteria andEndNotBetween(Date value1, Date value2) {
            addCriterion("END not between", value1, value2, "end");
            return (Criteria) this;
        }

        public Criteria andIdUserIsNull() {
            addCriterion("ID_USER is null");
            return (Criteria) this;
        }

        public Criteria andIdUserIsNotNull() {
            addCriterion("ID_USER is not null");
            return (Criteria) this;
        }

        public Criteria andIdUserEqualTo(BigDecimal value) {
            addCriterion("ID_USER =", value, "idUser");
            return (Criteria) this;
        }

        public Criteria andIdUserNotEqualTo(BigDecimal value) {
            addCriterion("ID_USER <>", value, "idUser");
            return (Criteria) this;
        }

        public Criteria andIdUserGreaterThan(BigDecimal value) {
            addCriterion("ID_USER >", value, "idUser");
            return (Criteria) this;
        }

        public Criteria andIdUserGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("ID_USER >=", value, "idUser");
            return (Criteria) this;
        }

        public Criteria andIdUserLessThan(BigDecimal value) {
            addCriterion("ID_USER <", value, "idUser");
            return (Criteria) this;
        }

        public Criteria andIdUserLessThanOrEqualTo(BigDecimal value) {
            addCriterion("ID_USER <=", value, "idUser");
            return (Criteria) this;
        }

        public Criteria andIdUserIn(List<BigDecimal> values) {
            addCriterion("ID_USER in", values, "idUser");
            return (Criteria) this;
        }

        public Criteria andIdUserNotIn(List<BigDecimal> values) {
            addCriterion("ID_USER not in", values, "idUser");
            return (Criteria) this;
        }

        public Criteria andIdUserBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ID_USER between", value1, value2, "idUser");
            return (Criteria) this;
        }

        public Criteria andIdUserNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ID_USER not between", value1, value2, "idUser");
            return (Criteria) this;
        }

        public Criteria andDescStateIsNull() {
            addCriterion("DESC_STATE is null");
            return (Criteria) this;
        }

        public Criteria andDescStateIsNotNull() {
            addCriterion("DESC_STATE is not null");
            return (Criteria) this;
        }

        public Criteria andDescStateEqualTo(String value) {
            addCriterion("DESC_STATE =", value, "descState");
            return (Criteria) this;
        }

        public Criteria andDescStateNotEqualTo(String value) {
            addCriterion("DESC_STATE <>", value, "descState");
            return (Criteria) this;
        }

        public Criteria andDescStateGreaterThan(String value) {
            addCriterion("DESC_STATE >", value, "descState");
            return (Criteria) this;
        }

        public Criteria andDescStateGreaterThanOrEqualTo(String value) {
            addCriterion("DESC_STATE >=", value, "descState");
            return (Criteria) this;
        }

        public Criteria andDescStateLessThan(String value) {
            addCriterion("DESC_STATE <", value, "descState");
            return (Criteria) this;
        }

        public Criteria andDescStateLessThanOrEqualTo(String value) {
            addCriterion("DESC_STATE <=", value, "descState");
            return (Criteria) this;
        }

        public Criteria andDescStateLike(String value) {
            addCriterion("DESC_STATE like", value, "descState");
            return (Criteria) this;
        }

        public Criteria andDescStateNotLike(String value) {
            addCriterion("DESC_STATE not like", value, "descState");
            return (Criteria) this;
        }

        public Criteria andDescStateIn(List<String> values) {
            addCriterion("DESC_STATE in", values, "descState");
            return (Criteria) this;
        }

        public Criteria andDescStateNotIn(List<String> values) {
            addCriterion("DESC_STATE not in", values, "descState");
            return (Criteria) this;
        }

        public Criteria andDescStateBetween(String value1, String value2) {
            addCriterion("DESC_STATE between", value1, value2, "descState");
            return (Criteria) this;
        }

        public Criteria andDescStateNotBetween(String value1, String value2) {
            addCriterion("DESC_STATE not between", value1, value2, "descState");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("STATE is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("STATE is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(String value) {
            addCriterion("STATE =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(String value) {
            addCriterion("STATE <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(String value) {
            addCriterion("STATE >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(String value) {
            addCriterion("STATE >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(String value) {
            addCriterion("STATE <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(String value) {
            addCriterion("STATE <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLike(String value) {
            addCriterion("STATE like", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotLike(String value) {
            addCriterion("STATE not like", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<String> values) {
            addCriterion("STATE in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<String> values) {
            addCriterion("STATE not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(String value1, String value2) {
            addCriterion("STATE between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(String value1, String value2) {
            addCriterion("STATE not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andCodUserCreaIsNull() {
            addCriterion("COD_USER_CREA is null");
            return (Criteria) this;
        }

        public Criteria andCodUserCreaIsNotNull() {
            addCriterion("COD_USER_CREA is not null");
            return (Criteria) this;
        }

        public Criteria andCodUserCreaEqualTo(String value) {
            addCriterion("COD_USER_CREA =", value, "codUserCrea");
            return (Criteria) this;
        }

        public Criteria andCodUserCreaNotEqualTo(String value) {
            addCriterion("COD_USER_CREA <>", value, "codUserCrea");
            return (Criteria) this;
        }

        public Criteria andCodUserCreaGreaterThan(String value) {
            addCriterion("COD_USER_CREA >", value, "codUserCrea");
            return (Criteria) this;
        }

        public Criteria andCodUserCreaGreaterThanOrEqualTo(String value) {
            addCriterion("COD_USER_CREA >=", value, "codUserCrea");
            return (Criteria) this;
        }

        public Criteria andCodUserCreaLessThan(String value) {
            addCriterion("COD_USER_CREA <", value, "codUserCrea");
            return (Criteria) this;
        }

        public Criteria andCodUserCreaLessThanOrEqualTo(String value) {
            addCriterion("COD_USER_CREA <=", value, "codUserCrea");
            return (Criteria) this;
        }

        public Criteria andCodUserCreaLike(String value) {
            addCriterion("COD_USER_CREA like", value, "codUserCrea");
            return (Criteria) this;
        }

        public Criteria andCodUserCreaNotLike(String value) {
            addCriterion("COD_USER_CREA not like", value, "codUserCrea");
            return (Criteria) this;
        }

        public Criteria andCodUserCreaIn(List<String> values) {
            addCriterion("COD_USER_CREA in", values, "codUserCrea");
            return (Criteria) this;
        }

        public Criteria andCodUserCreaNotIn(List<String> values) {
            addCriterion("COD_USER_CREA not in", values, "codUserCrea");
            return (Criteria) this;
        }

        public Criteria andCodUserCreaBetween(String value1, String value2) {
            addCriterion("COD_USER_CREA between", value1, value2, "codUserCrea");
            return (Criteria) this;
        }

        public Criteria andCodUserCreaNotBetween(String value1, String value2) {
            addCriterion("COD_USER_CREA not between", value1, value2, "codUserCrea");
            return (Criteria) this;
        }

        public Criteria andDateUserCreaIsNull() {
            addCriterion("DATE_USER_CREA is null");
            return (Criteria) this;
        }

        public Criteria andDateUserCreaIsNotNull() {
            addCriterion("DATE_USER_CREA is not null");
            return (Criteria) this;
        }

        public Criteria andDateUserCreaEqualTo(Date value) {
            addCriterion("DATE_USER_CREA =", value, "dateUserCrea");
            return (Criteria) this;
        }

        public Criteria andDateUserCreaNotEqualTo(Date value) {
            addCriterion("DATE_USER_CREA <>", value, "dateUserCrea");
            return (Criteria) this;
        }

        public Criteria andDateUserCreaGreaterThan(Date value) {
            addCriterion("DATE_USER_CREA >", value, "dateUserCrea");
            return (Criteria) this;
        }

        public Criteria andDateUserCreaGreaterThanOrEqualTo(Date value) {
            addCriterion("DATE_USER_CREA >=", value, "dateUserCrea");
            return (Criteria) this;
        }

        public Criteria andDateUserCreaLessThan(Date value) {
            addCriterion("DATE_USER_CREA <", value, "dateUserCrea");
            return (Criteria) this;
        }

        public Criteria andDateUserCreaLessThanOrEqualTo(Date value) {
            addCriterion("DATE_USER_CREA <=", value, "dateUserCrea");
            return (Criteria) this;
        }

        public Criteria andDateUserCreaIn(List<Date> values) {
            addCriterion("DATE_USER_CREA in", values, "dateUserCrea");
            return (Criteria) this;
        }

        public Criteria andDateUserCreaNotIn(List<Date> values) {
            addCriterion("DATE_USER_CREA not in", values, "dateUserCrea");
            return (Criteria) this;
        }

        public Criteria andDateUserCreaBetween(Date value1, Date value2) {
            addCriterion("DATE_USER_CREA between", value1, value2, "dateUserCrea");
            return (Criteria) this;
        }

        public Criteria andDateUserCreaNotBetween(Date value1, Date value2) {
            addCriterion("DATE_USER_CREA not between", value1, value2, "dateUserCrea");
            return (Criteria) this;
        }

        public Criteria andDeTermCreaIsNull() {
            addCriterion("DE_TERM_CREA is null");
            return (Criteria) this;
        }

        public Criteria andDeTermCreaIsNotNull() {
            addCriterion("DE_TERM_CREA is not null");
            return (Criteria) this;
        }

        public Criteria andDeTermCreaEqualTo(String value) {
            addCriterion("DE_TERM_CREA =", value, "deTermCrea");
            return (Criteria) this;
        }

        public Criteria andDeTermCreaNotEqualTo(String value) {
            addCriterion("DE_TERM_CREA <>", value, "deTermCrea");
            return (Criteria) this;
        }

        public Criteria andDeTermCreaGreaterThan(String value) {
            addCriterion("DE_TERM_CREA >", value, "deTermCrea");
            return (Criteria) this;
        }

        public Criteria andDeTermCreaGreaterThanOrEqualTo(String value) {
            addCriterion("DE_TERM_CREA >=", value, "deTermCrea");
            return (Criteria) this;
        }

        public Criteria andDeTermCreaLessThan(String value) {
            addCriterion("DE_TERM_CREA <", value, "deTermCrea");
            return (Criteria) this;
        }

        public Criteria andDeTermCreaLessThanOrEqualTo(String value) {
            addCriterion("DE_TERM_CREA <=", value, "deTermCrea");
            return (Criteria) this;
        }

        public Criteria andDeTermCreaLike(String value) {
            addCriterion("DE_TERM_CREA like", value, "deTermCrea");
            return (Criteria) this;
        }

        public Criteria andDeTermCreaNotLike(String value) {
            addCriterion("DE_TERM_CREA not like", value, "deTermCrea");
            return (Criteria) this;
        }

        public Criteria andDeTermCreaIn(List<String> values) {
            addCriterion("DE_TERM_CREA in", values, "deTermCrea");
            return (Criteria) this;
        }

        public Criteria andDeTermCreaNotIn(List<String> values) {
            addCriterion("DE_TERM_CREA not in", values, "deTermCrea");
            return (Criteria) this;
        }

        public Criteria andDeTermCreaBetween(String value1, String value2) {
            addCriterion("DE_TERM_CREA between", value1, value2, "deTermCrea");
            return (Criteria) this;
        }

        public Criteria andDeTermCreaNotBetween(String value1, String value2) {
            addCriterion("DE_TERM_CREA not between", value1, value2, "deTermCrea");
            return (Criteria) this;
        }

        public Criteria andCodUserModiIsNull() {
            addCriterion("COD_USER_MODI is null");
            return (Criteria) this;
        }

        public Criteria andCodUserModiIsNotNull() {
            addCriterion("COD_USER_MODI is not null");
            return (Criteria) this;
        }

        public Criteria andCodUserModiEqualTo(String value) {
            addCriterion("COD_USER_MODI =", value, "codUserModi");
            return (Criteria) this;
        }

        public Criteria andCodUserModiNotEqualTo(String value) {
            addCriterion("COD_USER_MODI <>", value, "codUserModi");
            return (Criteria) this;
        }

        public Criteria andCodUserModiGreaterThan(String value) {
            addCriterion("COD_USER_MODI >", value, "codUserModi");
            return (Criteria) this;
        }

        public Criteria andCodUserModiGreaterThanOrEqualTo(String value) {
            addCriterion("COD_USER_MODI >=", value, "codUserModi");
            return (Criteria) this;
        }

        public Criteria andCodUserModiLessThan(String value) {
            addCriterion("COD_USER_MODI <", value, "codUserModi");
            return (Criteria) this;
        }

        public Criteria andCodUserModiLessThanOrEqualTo(String value) {
            addCriterion("COD_USER_MODI <=", value, "codUserModi");
            return (Criteria) this;
        }

        public Criteria andCodUserModiLike(String value) {
            addCriterion("COD_USER_MODI like", value, "codUserModi");
            return (Criteria) this;
        }

        public Criteria andCodUserModiNotLike(String value) {
            addCriterion("COD_USER_MODI not like", value, "codUserModi");
            return (Criteria) this;
        }

        public Criteria andCodUserModiIn(List<String> values) {
            addCriterion("COD_USER_MODI in", values, "codUserModi");
            return (Criteria) this;
        }

        public Criteria andCodUserModiNotIn(List<String> values) {
            addCriterion("COD_USER_MODI not in", values, "codUserModi");
            return (Criteria) this;
        }

        public Criteria andCodUserModiBetween(String value1, String value2) {
            addCriterion("COD_USER_MODI between", value1, value2, "codUserModi");
            return (Criteria) this;
        }

        public Criteria andCodUserModiNotBetween(String value1, String value2) {
            addCriterion("COD_USER_MODI not between", value1, value2, "codUserModi");
            return (Criteria) this;
        }

        public Criteria andDateUserModiIsNull() {
            addCriterion("DATE_USER_MODI is null");
            return (Criteria) this;
        }

        public Criteria andDateUserModiIsNotNull() {
            addCriterion("DATE_USER_MODI is not null");
            return (Criteria) this;
        }

        public Criteria andDateUserModiEqualTo(Date value) {
            addCriterion("DATE_USER_MODI =", value, "dateUserModi");
            return (Criteria) this;
        }

        public Criteria andDateUserModiNotEqualTo(Date value) {
            addCriterion("DATE_USER_MODI <>", value, "dateUserModi");
            return (Criteria) this;
        }

        public Criteria andDateUserModiGreaterThan(Date value) {
            addCriterion("DATE_USER_MODI >", value, "dateUserModi");
            return (Criteria) this;
        }

        public Criteria andDateUserModiGreaterThanOrEqualTo(Date value) {
            addCriterion("DATE_USER_MODI >=", value, "dateUserModi");
            return (Criteria) this;
        }

        public Criteria andDateUserModiLessThan(Date value) {
            addCriterion("DATE_USER_MODI <", value, "dateUserModi");
            return (Criteria) this;
        }

        public Criteria andDateUserModiLessThanOrEqualTo(Date value) {
            addCriterion("DATE_USER_MODI <=", value, "dateUserModi");
            return (Criteria) this;
        }

        public Criteria andDateUserModiIn(List<Date> values) {
            addCriterion("DATE_USER_MODI in", values, "dateUserModi");
            return (Criteria) this;
        }

        public Criteria andDateUserModiNotIn(List<Date> values) {
            addCriterion("DATE_USER_MODI not in", values, "dateUserModi");
            return (Criteria) this;
        }

        public Criteria andDateUserModiBetween(Date value1, Date value2) {
            addCriterion("DATE_USER_MODI between", value1, value2, "dateUserModi");
            return (Criteria) this;
        }

        public Criteria andDateUserModiNotBetween(Date value1, Date value2) {
            addCriterion("DATE_USER_MODI not between", value1, value2, "dateUserModi");
            return (Criteria) this;
        }

        public Criteria andDeTermModiIsNull() {
            addCriterion("DE_TERM_MODI is null");
            return (Criteria) this;
        }

        public Criteria andDeTermModiIsNotNull() {
            addCriterion("DE_TERM_MODI is not null");
            return (Criteria) this;
        }

        public Criteria andDeTermModiEqualTo(String value) {
            addCriterion("DE_TERM_MODI =", value, "deTermModi");
            return (Criteria) this;
        }

        public Criteria andDeTermModiNotEqualTo(String value) {
            addCriterion("DE_TERM_MODI <>", value, "deTermModi");
            return (Criteria) this;
        }

        public Criteria andDeTermModiGreaterThan(String value) {
            addCriterion("DE_TERM_MODI >", value, "deTermModi");
            return (Criteria) this;
        }

        public Criteria andDeTermModiGreaterThanOrEqualTo(String value) {
            addCriterion("DE_TERM_MODI >=", value, "deTermModi");
            return (Criteria) this;
        }

        public Criteria andDeTermModiLessThan(String value) {
            addCriterion("DE_TERM_MODI <", value, "deTermModi");
            return (Criteria) this;
        }

        public Criteria andDeTermModiLessThanOrEqualTo(String value) {
            addCriterion("DE_TERM_MODI <=", value, "deTermModi");
            return (Criteria) this;
        }

        public Criteria andDeTermModiLike(String value) {
            addCriterion("DE_TERM_MODI like", value, "deTermModi");
            return (Criteria) this;
        }

        public Criteria andDeTermModiNotLike(String value) {
            addCriterion("DE_TERM_MODI not like", value, "deTermModi");
            return (Criteria) this;
        }

        public Criteria andDeTermModiIn(List<String> values) {
            addCriterion("DE_TERM_MODI in", values, "deTermModi");
            return (Criteria) this;
        }

        public Criteria andDeTermModiNotIn(List<String> values) {
            addCriterion("DE_TERM_MODI not in", values, "deTermModi");
            return (Criteria) this;
        }

        public Criteria andDeTermModiBetween(String value1, String value2) {
            addCriterion("DE_TERM_MODI between", value1, value2, "deTermModi");
            return (Criteria) this;
        }

        public Criteria andDeTermModiNotBetween(String value1, String value2) {
            addCriterion("DE_TERM_MODI not between", value1, value2, "deTermModi");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated do_not_delete_during_merge Tue May 29 17:23:43 COT 2018
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table TB_INTERVAL
     *
     * @mbggenerated Tue May 29 17:23:43 COT 2018
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}