package pe.certus.com.bean;

import java.util.Date;

/**
 * Created by Avantica on 6/1/2018.
 */
public class RegistryBean {

    private Date start;
    private Date end;
    private Integer idExploitation;
    private String descProg;
    private Integer codProd;
    private String descProd;
    private String descCampus;
    private String classroom;
    private String descUnitExploitation;
    private Date today;
    private String income;
    private String outcome;
    private String reunionType;

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public Integer getIdExploitation() {
        return idExploitation;
    }

    public void setIdExploitation(Integer idExploitation) {
        this.idExploitation = idExploitation;
    }

    public String getDescProg() {
        return descProg;
    }

    public void setDescProg(String descProg) {
        this.descProg = descProg;
    }

    public Integer getCodProd() {
        return codProd;
    }

    public void setCodProd(Integer codProd) {
        this.codProd = codProd;
    }

    public String getDescProd() {
        return descProd;
    }

    public void setDescProd(String descProd) {
        this.descProd = descProd;
    }

    public String getDescCampus() {
        return descCampus;
    }

    public void setDescCampus(String descCampus) {
        this.descCampus = descCampus;
    }

    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }

    public String getDescUnitExploitation() {
        return descUnitExploitation;
    }

    public void setDescUnitExploitation(String descUnitExploitation) {
        this.descUnitExploitation = descUnitExploitation;
    }

    public Date getToday() {
        return today;
    }

    public void setToday(Date today) {
        this.today = today;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    public String getReunionType() {
        return reunionType;
    }

    public void setReunionType(String reunionType) {
        this.reunionType = reunionType;
    }
}
