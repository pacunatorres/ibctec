<article class="rs-content-wrapper">
    <div class="rs-content">
        <div class="rs-inner">

            <!-- Begin Dashhead -->
            <div class="rs-dashhead m-b-lg">
                <div class="rs-dashhead-content">
                    <div class="rs-dashhead-titles">
                        <h6 class="rs-dashhead-subtitle text-uppercase">Configuraci&oacute;n</h6>

                        <div class="toggle-toolbar-btn">
                            <span class="fa fa-sort"></span>
                        </div>

                    </div>

                </div>

                <ol class="breadcrumb">
                    <li>
                        <a href="${pageContext.request.contextPath}/secure/admin/dashboard"><i
                                class="fa fa-home m-r"></i> Principal</a>
                    </li>
                    <li class="active">Configuraci&oacute;n acad&eacute;mica</li>
                </ol>

            </div>

            <!-- Begin default content width -->
            <div class="container-fluid">

                <div class="panel panel-plain panel-rounded">
                    <div class="panel-heading borderless">
                        <h3 class="panel-title"><span
                                style="font-weight: bold!important;">Configuraci&oacute;n acad&eacute;mica</span></h3>
                    </div>
                    <div>
                        <div id="formAcademicConfiguration" class="panel-body">

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="level" class="control-label">NIVEL</label>
                                        <select id="level"
                                                name="level">
                                            <option value="">- Seleccione -</option>
                                            <option value="TECNICO">TECNICO</option>
                                            <option value="CERTIFICACION">CERTIFICACION</option>
                                            <option value="IN HOUSE">IN HOUSE</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="checkbox checkbox-custom">
                                        <label>
                                            <input type="checkbox"
                                                   value=""
                                                   id="ipMarking"
                                                   name="ipMarking">
                                            <span class="checker"></span>
                                            Marcaci&oacute;n por IP
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="checkbox checkbox-custom">
                                        <label>
                                            <input type="checkbox"
                                                   value=""
                                                   name="accounting"
                                                   id="accounting">
                                            <span class="checker"></span>
                                            Contabilidad
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="incomeTolerance" class="control-label">Tolerancia de ingreso</label>
                                        <input type="text"
                                               class="form-control"
                                               name="incomeTolerance"
                                               id="incomeTolerance">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="outcomeTolerance" class="control-label">Tolerancia de salida</label>
                                        <input type="text"
                                               class="form-control"
                                               name="outcomeTolerance"
                                               id="outcomeTolerance">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="before" class="control-label">Previo</label>
                                        <input type="text"
                                               class="form-control"
                                               name="before"
                                               id="before">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="after" class="control-label">Despues</label>
                                        <input type="text"
                                               class="form-control"
                                               name="after"
                                               id="after">
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="panel-footer" style="height: 80px;border: none!important;">
                            <div class="form-group m-a-0 ">
                                <div style="float: right;">
                                    <button id="btnReset" type="reset" class="btn btn-default btn-wide">
                                        Restablecer&nbsp;&nbsp;
                                        <i class="fa fa-undo" aria-hidden="true"></i>
                                    </button>
                                    <button id="btnConfirmChangesSave" type="button"
                                            class="btn btn-success btn-wide">
                                        Guardar&nbsp;&nbsp;
                                        <i class="fa fa-floppy-o"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <%--tabla here--%>
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="table-responsive"
                                             style="font-size: 14px!important;">
                                            <table class="table table-bordered table-hover"
                                                   id="tblAcademicConfiguration">
                                                <thead style="background: #00205b; color: #FFF">
                                                <tr>
                                                    <th style="width: 5%;text-align: center!important;vertical-align: text-top;">
                                                        N&deg;
                                                    </th>
                                                    <th style="width: 15%;text-align: center!important;vertical-align: text-top;">
                                                        NIVEL
                                                    </th>
                                                    <th style="width: 12%;text-align: center!important;vertical-align: text-top;">
                                                        MARCACI&Oacute;N POR IP
                                                    </th>
                                                    <th style="width: 12%;text-align: center!important;vertical-align: text-top;">
                                                        CONTABILIDAD
                                                    </th>
                                                    <th style="width: 12%;text-align: center!important;vertical-align: text-top;">
                                                        TOLERANCIA DE INGRESO
                                                    </th>
                                                    <th style="width: 11%;text-align: center!important;vertical-align: text-top;">
                                                        TOLERANCIA DE SALIDA
                                                    </th>
                                                    <th style="width: 11%;text-align: center!important;vertical-align: text-top;">
                                                        PREVIO (min)
                                                    </th>
                                                    <th style="width: 11%;text-align: center!important;vertical-align: text-top;">
                                                        DESPU&Eacute;S (min)
                                                    </th>
                                                    <th style="width: 5%;text-align: center!important;vertical-align: text-top;"></th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>