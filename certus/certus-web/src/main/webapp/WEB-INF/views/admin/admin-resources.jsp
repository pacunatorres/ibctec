<link rel="shortcut icon" type="image/x-icon"
      href="${pageContext.request.contextPath}/resources/commons/images/favicon.png">

<link href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.min.css" rel="stylesheet" media="screen">

<link href="${pageContext.request.contextPath}/resources/admin/icons/entypo/entypo.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/admin/css/font-awesome.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/admin/icons/flag-icon/css/flag-icon.min.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/admin/icons/material-icon/material-icon.min.css" rel="stylesheet" >
<link href="${pageContext.request.contextPath}/resources/admin/icons/weather-icon/css/weather-icons.min.css" rel="stylesheet" >

<link href="${pageContext.request.contextPath}/resources/admin/css/plugins.css" rel="stylesheet" >
<link href="${pageContext.request.contextPath}/resources/admin/css/bootstrap.custom.css" rel="stylesheet" >
<link href="${pageContext.request.contextPath}/resources/admin/css/style.css" rel="stylesheet" >
<link href="${pageContext.request.contextPath}/resources/admin/css/helper.css" rel="stylesheet">

