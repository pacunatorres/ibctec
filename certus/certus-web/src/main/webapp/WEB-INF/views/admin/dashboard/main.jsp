<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<!DOCTYPE html>
<html lang=es>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><tiles:insertAttribute name="main-title"/></title>
    <tiles:insertAttribute name="admin-resources"/>
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<input id="contextPath" type="hidden" value="${pageContext.request.contextPath}"/>
<div id="rs-wrapper">
    <tiles:insertAttribute name="main-header"/>
    <tiles:insertAttribute name="main-sidebar"/>
    <tiles:insertAttribute name="main-content"/>
    <tiles:insertAttribute name="main-footer"/>
</div>
<div id="rs-screen-overlay"></div>
<tiles:insertAttribute name="main-scripts" ignore="true"/>
<script src="${pageContext.request.contextPath}/resources/commons/js/web/admin/configuration.js"></script>
<script src="${pageContext.request.contextPath}/resources/commons/js/web/admin/admin-account.js"></script>


<%--modal from coonfirm changes academic configuration--%>
<div style="margin-top: 10%;"
     class="modal fade"
     tabindex="-1"
     id="modalConfirmChanges"
     role="dialog"
     aria-labelledby="modalConfirmChangesLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content b-r-0 borderless">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalConfirmChangesLabel">Confirmar</h4>
            </div>
            <div class="modal-body">
                    <span style="margin: 40px;display: block;">
                        Esta seguro de continuar?
                    </span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                <button id="btnSave" type="button" class="btn btn-success btn-wide">Guardar cambios</button>
            </div>
        </div>
    </div>
</div>

</body>
</html>