<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<!DOCTYPE html>
<html lang=es>
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><tiles:insertAttribute name="main-title"/></title>
    <tiles:insertAttribute name="admin-resources"/>
</head>
<body>
<input id="contextPath" type="hidden" value="${pageContext.request.contextPath}"/>
<tiles:insertAttribute name="login-body"/>
<tiles:insertAttribute name="admin-scripts" ignore="true"/>
<script src="${pageContext.request.contextPath}/resources/commons/js/web/admin/secure.js"></script>
</body>
</html>