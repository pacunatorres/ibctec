<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
your browser</a> to improve your experience.</p>
<![endif]-->

<div id="rs-wrapper" class="bg-grad-certus">

    <div class="login-wrap p-a-md m-x-auto login-box-sales">
        <p class="text-center" style="margin-bottom: 40px">
            <img src="${pageContext.request.contextPath}/resources/admin/images/certus-logo-white.png"
                 alt="Certus - Marcaci&oacute;n Docente">
        </p>
        <div class="bg-white b-r-a padding-box-login light-bs"
             style="display: inline-block;">
            <form id="frmCheck"
                  class="m-b-lg"
                  action="${pageContext.request.contextPath}/secure_check?public=false"
                  method='POST'>
                <div class="clear-padding">
                    <div class="col-sm-12 col-xs-12 sign-up-form">
                        <label>Usuario</label>
                        <div class="col-sm-12 col-xs-12 mb-20 input-group">
                            <input name="username"
                                   type="text"
                                   class="form-control"
                                   placeholder="Correo Electr&oacute;nico"
                                   required>
                            <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                        </div>
                        <div style="height: 20px!important; display: block;"></div>
                        <label>Contrase&ntilde;a</label>
                        <div class="col-sm-12 col-xs-12 mb-20 input-group">
                            <input id="password"
                                   name="password"
                                   type="password"
                                   class="form-control"
                                   placeholder="Contrase&ntilde;a"
                                   required>
                            <span id="spShowPwd" class="input-group-addon"><i class="fa fa-eye fa-fw"></i></span>
                        </div>
                        <div id="divMessageError" class="col-xs-12 error-message-login">
                            <span id="spMessageError"></span>
                        </div>
                        <div style="height: 40px!important; display: block;"></div>
                        <button id="btnCheck"
                                class="btn btn-success btn-block text-uppercase"
                                type="submit">Iniciar Sesi&oacute;n
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>

    <footer class="rs-footer login-footer text-center">
        <span class="text-white small">&copy; 2018 Certus</span>
    </footer>
</div>

