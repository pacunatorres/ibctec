<article class="rs-content-wrapper">
    <div class="rs-content">
        <div class="rs-inner">

            <!-- Begin Dashhead -->
            <div class="rs-dashhead m-b-lg">
                <div class="rs-dashhead-content">
                    <div class="rs-dashhead-titles">
                        <h6 class="rs-dashhead-subtitle text-uppercase">Principal</h6>
                        <h3 class="rs-dashhead-title m-t"><span id="mainTitle"></span></h3>

                        <div class="toggle-toolbar-btn">
                            <span class="fa fa-sort"></span>
                        </div>

                    </div>

                </div>

                <ol class="breadcrumb">
                    <li class="active"><a href="javascript:void(0);"><i class="fa fa-home m-r"></i> Principal</a></li>
                </ol>

            </div>

            <!-- Begin default content width -->
            <div class="container-fluid">
                <img src="${pageContext.request.contextPath}/resources/admin/images/watermark-certus.png"
                     class="watermark-dashboard" alt="Certus">
            </div><!-- /.container-fluid -->

        </div><!-- /.rs-inner -->
    </div><!-- /.rs-content -->
</article>
<!-- /.rs-content-wrapper -->
<!-- END MAIN CONTENT -->
