<article class="rs-content-wrapper">
    <div class="rs-content">
        <div class="rs-inner">

            <!-- Begin Dashhead -->
            <div class="rs-dashhead m-b-lg">
                <div class="rs-dashhead-content">
                    <div class="rs-dashhead-titles">
                        <h6 class="rs-dashhead-subtitle text-uppercase">Configuraci&oacute;n</h6>

                        <div class="toggle-toolbar-btn">
                            <span class="fa fa-sort"></span>
                        </div>

                    </div>

                </div>

                <ol class="breadcrumb">
                    <li>
                        <a href="${pageContext.request.contextPath}/secure/admin/dashboard"><i
                                class="fa fa-home m-r"></i> Principal</a>
                    </li>
                    <li class="active">Configuraci&oacute;n de sede</li>
                </ol>

            </div>

            <!-- Begin default content width -->
            <div class="container-fluid">

                <div class="panel panel-plain panel-rounded">
                    <div class="panel-heading borderless">
                        <h3 class="panel-title"><span
                                style="font-weight: bold!important;">Configuraci&oacute;n de sede</span></h3>
                    </div>
                    <div>
                        <div id="formSiteConfiguration" class="panel-body">

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="campus" class="control-label">CAMPUS</label>
                                        <select id="campus"
                                                name="campus">
                                            <option value="">- Seleccione -</option>
                                            <option value="AQP">AQP</option>
                                            <option value="LIM">LIM</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="building" class="control-label">EDIFICIO</label>
                                        <select id="building"
                                                name="building">
                                            <option value="">- Seleccione -</option>
                                            <option value="AQ1">AQ1</option>
                                            <option value="LI1">LI1</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="classroom" class="control-label">AULA</label>
                                        <select id="classroom"
                                                name="classroom">
                                            <option value="">- Seleccione -</option>
                                            <option value="AQP-A101A">AQP-A101A</option>
                                            <option value="LIM-L101L">LIM-L101L</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="panel-footer" style="height: 80px;border: none!important;">
                            <div class="form-group m-a-0 ">
                                <div style="float: right;">
                                    <button id="btnReset" type="reset" class="btn btn-default btn-wide">
                                        Restablecer&nbsp;&nbsp;
                                        <i class="fa fa-undo" aria-hidden="true"></i>
                                    </button>
                                    <button id="btnConfirmChangesSave" type="button"
                                            class="btn btn-success btn-wide">
                                        Guardar&nbsp;&nbsp;
                                        <i class="fa fa-floppy-o"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <%--tabla here--%>
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="table-responsive"
                                             style="font-size: 14px!important;">
                                            <table class="table table-bordered table-hover" id="tblSite">
                                                <thead style="background: #00205b; color: #FFF">
                                                <tr>
                                                    <th style="width: 2%;text-align: center!important;vertical-align: text-top;">
                                                        N&deg;
                                                    </th>
                                                    <th style="width: 20%;text-align: center!important;vertical-align: text-top;">
                                                        CAMPUS
                                                    </th>
                                                    <th style="width: 20%;text-align: center!important;vertical-align: text-top;">
                                                        EDIFICIO
                                                    </th>
                                                    <th style="width: 24%;text-align: center!important;vertical-align: text-top;">
                                                        AULA
                                                    </th>
                                                    <th style="width: 24%;text-align: center!important;vertical-align: text-top;">
                                                        IP
                                                    </th>
                                                    <th style="width: 5%;text-align: center!important;vertical-align: text-top;"></th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</article>

