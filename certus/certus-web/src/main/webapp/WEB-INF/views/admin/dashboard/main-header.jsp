<nav class="navbar navbar-default rs-navbar navbar-static-top">
    <div class="container-fluid">
        <div class="navbar-header has-right-divider">
            <div class="rs-logo fixed-width">
                <a class="navbar-brand" href="#">
                    <img style="margin-left: 55px;" alt="Certus" src="${pageContext.request.contextPath}/resources/admin/images/logo-certus-dasboard-white.png">
                </a>
            </div>

            <button type="button" class="navbar-toggle collapsed sidebar-toggle" id="rs-sidebar-toggle-mobile">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#roosa-nav-collapse" aria-expanded="false">
                <span class="gcon gcon-dots-three-vertical f-s-sm"></span>
            </button>

        </div>

        <div class="collapse navbar-collapse" id="roosa-nav-collapse">
            <div class="navbar-right">
                <ul class="nav navbar-nav">

                    <li class="rs-user-nav dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            <span class="circle-notification badge-notification bg-success"></span>
                            <img src="${pageContext.request.contextPath}/resources/admin/images/avatar-user.png"
                                 class="rs-nav-avatar img-circle" alt="Usuario">
                            <span class="visible-xs-inline-block m-l">Bienvenido, <strong><span id="userAccountMenu"></span></strong></span>
                        </a>

                        <ul class="dropdown-menu lg-dropdown">
                            <li class="inherit-bg">
                                <a href="javascript:void(0);">
                                    <span class="f-s-xs f-w-500">Bienvenido, <span id="userAccountMenuDetail"></span></span><br/>
                                </a>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li class="dropdown-header text-uppercase">Configuraci&oacute;n de cuenta</li>
                            <li class="menu-icon"><a href="javascript:void(0);"><span
                                    class="mcon mcon-face rs-dropdown-icon"></span>Mi Cuenta</a></li>
                            <li class="menu-icon"><a href="javascript:void(0);"><span
                                    class="gcon gcon-lock-open rs-dropdown-icon"></span>Cambiar Contrase&ntilde;a</a>
                            </li>
                            <li class="menu-icon"><a href="${pageContext.request.contextPath}/secure-logout?public=false"><span
                                    class="gcon gcon-log-out rs-dropdown-icon"></span>Cerrar Sesi&oacute;n</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>


