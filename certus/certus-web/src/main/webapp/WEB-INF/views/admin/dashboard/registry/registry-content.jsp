<article class="rs-content-wrapper">
    <div class="rs-content">
        <div class="rs-inner">

            <!-- Begin Dashhead -->
            <div class="rs-dashhead m-b-lg">
                <div class="rs-dashhead-content">
                    <div class="rs-dashhead-titles">
                        <h6 class="rs-dashhead-subtitle text-uppercase">Marcaci&oacute;n docente</h6>

                        <div class="toggle-toolbar-btn">
                            <span class="fa fa-sort"></span>
                        </div>

                    </div>

                </div>

                <ol class="breadcrumb">
                    <li>
                        <a href="${pageContext.request.contextPath}/secure/admin/dashboard"><i
                                class="fa fa-home m-r"></i> Principal</a>
                    </li>
                    <li class="active">Marcaci&oacute;n</li>
                </ol>

            </div>

            <!-- Begin default content width -->
            <div class="container-fluid">

                <div class="panel panel-plain panel-rounded">
                    <div class="panel-heading borderless">
                        <h3 class="panel-title"><span
                                style="font-weight: bold!important;">Marcaci&oacute;n</span></h3>
                    </div>
                    <div id="frmRegisterUser">
                        <div class="panel-body">
                            <div class="col-sm-4 col-md-offset-4">
                                <div class="registry-teacher">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <span>  20:16:40</span>
                                </div>
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="row">
                                <%--tabla here--%>
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="table-responsive"
                                             style="font-size: 11px!important;">
                                            <table class="table table-bordered table-hover" id="tblUsers">
                                                <thead style="background: #00205b; color: #FFF">
                                                <tr>
                                                    <th style="width: 4%;text-align: center!important;vertical-align: text-top;">
                                                        N&deg;
                                                    </th>
                                                    <th style="width: 7%;text-align: center!important;vertical-align: text-top;">
                                                        PERIODO
                                                    </th>
                                                    <th style="width: 7%;text-align: center!important;vertical-align: text-top;">
                                                        NRC
                                                    </th>
                                                    <th style="width: 7%;text-align: center!important;vertical-align: text-top;">
                                                        MATERIA
                                                    </th>
                                                    <th style="width: 7%;text-align: center!important;vertical-align: text-top;">
                                                        CODIGO CURSO
                                                    </th>
                                                    <th style="width: 7%;text-align: center!important;vertical-align: text-top;">
                                                        NOMBRE DE CURSO
                                                    </th>
                                                    <th style="width: 7%;text-align: center!important;vertical-align: text-top;">
                                                        CAMPUS
                                                    </th>
                                                    <th style="width: 7%;text-align: center!important;vertical-align: text-top;">
                                                        AULA
                                                    </th>
                                                    <th style="width: 7%;text-align: center!important;vertical-align: text-top;">
                                                        FECHA
                                                    </th>
                                                    <th style="width: 7%;text-align: center!important;vertical-align: text-top;">
                                                        HR INI
                                                    </th>
                                                    <th style="width: 7%;text-align: center!important;vertical-align: text-top;">
                                                        HR FIN
                                                    </th>
                                                    <th style="width: 7%;text-align: center!important;vertical-align: text-top;">
                                                        INGRESO
                                                    </th>
                                                    <th style="width: 7%;text-align: center!important;vertical-align: text-top;">
                                                        SALIDA
                                                    </th>
                                                    <th style="width: 7%;text-align: center!important;vertical-align: text-top;">
                                                        TIPO DE REUNION
                                                    </th>
                                                    <th style="width: 5%;text-align: center!important;vertical-align: text-top;">
                                                        OPCIONES
                                                    </th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel-footer" style="height: 80px;border: none!important;">
                            <div class="form-group m-a-0 ">
                                <div style="float: right;">
                                    <button id="btnResetFilter" type="reset" class="btn btn-success btn-wide">
                                        INGRESO&nbsp;&nbsp;
                                        <i class="fa fa-sign-in" aria-hidden="true"></i>
                                    </button>
                                    <button id="btnSearch" type="submit" class="btn btn-success btn-wide">
                                        SALIDA&nbsp;&nbsp;
                                        <i class="fa fa-sign-out"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</article>