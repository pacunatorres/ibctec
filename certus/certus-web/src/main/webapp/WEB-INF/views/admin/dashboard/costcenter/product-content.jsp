<article class="rs-content-wrapper">
    <div class="rs-content">
        <div class="rs-inner">

            <!-- Begin Dashhead -->
            <div class="rs-dashhead m-b-lg">
                <div class="rs-dashhead-content">
                    <div class="rs-dashhead-titles">
                        <h6 class="rs-dashhead-subtitle text-uppercase">Centro de costos</h6>

                        <div class="toggle-toolbar-btn">
                            <span class="fa fa-sort"></span>
                        </div>

                    </div>

                </div>

                <ol class="breadcrumb">
                    <li>
                        <a href="${pageContext.request.contextPath}/secure/admin/dashboard"><i
                                class="fa fa-home m-r"></i> Principal</a>
                    </li>
                    <li class="active">Producto</li>
                </ol>

            </div>

            <!-- Begin default content width -->
            <div class="container-fluid">

                <div class="panel panel-plain panel-rounded">
                    <div class="panel-heading borderless">
                        <h3 class="panel-title"><span
                                style="font-weight: bold!important;">Producto</span></h3>
                    </div>
                    <div>
                        <div id="formProduct" class="panel-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="program" class="control-label">PROGRAMA</label>
                                        <select id="program"
                                                name="program">
                                            <option value="">- Seleccione -</option>
                                            <option value="A-CAB">ADMINISTRACI&Oacute;N BANCARIA</option>
                                            <option value="G-CBF">ADMIN DE NEGOCIOS BANCA Y FINANC</option>
                                            <option value="B-CNI">ADMIN DE NEGOCIOS INTERNACIONALES</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="codeProgram" class="control-label">CODIGO DE PROGRAMA</label>
                                        <input type="text"
                                               class="form-control"
                                               name="codeProgram"
                                               id="codeProgram"
                                               readonly>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="product" class="control-label">PRODUCTO</label>
                                        <select id="product"
                                                name="product">
                                            <option value="">- Seleccione -</option>
                                            <option value="001">ADMINISTRACI&Oacute;N BANCARIA</option>
                                            <option value="002">ADMINIS NEG INTERNACIONALES</option>
                                            <option value="003">ADMINIS BANCA Y FINAN</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="codeProduct" class="control-label">CODIGO PRODUCTO</label>
                                        <input type="text"
                                               class="form-control"
                                               name="codeProduct"
                                               id="codeProduct"
                                               readonly>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="panel-footer" style="height: 80px;border: none!important;">
                            <div class="form-group m-a-0 ">
                                <div style="float: right;">
                                    <button id="btnReset" type="reset" class="btn btn-default btn-wide">
                                        Restablecer&nbsp;&nbsp;
                                        <i class="fa fa-undo" aria-hidden="true"></i>
                                    </button>
                                    <button id="btnConfirmChangesSave" type="button"
                                            class="btn btn-success btn-wide">
                                        Guardar&nbsp;&nbsp;
                                        <i class="fa fa-floppy-o"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <%--tabla here--%>
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="table-responsive"
                                             style="font-size: 14px!important;">
                                            <table class="table table-bordered table-hover" id="tblProduct">
                                                <thead style="background: #00205b; color: #FFF">
                                                <tr>
                                                    <th style="width: 7%;text-align: center!important;vertical-align: text-top;">
                                                        N&deg;
                                                    </th>
                                                    <th style="width: 22%;text-align: center!important;vertical-align: text-top;">
                                                        CODIGO PROGRAMA
                                                    </th>
                                                    <th style="width: 22%;text-align: center!important;vertical-align: text-top;">
                                                        PROGRAMA
                                                    </th>
                                                    <th style="width: 22%;text-align: center!important;vertical-align: text-top;">
                                                        CODIGO PRODUCTO
                                                    </th>
                                                    <th style="width: 22%;text-align: center!important;vertical-align: text-top;">
                                                        PRODUCTO
                                                    </th>
                                                    <th style="width: 5%;text-align: center!important;vertical-align: text-top;">
                                                    </th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</article>