<article class="rs-content-wrapper">
    <div class="rs-content">
        <div class="rs-inner">

            <!-- Begin Dashhead -->
            <div class="rs-dashhead m-b-lg">
                <div class="rs-dashhead-content">
                    <div class="rs-dashhead-titles">
                        <h6 class="rs-dashhead-subtitle text-uppercase">Configuraci&oacute;n GTH</h6>

                        <div class="toggle-toolbar-btn">
                            <span class="fa fa-sort"></span>
                        </div>

                    </div>

                </div>

                <ol class="breadcrumb">
                    <li>
                        <a href="${pageContext.request.contextPath}/secure/admin/dashboard"><i
                                class="fa fa-home m-r"></i> Principal</a>
                    </li>
                    <li class="active">Periodos</li>
                </ol>

            </div>

            <!-- Begin default content width -->
            <div class="container-fluid">

                <div class="panel panel-plain panel-rounded">
                    <div class="panel-heading borderless">
                        <h3 class="panel-title"><span
                                style="font-weight: bold!important;">Periodos</span></h3>
                    </div>
                    <div>
                        <div id="frmInterval" class="panel-body">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="startDate" class="control-label">INICIO</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
                                            <input type="text"
                                                   class="form-control"
                                                   id="startDate"
                                                   name="startDate"
                                                   readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="endDate" class="control-label">FIN</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
                                            <input type="text"
                                                   class="form-control"
                                                   id="endDate"
                                                   name="endDate"
                                                   readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="year" class="control-label">A&Ntilde;O</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
                                            <input type="text"
                                                   class="form-control"
                                                   id="year"
                                                   name="year"
                                                   readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="month" class="control-label">MES</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>
                                            <input type="text"
                                                   class="form-control"
                                                   id="month"
                                                   name="month"
                                                   readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="panel-footer" style="height: 80px;border: none!important;">
                            <div class="form-group m-a-0 ">
                                <div style="float: right;">
                                    <button id="btnReset" type="reset" class="btn btn-default btn-wide">
                                        Restablecer&nbsp;&nbsp;
                                        <i class="fa fa-undo" aria-hidden="true"></i>
                                    </button>
                                    <button id="btnConfirmChangesSave" type="button"
                                            class="btn btn-success btn-wide">
                                        Guardar&nbsp;&nbsp;
                                        <i class="fa fa-floppy-o"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <%--tabla here--%>
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="table-responsive"
                                             style="font-size: 14px!important;">
                                            <table class="table table-bordered table-hover" id="tblInterval">
                                                <thead style="background: #00205b; color: #FFF">
                                                <tr>
                                                    <th style="width: 5%;text-align: center!important;vertical-align: text-top;">
                                                        N&deg;
                                                    </th>
                                                    <th style="width: 18%;text-align: center!important;vertical-align: text-top;">
                                                        A&Ntilde;O
                                                    </th>
                                                    <th style="width: 18%;text-align: center!important;vertical-align: text-top;">
                                                        MES
                                                    </th>
                                                    <th style="width: 18%;text-align: center!important;vertical-align: text-top;">
                                                        INICIO
                                                    </th>
                                                    <th style="width: 18%;text-align: center!important;vertical-align: text-top;">
                                                        FIN
                                                    </th>
                                                    <th style="width: 18%;text-align: center!important;vertical-align: text-top;">
                                                        ESTADO
                                                    </th>
                                                    <th style="width: 5%;text-align: center!important;vertical-align: text-top;">
                                                    </th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</article>