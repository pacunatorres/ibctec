<article class="rs-content-wrapper">
    <div class="rs-content">
        <div class="rs-inner">

            <!-- Begin Dashhead -->
            <div class="rs-dashhead m-b-lg">
                <div class="rs-dashhead-content">
                    <div class="rs-dashhead-titles">
                        <h6 class="rs-dashhead-subtitle text-uppercase">Centro de costos</h6>

                        <div class="toggle-toolbar-btn">
                            <span class="fa fa-sort"></span>
                        </div>

                    </div>

                </div>

                <ol class="breadcrumb">
                    <li>
                        <a href="${pageContext.request.contextPath}/secure/admin/dashboard"><i
                                class="fa fa-home m-r"></i> Principal</a>
                    </li>
                    <li class="active">Departamento</li>
                </ol>

            </div>

            <!-- Begin default content width -->
            <div class="container-fluid">

                <div class="panel panel-plain panel-rounded">
                    <div class="panel-heading borderless">
                        <h3 class="panel-title"><span
                                style="font-weight: bold!important;">Departamento</span></h3>
                    </div>
                    <div>
                        <div id="formDepartment" class="panel-body">

                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="level" class="control-label">NIVEL</label>
                                        <select id="level"
                                                name="level">
                                            <option value="">- Seleccione -</option>
                                            <option value="TE">TECNICO</option>
                                            <option value="CT">CERTIFICACIONES</option>
                                            <option value="IH">IN HOUSE</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="codeLevel" class="control-label">CODIGO DE NIVEL</label>
                                        <input type="text"
                                               class="form-control"
                                               name="codeLevel"
                                               id="codeLevel"
                                               readonly>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="department" class="control-label">DEPARTAMENTO</label>
                                        <select id="department"
                                                name="department">
                                            <option value="">- Seleccione -</option>
                                            <option value="000004">&Aacute;REA ACAD&Eacute;MICA</option>
                                            <option value="000028">INGRESOS ACAD&Eacute;MICOS</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="codeDepartment" class="control-label">CODIGO DEPARTAMENTO</label>
                                        <input type="text"
                                               class="form-control"
                                               name="codeDepartment"
                                               id="codeDepartment"
                                               readonly>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="panel-footer" style="height: 80px;border: none!important;">
                            <div class="form-group m-a-0 ">
                                <div style="float: right;">
                                    <button id="btnReset" type="reset" class="btn btn-default btn-wide">
                                        Restablecer&nbsp;&nbsp;
                                        <i class="fa fa-undo" aria-hidden="true"></i>
                                    </button>
                                    <button id="btnConfirmChangesSave" type="button"
                                            class="btn btn-success btn-wide">
                                        Guardar&nbsp;&nbsp;
                                        <i class="fa fa-floppy-o"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <%--tabla here--%>
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="table-responsive"
                                             style="font-size: 14px!important;">
                                            <table class="table table-bordered table-hover" id="tblDepartment">
                                                <thead style="background: #00205b; color: #FFF">
                                                <tr>
                                                    <th style="width: 7%;text-align: center!important;vertical-align: text-top;">
                                                        N&deg;
                                                    </th>
                                                    <th style="width: 22%;text-align: center!important;vertical-align: text-top;">
                                                        CODIGO NIVEL
                                                    </th>
                                                    <th style="width: 22%;text-align: center!important;vertical-align: text-top;">
                                                        NIVEL
                                                    </th>
                                                    <th style="width: 22%;text-align: center!important;vertical-align: text-top;">
                                                        CODIGO DEPARTAMENTO
                                                    </th>
                                                    <th style="width: 22%;text-align: center!important;vertical-align: text-top;">
                                                        DEPARTAMENTO
                                                    </th>
                                                    <th style="width: 5%;text-align: center!important;vertical-align: text-top;">
                                                    </th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</article>