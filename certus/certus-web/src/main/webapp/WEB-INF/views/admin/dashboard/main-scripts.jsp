<script src="${pageContext.request.contextPath}/resources/commons/js/plugins/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/js/vendor.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/js/plugins.js"></script>

<script src="${pageContext.request.contextPath}/resources/admin/js/Chart.bundle.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/js/jquery.easypiechart.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/js/morris.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/js/jquery.sparkline.min.js"></script>

<script src="${pageContext.request.contextPath}/resources/admin/js/apps.js"></script>
<script src="${pageContext.request.contextPath}/resources/admin/js/dashboard.js"></script>

<script src="${pageContext.request.contextPath}/resources/commons/js/plugins/bootstrap-notify.js"></script>
<script src="${pageContext.request.contextPath}/resources/commons/js/plugins/jquery.dataTables.min.js"></script>

<script src="${pageContext.request.contextPath}/resources/commons/js/componentes/componentes.js"></script>
