/**
 * @fileoverview Librerias de funciones para pagina medicion.
 *
 * @autor joanhuve@gmail.com 2018
 * @version 1.0
 */
$(document).ready(function () {
    fnHome.fnGetConfiguration();
    fnHome.fnLoadMainSlider();

});

var fnHome = function () {


    function fnGetConfiguration(callback) {

        var contextPath = $("#contextPath").val();
        $.getJSON("https://api.ipify.org?format=json", function (data) {
            var header = createHeader(RP_GET_CONFIGURATIONS, data.ip);
            localStorage.setItem("ip", JSON.stringify(data.ip));
            // console.log("#header: " + JSON.stringify(header));
            consultarAjax("POST", (contextPath + CONTEXT_PATH_PUBLIC + RP_GET_CONFIGURATIONS), JSON.stringify(header), contextPath, function (callbackGC) {
                console.log("# response: " + JSON.stringify(callbackGC));
                fnFillAccountSection(callbackGC.Body.Response.HtmlTemplateAccountUser);
                fnFillHomeMenu(callbackGC.Body.Response.MenuList);
                fnFillConfigurations(callbackGC.Body.Response.Configurations);
                //
                // localStorage.setItem("genders", JSON.stringify(callbackGC.Body.Response.Genders));
                //


                construirSelect("selLeavingFrom", callbackGC.Body.Response.Destinations);
                // construirSelect("selLeavingTo", callbackGC.Body.Response.Destinations);
                localStorage.setItem("leavingToValues", JSON.stringify(callbackGC.Body.Response.Destinations));

            });
        });
    }

    function fnFillAccountSection(accountTemplate) {
        $('#detailSession').empty();
        $('#detailSession').append(accountTemplate);
    }

    function fnFillHomeMenu(menuList) {
        var contextPath = $("#contextPath").val();
        var menu = '';
        var menuDestinations = '';
        var menuService = '';
        var menuProfile = '';
        var categories = '';
        $.each(menuList, function (i, item) {
            if (item.TypeMenu == 'PUBLIC') {
                menu += '<li class="dropdown" id="menup-' + item.OptionCode + '">' +
                    '<a class="dropdown-toggle" data-toggle="dropdown" href="' + contextPath + item.Url + '">' + item.Description + '</a>';
                if (item.SubMenu != null && item.SubMenu != '') {
                    menu += '<ul class="dropdown-menu">';
                    $.each(item.SubMenu, function (i, item) {
                        if (item.OptionParent == 3) {
                            categories += ' <button data-filter=".' + item.Description + '">' + item.Description + '</button>';
                        }
                        menu += '<li><a href="' + contextPath + item.Url + '">' + item.Description + '</a></li>';
                    });
                    menu += '</ul>';
                }
                menu += '</li>';
            } else if (item.TypeMenu == 'DESTINATIONS') {
                menuDestinations += '<li><a href="' + contextPath + item.Url + '">' + item.Description + '</a></li>'
            } else if (item.TypeMenu == 'SERVICES') {
                menuService += '<li><a href="' + contextPath + item.Url + '">' + item.Description + '</a></li>'
            }
        });

        $('#mainMenu').empty();
        $('#mainMenu').append(menu);
        $('#ulDestinations').append(menuDestinations);
        $('#ulServices').append(menuService);
    }

    function fnFillConfigurations(configurations) {
        var contact = '';
        var copyRight = '';
        var contentSocialNetworks = '';
        $.each(configurations, function (i, item) {
            if (item.Keyword == 'PHONE_CONTACT') {
                contact += '<a href="#" class="transition-effect"><i class="fa fa-phone"></i> ' + item.Description + '</a>'
                $('#contactPhone').html(item.Description);
            } else if (item.Keyword == 'EMAIL_CONTACT') {
                contact += '<a href="#" class="transition-effect"><i class="fa fa-envelope-o"></i> ' + item.Description + '</a>'
                $('#contactEmail').html(item.Description);
            } else if (item.Keyword == 'COPYRIGHT') {
                copyRight += '<p>' + item.Title + '</p>'
            } else if (item.Keyword == 'PRINCIPAL_ADDRESS') {
                $('#principalAddress').html('Oficina principal: ' + item.Description);
            } else if (item.Keyword == 'ABOUT_STORE') {
                $('#pAboutStore').html(item.Description);
            } else if (item.Keyword == 'SOCIAL_NETWORKS') {
                contentSocialNetworks += ' <li><a href="' + item.Value3 + '"><i class="' + item.Value2 + '"></i></a></li>'
            } else if (item.Keyword == 'URI_FACEBOOK') {
                localStorage.setItem("uriFacebook", item.Description);
            } else if (item.Keyword == 'URI_GOOGLE') {
                localStorage.setItem("uriGoogle", item.Description);
            }
        });
        $('#divContact').append(contact);
        $('#divCopyright').append(copyRight);
        $('#ulSocialNetwork').append(contentSocialNetworks);
    }

    function fnLoadMainSlider() {

        var slidesContent = [];
        var slides = JSON.parse(localStorage.getItem("slides"));

        $.each(slides, function (i, item) {
            if ((item.keyword).includes("BANNER_PRICIPAL")) {
                var content = new Object();
                content["image"] = '../../../../' + item.value2;
                content["title"] = '../../../../' + item.description;
                slidesContent.push(content);
            }
        });

        console.log("# " + JSON.stringify(slidesContent))

        var pathHome = location.href.split('/')[4];
        console.log('--------------- ' + pathHome)
        if (typeof pathHome != "undefined") {
            if (pathHome.includes("home")) {
                $(function ($) {
                    "use strict";
                    $.supersized({

                        //Functionality
                        slideshow: 1,		//Slideshow on/off
                        autoplay: 1,		//Slideshow starts playing automatically
                        start_slide: 1,		//Start slide (0 is random)
                        random: 0,		//Randomize slide order (Ignores start slide)
                        slide_interval: 5000,	//Length between transitions
                        transition: 1, 		//0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
                        transition_speed: 500,	//Speed of transition
                        new_window: 1,		//Image links open in new window/tab
                        pause_hover: 0,		//Pause slideshow on hover
                        keyboard_nav: 0,		//Keyboard navigation on/off
                        performance: 1,		//0-Normal, 1-Hybrid speed/quality, 2-Optimizes image quality, 3-Optimizes transition speed // (Only works for Firefox/IE, not Webkit)
                        image_protect: 1,		//Disables image dragging and right click with Javascript

                        //Size & Position
                        min_width: 0,		//Min width allowed (in pixels)
                        min_height: 0,		//Min height allowed (in pixels)
                        vertical_center: 1,		//Vertically center background
                        horizontal_center: 1,		//Horizontally center background
                        fit_portrait: 1,		//Portrait images will not exceed browser height
                        fit_landscape: 0,		//Landscape images will not exceed browser width

                        //Components
                        navigation: 1,		//Slideshow controls on/off
                        thumbnail_navigation: 1,		//Thumbnail navigation
                        slide_counter: 1,		//Display slide numbers
                        slide_captions: 1,		//Slide caption (Pull from "title" in slides array)
                        slides: slidesContent
                    });
                });
            }
        }
    }

    return {
        fnLoadMainSlider: fnLoadMainSlider,
        fnGetConfiguration: fnGetConfiguration
    };

}();

home = {
    cache: {
        ip: "",
        header: "",
        genders: "",
        typeDocuments: ""
    }
};