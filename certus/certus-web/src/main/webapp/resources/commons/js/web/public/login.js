/**
 * @fileoverview .
 *
 * @autor joanhuve@gmail.com
 * @version 1.0
 */
$(document).ready(function () {


    fnLogin.fnGetMessageError();
    fnLogin.fnAddLoader();

    formCheck = $('#frmLogin');
    limpiarFormulario(formCheck);

    formCheck.formValidation({
        framework: 'bootstrap',
        err: {
            container: 'tooltip'
        },
        live: 'enabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese su usuario.'
                    },
                    emailAddress: {
                        message: 'Ingreser un email valido.'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese su contraseña.'
                    }
                }
            }
        }
    }).on('err.field.fv', function (e, data) {
        var $icon = data.element.data('fv.icon');
        var title;
        if (typeof $icon.data('bs.tooltip') === 'undefined') {
            console.log('the property is not available...');
        } else {
            title = $icon.data('bs.tooltip').getTitle();
        }

        $icon.tooltip('destroy').tooltip({
            html: true,
            placement: 'right',
            title: title,
            container: 'body'
        });
    });

    $('#btnLogin').on('click', function (e) {
        $("#cargador").fadeIn();
        $(this).prop("disabled", true);
        var bootstrapValidator = formCheck.data('formValidation');
        bootstrapValidator.validate();
        if (bootstrapValidator.isValid()) {
            document.getElementById("frmLogin").submit();
            $("#cargador").fadeOut();
        }
    });

    $('#btnRegisterFacebook').on('click', function (e) {
        $(this).prop("disabled", true);
        document.location = localStorage.getItem("uriFacebook");
    });

    $('#btnRegisterGmail').on('click', function (e) {
        $(this).prop("disabled", true);
        document.location = localStorage.getItem("uriGoogle");
    });

    $('#divResetPassword a').on('click', function (e) {
        $('#resetPassword').modal('show');
    });

    $("#spShowPwd").mouseover(function () {

        $("#password").attr('type', 'text');
    });
    $("#spShowPwd").mouseout(function () {
        $("#password").attr('type', 'password');
    });

});

var fnLogin = function () {

    function fnAddLoader() {
        var cargador = '<div id="cargador" style="display: none">';
        cargador += '<img id="cargador-imagen" src="' + $("#contextPath").val() + '/resources/commons/images/loader.svg"></img></div>';
        $divLoader = $(cargador).appendTo('body');
    }

    function fnGetMessageError() {
        var url = new URL(document.URL);
        var error = url.searchParams.get("error");
        if (error == 'true') {
            $('#divMessageError').css('display', 'block');
            $('#spMessageError').html('Usuario o contrase&ntilde;a incorrectos!')
            setTimeout(function () {
                $('#divMessageError').fadeOut("slow");
            }, 5000);
        }
    }

    return {
        fnAddLoader: fnAddLoader,
        fnGetMessageError: fnGetMessageError
    };

}();