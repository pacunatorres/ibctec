/**
 * @fileoverview .
 *
 * @autor joanhuve@gmail.com
 * @version 1.0
 */
$(document).ready(function () {
    fnResetPassword.fnAddLoader();

    formResetPassword = $('#frmResetPassword');
    limpiarFormulario(formResetPassword);

    formResetPassword.formValidation({
        framework: 'bootstrap',
        err: {
            container: 'tooltip'
        },
        live: 'enabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese su email.'
                    },
                    emailAddress: {
                        message: 'Ingreser un email valido.'
                    }
                }
            }
        }
    }).on('err.field.fv', function (e, data) {

        var $icon = data.element.data('fv.icon');
        var title;
        if (typeof $icon.data('bs.tooltip') === 'undefined') {
            console.log('the property is not available...');
        } else {
            title = $icon.data('bs.tooltip').getTitle();
        }

        $icon.tooltip('destroy').tooltip({
            html: true,
            placement: 'right',
            title: title,
            container: 'body'
        });
    });

    $('#btnResetPassword').on('click', function (e) {
        var bootstrapValidator = formResetPassword.data('formValidation');
        var btnResetPassword = $('#btnResetPassword');
        bootstrapValidator.validate();
        if (bootstrapValidator.isValid()) {
            btnResetPassword.prop("disabled", true);
            $('#resetPassword').modal('hide');
            fnResetPassword.fnResetPassword(function (callback) {
                if (callback.Body.Error === undefined) {
                    mostrarMensajeFlotante(callback.Body.Response.Message, NOTIFICACION_EXITO);
                    if (!isLogin()) {
                        fnRedirectToLogin(callback);
                    }
                } else {
                    mostrarMensajeFlotante(callback.Body.Error.Message, NOTIFICACION_WARN);
                }
                btnResetPassword.prop("disabled", false);
            });
        }
        bootstrapValidator.resetForm(true);
    });

    function fnRedirectToLogin(cb) {
        $("#cargador").fadeIn();
        setTimeout(function () {
            var urlRedirect = $("#contextPath").val() + cb.Body.Response.Url;
            console.log('redirect to: ' + urlRedirect)
            document.location = urlRedirect;
        }, 5000);
    }

    function isLogin() {
        var pathHome = location.href.split('/')[5];
        return pathHome.includes("login") ? true : false;
    }

});

var fnResetPassword = function () {

    function fnAddLoader() {
        var cargador = '<div id="cargador" style="display: none">';
        cargador += '<img id="cargador-imagen" src="' + $("#contextPath").val() + '/resources/commons/images/loader.svg"></img></div>';
        $divLoader = $(cargador).appendTo('body');
    }

    function fnResetPassword(callback) {

        var contextPath = $("#contextPath").val();
        var ip = JSON.parse(localStorage.getItem("ip"));
        var data = createHeader(RP_REMEMBER_PASSWORD, ip);

        data['Body'] = {
            'Request': {
                "User": {
                    "Email": $('#username').val()
                }
            }
        };

        var urlRequest = (contextPath + CONTEXT_PATH_PUBLIC + RP_REMEMBER_PASSWORD);
        consultarAjax("POST", urlRequest, JSON.stringify(data), contextPath, function (callbackRP) {
            callback(callbackRP);
        });

    }

    return {
        fnAddLoader: fnAddLoader,
        fnResetPassword: fnResetPassword
    };

}();

register = {
    cache: {
        ip: "",
        header: ""
    }
};