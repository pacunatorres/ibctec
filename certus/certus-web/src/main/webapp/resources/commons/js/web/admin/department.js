/**
 * @fileoverview .
 *
 * @autor joanhuve@gmail.com
 * @version 1.0
 */
$(document).ready(function () {
    var contextPath = $("#contextPath").val();

    fnDepartment.getAllDepartment(function (callbackGAI) {
        if (callbackGAI.Body.Error === undefined) {
            mostrarMensajeFlotante(callbackGAI.Body.Response.Message, NOTIFICACION_EXITO);

            fnDepartment.fnRenderTable(contextPath);
            if (callbackGAI.Body.Response.Code === '1') {
                fnDepartment.fnUpdateTable(callbackGAI.Body.Response.CostCenterConfigurations);
            } else {
                mostrarMensajeFlotante(callbackGAI.Body.Response.Message, NOTIFICACION_WARN);
            }

        } else {
            mostrarMensajeFlotante(callbackGAI.Body.Error.Message, NOTIFICACION_ERROR);
        }
    });

    formDepartment = $('#formDepartment');
    limpiarFormulario(formDepartment);

    formDepartment.formValidation({
        framework: 'bootstrap',
        live: 'enabled',
        fields: {
            level: {
                validators: {
                    notEmpty: {
                        message: 'Seleccione nivel.'
                    }
                }
            },
            department: {
                validators: {
                    notEmpty: {
                        message: 'Seleccione departamento.'
                    }
                }
            }
        }
    });

    $('#btnConfirmChangesSave').on('click', function (e) {
        if (formDepartment.data('formValidation').validate().isValid()) {
            $('#btnConfirmChangesSave').prop("disabled", true);
            $('#modalConfirmChanges').modal('show');
        }
    });

    $('#btnSave').on('click', function (e) {
        $(this).prop("disabled", true);

        fnDepartment.fnRegisterDepartment(function (callbackAC) {
            if (callbackAC.Body.Error === undefined) {

                mostrarMensajeFlotante(callbackAC.Body.Response.Message, NOTIFICACION_EXITO);

                fnDepartment.getAllDepartment(function (callbackGAI) {
                    if (callbackGAI.Body.Error === undefined) {
                        mostrarMensajeFlotante("Se actualizo la lista de departamentos.", NOTIFICACION_EXITO);
                        if (callbackGAI.Body.Response.Code === '1') {
                            fnDepartment.fnUpdateTable(callbackGAI.Body.Response.CostCenterConfigurations);
                        } else {
                            mostrarMensajeFlotante(callbackGAI.Body.Response.Message, NOTIFICACION_WARN);
                        }
                    } else {
                        mostrarMensajeFlotante(callbackGAI.Body.Error.Message, NOTIFICACION_ERROR);
                    }
                });

            } else {
                mostrarMensajeFlotante(callbackAC.Body.Error.Message, NOTIFICACION_ERROR);
            }

            $('#codeLevel').val('');
            $('#codeDepartment').val('');
            $('#modalConfirmChanges').modal('hide');
            $('#btnConfirmChangesSave').prop("disabled", false);
            $('#btnSave').prop("disabled", false);
            $('#formDepartment').data('formValidation').resetForm(true);
        })
    });

    $('#btnReset').on('click', function (e) {
        $('#btnConfirmChangesSave').prop("disabled", false);
        $('#codeLevel').val('');
        $('#codeDepartment').val('');
        formDepartment.data('formValidation').resetForm(true);
    });

    $('#level').on('change', function () {
        $('#codeLevel').val(this.value);
    });

    $('#department').on('change', function () {
        $('#codeDepartment').val(this.value);
    });


});

var fnDepartment = function () {

    function fnRegisterDepartment(callback) {
        var contextPath = $("#contextPath").val();
        var ip = JSON.parse(localStorage.getItem("ip"));
        var data = createHeader(RP_REGISTER_DEPARTMENT, ip);
        data['Body'] = {
            'Request': {
                "CostCenterConfiguration": {
                    "Code": $('#codeLevel').val(),
                    "Description": $("#level option:selected").text(),
                    "CodeSelector": $('#codeDepartment').val(),
                    "DescriptionSelector": $("#department option:selected").text()
                }
            }
        };

        consultarAjax("POST", (contextPath + CONTEXT_PATH_PUBLIC + RP_REGISTER_DEPARTMENT), JSON.stringify(data), contextPath, function (callbackAC) {
            callback(callbackAC);
        });
    }

    function getAllDepartment(callback) {
        var contextPath = $("#contextPath").val();
        var ip = JSON.parse(localStorage.getItem("ip"));
        var data = createHeader(RP_GET_ALL_DEPARTMENTS, ip);
        consultarAjax("POST", (contextPath + CONTEXT_PATH_PUBLIC + RP_GET_ALL_DEPARTMENTS), JSON.stringify(data), contextPath, function (callbackFilter) {
            callback(callbackFilter);
        });
    }

    function fnRenderTable(pageContext) {
        $('#tblDepartment').dataTable().fnDestroy();
        $('#tblDepartment').DataTable({
            "ordering": true,
            "info": false,
            "bLengthChange": false,
            "bFilter": false,
            "iDisplayLength": 8,
            "order": [[0, 'asc']],
            "columnDefs": [{
                "className": "text-center",
                "targets": 0
            }, {
                "className": "text-center",
                "targets": 1
            }, {
                "className": "text-center",
                "targets": 2
            }, {
                "className": "text-center",
                "targets": 3
            }, {
                "className": "text-center",
                "targets": 4
            }, {
                "render": function (data, type, row) {
                    var headLi = '<ul class="list-actions">';
                    var liOptions = '<li>' +
                        '<button ' +
                        'type="button" ' +
                        'class="btn-link" ' +
                        'onClick="fnDepartment.fnDeleteDepartment(' + data + ')"> ' +
                        '<i class="fa fa-trash"></i>' +
                        '</button> ' +
                        '</li>';
                    var footLi = '</ul>';
                    return headLi + liOptions + footLi;
                },
                "targets": 5
            }],
            columns: [
                {data: 'IdCostCenter', orderable: false},
                {data: 'Code', orderable: false},
                {data: 'Description', orderable: false},
                {data: 'CodeSelector', orderable: false},
                {data: 'DescriptionSelector', orderable: false},
                {data: 'IdCostCenter', orderable: false}
            ]
        });
    }

    function fnUpdateTable(list) {
        var table = $('#tblDepartment').DataTable();
        table.clear().rows.add(list).draw();
        table.on('order.dt search.dt', function () {
            table.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    }

    function fnDeleteDepartment(code) {
        var contextPath = $("#contextPath").val();
        consultarAjax("DELETE", contextPath + CONTEXT_PATH_PUBLIC + RP_DELETE_DEPARTMENT + code, '', contextPath, function (callbackDelete) {
            if (callbackDelete.Body.Error === undefined) {
                mostrarMensajeFlotante(callbackDelete.Body.Response.Message, NOTIFICACION_EXITO);

                fnDepartment.getAllDepartment(function (callbackGAC) {
                    if (callbackGAC.Body.Error === undefined) {
                        mostrarMensajeFlotante("Se actualizo la lista de departamentos.", NOTIFICACION_EXITO);

                        if (callbackGAC.Body.Response.Code === '1') {
                            fnDepartment.fnUpdateTable(callbackGAC.Body.Response.CostCenterConfigurations);
                        } else {
                            mostrarMensajeFlotante(callbackGAC.Body.Response.Message, NOTIFICACION_WARN);
                        }

                    } else {
                        mostrarMensajeFlotante(callbackGAC.Body.Error.Message, NOTIFICACION_ERROR);
                    }
                });

            } else {
                mostrarMensajeFlotante(callbackDelete.Body.Error.Message, NOTIFICACION_ERROR);
            }
        });
    }

    return {
        fnRegisterDepartment: fnRegisterDepartment,
        getAllDepartment: getAllDepartment,
        fnRenderTable: fnRenderTable,
        fnUpdateTable: fnUpdateTable,
        fnDeleteDepartment: fnDeleteDepartment
    };

}();

