/**
 * @fileoverview .
 *
 * @autor joanhuve@gmail.com
 * @version 1.0
 */
$(document).ready(function () {
    var contextPath = $("#contextPath").val();

    fnAdminAccount.fnAddLoader();
    fnAdminAccount.fnGetUserData(contextPath);


});

var fnAdminAccount = function () {

    function fnAddLoader() {
        var cargador = '<div class="cargador" id="cargador" style="display: none">';
        cargador += '<img class="cargador-imagen" id="cargador-imagen" src="' + $("#contextPath").val() + '/resources/commons/images/loader.svg"/></div>';
        $divLoader = $(cargador).appendTo('body');
    }

    function fnGetUserData(contextPath) {
        var ip = JSON.parse(localStorage.getItem("ip"));
        var data = createHeader(RP_USER_DATA, ip);
        consultarAjax("POST", (contextPath + CONTEXT_PATH_PUBLIC + RP_USER_DATA), JSON.stringify(data), contextPath, function (callbackUD) {
            if (callbackUD.Body.Error === undefined) {

                var userData = callbackUD.Body.Response.User;
                localStorage.setItem("code", JSON.stringify(userData.Code));
                $("#sidebarTitle").html(userData.Name);
                $("#userAccountMenu").html(userData.Name);
                $("#userAccountMenuDetail").html(userData.Name);
                $('#mainTitle').html("Buen d&iacute;a, " + userData.Name + " !");

                console.log("__________________")
                console.log("###" + JSON.stringify(userData))
                console.log("__________________")
            } else {
                mostrarMensajeFlotante(callbackUD.Body.Error.Message, NOTIFICACION_ERROR);
            }
        });
    }


    return {
        fnAddLoader: fnAddLoader,
        fnGetUserData: fnGetUserData
    };

}();

account = {
    cache: {
        ip: "",
        header: ""
    }
};