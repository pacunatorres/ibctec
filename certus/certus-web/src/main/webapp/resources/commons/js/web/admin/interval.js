Array.prototype.last = function () {
    return this[this.length - 1];
}
/**
 * @fileoverview .
 *
 * @autor joanhuve@gmail.com
 * @version 1.0
 */
$(document).ready(function () {
    var contextPath = $("#contextPath").val();

    var now = new Date();
    $('#startDate').datepicker({
        dateFormat: 'MM-DD-YYYY',
        startDate: now,
        autoclose: true
    }).on('changeDate', function (event) {
        $('#frmInterval').formValidation('revalidateField', 'startDate');
        var year = new Date(event.format()).getFullYear();
        var month = new Date(event.format()).getMonth();
        $('#year').val(year);
        $('#month').val(fnInterval.getMonth(month));
        var newEndDate = new Date(new Date(event.format()).getTime() + 86400000);
        $('#endDate').datepicker('setStartDate', newEndDate);
    });

    $('#endDate').datepicker({
        dateFormat: 'MM-DD-YYYY',
        startDate: now,
        autoclose: true
    }).on('changeDate', function (e) {
        $('#frmInterval').formValidation('revalidateField', 'endDate');
    })

    fnInterval.getAllIntervals(function (callbackGAI) {
        if (callbackGAI.Body.Error === undefined) {
            mostrarMensajeFlotante(callbackGAI.Body.Response.Message, NOTIFICACION_EXITO);

            fnInterval.fnRenderIntervalTable(contextPath);
            if (callbackGAI.Body.Response.Code === '1') {
                fnInterval.fnUpdateIntervalTable(callbackGAI.Body.Response.IntervalConfigurations);
            } else {
                mostrarMensajeFlotante(callbackGAI.Body.Response.Message, NOTIFICACION_WARN);
            }

        } else {
            mostrarMensajeFlotante(callbackGAI.Body.Error.Message, NOTIFICACION_ERROR);
        }
    });

    formInterval = $('#frmInterval');
    limpiarFormulario(formInterval);

    formInterval.formValidation({
        framework: 'bootstrap',
        live: 'enabled',
        fields: {
            startDate: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese fecha de inicio.'
                    },
                    date: {
                        format: 'MM-DD-YYYY'
                    },
                    callback: {
                        callback: function (value) {
                            var date = value;
                            var isInvalidDate = fnInterval.isInvalidDate(date);
                            console.log(isInvalidDate);
                            if (isInvalidDate) {
                                return {
                                    valid: false,
                                    message: 'Ya existe un periodo con la fecha seleccionada.'
                                }
                            } else {
                                return true;
                            }
                        }
                    }
                }
            },
            endDate: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese fecha de fin.'
                    },
                    date: {
                        format: 'MM-DD-YYYY'
                    },
                    callback: {
                        callback: function (value) {
                            var date = value;
                            var isInvalidDate = fnInterval.isInvalidDate(date);
                            console.log(isInvalidDate);
                            if (isInvalidDate) {
                                return {
                                    valid: false,
                                    message: 'Ya existe un periodo con la fecha seleccionada.'
                                }
                            } else {
                                return true;
                            }
                        }
                    }
                }
            }
        }
    });

    $('#btnConfirmChangesSave').on('click', function (e) {
        if (formInterval.data('formValidation').validate().isValid()) {
            $('#btnConfirmChangesSave').prop("disabled", true);
            $('#modalConfirmChanges').modal('show');
        }
    });

    $('#btnSave').on('click', function (e) {
        $(this).prop("disabled", true);

        fnInterval.fnRegisterInterval(function (callbackAC) {
            if (callbackAC.Body.Error === undefined) {

                mostrarMensajeFlotante(callbackAC.Body.Response.Message, NOTIFICACION_EXITO);

                fnInterval.getAllIntervals(function (callbackGAI) {
                    if (callbackGAI.Body.Error === undefined) {
                        mostrarMensajeFlotante("Se actualizo la lista de periodos.", NOTIFICACION_EXITO);

                        if (callbackGAI.Body.Response.Code === '1') {
                            fnInterval.fnUpdateIntervalTable(callbackGAI.Body.Response.IntervalConfigurations);
                        } else {
                            mostrarMensajeFlotante(callbackGAI.Body.Response.Message, NOTIFICACION_WARN);
                        }

                    } else {
                        mostrarMensajeFlotante(callbackGAI.Body.Error.Message, NOTIFICACION_ERROR);
                    }
                });

            } else {
                mostrarMensajeFlotante(callbackAC.Body.Error.Message, NOTIFICACION_ERROR);
            }

            $('#year').val('');
            $('#month').val('');
            $('#modalConfirmChanges').modal('hide');
            $('#btnConfirmChangesSave').prop("disabled", false);
            $('#btnSave').prop("disabled", false);
            $('#frmInterval').data('formValidation').resetForm(true);
        })
    });

    $('#btnReset').on('click', function (e) {
        $('#btnConfirmChangesSave').prop("disabled", false);
        $('#year').val('');
        $('#month').val('');
        $('#startDate').datepicker("update", now);
        var bootstrapValidator = formInterval.data('formValidation');
        bootstrapValidator.resetForm(true);
    });

});

var fnInterval = function () {

    function getMonth(month) {
        const monthNames = ["ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO",
            "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE"];
        return monthNames[month];
    }

    function fnRegisterInterval(callback) {
        var contextPath = $("#contextPath").val();
        var ip = JSON.parse(localStorage.getItem("ip"));
        var data = createHeader(RP_REGISTER_INTERVAL, ip);
        data['Body'] = {
            'Request': {
                "IntervalConfiguration": {
                    "Year": $('#year').val(),
                    "Month": $('#month').val(),
                    "StartDate": $('#startDate').val(),
                    "EndDate": $('#endDate').val()
                }
            }
        };

        consultarAjax("POST", (contextPath + CONTEXT_PATH_PUBLIC + RP_REGISTER_INTERVAL), JSON.stringify(data), contextPath, function (callbackAC) {
            callback(callbackAC);
        });
    }

    function getAllIntervals(callback) {
        var contextPath = $("#contextPath").val();
        var ip = JSON.parse(localStorage.getItem("ip"));
        var data = createHeader(RP_GET_ALL_INTERVAL, ip);
        consultarAjax("POST", (contextPath + CONTEXT_PATH_PUBLIC + RP_GET_ALL_INTERVAL), JSON.stringify(data), contextPath, function (callbackFilter) {
            var list = callbackFilter.Body.Response.IntervalConfigurations;
            if (0 < list.length) {
                saveInterval(list);
            }
            callback(callbackFilter);
        });
    }

    function saveInterval(intervalList) {
        interval.intervalList = intervalList;
    }

    function isInvalidDate(date) {
        var arrayDate = (date).split("-");
        var dateMilliseconds = new Date(arrayDate[2], parseInt(arrayDate[0]) - 1, arrayDate[1]).getTime();

        var list = interval.intervalList;
        var dates = '';
        for (i = 0; i < list.length; i++) {
            var arrayStartDate = (list[i].StartDate).split("-");
            var startDateMilliseconds = Number(new Date(arrayStartDate[2], parseInt(arrayStartDate[0]) - 1, arrayStartDate[1]).getTime());

            var arrayEndDate = (list[i].EndDate).split("-");
            var endDateMilliseconds = Number(new Date(arrayEndDate[2], parseInt(arrayEndDate[0]) - 1, arrayEndDate[1]).getTime());

            var arrayDate = (date).split("-");
            var dateMilliseconds = Number(new Date(arrayDate[2], parseInt(arrayDate[0]) - 1, arrayDate[1]).getTime());

            if ((dateMilliseconds <= endDateMilliseconds) == (dateMilliseconds >= startDateMilliseconds)) {
                dates += '1';
            } else {
                dates += '0';
            }
        }
        return dates.indexOf('1') >= 0 ? true : false;
    }


    function fnRenderIntervalTable(pageContext) {
        $('#tblInterval').dataTable().fnDestroy();
        $('#tblInterval').DataTable({
            "ordering": true,
            "info": false,
            "bLengthChange": false,
            "bFilter": false,
            "iDisplayLength": 8,
            "order": [[0, 'asc']],
            "columnDefs": [{
                "className": "text-center",
                "targets": 0
            }, {
                "className": "text-center",
                "targets": 1
            }, {
                "className": "text-center",
                "targets": 2
            }, {
                "className": "text-center",
                "targets": 3
            }, {
                "className": "text-center",
                "targets": 4
            }, {
                "render": function (data, type, row) {
                    return '<span style="background-color: #00205B!important;" class="label label-default">' + row.DescriptionStatus + '</span>';
                },
                "className": "text-center",
                "targets": 5
            }, {
                "render": function (data, type, row) {
                    var headLi = '<ul class="list-actions">';
                    var liOptions = '<li>' +
                        '<button ' +
                        'type="button" ' +
                        'class="btn-link" ' +
                        'onClick="fnInterval.fnDeleteInterval(' + data + ')"> ' +
                        '<i class="fa fa-trash"></i>' +
                        '</button> ' +
                        '</li>';
                    var footLi = '</ul>';
                    return headLi + liOptions + footLi;
                },
                "targets": 6
            }],
            columns: [
                {data: 'Code', orderable: false},
                {data: 'Year', orderable: false},
                {data: 'Month', orderable: false},
                {data: 'StartDate', orderable: false},
                {data: 'EndDate', orderable: false},
                {data: 'DescriptionStatus', orderable: false},
                {data: 'Code', orderable: false}
            ]
        });
    }

    function fnUpdateIntervalTable(list) {
        var table = $('#tblInterval').DataTable();
        table.clear().rows.add(list).draw();
        table.on('order.dt search.dt', function () {
            table.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    }

    function fnDeleteInterval(code) {
        var contextPath = $("#contextPath").val();
        consultarAjax("DELETE", contextPath + CONTEXT_PATH_PUBLIC + RP_DELETE_INTERVAL + code, '', contextPath, function (callbackDelete) {
            if (callbackDelete.Body.Error === undefined) {
                mostrarMensajeFlotante(callbackDelete.Body.Response.Message, NOTIFICACION_EXITO);

                fnInterval.getAllIntervals(function (callbackGAC) {
                    if (callbackGAC.Body.Error === undefined) {
                        mostrarMensajeFlotante("Se actualizo la lista de periodos.", NOTIFICACION_EXITO);

                        if (callbackGAC.Body.Response.Code === '1') {
                            fnInterval.fnUpdateIntervalTable(callbackGAC.Body.Response.IntervalConfigurations);
                        } else {
                            mostrarMensajeFlotante(callbackGAC.Body.Response.Message, NOTIFICACION_WARN);
                        }

                    } else {
                        mostrarMensajeFlotante(callbackGAC.Body.Error.Message, NOTIFICACION_ERROR);
                    }
                });

            } else {
                mostrarMensajeFlotante(callbackDelete.Body.Error.Message, NOTIFICACION_ERROR);
            }
        });
    }

    return {
        getMonth: getMonth,
        fnRegisterInterval: fnRegisterInterval,
        getAllIntervals: getAllIntervals,
        fnRenderIntervalTable: fnRenderIntervalTable,
        fnUpdateIntervalTable: fnUpdateIntervalTable,
        fnDeleteInterval: fnDeleteInterval,
        isInvalidDate: isInvalidDate
    };

}();

interval = {
    intervalList: "",
    startDateMillisecond: "",
    endDateMillisecond: ""
};