/**
 * @fileoverview Librerias de funciones para pagina medicion.
 *
 * @autor joanhuve@gmail.com 2018
 * @version 1.0
 */
$(document).ready(function () {

    $('#departureDate').datepicker({
        dateFormat: 'mm-dd-yy',
        minDate: new Date(),
        maxDate: "+3M",
        onSelect: function (date) {
            var selectedDate = new Date(date);
            var msecsInADay = 86400000;
            var endDate = new Date(selectedDate.getTime() + msecsInADay);
            //Set Minimum Date of EndDatePicker After Selected Date of StartDatePicker
            $("#returnDate").datepicker("option", "minDate", endDate);
            $("#returnDate").datepicker("option", "maxDate", '+3M"');

        }
    })

    $('#returnDate').datepicker({
        dateFormat: 'mm-dd-yy'
    })


    $('#selLeavingFrom').on('change', function () {
        if ($(this).val() != '') {
            construirSelect("selLeavingTo", JSON.parse(localStorage.getItem("leavingToValues")));
            var value = $(this).val();
            $("#selLeavingTo option").each(function () {
                if ($(this).val() != '') {
                    if (value == $(this).val()) {
                        $('#selLeavingTo option[value=' + $(this).val() + ']').remove();
                    }
                }
            });
        }
    })

    formSearchPasajon = $('#frmSearchPasajon');
    limpiarFormulario(formSearchPasajon);

    formSearchPasajon.formValidation({
        framework: 'bootstrap',
        err: {
            container: 'tooltip'
        },
        live: 'enabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            selLeavingFrom: {
                validators: {
                    notEmpty: {
                        message: 'Seleccione su ciudad de origen.'
                    }
                }
            },
            selLeavingTo: {
                validators: {
                    notEmpty: {
                        message: 'Seleccione su ciudad de destino.'
                    }
                }
            },
            departureDate: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese la fecha de salida.'
                    },
                    date: {
                        format: 'DD-MM-YYYY'
                    }
                }
            }
        }
    });

    $('#btnSearch').on('click', function (e) {
        var bootstrapValidator = formSearchPasajon.data('formValidation');
        bootstrapValidator.validate();
        if (bootstrapValidator.isValid()) {
            alert("no se encontraron pasajes");
        }
    });

});

var fnSearch = function () {

    return {};

}();
