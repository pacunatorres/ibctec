/**
 * @fileoverview .
 *
 * @autor joanhuve@gmail.com
 * @version 1.0
 */
$(document).ready(function () {
    var contextPath = $("#contextPath").val();

    fnSiteConfiguration.getAllSites(function (callbackGAC) {
        if (callbackGAC.Body.Error === undefined) {
            mostrarMensajeFlotante(callbackGAC.Body.Response.Message, NOTIFICACION_EXITO);

            fnSiteConfiguration.fnRenderSiteTable(contextPath);
            if (callbackGAC.Body.Response.Code === '1') {
                fnSiteConfiguration.fnUpdateSiteTable(callbackGAC.Body.Response.AcademicConfigurations);
            } else {
                mostrarMensajeFlotante(callbackGAC.Body.Response.Message, NOTIFICACION_WARN);
            }

        } else {
            mostrarMensajeFlotante(callbackGAC.Body.Error.Message, NOTIFICACION_ERROR);
        }
    });

    formSiteConfiguration = $('#formSiteConfiguration');
    limpiarFormulario(formSiteConfiguration);
    formSiteConfiguration.formValidation({
        framework: 'bootstrap',
        live: 'enabled',
        fields: {
            campus: {
                validators: {
                    notEmpty: {
                        message: 'Seleccione el campus.'
                    }
                }
            },
            building: {
                validators: {
                    notEmpty: {
                        message: 'Seleccione edificio.'
                    }
                }
            },
            classroom: {
                validators: {
                    notEmpty: {
                        message: 'Seleccione aula.'
                    }
                }
            }
        }
    });

    $('#btnConfirmChangesSave').on('click', function (e) {
        if (formSiteConfiguration.data('formValidation').validate().isValid()) {
            $('#btnConfirmChangesSave').prop("disabled", true);
            $('#modalConfirmChanges').modal('show');
        }
    });

    $('#btnSave').on('click', function (e) {
        $(this).prop("disabled", true);

        fnSiteConfiguration.fnRegisterSiteConfiguration(function (callbackAC) {
            if (callbackAC.Body.Error === undefined) {

                mostrarMensajeFlotante(callbackAC.Body.Response.Message, NOTIFICACION_EXITO);

                fnSiteConfiguration.getAllSites(function (callbackGAC) {
                    if (callbackGAC.Body.Error === undefined) {
                        mostrarMensajeFlotante("Se actualizo la lista de sedes.", NOTIFICACION_EXITO);

                        if (callbackGAC.Body.Response.Code === '1') {
                            fnSiteConfiguration.fnUpdateSiteTable(callbackGAC.Body.Response.AcademicConfigurations);
                        } else {
                            mostrarMensajeFlotante(callbackGAC.Body.Response.Message, NOTIFICACION_WARN);
                        }

                    } else {
                        mostrarMensajeFlotante(callbackGAC.Body.Error.Message, NOTIFICACION_ERROR);
                    }
                });

            } else {
                mostrarMensajeFlotante(callbackAC.Body.Error.Message, NOTIFICACION_ERROR);
            }
            $('#modalConfirmChanges').modal('hide');
            $('#btnConfirmChangesSave').prop("disabled", false);
            $('#btnSave').prop("disabled", false);
            $('#formSiteConfiguration').data('formValidation').resetForm(true);
        })
    });


    $('#btnReset').on('click', function (e) {
        $('#btnConfirmChangesSave').prop("disabled", false);
        var bootstrapValidator = formSiteConfiguration.data('formValidation');
        bootstrapValidator.resetForm(true);
    });

});

var fnSiteConfiguration = function () {

    function fnRegisterSiteConfiguration(callback) {

        var contextPath = $("#contextPath").val();
        var ip = JSON.parse(localStorage.getItem("ip"));
        var data = createHeader(RP_REGISTER_SITE, ip);
        data['Body'] = {
            'Request': {
                "AcademicConfiguration": {
                    "Campus": $('#campus').val(),
                    "Building": $('#building').val(),
                    "Classroom": $('#classroom').val()
                }
            }
        };

        consultarAjax("POST", (contextPath + CONTEXT_PATH_PUBLIC + RP_REGISTER_SITE), JSON.stringify(data), contextPath, function (callbackAC) {
            callback(callbackAC);
        });

    }

    function getAllSites(callback) {
        var contextPath = $("#contextPath").val();
        var ip = JSON.parse(localStorage.getItem("ip"));
        var data = createHeader(RP_GET_ALL_SITE, ip);
        consultarAjax("POST", (contextPath + CONTEXT_PATH_PUBLIC + RP_GET_ALL_SITE), JSON.stringify(data), contextPath, function (callbackFilter) {
            callback(callbackFilter);
        });
    }

    function fnRenderSiteTable(pageContext) {
        $('#tblSite').dataTable().fnDestroy();
        $('#tblSite').DataTable({
            "ordering": true,
            "info": false,
            "bLengthChange": false,
            "bFilter": false,
            "iDisplayLength": 8,
            "order": [[0, 'asc']],
            "columnDefs": [{
                "className": "text-center",
                "targets": 0
            }, {
                "className": "text-center",
                "targets": 1
            }, {
                "className": "text-center",
                "targets": 2
            }, {
                "className": "text-center",
                "targets": 3
            }, {
                "className": "text-center",
                "targets": 4
            }, {
                "render": function (data, type, row) {
                    var headLi = '<ul class="list-actions">';
                    var liOptions = '<li>' +
                        '<button ' +
                        'type="button" ' +
                        'class="btn-link" ' +
                        'onClick="fnSiteConfiguration.fnDeleteSite(' + data + ')"> ' +
                        '<i class="fa fa-trash"></i>' +
                        '</button> ' +
                        '</li>';
                    var footLi = '</ul>';
                    return headLi + liOptions + footLi;
                },
                "targets": 5
            }],
            columns: [
                {data: 'Code', orderable: false},
                {data: 'Campus', orderable: false},
                {data: 'Building', orderable: false},
                {data: 'Classroom', orderable: false},
                {data: 'IpAddress', orderable: false},
                {data: 'Code', orderable: false}
            ]
        });
    }

    function fnUpdateSiteTable(list) {
        var table = $('#tblSite').DataTable();
        table.clear().rows.add(list).draw();
        table.on('order.dt search.dt', function () {
            table.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    }

    function fnDeleteSite(code) {
        var contextPath = $("#contextPath").val();
        consultarAjax("DELETE", contextPath + CONTEXT_PATH_PUBLIC + RP_DELETE_SITE + code, '', contextPath, function (callbackDelete) {
            if (callbackDelete.Body.Error === undefined) {
                mostrarMensajeFlotante(callbackDelete.Body.Response.Message, NOTIFICACION_EXITO);

                fnSiteConfiguration.getAllSites(function (callbackGAC) {
                    if (callbackGAC.Body.Error === undefined) {
                        mostrarMensajeFlotante("Se actualizo la lista de sedes.", NOTIFICACION_EXITO);

                        if (callbackGAC.Body.Response.Code === '1') {
                            fnSiteConfiguration.fnUpdateSiteTable(callbackGAC.Body.Response.AcademicConfigurations);
                        } else {
                            mostrarMensajeFlotante(callbackGAC.Body.Response.Message, NOTIFICACION_WARN);
                        }

                    } else {
                        mostrarMensajeFlotante(callbackGAC.Body.Error.Message, NOTIFICACION_ERROR);
                    }
                });

            } else {
                mostrarMensajeFlotante(callbackDelete.Body.Error.Message, NOTIFICACION_ERROR);
            }
        });
    }

    return {
        fnRegisterSiteConfiguration: fnRegisterSiteConfiguration,
        getAllSites: getAllSites,
        fnRenderSiteTable: fnRenderSiteTable,
        fnUpdateSiteTable: fnUpdateSiteTable,
        fnDeleteSite: fnDeleteSite
    };

}();

