/**
 * @fileoverview .
 *
 * @autor joanhuve@gmail.com
 * @version 1.0
 */
$(document).ready(function () {
    var contextPath = $("#contextPath").val();

    // TODO pendiente de implementar para el selector de niveles
    // fnAcademicConfiguration.fnLoadSelectors();

    fnAcademicConfiguration.getAllAcademicConfigurations(function (callbackGAC) {
        if (callbackGAC.Body.Error === undefined) {
            mostrarMensajeFlotante(callbackGAC.Body.Response.Message, NOTIFICACION_EXITO);

            fnAcademicConfiguration.fnRenderAcaConfTable(contextPath);
            if (callbackGAC.Body.Response.Code === '1') {
                fnAcademicConfiguration.fnUpdateAcaConfTable(callbackGAC.Body.Response.AcademicConfigurations);
            } else {
                mostrarMensajeFlotante(callbackGAC.Body.Response.Message, NOTIFICACION_WARN);
            }

        } else {
            mostrarMensajeFlotante(callbackGAC.Body.Error.Message, NOTIFICACION_ERROR);
        }
    })

    formAcademicConfiguration = $('#formAcademicConfiguration');
    limpiarFormulario(formAcademicConfiguration);

    formAcademicConfiguration.formValidation({
        framework: 'bootstrap',
        live: 'enabled',
        fields: {
            level: {
                validators: {
                    notEmpty: {
                        message: 'Seleccione un nivel.'
                    }
                }
            },
            incomeTolerance: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese un valor.'
                    },
                    digits: {
                        message: 'Solo debe contener digitos.'
                    }
                }
            },
            outcomeTolerance: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese un valor.'
                    },
                    digits: {
                        message: 'Solo debe contener digitos.'
                    }
                }
            },
            before: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese un valor.'
                    },
                    digits: {
                        message: 'Solo debe contener digitos.'
                    }
                }
            },
            after: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese un valor.'
                    },
                    digits: {
                        message: 'Solo debe contener digitos.'
                    }
                }
            }
        }
    });

    $('#btnConfirmChangesSave').on('click', function (e) {
        if (formAcademicConfiguration.data('formValidation').validate().isValid()) {
            $('#btnConfirmChangesSave').prop("disabled", true);
            $('#modalConfirmChanges').modal('show');
        }
    });

    $('#btnSave').on('click', function (e) {
        $(this).prop("disabled", true);

        fnAcademicConfiguration.fnRegisterAcademicConfiguration(function (callbackAC) {
            if (callbackAC.Body.Error === undefined) {

                mostrarMensajeFlotante(callbackAC.Body.Response.Message, NOTIFICACION_EXITO);

                fnAcademicConfiguration.getAllAcademicConfigurations(function (callbackGAC) {
                    if (callbackGAC.Body.Error === undefined) {
                        mostrarMensajeFlotante("Se actualizo la lista de configuraci&oacute;n.", NOTIFICACION_EXITO);

                        if (callbackGAC.Body.Response.Code === '1') {
                            var academicConfList = callbackGAC.Body.Response.AcademicConfigurations;
                            fnAcademicConfiguration.fnUpdateAcaConfTable(academicConfList);
                        } else {
                            mostrarMensajeFlotante(callbackGAC.Body.Response.Message, NOTIFICACION_WARN);
                        }

                    } else {
                        mostrarMensajeFlotante(callbackGAC.Body.Error.Message, NOTIFICACION_ERROR);
                    }
                });

            } else {
                mostrarMensajeFlotante(callbackAC.Body.Error.Message, NOTIFICACION_ERROR);
            }
            $('#ipMarking').prop('checked', false);
            $('#accounting').prop('checked', false);
            $('#modalConfirmChanges').modal('hide');
            $('#btnConfirmChangesSave').prop("disabled", false);
            $('#btnSave').prop("disabled", false);
            $('#formAcademicConfiguration').data('formValidation').resetForm(true);
        })
    });

    $('#btnReset').on('click', function (e) {
        $('#btnConfirmChangesSave').prop("disabled", false);
        var bootstrapValidator = formAcademicConfiguration.data('formValidation');
        bootstrapValidator.resetForm(true);
        $('#ipMarking').prop('checked', false);
        $('#accounting').prop('checked', false);
    });

});

var fnAcademicConfiguration = function () {

    function fnRegisterAcademicConfiguration(callback) {

        var contextPath = $("#contextPath").val();
        var ip = JSON.parse(localStorage.getItem("ip"));
        var data = createHeader(RP_REGISTER_ACADEMIC_CONFIGURATION, ip);
        data['Body'] = {
            'Request': {
                "AcademicConfiguration": {
                    "Level": $('#level').val(),
                    "IpMarking": $('#ipMarking').is(":checked") ? '1' : '0',
                    "Accounting": $('#accounting').is(":checked") ? '1' : '0',
                    "IncomeToleranceInMinits": $('#incomeTolerance').val(),
                    "OutcomeToleranceInMinits": $('#outcomeTolerance').val(),
                    "BeforeTimeInMinits": $('#before').val(),
                    "AfterTimeInMinits": $('#after').val()
                }
            }
        };

        consultarAjax("POST", (contextPath + CONTEXT_PATH_PUBLIC + RP_REGISTER_ACADEMIC_CONFIGURATION), JSON.stringify(data), contextPath, function (callbackAC) {
            callback(callbackAC);
        });

    }

    function getAllAcademicConfigurations(callback) {
        var contextPath = $("#contextPath").val();
        var ip = JSON.parse(localStorage.getItem("ip"));
        var data = createHeader(RP_GET_ALL_ACADEMIC_CONFIGURATION, ip);
        consultarAjax("POST", (contextPath + CONTEXT_PATH_PUBLIC + RP_GET_ALL_ACADEMIC_CONFIGURATION), JSON.stringify(data), contextPath, function (callbackFilter) {
            callback(callbackFilter);
        });
    }

    function fnRenderAcaConfTable(pageContext) {
        $('#tblAcademicConfiguration').dataTable().fnDestroy();
        $('#tblAcademicConfiguration').DataTable({
            "ordering": true,
            "info": false,
            "bLengthChange": false,
            "bFilter": false,
            "iDisplayLength": 8,
            "order": [[0, 'asc']],
            "columnDefs": [{
                "className": "text-center",
                "targets": 0
            }, {
                "render": function (data, type, row) {
                    var headLi = '<ul class="list-actions center-ul-datatable">';
                    var liOptions;
                    if (row.IpMarking === '1') {
                        liOptions = '<li>' +
                            '<button type="button" class="btn-link"> <i class="fa fa-check-square-o"></i>' +
                            '</button> ' +
                            '</li>';
                    } else {
                        liOptions = '<li>' +
                            '<button type="button" class="btn-link"> <i class="fa fa-square-o"></i>' +
                            '</button> ' +
                            '</li>';
                    }

                    var footLi = '</ul>';
                    return headLi + liOptions + footLi;
                },
                "targets": 2
            }, {
                "render": function (data, type, row) {
                    var headLi = '<ul class="list-actions center-ul-datatable">';
                    var liOptions;
                    if (row.Accounting === '1') {
                        liOptions = '<li>' +
                            '<button type="button" class="btn-link"> <i class="fa fa-check-square-o"></i>' +
                            '</button> ' +
                            '</li>';
                    } else {
                        liOptions = '<li>' +
                            '<button type="button" class="btn-link"> <i class="fa fa-square-o"></i>' +
                            '</button> ' +
                            '</li>';
                    }

                    var footLi = '</ul>';
                    return headLi + liOptions + footLi;
                },
                "targets": 3
            }, {
                "render": function (data, type, row) {
                    return row.IncomeToleranceInMinits + ' MIN';
                },
                "className": "text-center",
                "targets": 4
            }, {
                "render": function (data, type, row) {
                    return row.OutcomeToleranceInMinits + ' MIN';
                },
                "className": "text-center",
                "targets": 5
            }, {
                "render": function (data, type, row) {
                    return row.BeforeTimeInMinits + ' MIN';
                },
                "className": "text-center",
                "targets": 6
            }, {
                "render": function (data, type, row) {
                    return row.AfterTimeInMinits + ' MIN';
                },
                "className": "text-center",
                "targets": 7
            }, {
                "render": function (data, type, row) {
                    var headLi = '<ul class="list-actions">';
                    var liOptions = '<li>' +
                        '<button ' +
                        'type="button" ' +
                        'class="btn-link" ' +
                        'onClick="fnAcademicConfiguration.fnDeleteAcademicConfiguration(' + data + ')"> ' +
                        '<i class="fa fa-trash"></i>' +
                        '</button> ' +
                        '</li>';
                    var footLi = '</ul>';
                    return headLi + liOptions + footLi;
                },
                "targets": 8
            }],
            columns: [
                {data: 'Code', orderable: false},
                {data: 'Level', orderable: false},
                {data: 'IpMarking', orderable: false},
                {data: 'Accounting', orderable: false},
                {data: 'IncomeToleranceInMinits', orderable: false},
                {data: 'OutcomeToleranceInMinits', orderable: false},
                {data: 'BeforeTimeInMinits', orderable: false},
                {data: 'AfterTimeInMinits', orderable: false},
                {data: 'Code', orderable: false}
            ]
        });
    }

    function fnUpdateAcaConfTable(academicConfList) {
        var table = $('#tblAcademicConfiguration').DataTable();
        table.clear().rows.add(academicConfList).draw();
        table.on('order.dt search.dt', function () {
            table.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    }

    function fnDeleteAcademicConfiguration(code) {
        var contextPath = $("#contextPath").val();
        consultarAjax("DELETE", contextPath + CONTEXT_PATH_PUBLIC + RP_DELETE_ACADEMIC_CONFIGURATION + code, '', contextPath, function (callbackDelete) {
            if (callbackDelete.Body.Error === undefined) {
                mostrarMensajeFlotante(callbackDelete.Body.Response.Message, NOTIFICACION_EXITO);

                fnAcademicConfiguration.getAllAcademicConfigurations(function (callbackGAC) {
                    if (callbackGAC.Body.Error === undefined) {
                        mostrarMensajeFlotante("Se actualizo la lista de configuraci&oacute;n.", NOTIFICACION_EXITO);

                        if (callbackGAC.Body.Response.Code === '1') {
                            var academicConfList = callbackGAC.Body.Response.AcademicConfigurations;
                            fnAcademicConfiguration.fnUpdateAcaConfTable(academicConfList);
                        } else {
                            mostrarMensajeFlotante(callbackGAC.Body.Response.Message, NOTIFICACION_WARN);
                        }

                    } else {
                        mostrarMensajeFlotante(callbackGAC.Body.Error.Message, NOTIFICACION_ERROR);
                    }
                });

            } else {
                mostrarMensajeFlotante(callbackDelete.Body.Error.Message, NOTIFICACION_ERROR);
            }
        });
    }

    return {
        fnRegisterAcademicConfiguration: fnRegisterAcademicConfiguration,
        getAllAcademicConfigurations: getAllAcademicConfigurations,
        fnDeleteAcademicConfiguration: fnDeleteAcademicConfiguration,
        fnRenderAcaConfTable: fnRenderAcaConfTable,
        fnUpdateAcaConfTable: fnUpdateAcaConfTable
    };

}();

