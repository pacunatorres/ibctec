/**
 * @fileoverview .
 *
 * @autor joanhuve@gmail.com
 * @version 1.0
 */
$(document).ready(function () {
    var contextPath = $("#contextPath").val();
    fnConfiguration.fnGetConfiguration();
    fnConfiguration.fnGetMainMenu(contextPath);

});

var fnConfiguration = function () {

    function fnGetConfiguration() {
        $("#cargador").fadeIn();
        var contextPath = $("#contextPath").val();
        $.getJSON("https://api.ipify.org?format=json", function (data) {
            var header = createHeader(RP_GET_CONFIGURATIONS, data.ip);
            localStorage.setItem("ip", JSON.stringify(data.ip));
            // console.log("#header: " + JSON.stringify(header));
            consultarAjax("POST", (contextPath + CONTEXT_PATH_PUBLIC + RP_GET_CONFIGURATIONS) + '?origin=true', JSON.stringify(header), contextPath, function (callbackGC) {
                // TODO REMOVE
                console.log("_______CONFIGURATIONS___________")
                console.log("###" + JSON.stringify(callbackGC))
                console.log("_______CONFIGURATIONS___________")

                fnFillConfigurations(callbackGC.Body.Response.Configurations);
                localStorage.setItem("genders", JSON.stringify(callbackGC.Body.Response.Genders));
                localStorage.setItem("typeDocuments", JSON.stringify(callbackGC.Body.Response.DocumentTypes));
                localStorage.setItem("profiles", JSON.stringify(callbackGC.Body.Response.Profiles));

                $("#cargador").fadeOut();
            });
        });
    }

    function fnFillConfigurations(configurations) {
        var contentAbout = '';
        var contentSocialNetworks = '';
        var contentPaymentMethods = '';
        var copyRight = '';
        $.each(configurations, function (i, item) {
            if (item.Keyword == 'LIMIT_AGE') {
                localStorage.setItem("limitAge", item.Description);
            }
        });
    }

    function fnGetMainMenu(contextPath) {
        var ip = JSON.parse(localStorage.getItem("ip"));
        var data = createHeader(RP_DASHBOARD_MENU, ip);
        consultarAjax("POST", (contextPath + CONTEXT_PATH_PUBLIC + RP_DASHBOARD_MENU), JSON.stringify(data), contextPath, function (callbackUD) {
            if (callbackUD.Body.Error === undefined) {

                var menu = callbackUD.Body.Response.MenuList;

                // TODO REMOVE
                console.log("_______MENU___________")
                console.log("###" + JSON.stringify(menu))
                console.log("_______MENU___________")

                var sidebarMenu = '<li class="menu-header">Menu</li>';
                $.each(menu, function (index, value) {

                    var icon = '';
                    switch (value.OptionCode) {
                        case '2':
                            icon = 'fa fa-cogs';
                            break;
                        case "3":
                            icon = 'fa fa-calendar-check-o';
                            break;
                        case "4":
                            icon = 'fa fa-building';
                            break;
                        case "5":
                            icon = 'fa fa-hand-pointer-o';
                            break;
                        default:
                            icon = 'fa fa-home';
                            break;
                    }

                    sidebarMenu += '<li><a href="' + value.Url + '"><span class="' + icon + ' rs-icon-menu"></span>' + value.Description + '</a>';

                    if (value.SubMenu != null && value.SubMenu != '') {
                        sidebarMenu += '<ul>';
                        $.each(value.SubMenu, function (index, value) {
                            sidebarMenu += '<li><a href="' + value.Url + '">' + value.Description + '</a></li>';
                        });
                        sidebarMenu += '</ul>';
                    }
                    sidebarMenu += '</li>';
                });

                $('.default-sidebar-nav').append(sidebarMenu);

                // Sidebar multilevel click function menu
                // =============================================
                $('.default-sidebar-nav a').navSidebarMultiLevel();
                // Disable link if has chhildrens
                $(".default-sidebar-nav li:has(ul)").hover(function () {
                    $(this).children("a").click(function () {
                        return false;
                    });
                });

                // Sidebar toggle on mobile < 768
                // ===============================
                $('#rs-sidebar-toggle-mobile').showSidebar({
                    wrapper: '#rs-wrapper',
                    screenOverlay: '#rs-screen-overlay',
                    absoluteFooter: '.absolute-footer',
                });
                $('#rs-screen-overlay').hideSidebar({
                    wrapper: '#rs-wrapper',
                    screenOverlay: '#rs-screen-overlay',
                    absoluteFooter: '.absolute-footer',
                });

                var url = window.location.href;
                $('.default-sidebar-nav li a').each(function () {
                    if (url.includes($(this).attr('href'))) {
                        $(this).parents('li').addClass('active selected');
                    }
                });

            } else {
                mostrarMensajeFlotante(callbackUD.Body.Error.Message, NOTIFICACION_ERROR);
            }
        });
    }


    return {
        fnGetMainMenu: fnGetMainMenu,
        fnGetConfiguration: fnGetConfiguration
    };

}();

account = {
    cache: {
        ip: "",
        header: ""
    }
};