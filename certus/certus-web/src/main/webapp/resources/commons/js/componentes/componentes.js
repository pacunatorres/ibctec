/**
 * @fileoverview Libreria de componentes Javascript.
 *
 * @autor joanhuve@gmail.com
 * @version 1.0 2017
 */

/**
 * --------------------------------------------------------------------------
 * CONSTANTES
 * --------------------------------------------------------------------------
 */

/** @constant {number} */
var MOSTRAR = 1;

/** @constant {number} */
var OCULTAR = 0;

/** @constant {number} */
var ERROR = 0;

/** @constant {number} */
var INFO = 1;

/** @constant {number} */
var ENABLED = 1;

/** @constant {number} */
var DISABLED = 0;

/** @constant {number} */
var TIEMPO_VALI = 5000;

/** @constant {number} */
var TIEMPO_INFO = 15000;

/** @constant {number} */
var TIEMPO_INFO_02 = 1000;

/** @constant {string} */
var ESTADO_PROV_ENVIADO_BANCO = "01103";

/** @constant {string} */
var ESTADO_BANCO_PRESELECCIONADO = "0";// "01202";

/** @constant {number} */
var TOTAL_ROWS_POR_PAGINA = 10;

/** @constant {string} */
var PER_NATURAL = "02101";

/** @constant {string} */
var PER_JURIDICA = "02102";

/** @constant {string} */
var USD = "01401";

/** @constant {string} */
var PEN = "01402";

/**
 * Variables Globales
 */
var paginaActualMisReferidos = 0;
var paginaActualReferidosPorVendedor = 0;

/**
 * -- CONSTANTS
 */
var CONTEXT_PATH_PUBLIC = "/secure/"
var RP_GET_CONFIGURATIONS = "LoadConfiguration";
var RP_USER_DATA = "GetUser";
var RP_DASHBOARD_MENU = "DashboardMenu";
var RP_REGISTER_ACADEMIC_CONFIGURATION = "RegisterAcademicConfiguration";
var RP_GET_ALL_ACADEMIC_CONFIGURATION = "GetAcademicConfigurations";
var RP_DELETE_ACADEMIC_CONFIGURATION = "DeleteAcademicConfiguration/";
var RP_REGISTER_SITE = "RegisterSite";
var RP_GET_ALL_SITE = "GetSites";
var RP_DELETE_SITE = "DeleteSite/";
var RP_REGISTER_INTERVAL = "RegisterInterval";
var RP_GET_ALL_INTERVAL = "GetIntervals";
var RP_DELETE_INTERVAL = "DeleteInterval/";
var RP_REGISTER_EXPLOITATION = "RegisterExploitation";
var RP_GET_ALL_EXPLOITATIONS = "GetExploitations";
var RP_DELETE_EXPLOITATION = "DeleteExploitation/";
var RP_REGISTER_PRODUCT = "RegisterProduct";
var RP_GET_ALL_PRODUCTS = "GetProducts";
var RP_DELETE_PRODUCT = "DeleteProduct/";
var RP_REGISTER_DEPARTMENT = "RegisterDepartment";
var RP_GET_ALL_DEPARTMENTS = "GetDepartments";
var RP_DELETE_DEPARTMENT = "DeleteDepartment/";
var RP_GET_ALL_REGISTRY = "GetRegistry";
/**
 * Variables Conversion UMT - Coordenadas Geográficas
 */
var pi = 3.14159265358979;
var sm_a = 6378137.0;
var sm_b = 6356752.314;
var sm_EccSquared = 6.69437999013e-03;
var UTMScaleFactor = 0.9996;

var NOTIFICACION_EXITO = "success";
var NOTIFICACION_WARN = "warning";
var NOTIFICACION_ERROR = "danger";


/**
 * --------------------------------------------------------------------------
 * FUNCIONES
 * --------------------------------------------------------------------------
 */


/**
 * Componente que permite mostrar mensajes del sistema, de tipo informativo o de
 * error.
 *
 * @param {string}
 *            mensaje - Descripcion del mensaje del sistema.
 * @param {number}
 *            tipoMensaje - Mensaje informativo: 1(Cons INFO), Mensaje de error:
 *            0(Cons ERROR).
 * @param {number}
 *            tiempo - Tiempo en milisegundos en que el mensaje sera visible.
 * @param {requestCallback}
 *            callback - Nombre de la funcion que se ejecutara al finalizar el
 *            proceso. De indicarse null, se omite su uso.
 */
function mostrarMensaje(mensaje, tipoMensaje, tiempo, callback) {
    var clase = null;

    if (tipoMensaje === INFO) {
        clase = 'messageboxok';
    } else {
        clase = 'messageboxerror';
    }

    $(".btn_cerrar").stop(true);

    $('.btn_cerrar').css("display", "none");

    $("#msgbox").removeClass("messageboxok");
    $("#msgbox").removeClass("messageboxerror");
    $("#cerrar").removeAttr('style');

    $("#msgbox").addClass(clase);

    $("#msgbox").html(mensaje);

    $('.btn_cerrar').css("display", "block");
    $('.btn_cerrar').css("opacity", 0);
    $(".btn_cerrar").animate({
        opacity: 1
    }, "fast", function () {
        $(".btn_cerrar").animate({
            opacity: 1
        }, tiempo, function () {
            $(".btn_cerrar").animate({
                opacity: 0
            }, "fast", function () {
                if (typeof callback === 'function') {
                    callback();
                }
            });
        });
    });
}

/**
 * Componente que permite mostrar u ocultar la animacion(Gif) de precarga,
 * mientras se procesa una peticion en el servidor.
 *
 * @param {number}
 *            tipoOper - Mostrar animacion de precarga: 1(Cons MOSTRAR), Ocultar
 *            animacion de precarga: 0(Cons OCULTAR).
 * @param {requestCallback}
 *            callback - Nombre de la funcion que se ejecutara al finalizar el
 *            proceso. De indicarse null, se omite su uso.
 */
function animarCargador(tipoOper, callback) {
    var retardo = 500;

    if (tipoOper === MOSTRAR) {
        $(".preloader").fadeIn(retardo, function () {
            if (callback === null) {
                $(".btn_cerrar").stop(true);
                $(".btn_cerrar").hide();
            }
        });
    }

    if (tipoOper === OCULTAR) {
        $(".preloader").fadeOut(retardo, function () {
            if (typeof callback === 'function') {
                $(".btn_cerrar").stop(true).fadeOut();
                $(".preloader").stop(true).fadeOut();
                callback.call();
            }
        });
    }

    if (callback === null) {
        $("#msgbox").html("").removeClass('messageboxerror');
        $("#cerrar").removeClass('btnCerrar');
        $(".btn_cerrar").hide();
        $(".btn_cerrar").stop(true);
    }
}

function cerrarAlert() {
    $(".btn_cerrar").hide();
    $(".btn_cerrar").stop(true);
}

/**
 * Componente que permite realizar peticiones de tipo Ajax al servidor.
 *
 * @param {string}
 *            metodoEnv - Metodo de envio. Puede ser POST o GET.
 * @param {string}
 *            direccionUrl - Url del controlador que gestionara la peticion.
 * @param {string}
 *            jsonString - Objeto en formato Json que contiene los datos de la
 *            peticion.
 * @param {string}
 *            idBoton - Identificador del boton que dispara la peticion. Si no
 *            es disparado por un boton, sera null.
 * @param {requestCallback}
 *            callback - Nombre de la funcion que se ejecutara al finalizar el
 *            proceso. De indicarse null, se omite su uso.
 */
function consultarAjax(metodoEnv, direccionUrl, jsonString, contextPath, callback) {
    $("#cargador").fadeIn();
    $.ajax({
        type: metodoEnv,
        data: jsonString,
        async: true,
        url: direccionUrl,
        contentType: "application/json",
        success: function (respuesta) {
            callback(respuesta);
        },
        error: function (respuesta) {
            var resp = null;

            if (respuesta.status == "404") {
                resp = {
                    "mensajeRespuesta": "El recurso solicitado no existe (HTTP: 404).",
                    "codigoRespuesta": "99"
                };
            } else {
                resp = {
                    "mensajeRespuesta": "Error no identificado.",
                    "codigoRespuesta": "99"
                };
            }
            callback(resp);
        }
    }).done(function () {
        // hide spinner
        $("#cargador").fadeOut();
    });
}

function consultarAjaxWithoutTimeout(metodoEnv, direccionUrl, jsonString, contextPath, callback) {
    $.ajax({
        type: metodoEnv,
        data: jsonString,
        async: true,
        url: direccionUrl,
        timeout: 0,
        contentType: "application/json",
        success: function (respuesta) {
            callback(respuesta);
        },
        error: function (respuesta) {
            console.log('ERROR: ' + JSON.stringify(respuesta))
            var resp = null;

            if (respuesta.status == "404") {
                resp = {
                    "mensajeRespuesta": "El recurso solicitado no existe (HTTP: 404).",
                    "codigoRespuesta": "99"
                };
            } else {
                resp = {
                    "mensajeRespuesta": "Error no identificado. " + respuesta.status,
                    "codigoRespuesta": "99"
                };
            }
            callback(resp);
        }
    });
}

function consultarAjaxCORS(metodoEnv, direccionUrl, jsonString, contextPath, callback) {

    $.ajax({
        type: metodoEnv,
        data: jsonString,
        async: true,
        url: direccionUrl,
        dataType: 'jsonp',
        crossDomain: true,
        contentType: "application/json",
        success: function (respuesta) {
            callback(respuesta);
        },
        error: function (respuesta) {
            var resp = null;

            if (respuesta.status == "404") {
                resp = {
                    "mensajeRespuesta": "El recurso solicitado no existe (HTTP: 404).",
                    "codigoRespuesta": "99"
                };
            } else {
                resp = {
                    "mensajeRespuesta": "Error no identificado.",
                    "codigoRespuesta": "99"
                };
            }
            callback(resp);
        }
    });
}

/**
 * Componente que permite abrir una ventana de dialogo.
 *
 * @param {string}
 *            idDialog - Identificador de la ventana de dialogo (Por ejm.
 *            #MyModal).
 */
function abrirDialogo(idDialog, callback) {
    $("#msgbox").html("").removeClass('messageboxerror');
    $("#cerrar").removeAttr('style');
    $(".btn_cerrar").hide();

    $(idDialog).slideDown();

    $("#backDialog").fadeIn("fast", function () {
        if (typeof callback === 'function') {
            callback();
        }
    });
}

/**
 * Componente que permite cerrar una ventana de dialogo.
 *
 * @param {string}
 *            idDialog - Identificador de la ventana de dialogo (Por ejm.
 *            #MyModal).
 */
function cerrarDialogo(idDialog, callback) {
    $(idDialog).hide();

    $("#backDialog").fadeOut("fast", function () {
        if (typeof callback === 'function') {
            callback();
        }
    });
}

/**
 * Componente que permite desactivar un boton de tipo submit.
 *
 * @param {string}
 *            idBoton - Identificador del boton de tipo submit (Por ejm.
 *            #btnAceptar).
 * @param {number}
 *            accion - Activar boton: 1(Cons ENABLED), Desactivar boton: 0(Cons
 *            DISABLED).
 */
function cambiarEstadoBoton(idBoton, accion) {
    if (idBoton !== null) {
        if (accion === ENABLED) {
            $(idBoton).prop("disabled", null);
        } else {
            $(idBoton).prop("disabled", "disabled");
        }
    }
}

/**
 * Componente que permite construir un menu de opciones y subopciones.
 *
 */
function menu() {
    var url = "generarMenu";
    var idPadre = [];
    var i;

    $.getJSON(url, function (datos) {
        $.each(datos.menuPadre, function (key, valorP) {
            var img = 'img/' + valorP.icono;

            var menuPadre = "<li>" + "<a href='#'>" + "<img alt='' src=" + img
                + " />" + valorP.descripcionP + "</a>" + "</li>";

            $(".nav").append(menuPadre);

            $.each(datos.menuHijo, function (key, valorH) {
                if (valorP.idMenuP === valorH.idPadre) {
                    var menuHijo = "<ul class='dropdown-menu'>" + "<li>"
                        + "<a href=" + valorH.ruta + ">"
                        + valorH.descripcionH + "</a>" + "</li>" + "</ul>";
                    console.log(menuHijo);
                    $(".dropdown-menu").append(menuHijo);
                }
            });
        });
    });
}

/**
 * Funcion que permite obtener el valor de un elemento.
 *
 * @param {string}
 *            selector - Selector del elemento a evaluar.
 * @returns {string}
 */
function obtenerValorElemento(selector) {
    var value = $(selector).val();
    return verificarValorElemento(value);
}

/**
 * Funcion que permite verificar el valor de un elemento.
 *
 * @param {string}
 *            value - Valor del elemento.
 * @returns {string}
 */
function verificarValorElemento(value) {

    if (typeof value === "undefined") {
        value = null;
    }

    if ((value != null) && ($.trim(value) == "")) {
        value = null;
    }

    if (value != null) {
        value = $.trim(value);
    }

    return value;

}

/**
 * Funcion que permite verificar el valor numerico de un elemento.
 *
 * @param {string}
 *            value - Valor numerico del elemento.
 * @returns {string}
 */
function verificarValorNumericoElemento(value) {

    if (typeof value === "undefined") {
        value = null;
    }

    if ((value != null) && ($.trim(value) == "")) {
        value = null;
    }

    if (value != null) {
        value = $.trim(value);
    }

    // numeros enteros y decimales
    if (!/^[0-9]+(\.[0-9]+)?$/.test(value)) {
        value = null;
    }

    return value;

}

/**
 * Funcion que permite validar una cadena frente a un conjunto de caracteres
 * permitidos.
 *
 * @param {string}
 *            valor - Cadena de caracteres a evaluar.
 * @param {string}
 *            valores - Cadena de caracteres permitidos.
 * @returns {boolean}
 */
function validarValorAlfanumerico(valor, valores) {

    if (verificarValorElemento(valor) == null) {
        return false;
    }

    for (var i = 0; i < valor.length; i++) {
        if (valores.indexOf(valor.charAt(i)) == -1) {
            return false;
        }
    }

    return true;

}

/**
 * Funcion que permite validar una cadena, verificando que no tenga solo valores
 * numericos.
 *
 * @param {string}
 *            valor - Cadena de caracteres a evaluar.
 * @returns {boolean}
 */
function validarValorNoSoloNumerico(valor) {

    var ind = false;

    if (verificarValorElemento(valor) == null) {
        return ind;
    }

    if (valor.length == 0) {
        return ind;
    }

    var valores = "0123456789";
    for (var i = 0; i < valor.length; i++) {
        if (valores.indexOf(valor.charAt(i)) == -1) {
            ind = true;
        }
    }

    return ind;

}

/**
 * Funcion que permite formatear un numero decimal que ha sido representado con
 * coma, y no con punto.
 *
 */
function formatearNumeroDecimal(valor) {
    if (valor.indexOf(',') != -1) {
        valor = valor.replace(',', '.');
    }
    return valor;
}

/**
 * Funcion que permite mostrar un numero con comas en miles.
 *
 * @param n
 * @returns
 */
function numeroConComas(n) {
    var parts = n.toString().split(".");
    return parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        + (parts[1] ? "." + parts[1] : "");
}

/**
 * Funcion que llena de datos un campo seleccionable
 *
 * @param nombreControl
 * @param nuevaOpciones
 */
function construirSelect(nombreControl, nuevaOpciones, valueSel) {
    if (nuevaOpciones.length > 0) {
        var select = $('select[name="' + nombreControl + '"]');
        $('option', select).remove();
        $.each(nuevaOpciones, function (key, obj) {
            select.append('<option value="' + obj.Code + '">' + obj.Description + '</option>');
        });
        if (valueSel == null) {
            select.val("")
        } else {
            select.val(valueSel)
        }
        ;

    }
}

/**
 * Funcion que limpia todos los campos de un formulario
 * @param form
 */
function limpiarFormulario(form) {

    //-- Itera todos los elementos inputs del formulario que se mapean
    $(':input', form).each(function () {

        var type = this.type;
        var tag = this.tagName.toLowerCase(); // normalize case

        //-- Limpiar: text inputs, password inputs y textareas
        if (type == 'text' || type == 'password' || tag == 'textarea') {
            this.value = "";
        }

        //-- Limpiar: checkboxes y radios se necesita tener su check status limpio
        else if (type == 'checkbox' || type == 'radio') {
            this.checked = false;
        }

        //-- Limpiar: select (trabaj con select simples y multiples)
        else if (tag == 'select') {
            this.selectedIndex = '';
        }

    });
    //form.bootstrapValidator('resetForm', true);

};

/**
 * Funcion que codifica una cadena en texto html por ejemplo: "pájaro" se convertira en "p&acute;jaro"
 * @returns {String}
 */
function htmlEncode(value) {
    return $('<div/>').text(value).html();
}
/**
 * Funcion que decodifica un texto html en cadena por ejemplo: "p&acute;jaro"  se convertira en "pájaro"
 * @returns {String}
 */
function htmlDecode(value) {
    return $('<div/>').html(value).text();
}

/**
 * Obtener todos los valores de los campos del formulario como objeto JSON
 * @returns {String}
 */
$.fn.serializeObject = function () {
    var disabledInput = this.find(':input:disabled').removeAttr('disabled');

    var o = {};
    var a = this.serializeArray();

    disabledInput.attr('disabled', 'disabled');

    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

/**
 * Obtener el dia actual en formato dd/mm/yyyy
 * @returns {String}
 */
function obtenerDiaActual() {
    var fullDate = new Date();
    var twoDigitDate = (fullDate.getDate().length === 1) ? fullDate.getDate() : '0' + fullDate.getDate();
    var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : '0' + (fullDate.getMonth() + 1);
    return twoDigitDate + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
}

/**
 * Obtener la resta de dias de una fecha en formato dd/mm/yyyy
 * @param fecha
 * @param dias
 * @returns {String}
 */
function restarDias(fecha, dias) {
    var partes = fecha.split("/");

    var dateOffset = (24 * 60 * 60 * 1000) * dias;
    var diaNuevo = new Date(partes[2], partes[1] - 1, partes[0]);
    diaNuevo.setTime(diaNuevo.getTime() - dateOffset);

    var twoDigitDate = (diaNuevo.getDate().length === 1) ? diaNuevo.getDate() : '0' + diaNuevo.getDate();
    var twoDigitMonth = ((diaNuevo.getMonth().length + 1) === 1) ? (diaNuevo.getMonth() + 1) : '0' + (diaNuevo.getMonth() + 1);

    return twoDigitDate + "/" + twoDigitMonth + "/" + diaNuevo.getFullYear();
}

/**
 * @param div Div a Mostrar / Ocultar
 * @param valorSeleccionado Valor que se quiere mostrar / ocultar
 * @param posicion Posicion que se selecciona en el combo (this.value)
 * @param opcion Mostrar / Ocultar
 * @param Funcion de validacion
 */
function mostrarCboDependiente(div, valorSeleccionado, posicion, opcion, callBack) {
    var valida = false;
    if (opcion == 'mostrar') {
        tDisplay = 'block';
        tDisplayE = 'none';

    } else if (opcion == 'ocultar') {
        tDisplay = 'none';
        tDisplayE = 'block';
        if (valorSeleccionado == '') {
            valorSeleccionado = posicion;
        }
    }

    if (posicion == valorSeleccionado) {
        if (div !== null) {
            div.style.display = tDisplay;
            valida = true;
        }
    } else {
        if (div !== null) {
            div.style.display = tDisplayE;
            valida = false;
        }
    }

    callBack(valida);
}

/**
 * Mover a una seccion de la pagina con animacion
 * @param id
 */
function scrollTo(id) {
    $('html,body').animate({
        scrollTop: $("#" + id).offset().top
    }, 'slow');
}

/**
 * Muestra el mensaje estilo Bootstrap Alert. El tipo de mensaje viene desde la inicializacion del componente donde puede ser "alert-success" "alert-danger"
 * mas informacion mirar el api de Bootstrap http://getbootstrap.com/components/#alerts
 *
 * Finalidad del metodo, es mostrar el mensaje por cierta cantidad de segundos y ocultarlo.
 * Ejemplo:
 *    - Inicializa <div id="alertaMensajeSuccess" class="alert alert-success" role="alert" style="margin-top: 5px;display:none;"></div>
 *    - Se llama a este metodo para visualizarlo
 *
 * @param idMensaje
 * @param textoMensaje
 * @param tiempoMensaje
 */
function mostrarMensajeClasico(idMensaje, textoMensaje, tiempoMensaje) {
    $(idMensaje).html(textoMensaje);
    $(idMensaje).show();
    $(idMensaje).fadeTo(tiempoMensaje, 500).slideUp(500, function () {
        $(idMensaje).hide();
    });
}

/**
 * Muestra mensaje en una notificacion basado en Boostrap Alert. Finalidad del metodo, es mostrar notificaciones en la parte superior
 * derecha que esten por un tiempo minimo indicando una accion del sistema. Lo unico que se hace es colocar el metodo presente y listo se visualizara
 * la notificacion cada vez que se llame al metodo
 *
 * @param textMensaje
 * @param tipoMensaje
 */
function mostrarMensajeFlotante(textMensaje, tipoMensaje) {
    $.notify({
        message: textMensaje
    }, {
        type: tipoMensaje
    });
}

function alertaGenerica(op, msg, time) {
    if (time == undefined)
        time = 900;
    var n = noty({
        text: msg,
        maxVisible: 1,
        type: op,
        killer: true,
        timeout: time,
        layout: 'top'
    });
}

/**
 * Convierte Deg a Radianes
 * @param deg
 * @returns {Number}
 */
function DegToRad(deg) {
    return (deg / 180.0 * pi)
}

/**
 * Convierte Radianes a Deg
 * @param rad
 * @returns {Number}
 */
function RadToDeg(rad) {
    return (rad / pi * 180.0)
}

/**
 * @param phi
 * @returns {Number}
 */
function ArcLengthOfMeridian(phi) {
    var alpha, beta, gamma, delta, epsilon, n;
    var result;
    n = (sm_a - sm_b) / (sm_a + sm_b);
    alpha = ((sm_a + sm_b) / 2.0)
        * (1.0 + (Math.pow(n, 2.0) / 4.0) + (Math.pow(n, 4.0) / 64.0));
    beta = (-3.0 * n / 2.0) + (9.0 * Math.pow(n, 3.0) / 16.0)
        + (-3.0 * Math.pow(n, 5.0) / 32.0);
    gamma = (15.0 * Math.pow(n, 2.0) / 16.0)
        + (-15.0 * Math.pow(n, 4.0) / 32.0);
    delta = (-35.0 * Math.pow(n, 3.0) / 48.0)
        + (105.0 * Math.pow(n, 5.0) / 256.0);
    epsilon = (315.0 * Math.pow(n, 4.0) / 512.0);
    result = alpha
        * (phi + (beta * Math.sin(2.0 * phi))
        + (gamma * Math.sin(4.0 * phi))
        + (delta * Math.sin(6.0 * phi))
        + (epsilon * Math.sin(8.0 * phi)));
    return result;
}

/**
 * @param zone
 * @returns {Number}
 */
function UTMCentralMeridian(zone) {
    var cmeridian;
    cmeridian = DegToRad(-183.0 + (zone * 6.0));
    return cmeridian;
}

/**
 * @param y
 * @returns {Number}
 */
function FootPointLatitude(y) {
    var y_, alpha_, beta_, gamma_, delta_, epsilon_, n;
    var result;
    n = (sm_a - sm_b) / (sm_a + sm_b);
    alpha_ = ((sm_a + sm_b) / 2.0)
        * (1 + (Math.pow(n, 2.0) / 4) + (Math.pow(n, 4.0) / 64));
    y_ = y / alpha_;
    beta_ = (3.0 * n / 2.0) + (-27.0 * Math.pow(n, 3.0) / 32.0)
        + (269.0 * Math.pow(n, 5.0) / 512.0);
    gamma_ = (21.0 * Math.pow(n, 2.0) / 16.0)
        + (-55.0 * Math.pow(n, 4.0) / 32.0);
    delta_ = (151.0 * Math.pow(n, 3.0) / 96.0)
        + (-417.0 * Math.pow(n, 5.0) / 128.0);
    epsilon_ = (1097.0 * Math.pow(n, 4.0) / 512.0);
    result = y_ + (beta_ * Math.sin(2.0 * y_))
        + (gamma_ * Math.sin(4.0 * y_))
        + (delta_ * Math.sin(6.0 * y_))
        + (epsilon_ * Math.sin(8.0 * y_));
    return result;
}

/**
 * @param phi
 * @param lambda
 * @param lambda0
 * @param xy
 */
function MapLatLonToXY(phi, lambda, lambda0, xy) {
    var N, nu2, ep2, t, t2, l;
    var l3coef, l4coef, l5coef, l6coef, l7coef, l8coef;
    var tmp;
    ep2 = (Math.pow(sm_a, 2.0) - Math.pow(sm_b, 2.0)) / Math.pow(sm_b, 2.0);
    nu2 = ep2 * Math.pow(Math.cos(phi), 2.0);
    N = Math.pow(sm_a, 2.0) / (sm_b * Math.sqrt(1 + nu2));
    t = Math.tan(phi);
    t2 = t * t;
    tmp = (t2 * t2 * t2) - Math.pow(t, 6.0);
    l = lambda - lambda0;
    l3coef = 1.0 - t2 + nu2;
    l4coef = 5.0 - t2 + 9 * nu2 + 4.0 * (nu2 * nu2);
    l5coef = 5.0 - 18.0 * t2 + (t2 * t2) + 14.0 * nu2
        - 58.0 * t2 * nu2;
    l6coef = 61.0 - 58.0 * t2 + (t2 * t2) + 270.0 * nu2
        - 330.0 * t2 * nu2;
    l7coef = 61.0 - 479.0 * t2 + 179.0 * (t2 * t2) - (t2 * t2 * t2);
    l8coef = 1385.0 - 3111.0 * t2 + 543.0 * (t2 * t2) - (t2 * t2 * t2);
    xy[0] = N * Math.cos(phi) * l
        + (N / 6.0 * Math.pow(Math.cos(phi), 3.0) * l3coef * Math.pow(l, 3.0))
        + (N / 120.0 * Math.pow(Math.cos(phi), 5.0) * l5coef * Math.pow(l, 5.0))
        + (N / 5040.0 * Math.pow(Math.cos(phi), 7.0) * l7coef * Math.pow(l, 7.0));
    xy[1] = ArcLengthOfMeridian(phi)
        + (t / 2.0 * N * Math.pow(Math.cos(phi), 2.0) * Math.pow(l, 2.0))
        + (t / 24.0 * N * Math.pow(Math.cos(phi), 4.0) * l4coef * Math.pow(l, 4.0))
        + (t / 720.0 * N * Math.pow(Math.cos(phi), 6.0) * l6coef * Math.pow(l, 6.0))
        + (t / 40320.0 * N * Math.pow(Math.cos(phi), 8.0) * l8coef * Math.pow(l, 8.0));
    return;
}

function MapXYToLatLon(x, y, lambda0, philambda) {
    var phif, Nf, Nfpow, nuf2, ep2, tf, tf2, tf4, cf;
    var x1frac, x2frac, x3frac, x4frac, x5frac, x6frac, x7frac, x8frac;
    var x2poly, x3poly, x4poly, x5poly, x6poly, x7poly, x8poly;
    phif = FootPointLatitude(y);
    ep2 = (Math.pow(sm_a, 2.0) - Math.pow(sm_b, 2.0))
        / Math.pow(sm_b, 2.0);
    cf = Math.cos(phif);
    nuf2 = ep2 * Math.pow(cf, 2.0);
    Nf = Math.pow(sm_a, 2.0) / (sm_b * Math.sqrt(1 + nuf2));
    Nfpow = Nf;
    tf = Math.tan(phif);
    tf2 = tf * tf;
    tf4 = tf2 * tf2;
    x1frac = 1.0 / (Nfpow * cf);
    Nfpow *= Nf;
    x2frac = tf / (2.0 * Nfpow);
    Nfpow *= Nf;
    x3frac = 1.0 / (6.0 * Nfpow * cf);
    Nfpow *= Nf;
    x4frac = tf / (24.0 * Nfpow);
    Nfpow *= Nf;
    x5frac = 1.0 / (120.0 * Nfpow * cf);
    Nfpow *= Nf;
    x6frac = tf / (720.0 * Nfpow);
    Nfpow *= Nf;
    x7frac = 1.0 / (5040.0 * Nfpow * cf);
    Nfpow *= Nf;
    x8frac = tf / (40320.0 * Nfpow);
    x2poly = -1.0 - nuf2;
    x3poly = -1.0 - 2 * tf2 - nuf2;
    x4poly = 5.0 + 3.0 * tf2 + 6.0 * nuf2 - 6.0 * tf2 * nuf2
        - 3.0 * (nuf2 * nuf2) - 9.0 * tf2 * (nuf2 * nuf2);
    x5poly = 5.0 + 28.0 * tf2 + 24.0 * tf4 + 6.0 * nuf2 + 8.0 * tf2 * nuf2;
    x6poly = -61.0 - 90.0 * tf2 - 45.0 * tf4 - 107.0 * nuf2
        + 162.0 * tf2 * nuf2;
    x7poly = -61.0 - 662.0 * tf2 - 1320.0 * tf4 - 720.0 * (tf4 * tf2);
    x8poly = 1385.0 + 3633.0 * tf2 + 4095.0 * tf4 + 1575 * (tf4 * tf2);
    philambda[0] = phif + x2frac * x2poly * (x * x)
        + x4frac * x4poly * Math.pow(x, 4.0)
        + x6frac * x6poly * Math.pow(x, 6.0)
        + x8frac * x8poly * Math.pow(x, 8.0);
    philambda[1] = lambda0 + x1frac * x
        + x3frac * x3poly * Math.pow(x, 3.0)
        + x5frac * x5poly * Math.pow(x, 5.0)
        + x7frac * x7poly * Math.pow(x, 7.0);
    return;
}

/**
 * @param lat
 * @param lon
 * @param zone
 * @param xy
 * @returns
 */
function LatLonToUTMXY(lat, lon, zone, xy) {
    MapLatLonToXY(lat, lon, UTMCentralMeridian(zone), xy);
    xy[0] = xy[0] * UTMScaleFactor + 500000.0;
    xy[1] = xy[1] * UTMScaleFactor;
    if (xy[1] < 0.0)
        xy[1] = xy[1] + 10000000.0;
    return zone;
}
/**
 * @param x
 * @param y
 * @param zone
 * @param southhemi
 * @param latlon
 * @returns
 */
function UTMXYToLatLon(x, y, zone, southhemi, latlon) {
    var cmeridian;
    x -= 500000.0;
    x /= UTMScaleFactor;
    if (southhemi)
        y -= 10000000.0;
    y /= UTMScaleFactor;
    cmeridian = UTMCentralMeridian(zone);
    MapXYToLatLon(x, y, cmeridian, latlon);
    return;
}
/**
 * @param latitud
 * @param longitud
 * @returns
 */
function ValidarPuntosGeograficos(latitud, longitud) {
    var regExpLat = /^(\+|-)?(?:90(?:(?:\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,25})?))$/;
    var regExpLng = /^(\+|-)?(?:180(?:(?:\.0{1,6})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,25})?))$/;
    if (!(regExpLat.test(latitud))) {
        return false;
    }
    if (!(regExpLng.test(longitud))) {
        return false;
    }
    return true;
}

function distribuidoTipoRegimen(value) {
    var tipoRegimen;
    if (value == 1) {
        tipoRegimen = "R\xe9gimen General";
    } else if (value == 2) {
        tipoRegimen = "R\xe9gimen RUS";
    } else if (value == 3) {
        tipoRegimen = "R\xe9gimen Especial";
    }
    return tipoRegimen;
}
/**
 * @param r
 * @param n
 * @param p
 * @returns
 */
function PMT(r, n, p) {
    var pmt = 0;
    var r1 = parseFloat(r) + parseFloat(1);
    pmt = ( p * Math.pow(parseFloat(r1), n) ) * r / ((1) * (1 - Math.pow(parseFloat(r1), n)));
    return parseFloat(pmt).toFixed(2);
}

/**
 * Metodo para obtener descripcion del mes
 * @param monthNumber
 * @returns
 */
function obtenerDescripcionMes(mes) {
    var meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
    return meses[parseInt(mes) - 1];
}


/**
 * Metodo que coloca todos los campos de un formulario en solo lectura
 * @param id
 */
function obtenerCamposSoloLectura(id) {
    $.each($(id).serializeArray(), function (index, value) {
        $('[name="' + value.name + '"]').attr('readonly', 'readonly');
    });
}

function formaterTimeStamp(fechaTimeStamp) {
    date = new Date(fechaTimeStamp * 1000);
    var d = new Date(fechaTimeStamp);
    var n = d.getMilliseconds();
    datevalues = [
        date.getFullYear(),
        date.getMonth() + 1,
        date.getDate(),
        date.getHours(),
        date.getMinutes(),
        date.getSeconds(),
    ];
    return datevalues[2] + "-" + datevalues[1] + "-" + datevalues[0];
}

function quitarLetras(cadena) {
    return cadena.replace(/\D/g, '');
}

function removerAcentos(cadena) {

    var defaultDiacriticsRemovalMap = [
        {
            'base': 'A',
            'letters': '\u0041\u24B6\uFF21\u00C0\u00C1\u00C2\u1EA6\u1EA4\u1EAA\u1EA8\u00C3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\u00C4\u01DE\u1EA2\u00C5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F'
        },
        {'base': 'AA', 'letters': '\uA732'},
        {'base': 'AE', 'letters': '\u00C6\u01FC\u01E2'},
        {'base': 'AO', 'letters': '\uA734'},
        {'base': 'AU', 'letters': '\uA736'},
        {'base': 'AV', 'letters': '\uA738\uA73A'},
        {'base': 'AY', 'letters': '\uA73C'},
        {'base': 'B', 'letters': '\u0042\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181'},
        {'base': 'C', 'letters': '\u0043\u24B8\uFF23\u0106\u0108\u010A\u010C\u00C7\u1E08\u0187\u023B\uA73E'},
        {
            'base': 'D',
            'letters': '\u0044\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779\u00D0'
        },
        {'base': 'DZ', 'letters': '\u01F1\u01C4'},
        {'base': 'Dz', 'letters': '\u01F2\u01C5'},
        {
            'base': 'E',
            'letters': '\u0045\u24BA\uFF25\u00C8\u00C9\u00CA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\u00CB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E'
        },
        {'base': 'F', 'letters': '\u0046\u24BB\uFF26\u1E1E\u0191\uA77B'},
        {
            'base': 'G',
            'letters': '\u0047\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E'
        },
        {
            'base': 'H',
            'letters': '\u0048\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D'
        },
        {
            'base': 'I',
            'letters': '\u0049\u24BE\uFF29\u00CC\u00CD\u00CE\u0128\u012A\u012C\u0130\u00CF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197'
        },
        {'base': 'J', 'letters': '\u004A\u24BF\uFF2A\u0134\u0248'},
        {
            'base': 'K',
            'letters': '\u004B\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2'
        },
        {
            'base': 'L',
            'letters': '\u004C\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780'
        },
        {'base': 'LJ', 'letters': '\u01C7'},
        {'base': 'Lj', 'letters': '\u01C8'},
        {'base': 'M', 'letters': '\u004D\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C'},
        {
            'base': 'N',
            'letters': '\u004E\u24C3\uFF2E\u01F8\u0143\u00D1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4'
        },
        {'base': 'NJ', 'letters': '\u01CA'},
        {'base': 'Nj', 'letters': '\u01CB'},
        {
            'base': 'O',
            'letters': '\u004F\u24C4\uFF2F\u00D2\u00D3\u00D4\u1ED2\u1ED0\u1ED6\u1ED4\u00D5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\u00D6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\u00D8\u01FE\u0186\u019F\uA74A\uA74C'
        },
        {'base': 'OI', 'letters': '\u01A2'},
        {'base': 'OO', 'letters': '\uA74E'},
        {'base': 'OU', 'letters': '\u0222'},
        {'base': 'OE', 'letters': '\u008C\u0152'},
        {'base': 'oe', 'letters': '\u009C\u0153'},
        {'base': 'P', 'letters': '\u0050\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754'},
        {'base': 'Q', 'letters': '\u0051\u24C6\uFF31\uA756\uA758\u024A'},
        {
            'base': 'R',
            'letters': '\u0052\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782'
        },
        {
            'base': 'S',
            'letters': '\u0053\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784'
        },
        {
            'base': 'T',
            'letters': '\u0054\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786'
        },
        {'base': 'TZ', 'letters': '\uA728'},
        {
            'base': 'U',
            'letters': '\u0055\u24CA\uFF35\u00D9\u00DA\u00DB\u0168\u1E78\u016A\u1E7A\u016C\u00DC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244'
        },
        {'base': 'V', 'letters': '\u0056\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245'},
        {'base': 'VY', 'letters': '\uA760'},
        {'base': 'W', 'letters': '\u0057\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72'},
        {'base': 'X', 'letters': '\u0058\u24CD\uFF38\u1E8A\u1E8C'},
        {
            'base': 'Y',
            'letters': '\u0059\u24CE\uFF39\u1EF2\u00DD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE'
        },
        {
            'base': 'Z',
            'letters': '\u005A\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762'
        },
        {
            'base': 'a',
            'letters': '\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250'
        },
        {'base': 'aa', 'letters': '\uA733'},
        {'base': 'ae', 'letters': '\u00E6\u01FD\u01E3'},
        {'base': 'ao', 'letters': '\uA735'},
        {'base': 'au', 'letters': '\uA737'},
        {'base': 'av', 'letters': '\uA739\uA73B'},
        {'base': 'ay', 'letters': '\uA73D'},
        {'base': 'b', 'letters': '\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253'},
        {'base': 'c', 'letters': '\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184'},
        {
            'base': 'd',
            'letters': '\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A'
        },
        {'base': 'dz', 'letters': '\u01F3\u01C6'},
        {
            'base': 'e',
            'letters': '\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD'
        },
        {'base': 'f', 'letters': '\u0066\u24D5\uFF46\u1E1F\u0192\uA77C'},
        {
            'base': 'g',
            'letters': '\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F'
        },
        {
            'base': 'h',
            'letters': '\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265'
        },
        {'base': 'hv', 'letters': '\u0195'},
        {
            'base': 'i',
            'letters': '\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131'
        },
        {'base': 'j', 'letters': '\u006A\u24D9\uFF4A\u0135\u01F0\u0249'},
        {
            'base': 'k',
            'letters': '\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3'
        },
        {
            'base': 'l',
            'letters': '\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747'
        },
        {'base': 'lj', 'letters': '\u01C9'},
        {'base': 'm', 'letters': '\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F'},
        {
            'base': 'n',
            'letters': '\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5'
        },
        {'base': 'nj', 'letters': '\u01CC'},
        {
            'base': 'o',
            'letters': '\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275'
        },
        {'base': 'oi', 'letters': '\u01A3'},
        {'base': 'ou', 'letters': '\u0223'},
        {'base': 'oo', 'letters': '\uA74F'},
        {'base': 'p', 'letters': '\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755'},
        {'base': 'q', 'letters': '\u0071\u24E0\uFF51\u024B\uA757\uA759'},
        {
            'base': 'r',
            'letters': '\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783'
        },
        {
            'base': 's',
            'letters': '\u0073\u24E2\uFF53\u00DF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B'
        },
        {
            'base': 't',
            'letters': '\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787'
        },
        {'base': 'tz', 'letters': '\uA729'},
        {
            'base': 'u',
            'letters': '\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289'
        },
        {'base': 'v', 'letters': '\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C'},
        {'base': 'vy', 'letters': '\uA761'},
        {'base': 'w', 'letters': '\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73'},
        {'base': 'x', 'letters': '\u0078\u24E7\uFF58\u1E8B\u1E8D'},
        {
            'base': 'y',
            'letters': '\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF'
        },
        {'base': 'z', 'letters': '\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763'}
    ];

    var diacriticsMap = {};
    for (var i = 0; i < defaultDiacriticsRemovalMap.length; i++) {
        var letters = defaultDiacriticsRemovalMap [i].letters;
        for (var j = 0; j < letters.length; j++) {
            diacriticsMap[letters[j]] = defaultDiacriticsRemovalMap [i].base;
        }
    }
    return cadena.replace(/[^\u0000-\u007E]/g, function (a) {
        return diacriticsMap[a] || a;
    });
}

function getBrowserVersion() {
    var ua = navigator.userAgent, tem,
        M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if (/trident/i.test(M[1])) {
        tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE ' + (tem[1] || '');
    }
    if (M[1] === 'Chrome') {
        tem = ua.match(/\b(OPR|Edge)\/(\d+)/);
        if (tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
    if ((tem = ua.match(/version\/(\d+)/i)) != null) M.splice(1, 1, tem[1]);
    return M.join(',');
}

function calculateSpeed(data) {
    var timeInSeconds = (data.endTime - data.startTime) / parseFloat('1000f');
    var fileSizeBits = parseFloat(data.lengthFile) * parseInt(8);
    var fileSizeKBits = parseFloat(fileSizeBits / parseFloat('1000f'));
    var fileSizeMBits = parseFloat(fileSizeKBits / parseFloat('1000f'));

    var transferRateBitePerSecond = fileSizeBits / timeInSeconds;
    var transferRateKilobitePerSecond = fileSizeKBits / timeInSeconds;
    var transferRateMegabitePerSecond = fileSizeMBits / timeInSeconds;

    var valueBites = transferRateBitePerSecond.toFixed(2);
    var valueKilobites = transferRateKilobitePerSecond.toFixed(2);
    var valueMegabites = transferRateMegabitePerSecond.toFixed(2);

    var dataOutput = new Object();
    dataOutput["fileName"] = data.fileName;
    dataOutput["attempt"] = data.attempt;
    dataOutput["dataLength"] = data.lengthFile;
    dataOutput["timeInSeconds"] = timeInSeconds;
    dataOutput["speedBytePerSecond"] = valueBites;
    dataOutput["speedKilobytePerSecond"] = valueKilobites;
    dataOutput["speedMegabytePerSecond"] = valueMegabites;
    return dataOutput;
}

function uploadFileXhrJSONAndCalculateSpeed(contextPath, path, dataFile, fileName, method, attempt, callback) {
    var xhr = new XMLHttpRequest();
    var startTime, endTime;
    var url = contextPath + path;
    fileSize = dataFile.length;
    xhr.open(method, url, true);
    xhr.setRequestHeader("Content-type", "application/json");

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            endTime = (new Date()).getTime();
            var dataFile = {
                'fileName': fileName,
                'attempt': attempt,
                'startTime': startTime,
                'endTime': endTime,
                'lengthFile': fileSize
            };

            var dataOutput = calculateSpeed(dataFile);
            callback(dataOutput);
        }
    }
    startTime = (new Date()).getTime();

    xhr.addEventListener('progress', function (e) {
        console.log(Math.ceil(e.loaded / e.total) * 100 + '%');
    }, false);

    var dataUploadFile = {
        'uploadDataFile': dataFile,
        'fileName': fileName,
        'attempt': attempt
    };

    xhr.send(JSON.stringify(dataUploadFile));
}

function downloadFileXhrJSONAndCalculateSpeed(contextPath, path, fileName, method, attempt, callback) {
    var startTime, endTime, fileSize;
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {

        if (xhr.readyState === 4 && xhr.status === 200) {
            endTime = (new Date()).getTime();
            fileSize = xhr.responseText.length;

            var dataFile = {
                'fileName': fileName,
                'attempt': attempt,
                'startTime': startTime,
                'endTime': endTime,
                'lengthFile': fileSize
            };

            var dataOutput = calculateSpeed(dataFile);
            callback(dataOutput);

        }
    }

    xhr.addEventListener('progress', function (e) {
        console.log(Math.ceil(e.loaded / e.total) * 100 + '%');
    }, false);

    startTime = (new Date()).getTime();
    var url = contextPath + path + fileName;
    xhr.open(method, url, true);
    xhr.send();
}

function downloadFileXhrJSON(contextPath, path, fileName, method, callback) {
    var startTime, endTime, fileSize;
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            endTime = (new Date()).getTime();
            callback(xhr.responseText);
        }
    }
    startTime = (new Date()).getTime();
    var url = contextPath + path + fileName;
    xhr.open(method, url, true);
    xhr.send();
}

function createHeader(serviceName, ip) {
    var body = new Object();
    var headerBody = new Object();
    headerBody["ApplicationId"] = "WEB";
    headerBody["ServiceName"] = serviceName;
    headerBody["DeviceId"] = ip;
    headerBody["DeviceType"] = "WEB";
    headerBody["DeviceVersion"] = "1.0";
    headerBody["AppVersion"] = "1.0";
    body["Body"] = "";
    var data = {
        'Header': headerBody,
        'Body': {},
    }
    return data;
};

function encode(string) {
    return btoa(string);
};

function decode(string) {
    return atob(string);
};

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};


