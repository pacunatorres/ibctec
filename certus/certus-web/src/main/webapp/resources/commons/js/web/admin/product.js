/**
 * @fileoverview .
 *
 * @autor joanhuve@gmail.com
 * @version 1.0
 */
$(document).ready(function () {
    var contextPath = $("#contextPath").val();

    fnProduct.getAllProducts(function (callbackGAI) {
        if (callbackGAI.Body.Error === undefined) {
            mostrarMensajeFlotante(callbackGAI.Body.Response.Message, NOTIFICACION_EXITO);

            fnProduct.fnRenderTable(contextPath);
            if (callbackGAI.Body.Response.Code === '1') {
                fnProduct.fnUpdateTable(callbackGAI.Body.Response.CostCenterConfigurations);
            } else {
                mostrarMensajeFlotante(callbackGAI.Body.Response.Message, NOTIFICACION_WARN);
            }

        } else {
            mostrarMensajeFlotante(callbackGAI.Body.Error.Message, NOTIFICACION_ERROR);
        }
    });

    formProduct = $('#formProduct');
    limpiarFormulario(formProduct);

    formProduct.formValidation({
        framework: 'bootstrap',
        live: 'enabled',
        fields: {
            program: {
                validators: {
                    notEmpty: {
                        message: 'Seleccione programa.'
                    }
                }
            },
            product: {
                validators: {
                    notEmpty: {
                        message: 'Seleccione producto.'
                    }
                }
            }
        }
    });

    $('#btnConfirmChangesSave').on('click', function (e) {
        if (formProduct.data('formValidation').validate().isValid()) {
            $('#btnConfirmChangesSave').prop("disabled", true);
            $('#modalConfirmChanges').modal('show');
        }
    });

    $('#btnSave').on('click', function (e) {
        $(this).prop("disabled", true);

        fnProduct.fnRegisterProduct(function (callbackAC) {
            if (callbackAC.Body.Error === undefined) {

                mostrarMensajeFlotante(callbackAC.Body.Response.Message, NOTIFICACION_EXITO);

                fnProduct.getAllProducts(function (callbackGAI) {
                    if (callbackGAI.Body.Error === undefined) {
                        mostrarMensajeFlotante("Se actualizo la lista de productos.", NOTIFICACION_EXITO);
                        if (callbackGAI.Body.Response.Code === '1') {
                            fnProduct.fnUpdateTable(callbackGAI.Body.Response.CostCenterConfigurations);
                        } else {
                            mostrarMensajeFlotante(callbackGAI.Body.Response.Message, NOTIFICACION_WARN);
                        }
                    } else {
                        mostrarMensajeFlotante(callbackGAI.Body.Error.Message, NOTIFICACION_ERROR);
                    }
                });

            } else {
                mostrarMensajeFlotante(callbackAC.Body.Error.Message, NOTIFICACION_ERROR);
            }

            $('#codeProgram').val('');
            $('#codeProduct').val('');
            $('#modalConfirmChanges').modal('hide');
            $('#btnConfirmChangesSave').prop("disabled", false);
            $('#btnSave').prop("disabled", false);
            $('#formProduct').data('formValidation').resetForm(true);
        })
    });

    $('#btnReset').on('click', function (e) {
        $('#btnConfirmChangesSave').prop("disabled", false);
        $('#codeProgram').val('');
        $('#codeProduct').val('');
        formProduct.data('formValidation').resetForm(true);
    });

    $('#program').on('change', function () {
        $('#codeProgram').val(this.value);
    });

    $('#product').on('change', function () {
        $('#codeProduct').val(this.value);
    });

});

var fnProduct = function () {

    function fnRegisterProduct(callback) {
        var contextPath = $("#contextPath").val();
        var ip = JSON.parse(localStorage.getItem("ip"));
        var data = createHeader(RP_REGISTER_PRODUCT, ip);
        data['Body'] = {
            'Request': {
                "CostCenterConfiguration": {
                    "Code": $('#codeProgram').val(),
                    "Description": $("#program option:selected").text(),
                    "CodeSelector": $('#codeProduct').val(),
                    "DescriptionSelector": $("#product option:selected").text()
                }
            }
        };

        consultarAjax("POST", (contextPath + CONTEXT_PATH_PUBLIC + RP_REGISTER_PRODUCT), JSON.stringify(data), contextPath, function (callbackAC) {
            callback(callbackAC);
        });
    }

    function getAllProducts(callback) {
        var contextPath = $("#contextPath").val();
        var ip = JSON.parse(localStorage.getItem("ip"));
        var data = createHeader(RP_GET_ALL_PRODUCTS, ip);
        consultarAjax("POST", (contextPath + CONTEXT_PATH_PUBLIC + RP_GET_ALL_PRODUCTS), JSON.stringify(data), contextPath, function (callbackFilter) {
            callback(callbackFilter);
        });
    }

    function fnRenderTable(pageContext) {
        $('#tblProduct').dataTable().fnDestroy();
        $('#tblProduct').DataTable({
            "ordering": true,
            "info": false,
            "bLengthChange": false,
            "bFilter": false,
            "iDisplayLength": 8,
            "order": [[0, 'asc']],
            "columnDefs": [{
                "className": "text-center",
                "targets": 0
            }, {
                "className": "text-center",
                "targets": 1
            }, {
                "className": "text-center",
                "targets": 2
            }, {
                "className": "text-center",
                "targets": 3
            }, {
                "className": "text-center",
                "targets": 4
            }, {
                "render": function (data, type, row) {
                    var headLi = '<ul class="list-actions">';
                    var liOptions = '<li>' +
                        '<button ' +
                        'type="button" ' +
                        'class="btn-link" ' +
                        'onClick="fnProduct.fnDeleteProduct(' + data + ')"> ' +
                        '<i class="fa fa-trash"></i>' +
                        '</button> ' +
                        '</li>';
                    var footLi = '</ul>';
                    return headLi + liOptions + footLi;
                },
                "targets": 5
            }],
            columns: [
                {data: 'IdCostCenter', orderable: false},
                {data: 'Code', orderable: false},
                {data: 'Description', orderable: false},
                {data: 'CodeSelector', orderable: false},
                {data: 'DescriptionSelector', orderable: false},
                {data: 'IdCostCenter', orderable: false}
            ]
        });
    }

    function fnUpdateTable(list) {
        var table = $('#tblProduct').DataTable();
        table.clear().rows.add(list).draw();
        table.on('order.dt search.dt', function () {
            table.column(0, {
                search: 'applied',
                order: 'applied'
            }).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    }

    function fnDeleteProduct(code) {
        var contextPath = $("#contextPath").val();
        consultarAjax("DELETE", contextPath + CONTEXT_PATH_PUBLIC + RP_DELETE_PRODUCT + code, '', contextPath, function (callbackDelete) {
            if (callbackDelete.Body.Error === undefined) {
                mostrarMensajeFlotante(callbackDelete.Body.Response.Message, NOTIFICACION_EXITO);

                fnProduct.getAllProducts(function (callbackGAC) {
                    if (callbackGAC.Body.Error === undefined) {
                        mostrarMensajeFlotante("Se actualizo la lista de productos.", NOTIFICACION_EXITO);

                        if (callbackGAC.Body.Response.Code === '1') {
                            fnProduct.fnUpdateTable(callbackGAC.Body.Response.CostCenterConfigurations);
                        } else {
                            mostrarMensajeFlotante(callbackGAC.Body.Response.Message, NOTIFICACION_WARN);
                        }

                    } else {
                        mostrarMensajeFlotante(callbackGAC.Body.Error.Message, NOTIFICACION_ERROR);
                    }
                });

            } else {
                mostrarMensajeFlotante(callbackDelete.Body.Error.Message, NOTIFICACION_ERROR);
            }
        });
    }

    return {
        fnRegisterProduct: fnRegisterProduct,
        getAllProducts: getAllProducts,
        fnRenderTable: fnRenderTable,
        fnUpdateTable: fnUpdateTable,
        fnDeleteProduct: fnDeleteProduct
    };

}();

