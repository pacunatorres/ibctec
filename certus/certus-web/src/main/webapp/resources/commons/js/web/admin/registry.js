/**
 * @fileoverview .
 *
 * @autor joanhuve@gmail.com
 * @version 1.0
 */
$(document).ready(function () {
    var contextPath = $("#contextPath").val();
    
    fnRegistry.getAllRegistry(function (callbackGAI) {
        if (callbackGAI.Body.Error === undefined) {
            mostrarMensajeFlotante(callbackGAI.Body.Response.Message, NOTIFICACION_EXITO);

            console.log(JSON.stringify(callbackGAI.Body.Response.Registrations))
            // fnRegistry.fnRenderTable(contextPath);
            // if (callbackGAI.Body.Response.Code === '1') {
            //     fnRegistry.fnUpdateTable(callbackGAI.Body.Response.Registrations);
            // } else {
            //     mostrarMensajeFlotante(callbackGAI.Body.Response.Message, NOTIFICACION_WARN);
            // }

        } else {
            mostrarMensajeFlotante(callbackGAI.Body.Error.Message, NOTIFICACION_ERROR);
        }
    });


});

var fnRegistry = function () {

    function getAllRegistry(callback) {
        var contextPath = $("#contextPath").val();
        var ip = JSON.parse(localStorage.getItem("ip"));
        var data = createHeader(RP_GET_ALL_REGISTRY, ip);
        consultarAjax("POST", (contextPath + CONTEXT_PATH_PUBLIC + RP_GET_ALL_REGISTRY), JSON.stringify(data), contextPath, function (callbackFilter) {
            callback(callbackFilter);
        });
    }

    return {
        getAllRegistry: getAllRegistry
    };

}();

