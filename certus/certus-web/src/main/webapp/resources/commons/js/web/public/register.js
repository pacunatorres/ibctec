/**
 * @fileoverview .
 *
 * @autor joanhuve@gmail.com
 * @version 1.0
 */
$(document).ready(function () {
    fnRegister.fnAddLoader();
    formRegister = $('#frmRegister');
    limpiarFormulario(formRegister);

    formRegister.formValidation({
        framework: 'bootstrap',
        err: {
            container: 'tooltip'
        },
        live: 'enabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            email: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese su email.'
                    },
                    emailAddress: {
                        message: 'Ingreser un email valido.'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese su contraseña.'
                    },
                    stringLength: {
                        min: 6,
                        message: 'Debe contener almenos 6 caracteres'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9][A-Za-z\d$@$!%*?+-/&]+$/i,
                        message: 'Ingreso un caracter no permitido'
                    }
                }
            },
            confirmPassword: {
                validators: {
                    notEmpty: {
                        message: 'Repita su contrase&ntilde;a.'
                    },
                    identical: {
                        field: 'password',
                        message: 'Las contraseñas no coinciden'
                    },
                    stringLength: {
                        min: 6,
                        message: 'Debe contener almenos 6 caracteres'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z0-9][A-Za-z\d$@$!%*?+-/&]+$/i,
                        message: 'Ingreso un caracter no permitido'
                    }
                }
            },
            'tc[]': {
                validators: {
                    choice: {
                        min: 1,
                        message: 'Debe aceptar los Terminos & Condiciones.'
                    }
                }
            }
        }
    }).on('err.field.fv', function (e, data) {
        // $('#messageModal').modal('show');
        // Get the tooltip

        var $icon = data.element.data('fv.icon');
        var title;
        if (typeof $icon.data('bs.tooltip') === 'undefined') {
            console.log('the property is not available...');
        } else {
            title = $icon.data('bs.tooltip').getTitle();
        }

        $icon.tooltip('destroy').tooltip({
            html: true,
            placement: 'right',
            title: title,
            container: 'body'
        });
    });

    $('#btnRegister').on('click', function (e) {
        $(this).prop("disabled", true);
        var bootstrapValidator = formRegister.data('formValidation');
        bootstrapValidator.validate();

        var validForm = bootstrapValidator.isValid();

        fnRegister.fnValidateExitsUser($('#email').val(), validForm, function (cb) {
            if (cb) {
                bootstrapValidator.updateMessage('email', 'notEmpty', 'El email ingresado ya existe');
                bootstrapValidator.updateStatus('email', 'INVALID').validateField('email');
                mostrarMensajeFlotante('El email ingresado ya existe, intenta recuperando tu contrase&ntilde;a', NOTIFICACION_WARN);
                $('#btnRegister').prop("disabled", false);
                $('#resetPassword').modal('show');
            } else {
                fnRegister.fnRegisterUser(function (cb) {
                    if (cb.Body.Error === undefined) {
                        mostrarMensajeFlotante(cb.Body.Response.Message, NOTIFICACION_EXITO);
                        fnRegister.callDestroyUser(function (callbackDU) {
                            if ('OK' == callbackDU) {
                                fnRedirectToLogin(cb);
                            } else {
                                console.log('Error destroy users');
                                fnRedirectToLogin(cb);
                            }
                        });
                        $('#btnRegister').prop("disabled", false);
                    } else {
                        mostrarMensajeFlotante(cb.Body.Error.Message, NOTIFICACION_ERROR);
                        $('#btnRegister').prop("disabled", false);
                    }
                })
            }
        });


    });

    $('#btnRegisterFacebook').on('click', function (e) {
        $(this).prop("disabled", true);
        document.location = localStorage.getItem("uriFacebook");
    });

    $('#btnRegisterGmail').on('click', function (e) {
        $(this).prop("disabled", true);
        document.location = localStorage.getItem("uriGoogle");
    });

    $('#btnReset').on('click', function (e) {
        $('#btnRegister').prop("disabled", false);
        var bootstrapValidator = formRegister.data('formValidation');
        var fields = bootstrapValidator.getOptions().fields,
            $parent, $icon;

        for (var field in fields) {
            $parent = $('[name="' + field + '"]').parents('.form-group');
            $icon = $parent.find('.form-control-feedback[data-fv-icon-for="' + field + '"]');
            $icon.tooltip('destroy');
        }

        bootstrapValidator.resetForm(true);
        bootstrapValidator.updateMessage('email', 'notEmpty', 'Ingrese un email.');
    });

    function fnRedirectToLogin(cb) {
        $("#cargador").fadeIn();
        setTimeout(function () {
            var urlRedirect = $("#contextPath").val() + cb.Body.Response.Url;
            console.log('redirect to: ' + urlRedirect)
            document.location = urlRedirect;
        }, 3000);
    }

    $("#spShowPwd").mouseover(function () {
        $("#password").attr('type', 'text');
    });
    $("#spShowPwd").mouseout(function () {
        $("#password").attr('type', 'password');
    });
    $("#spShowRePwd").mouseover(function () {
        $("#confirmPassword").attr('type', 'text');
    });
    $("#spShowRePwd").mouseout(function () {
        $("#confirmPassword").attr('type', 'password');
    });

});

var fnRegister = function () {

    function fnAddLoader() {
        var cargador = '<div id="cargador" style="display: none">';
        cargador += '<img id="cargador-imagen" src="' + $("#contextPath").val() + '/resources/commons/images/loader.svg"></img></div>';
        $divLoader = $(cargador).appendTo('body');
    }

    function fnValidateExitsUser(email, validForm, callback) {
        var contextPath = $("#contextPath").val();
        var ip = JSON.parse(localStorage.getItem("ip"));
        var data = createHeader(RP_VALIDATE_USER, ip);
        if (validForm && email != '') {
            data['Body'] = {'Request': {'User': {'Email': email}}};
            callValidateUser(data, contextPath, function (callbackVU) {
                if (callbackVU.Body.Response === undefined) {
                    mostrarMensajeFlotante(
                        'Ocurri&oacute; un error inesperado, por favor intente mas tarde nuevamente',
                        NOTIFICACION_ERROR
                    );
                } else {
                    callback(callbackVU.Body.Response.ExistUser);
                }
            });
        }
    }

    function fnRegisterUser(callback) {

        var contextPath = $("#contextPath").val();
        var ip = JSON.parse(localStorage.getItem("ip"));
        var data = createHeader(RP_REGISTER_USER, ip);

        data['Body'] = {
            'Request': {
                "User": {
                    "Email": $('#email').val(),
                    "Password": $('#password').val(),
                    "StateSubscription": $('#tc').is(":checked") ? '1' : '0'
                }
            }
        };

        consultarAjax("POST", (contextPath + CONTEXT_PATH_PUBLIC + RP_REGISTER_USER), JSON.stringify(data), contextPath, function (callbackVU) {
            callback(callbackVU);
        });

    }

    function callValidateUser(data, contextPath, callback) {
        consultarAjax("POST", (contextPath + CONTEXT_PATH_PUBLIC + RP_VALIDATE_USER), JSON.stringify(data), contextPath, function (callbackVU) {
            console.log(JSON.stringify(callbackVU));
            callback(callbackVU);
        });
    }

    function callDestroyUser(callback) {
        var contextPath = $('#contextPath').val();
        consultarAjax("GET", (contextPath + CONTEXT_PATH_PUBLIC + RP_DESTROY_USERS), '', contextPath, function (callbackDU) {
            callback(callbackDU);
        });
    }

    return {
        fnAddLoader: fnAddLoader,
        fnValidateExitsUser: fnValidateExitsUser,
        fnRegisterUser: fnRegisterUser,
        callDestroyUser: callDestroyUser
    };

}();

register = {
    cache: {
        ip: "",
        header: ""
    }
};