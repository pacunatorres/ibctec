/**
 * @fileoverview .
 *
 * @autor joanhuve@gmail.com
 * @version 1.0
 */
$(document).ready(function () {
    var contextPath = $("#contextPath").val();

    fnAccount.fnAddLoader();
    fnAccount.fnGetUserData(contextPath);

    $("#editProfile").click(function () {
        $('.tabAccount li.active').removeClass('active');
        $('.editProfile').addClass('active');
    });

    formChangePassword = $('#frmChangePassword');
    limpiarFormulario(formChangePassword);

    formChangePassword.formValidation({
        framework: 'bootstrap',
        err: {
            container: 'tooltip'
        },
        live: 'enabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            oldPassword: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese su contrase&ntilde;a anterior.'
                    }
                }
            },
            newPassword: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese su nueva contrase&ntilde;a.'
                    },
                    stringLength: {
                        min: 6,
                        message: 'Debe contener almenos 8 caracteres'
                    },
                    callback: {
                        callback: function (value, validator, $field) {
                            var oldPassword = $('#oldPassword').val();
                            if (oldPassword === value) {
                                return {
                                    valid: false,
                                    message: 'La contrase&ntilde;a nueva debe ser diferente a la anterior.'
                                }
                            } else {
                                return true;
                            }
                        }
                    }
                }
            },
            confirmPassword: {
                validators: {
                    notEmpty: {
                        message: 'Repita su nueva contrase&ntilde;a.'
                    },
                    identical: {
                        field: 'newPassword',
                        message: 'Las contraseñas no coinciden'
                    },
                    stringLength: {
                        min: 6,
                        message: 'Debe contener almenos 8 caracteres'
                    }
                }
            }
        }
    }).on('err.field.fv', function (e, data) {
        var $icon = data.element.data('fv.icon');
        var title;
        if (typeof $icon.data('bs.tooltip') === 'undefined') {
            console.log('the property is not available...');
        } else {
            title = $icon.data('bs.tooltip').getTitle();
        }
        $icon.tooltip('destroy').tooltip({
            html: true,
            placement: 'right',
            title: title,
            container: 'body'
        });
    });

    $('#btnAcceptUpdatePassword').on('click', function (e) {
        var bootstrapValidator = formChangePassword.data('formValidation');
        bootstrapValidator.validate();
        if (bootstrapValidator.isValid()) {
            $(this).prop("disabled", true);
            fnAccount.fnChangePassword(function (callbackCP) {
                if (callbackCP.Body.Error === undefined) {
                    mostrarMensajeFlotante(callbackCP.Body.Response.Message, NOTIFICACION_EXITO);
                    fnRedirectToLogin(callbackCP);
                } else {
                    $('#btnAcceptUpdatePassword').prop("disabled", false);
                    bootstrapValidator.resetForm(true);
                    mostrarMensajeFlotante(callbackCP.Body.Error.Message, NOTIFICACION_ERROR);
                }
            })
        }
    });

    function fnRedirectToLogin(cb) {
        $("#cargador").fadeIn();
        setTimeout(function () {
            var urlRedirect = $("#contextPath").val() + cb.Body.Response.Url;
            console.log('redirect to: ' + urlRedirect)
            document.location = urlRedirect;
        }, 3000);
    }

});

var fnAccount = function () {

    function fnAddLoader() {
        var cargador = '<div id="cargador" style="display: none">';
        cargador += '<img id="cargador-imagen" src="' + $("#contextPath").val() + '/resources/commons/images/loader.svg"></img></div>';
        $divLoader = $(cargador).appendTo('body');
    }

    function fnLoadSelectors() {
        construirSelect("selTipoDocumento", JSON.parse(localStorage.getItem("typeDocuments")));
        construirSelect("selGenero", JSON.parse(localStorage.getItem("genders")));
    }


    function fnGetUserData(contextPath) {

        var ip = JSON.parse(localStorage.getItem("ip"));
        var data = createHeader(RP_VALIDATE_USER, ip);
        consultarAjax("POST", (contextPath + CONTEXT_PATH_PUBLIC + RP_USER_DATA), JSON.stringify(data), contextPath, function (callbackUD) {
            if (callbackUD.Body.Error === undefined) {

                var userData = callbackUD.Body.Response.User;

                /*if (userData.DocumentType === 'R') {
                 $('#divInfoSocialNetwork').css('display', 'none');
                 $('#btnUpdatePassword').css('display', 'none');
                 } else {

                 $("#selTipoDocumento option").each(function () {
                 if ($(this).val() == userData.DocumentType) {
                 $(this).attr('selected', 'selected');
                 }
                 });
                 $('#selTipoDocumento').prop('disabled', true);
                 $('#selTipoDocumento').css('background-color', '#EBEBE5');
                 $('#txtNumeroDocumento').val(userData.DocumentNumber);
                 $('#txtNumeroDocumento').prop('disabled', true);

                 }*/

                $('#code').val(userData.Code);
                /*$('#spWelcomeNames').html(userData.Name + ' !');
                 $('#txtNombres').val(userData.Name);
                 $('#txtApellidoPaterno').val(userData.FirstLastName);
                 $('#txtApellidoMaterno').val(userData.MiddleLastName);

                 $('#txtEmail').val(userData.Email);
                 $('#txtEmail').prop('disabled', true);
                 $('#txtDireccion').val(userData.Address);
                 $("#selGenero option").each(function () {
                 if ($(this).val() == userData.Gender) {
                 $(this).attr('selected', 'selected');
                 }
                 });

                 var birthDate = (userData.BirthDate).split(' ')[0];
                 $('#txtFechaNacimiento').val(birthDate);
                 $('#txtTelFijo').val(userData.Phone);
                 $('#txtTelMovil').val(userData.MovilPhone);
                 $('#chkSubcribirme').prop('checked', userData.StateSubscription == '1' ? true : false);*/
            } else {
                mostrarMensajeFlotante(callbackUD.Body.Error.Message, NOTIFICACION_ERROR);
            }
        });
    }

    function fnUpdateUser(callback) {
        var contextPath = $("#contextPath").val();
        var ip = JSON.parse(localStorage.getItem("ip"));
        var data = createHeader(RP_UPDATE_USER, ip);
        var codeUser = $('#txtCode').val();

        data['Body'] = {
            'Request': {
                "User": {
                    "Name": $('#txtNombres').val(),
                    "FirstLastName": $('#txtApellidoPaterno').val(),
                    "MiddleLastName": $('#txtApellidoMaterno').val(),
                    "Address": $('#txtDireccion').val(),
                    "Gender": $('#selGenero').val(),
                    "BirthDate": $('#txtFechaNacimiento').val(),
                    "Phone": $('#txtTelFijo').val(),
                    "MovilPhone": $('#txtTelMovil').val(),
                    "StateSubscription": $('#chkSubcribirme').is(":checked") ? '1' : '0'
                }
            }
        };

        var urlRequest = (contextPath + CONTEXT_PATH_PUBLIC + RP_UPDATE_USER) + codeUser;
        consultarAjax("PUT", urlRequest, JSON.stringify(data), contextPath, function (callbackUU) {
            callback(callbackUU);
        });

    }

    function fnChangePassword(callback) {

        var contextPath = $("#contextPath").val();
        var ip = JSON.parse(localStorage.getItem("ip"));
        var data = createHeader(RP_CHANGE_PASSWORD, ip);
        var codeUser = $('#code').val();

        data['Body'] = {
            'Request': {
                "User": {
                    "Password": $('#oldPassword').val(),
                    "NewPassword": $('#newPassword').val()
                }
            }
        };


        var urlRequest = (contextPath + CONTEXT_PATH_PUBLIC + RP_CHANGE_PASSWORD) + codeUser;
        consultarAjax("PUT", urlRequest, JSON.stringify(data), contextPath, function (callbackCP) {
            callback(callbackCP);
        });

    }

    return {
        fnLoadSelectors: fnLoadSelectors,
        fnAddLoader: fnAddLoader,
        fnGetUserData: fnGetUserData,
        fnUpdateUser: fnUpdateUser,
        fnChangePassword: fnChangePassword
    };

}();

account = {
    cache: {
        ip: "",
        header: ""
    }
};