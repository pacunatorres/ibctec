/**
 * @fileoverview .
 *
 * @autor joanhuve@gmail.com
 * @version 1.0
 */
$(document).ready(function () {

    fnCheck.fnGetIp();
    fnCheck.fnGetMessageError();
    fnCheck.fnAddLoader();

    formCheck = $('#frmCheck');
    limpiarFormulario(formCheck);

    formCheck.formValidation({
        framework: 'bootstrap',
        err: {
            container: 'tooltip'
        },
        live: 'enabled',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese su usuario.'
                    },
                    emailAddress: {
                        message: 'Ingreser un email valido.'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'Ingrese su contraseña.'
                    }
                }
            }
        }
    }).on('err.field.fv', function (e, data) {
        var $icon = data.element.data('fv.icon');
        var title;
        if (typeof $icon.data('bs.tooltip') === 'undefined') {
            console.log('the property is not available...');
        } else {
            title = $icon.data('bs.tooltip').getTitle();
        }

        $icon.tooltip('destroy').tooltip({
            html: true,
            placement: 'right',
            title: title,
            container: 'body'
        });
    });

    $('#btnCheck').on('click', function (e) {
        $(this).prop("disabled", true);
        var bootstrapValidator = formCheck.data('formValidation');
        bootstrapValidator.validate();
        if (bootstrapValidator.isValid()) {
            $("#cargador").fadeIn();
            document.getElementById("frmCheck").submit();
        }
    });

    $(document).bind('keypress', function(e) {
        if(e.keyCode==13){
            $('#btnCheck').trigger('click');
        }
    });

});

var fnCheck = function () {

    function fnAddLoader() {
        var cargador = '<div id="cargador" class="cargador" style="display: none">';
        cargador += '<img id="cargador-imagen" class="cargador-imagen" src="' + $("#contextPath").val() + '/resources/commons/images/loader.svg"></img></div>';
        $divLoader = $(cargador).appendTo('body');
    }

    function fnGetMessageError() {
        var url = new URL(document.URL);
        var error = url.searchParams.get("error");
        if (error == 'true') {
            $('#divMessageError').css('display', 'block');
            $('#spMessageError').html(decode(url.searchParams.get("msg")));
            setTimeout(function () {
                $('#divMessageError').fadeOut("slow");
            }, 5000);
        }
    }

    function fnGetIp(callback) {
        var contextPath = $("#contextPath").val();
        $.getJSON("https://api.ipify.org?format=json", function (data) {
            localStorage.setItem("ip", JSON.stringify(data.ip));
        });
    }

    return {
        fnGetIp: fnGetIp,
        fnAddLoader: fnAddLoader,
        fnGetMessageError: fnGetMessageError
    };

}();