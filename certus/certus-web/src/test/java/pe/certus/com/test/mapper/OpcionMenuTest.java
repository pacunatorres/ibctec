package pe.certus.com.test.mapper;


import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import pe.certus.com.entity.OptionMenu;
import pe.certus.com.mapper.OptionMenuMapper;
import pe.certus.com.test.base.BaseTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import static org.junit.Assert.assertNotNull;

public class OpcionMenuTest extends BaseTest {

    @Autowired
    OptionMenuMapper opcionMenuMapper;

    private Logger LOGGER = Logger.getLogger(OpcionMenuTest.class.getName());

    @Test
    public void getOptionMenu() {
        List<OptionMenu> listOptionMenu = new ArrayList<>();
        try {

            List<String> profiles = new ArrayList<>();
            profiles.add("ROLE_ANONYMO");

            List<String> typeMenus = new ArrayList<>();
            typeMenus.add("PUBLIC");
            typeMenus.add("PUBLIC_SECONDARY");
            typeMenus.add("FOOTER_CLIENT_SERVICE");
            typeMenus.add("FOOTER_PROFILE");

            Map<String, Object> params = new HashMap<String, Object>();
            params.put("state", "1");
            params.put("typeMenus", typeMenus);
            params.put("profiles", profiles);

            listOptionMenu = opcionMenuMapper.getOptionMenu(params);
            for (OptionMenu om : listOptionMenu) {
                LOGGER.info(gson.toJson(om));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertNotNull(listOptionMenu);
    }

}
