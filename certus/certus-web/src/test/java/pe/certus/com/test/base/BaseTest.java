package pe.certus.com.test.base;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:/applicationContext-test.xml")
@TransactionConfiguration(defaultRollback = true)
public abstract class BaseTest {

    public Gson gson;

    public BaseTest() {
        gson = new GsonBuilder().setPrettyPrinting().create();
    }

}
