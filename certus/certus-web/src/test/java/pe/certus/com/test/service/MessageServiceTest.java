package pe.certus.com.test.service;

import pe.certus.com.constants.Constants;
import pe.certus.com.message.MessageMail;
import pe.certus.com.service.MessageService;
import pe.certus.com.test.base.BaseTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Locale;
import java.util.logging.Logger;

import static org.junit.Assert.assertNotNull;

public class MessageServiceTest extends BaseTest {

    @Autowired
    MessageService messageService;

    private Logger LOGGER = Logger.getLogger(MessageServiceTest.class.getName());

    @Test
    public void senMailMessage() {

        MessageMail messageMail = new MessageMail();

        try {
            Locale locale = new Locale("es", "pe");

            messageMail.setFullName("Leslie Lenny");
            messageMail.setUserName("llenny");
            messageMail.setEmail("joanhuve@gmail.com");
            messageMail.setPassword("123321");

            LOGGER.info("Response: " + messageService.sendMailFromTemplate(
                    Constants.TypesTransactionMessage.REGISTERUSER,
                    messageMail,
                    locale));

        } catch (Exception e) {
            e.printStackTrace();
        }

        assertNotNull(messageMail);
    }


}
