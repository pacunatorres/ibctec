package pe.certus.com.test.service;

import com.google.gson.Gson;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import pe.certus.com.request.Request;
import pe.certus.com.service.ConfigurationService;
import pe.certus.com.test.base.BaseTest;

import java.util.logging.Logger;

/**
 * Created by Jose Hurtado on 26/05/2018.
 */
public class ConfigurationServiceTest extends BaseTest {

    private Logger LOGGER = Logger.getLogger(ConfigurationServiceTest.class.getName());

    @Autowired
    ConfigurationService configurationService;

    @Test
    public void getAllAcademicConfiguration() throws Exception {
        Request request = new Request();
        request.setIpRequest("127.0.0.1");

        LOGGER.info(">> " + new Gson().toJson(
                configurationService.getAllAcademicConfiguration(request)));
    }

}
