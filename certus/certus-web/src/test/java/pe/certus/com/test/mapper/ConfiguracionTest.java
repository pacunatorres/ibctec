package pe.certus.com.test.mapper;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import pe.certus.com.entity.Configuration;
import pe.certus.com.mapper.ConfigurationMapper;
import pe.certus.com.test.base.BaseTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import static org.junit.Assert.assertNotNull;

/**
 * Created by jose.hurtado on 10/11/2017.
 */
public class ConfiguracionTest extends BaseTest {

    @Autowired
    ConfigurationMapper configurationMapper;

    private Logger LOGGER = Logger.getLogger(ConfiguracionTest.class.getName());

    @Test
    public void getOptionMenu() {
        List<Configuration> configuracionList = new ArrayList<>();
        try {
            List<String> keywords = new ArrayList<>();
            keywords.add("ABOUT_STORE");
            keywords.add("SOCIAL_NETWORKS");
            keywords.add("PAYMENT_METHODS");
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("state", "1");
            params.put("keywords", keywords);
            configuracionList = configurationMapper.getConfigurationByParams(params);
            for (Configuration om : configuracionList) {
                LOGGER.info(gson.toJson(om));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        assertNotNull(configuracionList);
    }


}
