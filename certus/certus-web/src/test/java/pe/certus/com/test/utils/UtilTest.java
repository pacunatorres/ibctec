package pe.certus.com.test.utils;

import pe.certus.com.constants.Constants;
import pe.certus.com.util.Decrypt;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;

/**
 * Created by jose.hurtado on 8/17/2017.
 */
public class UtilTest {

    @Test
    public void encryptDecryptPassword() throws Exception {
        String password = "123456";
        String passwordEncrypt = Decrypt.encrypt(password + Constants.PHRASE_KEY);
        System.out.println("-> " + passwordEncrypt);
        System.out.println("-> " + Decrypt.decrypt(passwordEncrypt).replace(Constants.PHRASE_KEY, StringUtils.EMPTY));
    }
    
}
