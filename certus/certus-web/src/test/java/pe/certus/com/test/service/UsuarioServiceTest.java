package pe.certus.com.test.service;

import com.google.gson.Gson;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import pe.certus.com.message.User;
import pe.certus.com.request.Request;
import pe.certus.com.service.UserService;
import pe.certus.com.test.base.BaseTest;

import java.io.File;
import java.util.logging.Logger;

import static org.junit.Assert.assertNotNull;
import static pe.certus.com.constants.Constants.CACHE_USER;

public class UsuarioServiceTest extends BaseTest {

    @Autowired
    UserService userService;

    private Logger LOGGER = Logger.getLogger(UsuarioServiceTest.class.getName());

    @Test
    public void getAuthenticateUserAndSaveSession() {
        User user = new User();
        Request request = new Request();
        try {
            user = userService.userInformation("jhurtado");
            request.setUser(user);
            String res = userService.createSession(request, "1234567");
            LOGGER.info(gson.toJson(res));

            // get data from session
            CacheManager manager = CacheManager.create();
            Cache cache = manager.getCache(CACHE_USER);
            Element element = cache.get("123456");
            if (element == null) {
                LOGGER.info("Dont have session");
                return;
            }

            Object value = element.getObjectValue();
            LOGGER.info(gson.toJson(value));
            System.out.println(new File(System.getProperty("java.io.tmpdir")).getAbsolutePath());
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertNotNull(user);
    }

    @Test
    public void updateByPrimaryKeySelective() throws Exception {
        Request request = new Request();
        User user = new User();
        user.setState("0");
        request.setUser(user);
        LOGGER.info(">> " + new Gson().toJson(userService.updateUserSelective("4", request)));
    }

    @Test
    public void getUserByCode() throws Exception {
        LOGGER.info(">> " + new Gson().toJson(userService.getUserByCode(4)));
    }

}
