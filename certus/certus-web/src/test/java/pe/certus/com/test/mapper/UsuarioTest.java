package pe.certus.com.test.mapper;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import pe.certus.com.dao.UserDao;
import pe.certus.com.entity.User;
import pe.certus.com.mapper.UserMapper;
import pe.certus.com.test.base.BaseTest;

import java.util.logging.Logger;

import static org.junit.Assert.assertNotNull;

public class UsuarioTest extends BaseTest {

    @Autowired
    UserMapper userMapper;

    @Autowired
    private UserDao userDao;

    private Logger LOGGER = Logger.getLogger(UsuarioTest.class.getName());

    @Test
    public void getUserByUserName() {
        User usuario = new User();
        try {
            usuario = userMapper.getUserByUserName("jhurtado", "1");
            LOGGER.info(gson.toJson(usuario));
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertNotNull(usuario);
    }


    @Test
    public void getUserByUserNameAndPassword() {
        LOGGER.info(">> " + userMapper.getUserByUserNameAndPassword("jhurtado",
                "RetvHzUi4hwSTPrMhMpV2g=="));
    }

}
