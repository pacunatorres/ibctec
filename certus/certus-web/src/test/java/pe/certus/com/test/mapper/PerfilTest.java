package pe.certus.com.test.mapper;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import pe.certus.com.entity.Role;
import pe.certus.com.mapper.RoleMapper;
import pe.certus.com.test.base.BaseTest;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static org.junit.Assert.assertNotNull;

public class PerfilTest extends BaseTest {

    @Autowired
    RoleMapper roleMapper;

    private Logger LOGGER = Logger.getLogger(PerfilTest.class.getName());

    @Test
    public void getOptionMenu() {
        List<Role> profileList = new ArrayList<>();
        try {
            profileList = roleMapper.getProfileByUserName("1", "jhurtado");
            for (Role om : profileList) {
                LOGGER.info(gson.toJson(om));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        assertNotNull(profileList);
    }

}
