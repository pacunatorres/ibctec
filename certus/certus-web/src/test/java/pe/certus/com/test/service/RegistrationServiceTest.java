package pe.certus.com.test.service;

import com.google.gson.Gson;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import pe.certus.com.request.Request;
import pe.certus.com.service.RegistrationService;
import pe.certus.com.test.base.BaseTest;

import java.util.logging.Logger;

/**
 * Created by jose.hurtado on 6/1/2018.
 */
public class RegistrationServiceTest extends BaseTest {

    @Autowired
    RegistrationService registrationService;

    private Logger LOGGER = Logger.getLogger(RegistrationServiceTest.class.getName());

    @Test
    public void getAllRegistration() throws Exception {
        Request request = new Request();
        request.setIpRequest("127.0.0.1");
        LOGGER.info(">> " + new Gson().toJson(registrationService.getAllRegistry(request)));
    }

}
