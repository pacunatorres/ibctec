package pe.certus.com.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import pe.certus.com.request.Request;
import pe.certus.com.response.Response;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({"request", "response"})
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Body<Req extends Request, Res extends Response> {

    @JsonProperty("Request")
    private Req request;

    @JsonProperty("Response")

    private Res response;

    @JsonProperty("Error")
    private Error error;

    public Req getRequest() {
        return request;
    }

    public void setRequest(Req request) {
        this.request = request;
    }

    public Res getResponse() {
        return response;
    }

    public void setResponse(Res response) {
        this.response = response;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        if (request != null) {
            sb.append("request=").append(request).append(",");
        }
        if (response != null) {
            sb.append("response=").append(response).append(",");
        }
        if (error != null) {
            sb.append("role=").append(error).append(",");
        }
        sb.append("]");

        return sb.toString();
    }
}
