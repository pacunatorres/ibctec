package pe.certus.com.exception;

import pe.certus.com.message.Header;
import org.springframework.security.core.AuthenticationException;

public class SalesGeneralException extends AuthenticationException {

    private static final long serialVersionUID = 3186105543822592034L;

    private String errorCode;
    private String title;
    private String type;
    private Header header;

    public SalesGeneralException(String message) {
        super(message);
    }

    public SalesGeneralException(String message, String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public SalesGeneralException(String message, String errorCode, Header header) {
        super(message);
        this.errorCode = errorCode;
        this.header = header;
    }

    public SalesGeneralException(String message, String errorCode, String title, String type) {
        super(message);
        this.errorCode = errorCode;
        this.title = title;
        this.type = type;
    }

    public SalesGeneralException(String message, String errorCode, Throwable t) {
        super(message, t);
        this.errorCode = errorCode;
    }

    public SalesGeneralException(String message, Throwable t) {
        super(message, t);
    }

    public String getErrorCode() {
        return this.errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Header getHeader() {
        return this.header;
    }
}

 