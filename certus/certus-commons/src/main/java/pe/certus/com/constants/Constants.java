package pe.certus.com.constants;

/**
 * Created by jose.hurtado on 8/17/2017.
 */
public class Constants {

    public static final String FLAG_ACTIVE = "1";
    public static final String FLAG_INACTIVE = "0";
    public static final String OK = "OK";
    public static final String PHRASE_KEY = "PSJN2018";
    public static final String ACCEPT_APPLICATION_JSON = "Accept=application/json";
    public static final String PRODUCES_APPLICATION_JSON_CHARSET_UTF_8 = "application/json; charset=utf-8";

    public static final String USER_GENERIC_PUBLIC = "public_web";
    public static final String KEYWORD_REDIRECT_URI = "REDIRECT_URI";
    public static final String KEYWORD_CLIENT_ID = "CLIENT_ID";
    public static final String PUBLIC_KEYWORD = "public";
    public static final String KEYWORD_TRUE = "true";
    public static final String KEYWORD_FALSE = "false";
    public static final String CACHE_USER = "cacheUserCERTUS";
    public static final String KEYWORKD_SESSION_USER = "user";
    public static final String KEYWORKD_SESSION_MENU = "menuList";
    public static final String EMAIL_TEMPLATE_ENCODING = "UTF-8";
    public static final String PNG_MIME = "image/png";

    public static final String PATTERN_USER_DATE = "dd-MM-yyyy HH:mm:ss";
    public static final String PATTERN_DATE = "MM-dd-yyyy";
    public static final String KEYWORD_ESTIMADO_CLIENTE = "Estimado Cliente";
    public static final String NULL_KEYWORD = "null";
    public static final String PIPE = "|";
    public static final String PARAMETER_ERROR_LOGIN = "?error=true&msg=";

    public enum TypeRole {
        ROLE_ADMIN,
        ROLE_VISITOR,
        ROLE_ANONYMO,
        ROLE_CLIENT
    }

    public enum TypeMenu {
        PRIVATE,
        PUBLIC,
        PUBLIC_SECONDARY,
        FOOTER_CLIENT_SERVICE,
        FOOTER_PROFILE,
        DESTINATIONS,
        SERVICES
    }

    public enum TypeTableConfiguration {
        COPYRIGHT,
        GENDER,
        LIMIT_AGE,
        PHONE_CONTACT,
        EMAIL_CONTACT,
        PRINCIPAL_ADDRESS
    }

    public enum TypesTransactionMessage {
        REGISTERUSER,
        CHANGEPASSWORD,
        REMEMBERPASSWORD;
    }

    public enum TypesUser {
        USER_WEB("W"),
        USER_SOCIAL_NETWORK("S");

        private String typeUser;

        TypesUser(String typeUser) {
            this.setTypeUser(typeUser);
        }

        public String getTypeUser() {
            return typeUser;
        }

        public void setTypeUser(String typeUser) {
            this.typeUser = typeUser;
        }
    }

    static public class ERROR_CODES {
        public static final String GENERIC_ERROR = "000";
        public static final String DATABASE_ERROR = "001";
        public static final String ERROR_REGISTER = "002";
        public static final String ERROR_VALIDATE_USER = "003";
        public static final String ERROR_GET_CONFIGURATIONS = "004";
        public static final String ERROR_GET_USER_FROM_CACHE = "005";
        public static final String ERROR_UPDATE = "006";
        public static final String ERROR_DELETE = "007";
        public static final String ERROR_SESSION_EXPIRED = "008";
        public static final String ERROR_GET_ALL_ROWS = "012";
        public static final String ERROR_GET_BY_CODE = "014";
    }

    static public class RESOURCE_PATH {
        public static final String LOAD_CONFIGURATION = "/LoadConfiguration";
        public static final String GET_USER = "/GetUser";
        public static final String GET_MENU_DASHBOARD = "/DashboardMenu";
        public static final String REGISTER_ACADEMIC_CONFIGURATION = "/RegisterAcademicConfiguration";
        public static final String GET_ALL_ACADEMIC_CONFIGURATION = "/GetAcademicConfigurations";
        public static final String DELETE_ACADEMIC_CONFIGURATION = "/DeleteAcademicConfiguration";
        public static final String REGISTER_SITE = "/RegisterSite";
        public static final String GET_ALL_SITES = "/GetSites";
        public static final String DELETE_SITE = "/DeleteSite";
        public static final String REGISTER_INTERVAL = "/RegisterInterval";
        public static final String GET_ALL_INTERVAL = "/GetIntervals";
        public static final String DELETE_INTERVAL = "/DeleteInterval";
        public static final String REGISTER_EXPLOITATION_UNIT = "/RegisterExploitation";
        public static final String GET_ALL_EXPLOITATION_UNITS = "/GetExploitations";
        public static final String DELETE_EXPLOITATION_UNIT = "/DeleteExploitation";
        public static final String REGISTER_PRODUCT = "/RegisterProduct";
        public static final String GET_ALL_PRODUCTS = "/GetProducts";
        public static final String DELETE_PRODUCT = "/DeleteProduct";
        public static final String REGISTER_DEPARTMENT = "/RegisterDepartment";
        public static final String GET_ALL_DEPARTMENT = "/GetDepartments";
        public static final String DELETE_DEPARTMENT = "/DeleteDepartment";
        public static final String GET_ALL_REGISTRY = "/GetRegistry";
    }

    static public class INTERVAL_STATUS {
        public static final String CLOSED = "CERRADO";
        public static final String OPENED = "ABIERTO";

    }

}
