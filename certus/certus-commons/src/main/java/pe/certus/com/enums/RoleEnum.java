package pe.certus.com.enums;

/**
 * @author jose.hurtado
 */
public enum RoleEnum {

    ROLE_ADMIN("ROLE_ADMIN"),
    ROLE_VISITOR("ROLE_VISITOR"),
    ROLE_ANONYMO("ROLE_ANONYMO"),
    ROLE_ANONYMOUS("ROLE_ANONYMOUS"),
    ROLE_CLIENT("ROLE_CLIENT");

    private String valor;

    RoleEnum(String valor) {
        this.setValor(valor);
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

}
