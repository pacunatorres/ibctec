package pe.certus.com.util;

import pe.certus.com.message.Body;
import pe.certus.com.message.Header;
import pe.certus.com.message.Parameters;
import pe.certus.com.message.Error;

public class ParameterUtil {

    public ParameterUtil() {

    }

    public static Parameters buildResponseError(Error error) {
        Parameters parameters = new Parameters();
        parameters.setBody(new Body());
        parameters.getBody().setError(error);
        return parameters;
    }

    public static Parameters buildResponseError(Header header, Error error) {
        Parameters parameters = new Parameters();
        parameters.setHeader(header);
        parameters.setBody(new Body());
        parameters.getBody().setError(error);
        return parameters;
    }
}
