package pe.certus.com.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * @author jose.hurtado
 */
public class Utils {

    private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);

    /***
     *
     * @param exception
     * @return captura el stactrace de un exception
     */
    public static String obtenerStackTraceFromException(Exception exception) {
        StringWriter stringWriter = new StringWriter();
        exception.printStackTrace(new PrintWriter(stringWriter, true));
        return stringWriter.toString();
    }

    /**
     * @return obtiene el tiempo que demoro un proceso
     */
    public static String obtenerTiempoTranscurrido(long tiempo) {
        String segundos = String.valueOf((int) (tiempo / 1000) % 60);
        String minutos = String.valueOf((int) ((tiempo / (1000 * 60)) % 60));
        String horas = String.valueOf((int) ((tiempo / (1000 * 60 * 60)) % 24));
        return horas.concat(":").concat(minutos).concat(":").concat(segundos);
    }

    /**
     * @param value
     * @return retorna encriptacion md5
     * @throws Exception
     */
    public static String getMD5(String value) {
        String md5 = StringUtils.EMPTY;
        try {
            byte[] bytesOfMessage = value.getBytes("UTF-8");
            MessageDigest md;
            md = MessageDigest.getInstance("MD5");
            byte[] theDigest = md.digest(bytesOfMessage);
            md5 = new String(Hex.encodeHex(theDigest));
        } catch (NoSuchAlgorithmException e) {
            LOGGER.info("< error al ejecutar algoritmo encriptacion MD5>");
        } catch (UnsupportedEncodingException e) {
            LOGGER.info("< error al codificar UTF-8 >");
        }
        return md5;
    }

    /**
     * @param ipAddress
     * @return retorna una ip en formato decimal
     */
    public static long stringToLong(String ipAddress) {
        String[] ipAddressInArray = ipAddress.split("\\.");
        long result = 0;
        for (int i = 0; i < ipAddressInArray.length; i++) {
            int power = 3 - i;
            int ip = Integer.parseInt(ipAddressInArray[i]);
            result += ip * Math.pow(256, power);
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(stringToLong("190.113.211.234"));
        // validar io
        System.out.println(validarIP("190.113.211.234"));
    }

    /**
     * @param decimal to string
     * @return
     */
    public static String decimalToString(long i) {
        return ((i >> 24) & 0xFF) + "." + ((i >> 16) & 0xFF) + "." + ((i >> 8) & 0xFF) + "." + (i & 0xFF);
    }

    /**
     * Retorna la IP del servidor o el nombre del host true = IP : false = Nombre
     *
     * @param opcion
     * @return String
     */
    public static String obtenerIpNombreHost(boolean opcion) {
        try {
            InetAddress objInetAddress = InetAddress.getLocalHost();
            return opcion ? objInetAddress.getHostAddress() : objInetAddress.getHostName();
        } catch (UnknownHostException e) {
            LOGGER.error("< error al obtener ip ---> ", e);
            return null;
        } catch (Exception e) {
            LOGGER.error("< error desconocido ---> ", e);
            return null;
        }
    }

    /**
     * @param cadena
     * @return retorna cadena sin caracteres
     */
    public static String removerCaracteresDeNumeros(String cadena) {
        return cadena.replaceAll("[^\\d.]", "");
    }

    /**
     * @return obtiene la fecha actual
     */
    public static String obtenerFechaActual() {
        long yourmilliseconds = System.currentTimeMillis();
        SimpleDateFormat sdf = new SimpleDateFormat("mm-dd-YYYY HH:mm");
        Date resultdate = new Date(yourmilliseconds);
        return sdf.format(resultdate);
    }

    public static boolean validarIP(String ip) {
        Pattern PATTERN_IP = Pattern.compile("^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");
        return StringUtils.isBlank(ip) ? false : PATTERN_IP.matcher(ip).matches();
    }

    /**
     * @param str
     * @return retorna una cadena sin acentos o simbolos caracteristicos del idioma espaniol
     */
    public static String removerAcentos(String str) {
        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }

    /**
     * @return imprime en log json identado
     * @jhurtado
     */
    public static String obtenerJsonFormateado(Object obj) {
        return new GsonBuilder().disableHtmlEscaping().setPrettyPrinting().create().toJson(obj);
    }

    public static String obtenerJson(Object obj) {
        return new Gson().toJson(obj);
    }

    public static Date parseStringToDate(String birthDate, String pattern) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat(pattern);
        return formatter.parse(birthDate);
    }

}
