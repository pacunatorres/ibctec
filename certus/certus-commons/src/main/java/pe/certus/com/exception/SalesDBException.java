package pe.certus.com.exception;

public class SalesDBException extends SalesGeneralException {

    private static final long serialVersionUID = -4938556712560828963L;

    public SalesDBException(String message) {
        super(message);
    }

    public SalesDBException(String message, String errorCode) {
        super(message, errorCode);
    }

    public SalesDBException(String message, String errorCode, Exception objException) {
        super(message, errorCode, objException);
    }

}
