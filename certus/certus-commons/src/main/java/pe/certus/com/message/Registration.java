package pe.certus.com.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by Avantica on 6/1/2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Registration implements Serializable {

    @JsonProperty("Start")
    private String start;
    @JsonProperty("End")
    private String end;
    @JsonProperty("IdExploitation")
    private String idExploitation;
    @JsonProperty("DescProg")
    private String descProg;
    @JsonProperty("CodProd")
    private String codProd;
    @JsonProperty("DescProd")
    private String descProd;
    @JsonProperty("DescCampus")
    private String descCampus;
    @JsonProperty("Classroom")
    private String classroom;
    @JsonProperty("DescUnitExploitation")
    private String descUnitExploitation;
    @JsonProperty("Today")
    private String today;
    @JsonProperty("Income")
    private String income;
    @JsonProperty("Outcome")
    private String outcome;
    @JsonProperty("ReunionType")
    private String reunionType;

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getIdExploitation() {
        return idExploitation;
    }

    public void setIdExploitation(String idExploitation) {
        this.idExploitation = idExploitation;
    }

    public String getDescProg() {
        return descProg;
    }

    public void setDescProg(String descProg) {
        this.descProg = descProg;
    }

    public String getCodProd() {
        return codProd;
    }

    public void setCodProd(String codProd) {
        this.codProd = codProd;
    }

    public String getDescProd() {
        return descProd;
    }

    public void setDescProd(String descProd) {
        this.descProd = descProd;
    }

    public String getDescCampus() {
        return descCampus;
    }

    public void setDescCampus(String descCampus) {
        this.descCampus = descCampus;
    }

    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }

    public String getDescUnitExploitation() {
        return descUnitExploitation;
    }

    public void setDescUnitExploitation(String descUnitExploitation) {
        this.descUnitExploitation = descUnitExploitation;
    }

    public String getToday() {
        return today;
    }

    public void setToday(String today) {
        this.today = today;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    public String getReunionType() {
        return reunionType;
    }

    public void setReunionType(String reunionType) {
        this.reunionType = reunionType;
    }
}
