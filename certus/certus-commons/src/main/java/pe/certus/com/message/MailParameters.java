package pe.certus.com.message;

import java.io.Serializable;

/**
 * Created by jose.hurtado on 11/20/2017.
 */
public class MailParameters implements Serializable {

    private String subject;
    private String fromEmail;
    private String toEmail;
    private String templateHtml;

    public MailParameters(String subject, String templateHtml) {
        this.subject = subject;
        this.templateHtml = templateHtml;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getToEmail() {
        return toEmail;
    }

    public void setToEmail(String toEmail) {
        this.toEmail = toEmail;
    }

    public String getTemplateHtml() {
        return templateHtml;
    }

    public void setTemplateHtml(String templateHtml) {
        this.templateHtml = templateHtml;
    }
}
