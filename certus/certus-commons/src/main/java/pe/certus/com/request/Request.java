package pe.certus.com.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import pe.certus.com.message.*;

import java.util.List;

/**
 * Created by jose.hurtado on 8/28/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Request {

    @JsonProperty("User")
    private User user;

    @JsonProperty("MenuList")
    private List<Menu> menuList;

    @JsonProperty("AcademicConfiguration")
    private AcademicConfiguration academicConfiguration;

    @JsonProperty("IntervalConfiguration")
    private IntervalConfiguration intervalConfiguration;

    @JsonProperty("CostCenterConfiguration")
    private CostCenterConfiguration costCenterConfiguration;

    private String ipRequest;

    public String getIpRequest() {
        return ipRequest;
    }

    public void setIpRequest(String ipRequest) {
        this.ipRequest = ipRequest;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Menu> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<Menu> menuList) {
        this.menuList = menuList;
    }

    public AcademicConfiguration getAcademicConfiguration() {
        return academicConfiguration;
    }

    public void setAcademicConfiguration(AcademicConfiguration academicConfiguration) {
        this.academicConfiguration = academicConfiguration;
    }

    public IntervalConfiguration getIntervalConfiguration() {
        return intervalConfiguration;
    }

    public void setIntervalConfiguration(IntervalConfiguration intervalConfiguration) {
        this.intervalConfiguration = intervalConfiguration;
    }

    public CostCenterConfiguration getCostCenterConfiguration() {
        return costCenterConfiguration;
    }

    public void setCostCenterConfiguration(CostCenterConfiguration costCenterConfiguration) {
        this.costCenterConfiguration = costCenterConfiguration;
    }
}
