package pe.certus.com.bean;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UsageLog implements Serializable {


    private static final long serialVersionUID = 1L;
    private String phone, deviceId, deviceType, deviceVersion, webService, duration, status;
    private Date time;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = "";
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceVersion() {
        return deviceVersion;
    }

    public void setDeviceVersion(String deviceVersion) {
        this.deviceVersion = deviceVersion;
    }

    public String getWebService() {
        return webService;
    }

    public void setWebService(String webService) {
        this.webService = webService;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public String toString() {
        StringBuilder message = new StringBuilder();
        message.append(getPhone() + ",")
                .append(getDeviceId() + ",")
                .append(getDeviceType() + ",")
                .append(getDeviceVersion() + ",")
                .append(getWebService() + ",")
                .append(getDuration() + ",")
                .append(getStatus() + ",");
        try {
            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            message.append(sdf.format(getTime()));
        } catch (Exception e) {
            message.append(getTime());
        }
        return message.toString();
    }
}
