package pe.certus.com.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by jose.hurtado on 10/11/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AcademicConfiguration implements Serializable {

    @JsonProperty("Code")
    private String code;
    @JsonProperty("Level")
    private String level;
    @JsonProperty("IpMarking")
    private String ipMarking;
    @JsonProperty("Accounting")
    private String accounting;
    @JsonProperty("IncomeToleranceInMinits")
    private Integer incomeToleranceInMinits;
    @JsonProperty("OutcomeToleranceInMinits")
    private Integer outcomeToleranceInMinits;
    @JsonProperty("BeforeTimeInMinits")
    private Integer beforeTimeInMinits;
    @JsonProperty("AfterTimeInMinits")
    private Integer afterTimeInMinits;
    @JsonProperty("Campus")
    private String campus;
    @JsonProperty("Building")
    private String building;
    @JsonProperty("Classroom")
    private String classroom;
    @JsonProperty("IpAddress")
    private String ipAddress;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getIpMarking() {
        return ipMarking;
    }

    public void setIpMarking(String ipMarking) {
        this.ipMarking = ipMarking;
    }

    public String getAccounting() {
        return accounting;
    }

    public void setAccounting(String accounting) {
        this.accounting = accounting;
    }

    public Integer getIncomeToleranceInMinits() {
        return incomeToleranceInMinits;
    }

    public void setIncomeToleranceInMinits(Integer incomeToleranceInMinits) {
        this.incomeToleranceInMinits = incomeToleranceInMinits;
    }

    public Integer getOutcomeToleranceInMinits() {
        return outcomeToleranceInMinits;
    }

    public void setOutcomeToleranceInMinits(Integer outcomeToleranceInMinits) {
        this.outcomeToleranceInMinits = outcomeToleranceInMinits;
    }

    public Integer getBeforeTimeInMinits() {
        return beforeTimeInMinits;
    }

    public void setBeforeTimeInMinits(Integer beforeTimeInMinits) {
        this.beforeTimeInMinits = beforeTimeInMinits;
    }

    public Integer getAfterTimeInMinits() {
        return afterTimeInMinits;
    }

    public void setAfterTimeInMinits(Integer afterTimeInMinits) {
        this.afterTimeInMinits = afterTimeInMinits;
    }

    public String getCampus() {
        return campus;
    }

    public void setCampus(String campus) {
        this.campus = campus;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getClassroom() {
        return classroom;
    }

    public void setClassroom(String classroom) {
        this.classroom = classroom;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}
