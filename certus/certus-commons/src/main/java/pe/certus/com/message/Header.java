package pe.certus.com.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Header implements Serializable {

    private static final long serialVersionUID = 124884959982604113L;

    @JsonProperty("ApplicationId")
    private String applicationId;

    @JsonProperty("ServiceName")
    private String serviceName;

    @JsonProperty("DeviceId")
    private String deviceId;

    @JsonProperty("DeviceType")
    private String deviceType;

    @JsonProperty("DeviceVersion")
    private String deviceVersion;

    @JsonProperty("AppVersion")
    private String appVersion;

    /**
     * @return the appVersion
     */
    public String getAppVersion() {
        return appVersion;
    }

    /**
     * @param appVersion the appVersion to set
     */
    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceVersion() {
        return deviceVersion;
    }

    public void setDeviceVersion(String deviceVersion) {
        this.deviceVersion = deviceVersion;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        if (applicationId != null) {
            sb.append("applicationId=").append(applicationId).append(",");
        }
        if (deviceId != null) {
            sb.append("deviceId=").append(deviceId).append(",");
        }
        if (deviceType != null) {
            sb.append("deviceType=").append(deviceType).append(",");
        }
        if (deviceVersion != null) {
            sb.append("deviceVersion=").append(deviceVersion).append(",");
        }
        if (appVersion != null) {
            sb.append("appVersion=").append(appVersion).append(",");
        }
        sb.append("]");

        return sb.toString();
    }
}
