package pe.certus.com.util;

import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Decrypt {

    final static String VECTORINIT = "H0U0R0T0A0D0O000";
    final static String ENCODING = "UTF-8";
    final static String ALGORITHM = "AES/CBC/PKCS5Padding";
    final static String DECRYPT_ALGORITHM = "AES/CBC/NoPadding";
    final static String ENCRYPTIONKEY = "V1E2R3G4A5R6A789";

    private static final Logger logger = LogManager.getLogger(Decrypt.class);

    public static String decrypt(String password) {
        String decrypt = new String();
        try {
            byte[] cipherText = Base64.decodeBase64(password.getBytes());
            decrypt = decrypt(cipherText, ENCRYPTIONKEY);
        } catch (Exception e) {
            logger.info(e.getMessage());
        }
        return decrypt;
    }

    private static String decrypt(byte[] cipherText, String encryptionKey) throws Exception {
        Cipher cipher = Cipher.getInstance(DECRYPT_ALGORITHM);
        SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes(ENCODING), "AES");
        cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(VECTORINIT.getBytes(ENCODING)));
        return new String(cipher.doFinal(cipherText), ENCODING).trim();
    }

    public static String encrypt(String plainText) throws Exception {
        SecretKeySpec key = new SecretKeySpec(ENCRYPTIONKEY.getBytes(ENCODING), "AES");
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(VECTORINIT.getBytes(ENCODING));
        cipher.init(Cipher.ENCRYPT_MODE, key, ivParameterSpec);
        byte[] result = cipher.doFinal(plainText.getBytes(ENCODING));
        return Base64.encodeBase64String(result);
    }

    public static void main(String[] args) throws Exception {
        System.out.println(encrypt("020381ENTEL"));
    }

}