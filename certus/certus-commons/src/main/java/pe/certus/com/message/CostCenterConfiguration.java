package pe.certus.com.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by jose.hurtado on 10/11/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CostCenterConfiguration implements Serializable {

    @JsonProperty("IdCostCenter")
    private String IdCostCenter;
    @JsonProperty("Code")
    private String code;
    @JsonProperty("Description")
    private String description;
    @JsonProperty("CodeSelector")
    private String codeSelector;
    @JsonProperty("DescriptionSelector")
    private String descriptionSelector;

    public String getIdCostCenter() {
        return IdCostCenter;
    }

    public void setIdCostCenter(String idCostCenter) {
        IdCostCenter = idCostCenter;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCodeSelector() {
        return codeSelector;
    }

    public void setCodeSelector(String codeSelector) {
        this.codeSelector = codeSelector;
    }

    public String getDescriptionSelector() {
        return descriptionSelector;
    }

    public void setDescriptionSelector(String descriptionSelector) {
        this.descriptionSelector = descriptionSelector;
    }
}
