package pe.certus.com.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import pe.certus.com.message.*;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jose.hurtado on 8/28/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response extends BaseResponse implements Serializable {

    @JsonProperty("User")
    private User user;

    @JsonProperty("MenuList")
    private List<Menu> menuList;

    @JsonProperty("Configurations")
    private List<Configuration> configurations;

    @JsonProperty("DocumentTypes")
    private List<GenericCombo> documentTypes;

    @JsonProperty("Genders")
    private List<GenericCombo> genders;

    @JsonProperty("Destinations")
    private List<GenericCombo> destinations;

    @JsonProperty("ExistUser")
    private boolean existUser;

    @JsonProperty("HtmlTemplateAccountUser")
    private String htmlTemplateAccountUser;

    @JsonProperty("Profiles")
    private List<GenericCombo> profiles;

    @JsonProperty("Users")
    private List<User> users;

    @JsonProperty("AcademicConfigurations")
    private List<AcademicConfiguration> academicConfigurations;

    @JsonProperty("IntervalConfigurations")
    private List<IntervalConfiguration> intervalConfigurations;

    @JsonProperty("CostCenterConfigurations")
    private List<CostCenterConfiguration> costCenterConfigurations;

    @JsonProperty("Registrations")
    private List<Registration> registrations;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Menu> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<Menu> menuList) {
        this.menuList = menuList;
    }

    public List<Configuration> getConfigurations() {
        return configurations;
    }

    public void setConfigurations(List<Configuration> configurations) {
        this.configurations = configurations;
    }

    public List<GenericCombo> getDocumentTypes() {
        return documentTypes;
    }

    public void setDocumentTypes(List<GenericCombo> documentTypes) {
        this.documentTypes = documentTypes;
    }

    public List<GenericCombo> getGenders() {
        return genders;
    }

    public void setGenders(List<GenericCombo> genders) {
        this.genders = genders;
    }

    public boolean isExistUser() {
        return existUser;
    }

    public void setExistUser(boolean existUser) {
        this.existUser = existUser;
    }

    public List<GenericCombo> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<GenericCombo> destinations) {
        this.destinations = destinations;
    }

    public String getHtmlTemplateAccountUser() {
        return htmlTemplateAccountUser;
    }

    public void setHtmlTemplateAccountUser(String htmlTemplateAccountUser) {
        this.htmlTemplateAccountUser = htmlTemplateAccountUser;
    }

    public List<GenericCombo> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<GenericCombo> profiles) {
        this.profiles = profiles;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<AcademicConfiguration> getAcademicConfigurations() {
        return academicConfigurations;
    }

    public void setAcademicConfigurations(List<AcademicConfiguration> academicConfigurations) {
        this.academicConfigurations = academicConfigurations;
    }

    public List<IntervalConfiguration> getIntervalConfigurations() {
        return intervalConfigurations;
    }

    public void setIntervalConfigurations(List<IntervalConfiguration> intervalConfigurations) {
        this.intervalConfigurations = intervalConfigurations;
    }

    public List<CostCenterConfiguration> getCostCenterConfigurations() {
        return costCenterConfigurations;
    }

    public void setCostCenterConfigurations(List<CostCenterConfiguration> costCenterConfigurations) {
        this.costCenterConfigurations = costCenterConfigurations;
    }

    public List<Registration> getRegistrations() {
        return registrations;
    }

    public void setRegistrations(List<Registration> registrations) {
        this.registrations = registrations;
    }
}
