package pe.certus.com.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jose.hurtado on 8/28/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Menu implements Serializable {

    @JsonProperty("MenuId")
    private Integer menuId;

    @JsonProperty("TypeMenu")
    private String typeMenu;

    @JsonProperty("OptionCode")
    private String optionCode;

    @JsonProperty("OptionParent")
    private String optionParent;

    @JsonProperty("Description")
    private String description;

    @JsonProperty("Url")
    private String url;

    @JsonProperty("Level")
    private String level;

    @JsonProperty("Order")
    private String order;

    @JsonProperty("State")
    private String state;

    @JsonProperty("IsAlive")
    private String isAlive;

    @JsonProperty("SubMenu")
    private List<Menu> subMenu;

    public Integer getMenuId() {
        return menuId;
    }

    public void setMenuId(Integer menuId) {
        this.menuId = menuId;
    }

    public String getTypeMenu() {
        return typeMenu;
    }

    public void setTypeMenu(String typeMenu) {
        this.typeMenu = typeMenu;
    }

    public String getOptionCode() {
        return optionCode;
    }

    public void setOptionCode(String optionCode) {
        this.optionCode = optionCode;
    }

    public String getOptionParent() {
        return optionParent;
    }

    public void setOptionParent(String optionParent) {
        this.optionParent = optionParent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getIsAlive() {
        return isAlive;
    }

    public void setIsAlive(String isAlive) {
        this.isAlive = isAlive;
    }

    public List<Menu> getSubMenu() {
        return subMenu;
    }

    public void setSubMenu(List<Menu> subMenu) {
        this.subMenu = subMenu;
    }
}
