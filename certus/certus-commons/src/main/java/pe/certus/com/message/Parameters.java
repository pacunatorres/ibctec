// Copyright 2014 ENTEL, All Rights Reserved.
package pe.certus.com.message;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Parameters<T extends Body> {

    @JsonProperty("Header")
    private Header header;

    @JsonProperty("Body")
    private T body;

    public Header getHeader() {
        return header;
    }

    public void setHeader(Header header) {
        this.header = header;
    }

    public T getBody() {
        return body;
    }

    public void setBody(T body) {
        this.body = body;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        if (header != null) {
            sb.append("header=").append(header).append(",");
        }
        if (body != null) {
            sb.append("body=").append(body).append(",");
        }
        sb.append("]");

        return sb.toString();
    }
}
