package pe.certus.com.service.impl;

import com.google.gson.Gson;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import pe.certus.com.config.ApplicationProperties;
import pe.certus.com.constants.Constants;
import pe.certus.com.dao.UserDao;
import pe.certus.com.exception.SalesGeneralException;
import pe.certus.com.message.Menu;
import pe.certus.com.message.User;
import pe.certus.com.request.Request;
import pe.certus.com.response.Response;
import pe.certus.com.service.UserService;
import pe.certus.com.util.UserUtil;
import pe.certus.com.util.Utils;

import java.util.List;

import static pe.certus.com.constants.Constants.CACHE_USER;
import static pe.certus.com.constants.Constants.ERROR_CODES;
import static pe.certus.com.util.UserUtil.getMenuFromJsonFormat;
import static pe.certus.com.util.UserUtil.getUserFromJsonFormat;

/**
 * Created by jose.hurtado on 8/18/2017.
 */
@Service
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserDao userDao;
    @Autowired
    private ApplicationProperties properties;

    @Override
    public User userInformation(String username) {
        pe.certus.com.entity.User usuario = userDao.getUserByUserName(username);
        LOGGER.info("< user information " + Utils.obtenerJsonFormateado(usuario) + " >");
        return getUser(usuario);
    }

    @Override
    @Cacheable(value = CACHE_USER, key = "#key")
    public String createSession(Request request, String key) {
        LOGGER.info("< create request session >");
        return new Gson().toJson(request);
    }

    @Override
    @CacheEvict(value = CACHE_USER, key = "#key")
    public void destroySession(String key) {
    }

    @Override
    public pe.certus.com.entity.User getUserByEmail(String email) throws SalesGeneralException {
        return userDao.getUserByEmail(email);
    }

    @Override
    public Response getSessionDataFromCache(boolean isDashboardMenu) throws SalesGeneralException {

        Response response = new Response();
        User user;
        List<Menu> listMenu;
        try {

            String sessionID = UserUtil.getSessionIDFromWebAuthenticationDetails();

            if (!isDashboardMenu) {
                user = getUserFromJsonFormat(sessionID);
                if (user == null) {
                    throwingSessionError();
                }
                response.setUser(user);
            } else {
                listMenu = getMenuFromJsonFormat(sessionID);
                if (listMenu == null) {
                    throwingSessionError();
                }
                response.setMenuList(listMenu);
            }

            LOGGER.info("user from session cache: " + Utils.obtenerJson(response));

        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.ERROR_GET_USER_FROM_CACHE);
        }
        return response;
    }

    @Override
    public Response updateUserSelective(String code, Request request) throws SalesGeneralException {
        Response response = new Response();
        try {

            pe.certus.com.entity.User userEntity = UserUtil.fillUserDataForUpdateSelective(code, request.getUser());
            LOGGER.info("Record for update user: " + Utils.obtenerJsonFormateado(userEntity));

            if (NumberUtils.INTEGER_ZERO < userDao.updateUserSelective(userEntity)) {
                response.setCode(NumberUtils.INTEGER_ONE.toString());
                response.setMessage("Se guardo satisfactoriamente.");
            } else {
                throw new SalesGeneralException("No se registraron los cambios", Constants.ERROR_CODES.ERROR_UPDATE);
            }

        } catch (SalesGeneralException eX) {
            throw new SalesGeneralException(eX.getMessage(), Constants.ERROR_CODES.ERROR_UPDATE);
        } catch (Exception e) {
        	e.printStackTrace();
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
        return response;
    }

    @Override
    public Response getUserByCode(Integer code) throws SalesGeneralException {
        Response response = new Response();

        try {

            LOGGER.info("user by code: " + code);
            pe.certus.com.entity.User userEntity = userDao.getUserByCode(code);

            if (userEntity == null) {
                throw new SalesGeneralException("Error obteniendo datos del usuario",
                        Constants.ERROR_CODES.ERROR_GET_BY_CODE);
            }

            response.setUser(getUser(userEntity));
            response.setCode(NumberUtils.INTEGER_ONE.toString());
            response.setMessage("Se obtuvieron datos del usuario satisfactoriamente.");

            LOGGER.info("user data: " + Utils.obtenerJson(response));

        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.ERROR_GET_BY_CODE);
        }
        return response;
    }

    public void throwingSessionError() {
        throw new SalesGeneralException(properties.getErrorMessageSessionExpired(),
                ERROR_CODES.ERROR_SESSION_EXPIRED);
    }

    private User getUser(pe.certus.com.entity.User userEntity) {
        User user = new User();
        user.setCode(userEntity.getIdUser().toString());
        user.setName(userEntity.getName());
        user.setFirstLastName(userEntity.getLastName());
        user.setEmail(userEntity.getEmail());
        user.setGender(userEntity.getGender());
        user.setPassword(userEntity.getPassword());
        user.setPhone(userEntity.getPhone().toString());
        user.setState(userEntity.getState());
        // TODO Implementar ultima sesion
//        if (userEntity.getUltimaSesion() != null) {
//            user.setLastSession(getReadableUserDates(PATTERN_USER_DATE, userEntity.getUltimaSesion()));
//        }
        return user;
    }

}
