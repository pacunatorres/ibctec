package pe.certus.com.config;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import pe.certus.com.constants.Constants;

import java.util.HashMap;
import java.util.Map;


@Configuration
public class ApplicationProperties implements InitializingBean {

    @Value("${url.login.private}")
    private String urlLoginPrivate;

    @Value("${url.successful.auth.admin}")
    private String pathSuccessfulLoginAdmin;

    @Value("${invalid.password.message}")
    private String invalidPasswordMessage;

    @Value("${url.redirect.dashboard.filter}")
    private String urlRedirectDashboardFilter;

    @Value("${error.message.register}")
    private String errorMessageRegister;

    @Value("${error.message.delete}")
    private String errorMessageDelete;

    @Value("${error.message.generic}")
    private String errorMessageGeneric;

    @Value("${error.message.database}")
    private String errorMessageDataBaseError;

    @Value("${error.message.exist.user}")
    private String errorMessageValidateExitsUser;

    @Value("${error.message.retrieve.configurations}")
    private String errorMessageGetConfigurations;

    @Value("${error.message.retrieve.user.cache}")
    private String errorMessageGetUserCache;

    @Value("${error.message.access.denied}")
    private String errorMessageAccessDenied;

    @Value("${error.message.session.exists}")
    private String errorMessageSessionExists;

    @Value("${error.message.session.expired}")
    private String errorMessageSessionExpired;

    @Value("${error.message.get.all.rows}")
    private String errorMessageGetAllRows;

    @Value("${error.message.get.user.bycode}")
    private String errorMessageGetUserByCode;

    @Value("${mail.from}")
    private String mailFrom;

    @Value("${mail.message.register.user}")
    private String mailMessageRegisterUser;

    @Value("${mail.message.change.password}")
    private String mailMessageChangePassword;

    @Value("${mail.message.remember.password}")
    private String mailMessageRememberPassword;

    @Value("${mail.template.register.user}")
    private String mailTemplateRegisterUser;

    @Value("${mail.template.change.password}")
    private String mailTemplateChangePassword;

    @Value("${mail.template.remember.password}")
    private String mailTemplateRememberPassword;

    @Value("${mail.is.html}")
    private boolean mailIsHtml;

    @Value("${mail.is.multipart}")
    private boolean mailIsMultipart;

    @Value("${mail.template.encoding}")
    private String mailTemplateEncoding;

    @Value("${session.timeout}")
    private Integer sessionTimeout;

    private Map<String, String> mapErrors;

    public String getUrlLoginPrivate() {
        return urlLoginPrivate;
    }

    public String getPathSuccessfulLoginAdmin() {
        return pathSuccessfulLoginAdmin;
    }

    public String getInvalidPasswordMessage() {
        return invalidPasswordMessage;
    }

    public String getErrorMessageRegister() {
        return errorMessageRegister;
    }

    public String getErrorMessageDelete() {
        return errorMessageDelete;
    }

    public String getErrorMessageGeneric() {
        return errorMessageGeneric;
    }

    public String getErrorMessageDataBaseError() {
        return errorMessageDataBaseError;
    }

    public String getErrorMessageValidateExitsUser() {
        return errorMessageValidateExitsUser;
    }

    public String getErrorMessageGetConfigurations() {
        return errorMessageGetConfigurations;
    }

    public String getErrorMessageAccessDenied() {
        return errorMessageAccessDenied;
    }

    public String getErrorMessageSessionExists() {
        return errorMessageSessionExists;
    }

    public String getErrorMessageGetUserCache() {
        return errorMessageGetUserCache;
    }

    public String getErrorMessageSessionExpired() {
        return errorMessageSessionExpired;
    }

    public String getMailMessageRegisterUser() {
        return mailMessageRegisterUser;
    }

    public String getMailMessageChangePassword() {
        return mailMessageChangePassword;
    }

    public String getMailMessageRememberPassword() {
        return mailMessageRememberPassword;
    }

    public String getMailFrom() {
        return mailFrom;
    }

    public String getMailTemplateRegisterUser() {
        return mailTemplateRegisterUser;
    }

    public String getMailTemplateChangePassword() {
        return mailTemplateChangePassword;
    }

    public String getMailTemplateRememberPassword() {
        return mailTemplateRememberPassword;
    }

    public boolean isMailIsHtml() {
        return mailIsHtml;
    }

    public boolean isMailIsMultipart() {
        return mailIsMultipart;
    }

    public String getMailTemplateEncoding() {
        return mailTemplateEncoding;
    }

    public Integer getSessionTimeout() {
        return sessionTimeout;
    }

    public String getUrlRedirectDashboardFilter() {
        return urlRedirectDashboardFilter;
    }

    public String getErrorMessageGetUserByCode() {
        return errorMessageGetUserByCode;
    }

    public String getErrorMessageGetAllRows() {
        return errorMessageGetAllRows;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        initializeErrorMap();
    }

    public Map<String, String> getMapErrors() {
        return mapErrors;
    }

    private void initializeErrorMap() {
        mapErrors = new HashMap<String, String>();
        mapErrors.put(Constants.ERROR_CODES.GENERIC_ERROR, getErrorMessageGeneric());
        mapErrors.put(Constants.ERROR_CODES.DATABASE_ERROR, getErrorMessageDataBaseError());
        mapErrors.put(Constants.ERROR_CODES.ERROR_REGISTER, getErrorMessageRegister());
        mapErrors.put(Constants.ERROR_CODES.ERROR_DELETE, getErrorMessageDelete());
        mapErrors.put(Constants.ERROR_CODES.ERROR_VALIDATE_USER, getErrorMessageValidateExitsUser());
        mapErrors.put(Constants.ERROR_CODES.ERROR_GET_CONFIGURATIONS, getErrorMessageGetConfigurations());
        mapErrors.put(Constants.ERROR_CODES.ERROR_GET_USER_FROM_CACHE, getErrorMessageGetUserCache());
        mapErrors.put(Constants.ERROR_CODES.ERROR_SESSION_EXPIRED, getErrorMessageSessionExpired());
        mapErrors.put(Constants.ERROR_CODES.ERROR_GET_ALL_ROWS, getErrorMessageGetAllRows());
        mapErrors.put(Constants.ERROR_CODES.ERROR_GET_BY_CODE, getErrorMessageGetUserByCode());
    }

}
