package pe.certus.com.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import pe.certus.com.constants.Constants;
import pe.certus.com.dao.UserDao;
import pe.certus.com.entity.OptionMenu;
import pe.certus.com.entity.Role;
import pe.certus.com.entity.User;
import pe.certus.com.exception.SalesDBException;
import pe.certus.com.exception.SalesGeneralException;
import pe.certus.com.repository.SalesRepository;

import java.util.List;

/**
 * Created by jose.hurtado on 8/17/2017.
 */
@Repository
public class UserDaoImpl implements UserDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);

    @Autowired
    SalesRepository salesRepository;

    @Override
    @Cacheable(value = "cacheMenu")
    public List<OptionMenu> getOptionMenu(List<String> profiles, List<String> typeMenus) {
        List<OptionMenu> opcionMenuList;
        try {

            opcionMenuList = salesRepository.getOptionMenu(profiles, typeMenus);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
        return opcionMenuList;
    }

    @Override
    public List<Role> getProfileByUserName(String userName) {
        List<Role> perfilList;
        try {

            perfilList = salesRepository.getProfileByUserName(userName);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
        return perfilList;
    }

    @Override
    public User getUserByUserName(String userName) {
        User usuario;
        try {

            usuario = salesRepository.getUserByUserName(userName);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
        return usuario;
    }

    @Override
    public boolean isValidLogin(String userName, String password) {
        try {
            return salesRepository.getUserByUserNameAndPassword(userName, password) == 1 ? true : false;
        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }

    @Override
    public User getUserByEmail(String email) throws SalesGeneralException {
        try {

            return salesRepository.getUserByEmail(email);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }

    @Override
    public List<Role> getAllRoles() throws SalesGeneralException {
        try {

            return salesRepository.getAllRoles();

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }


    @Override
    public Integer updateUserSelective(User userEntity) throws SalesGeneralException {
        try {

            return salesRepository.updateUserSelective(userEntity);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }

    @Override
    public User getUserByCode(Integer code) throws SalesGeneralException {
        try {

            return salesRepository.getUserByCode(code);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }
}
