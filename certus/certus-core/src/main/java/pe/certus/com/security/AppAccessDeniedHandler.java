package pe.certus.com.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;
import pe.certus.com.config.ApplicationProperties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AppAccessDeniedHandler implements AccessDeniedHandler {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    ApplicationProperties properties;

    @Override
    public void handle(HttpServletRequest request,
                       HttpServletResponse response,
                       AccessDeniedException accessDeniedException) throws IOException, ServletException {
        LOGGER.info("<inicio>");
        LOGGER.info("<redireccionando a pagina de error " + properties.getUrlLoginPrivate() + ">");
        response.sendRedirect(request.getContextPath() + properties.getUrlLoginPrivate());
        request.getSession().setAttribute("mensaje", "Ud. no tiene acceso para ingresar.");
        LOGGER.info("<fin>");
    }

}
