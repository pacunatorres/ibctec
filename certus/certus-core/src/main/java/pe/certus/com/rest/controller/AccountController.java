package pe.certus.com.rest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import pe.certus.com.constants.Constants;
import pe.certus.com.enums.ValidationExecution;
import pe.certus.com.exception.SalesGeneralException;
import pe.certus.com.message.Parameters;
import pe.certus.com.service.UserService;

/**
 * Created by jose.hurtado on 11/2/2017.
 */
@Controller
@Scope("request")
@RequestMapping(value = "/secure")
@Transactional(
        propagation = Propagation.REQUIRED,
        rollbackFor = Throwable.class
)
public class AccountController {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    private UserService userService;

    @ValidationExecution
    @RequestMapping(value = Constants.RESOURCE_PATH.GET_USER,
            method = RequestMethod.POST,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters getUserAccountData(@RequestParam(value = "code", required = false) Integer code,
                                         @RequestBody Parameters parameters) {
        try {

            parameters.getBody().setResponse(code != null
                    ? userService.getUserByCode(code)
                    : userService.getSessionDataFromCache(false));

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }

    @ValidationExecution
    @RequestMapping(value = Constants.RESOURCE_PATH.GET_MENU_DASHBOARD,
            method = RequestMethod.POST,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters getDashboardAccountMenu(@RequestBody Parameters parameters) {
        try {

            parameters.getBody().setResponse(userService.getSessionDataFromCache(true));

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }

}
