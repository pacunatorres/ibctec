package pe.certus.com.builder;

import pe.certus.com.config.ApplicationProperties;
import pe.certus.com.message.MailParameters;

/**
 * Created by jose.hurtado on 11/20/2017.
 */
public interface HtmlMessageBuilder {

    MailParameters mailParametersBuilder(String template);

    class MailParametersRegisterUser implements HtmlMessageBuilder {

        private ApplicationProperties properties;

        public MailParametersRegisterUser(ApplicationProperties properties) {
            this.properties = properties;
        }

        @Override
        public MailParameters mailParametersBuilder(String template) {
            return new MailParameters(properties.getMailMessageRegisterUser(), template);
        }

    }

    class MailParametersChangePassword implements HtmlMessageBuilder {

        private ApplicationProperties properties;

        public MailParametersChangePassword(ApplicationProperties properties) {
            this.properties = properties;
        }

        @Override
        public MailParameters mailParametersBuilder(String template) {
            return new MailParameters(properties.getMailMessageChangePassword(), template);
        }

    }

    class MailParametersRememberPassword implements HtmlMessageBuilder {

        private ApplicationProperties properties;

        public MailParametersRememberPassword(ApplicationProperties properties) {
            this.properties = properties;
        }

        @Override
        public MailParameters mailParametersBuilder(String template) {
            return new MailParameters(properties.getMailMessageRememberPassword(), template);
        }

    }

}
