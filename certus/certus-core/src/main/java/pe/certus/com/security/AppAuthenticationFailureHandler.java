package pe.certus.com.security;

import pe.certus.com.config.ApplicationProperties;
import pe.certus.com.constants.Constants;
import pe.certus.com.util.AuthenticationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.ProviderNotFoundException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static pe.certus.com.util.Util.encodeString;

/**
 * Created by Jose.Hurtado on 4/17/2018.
 */
public class AppAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    ApplicationProperties properties;

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationFailure(HttpServletRequest request,
                                        HttpServletResponse response,
                                        AuthenticationException exception) throws IOException, ServletException {


        String messageToSend;


        LOGGER.info("Exception throwing: " + exception.getClass());

        if (exception.getClass().isAssignableFrom(UsernameNotFoundException.class)) {
            messageToSend = encodeString(properties.getInvalidPasswordMessage());
        } else if (exception.getClass().isAssignableFrom(ProviderNotFoundException.class)) {
            messageToSend = encodeString(properties.getInvalidPasswordMessage());
        } else {
            messageToSend = encodeString(properties.getErrorMessageGeneric());
        }


        String refererUrl = request.getHeader("Referer");
        LOGGER.info("Failure login url request: " + refererUrl);

        String targetUrl = AuthenticationUtil.getTargetRedirect(
                properties,
                refererUrl).concat(Constants.PARAMETER_ERROR_LOGIN + messageToSend);

        LOGGER.info("Failure login url redirect: " + targetUrl);
        redirectStrategy.sendRedirect(request, response, targetUrl);

    }


}
