package pe.certus.com.aop;

import pe.certus.com.bean.UsageLog;
import pe.certus.com.message.Parameters;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

import java.util.Calendar;

@Aspect
public class PerformanceAspect {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    private final Environment env;

    public PerformanceAspect(Environment env) {
        this.env = env;
    }

    /**
     * Pointcut that matches all controllers with Parameters as return type.
     */
    @Pointcut("execution(public pe.certus.com.message.Parameters " +
            "pe.certus.com.rest.controller.*.*" +
            "(pe.certus.com.message.Parameters))")
    public void loggingPointcutParameters() {
        // Method is empty as this is just a Poincut, the implementations are in the advices.
    }

    @Pointcut(" execution(public * pe.certus.com.rest.controller.*.*(..)) " +
            "&& !execution(public pe.certus.com.message.Parameters " +
            "pe.certus.com.rest.controller.*.*(..))")
    public void loggingPointcut() {
        // Method is empty as this is just a Poincut, the implementations are in the advices.
    }

    /**
     * Advice that logs when a method is entered and exited.
     */
    @Around("loggingPointcutParameters()")
    public Object logAroundParameters(ProceedingJoinPoint joinPoint) throws Throwable {
        Calendar startDate = Calendar.getInstance();
        try {
            Object result = joinPoint.proceed();
            Parameters parameters = (Parameters) result;
            Calendar endDate = Calendar.getInstance();
            double duration = endDate.getTimeInMillis() - startDate.getTimeInMillis();
            String status = "OK";
            UsageLog usageLogBean = new UsageLog();
            usageLogBean.setDeviceId(parameters.getHeader().getDeviceId());
            usageLogBean.setDeviceType(parameters.getHeader().getDeviceType());
            usageLogBean.setDeviceVersion(parameters.getHeader().getDeviceVersion());
            usageLogBean.setWebService(parameters.getHeader().getServiceName());
            usageLogBean.setDuration(String.valueOf(duration));
            usageLogBean.setStatus(status);
            usageLogBean.setTime(startDate.getTime());
            LOG.info(usageLogBean.toString());
            parameters.getBody().setRequest(null);
            return parameters;
        } catch (IllegalArgumentException e) {
            LOG.error("Illegal arguments");
            throw e;
        }
    }

    @Around("loggingPointcut()")
    public Object logAround(ProceedingJoinPoint joinPoint) throws Throwable {
        Calendar startDate = Calendar.getInstance();
        try {
            Object result = joinPoint.proceed();
            Calendar endDate = Calendar.getInstance();
            double duration = endDate.getTimeInMillis() - startDate.getTimeInMillis();
            String status = "OK";
            LOG.info(duration + ", " + status);
            return result;
        } catch (IllegalArgumentException e) {
            LOG.error("Illegal arguments");
            throw e;
        }
    }
}
