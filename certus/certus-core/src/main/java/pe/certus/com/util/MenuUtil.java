package pe.certus.com.util;

import pe.certus.com.entity.OptionMenu;
import pe.certus.com.message.Menu;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jose.hurtado on 10/4/2017.
 */
public class MenuUtil {

    public static List<Menu> getMenu(List<OptionMenu> menuList) {

        List<Menu> menuArrayList = new ArrayList<Menu>();
        List<Menu> subMenuArrayList = new ArrayList<Menu>();
        Menu menu = null;
        for (OptionMenu om : menuList) {
            subMenuArrayList = fillSubMenuList(subMenuArrayList, om);
            menu = fillMenu(menuArrayList, subMenuArrayList, menu, om);
        }
        return menuArrayList;
    }

    private static Menu fillMenu(List<Menu> menuArrayList, List<Menu> subMenuArrayList, Menu menu, OptionMenu om) {
        if (subMenuArrayList.isEmpty()) {
            menu = new Menu();
            fillElementFromMenu(om, menu);
            menuArrayList.add(menu);
        } else {
            menu.setSubMenu(subMenuArrayList);
        }
        return menu;
    }

    private static List<Menu> fillSubMenuList(List<Menu> subMenuArrayList, OptionMenu om) {
        Menu subMenu;
        if (om.getCodOption().substring(0, 1).equals(om.getCodParent())) {
            subMenu = new Menu();
            fillElementFromMenu(om, subMenu);
            subMenuArrayList.add(subMenu);
        } else {
            subMenuArrayList = new ArrayList<Menu>();
        }
        return subMenuArrayList;
    }

    private static void fillElementFromMenu(OptionMenu om, Menu menu) {
        menu.setMenuId(om.getIdMenu().intValue());
        menu.setTypeMenu(om.getTypeMenu());
        menu.setOptionCode(om.getCodOption());
        menu.setOptionParent(om.getCodParent());
        menu.setDescription(om.getDescription());
        menu.setUrl(om.getUrl());
        menu.setLevel(om.getLevel());
        menu.setOrder(om.getOrder());
        menu.setIsAlive(om.getFlagSession());
    }


}
