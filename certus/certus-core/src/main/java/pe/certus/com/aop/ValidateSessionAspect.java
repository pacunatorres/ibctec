package pe.certus.com.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import pe.certus.com.config.ApplicationProperties;
import pe.certus.com.constants.Constants;
import pe.certus.com.dao.UserDao;
import pe.certus.com.entity.OptionMenu;
import pe.certus.com.enums.RoleEnum;
import pe.certus.com.message.Menu;
import pe.certus.com.message.Parameters;
import pe.certus.com.rest.error.CustomAccessDeniedException;
import pe.certus.com.rest.error.SessionExistException;
import pe.certus.com.util.MenuUtil;
import pe.certus.com.util.UserUtil;
import pe.certus.com.util.Utils;

import java.util.ArrayList;
import java.util.List;

import static pe.certus.com.util.UserUtil.getElementFromCache;

@Aspect
public class ValidateSessionAspect extends HandlerInterceptorAdapter {

    public static final String ACCOUNT_PATH = "account";
    public static final String DASHBOARD_PATH = "dashboard";
    public static final String LOGIN_PATH = "login";
    public static final String REGISTER_PATH = "register";
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    @Autowired
    UserDao userDao;
    private Environment env;
    @Autowired
    private ApplicationProperties properties;

    public ValidateSessionAspect(Environment env) {
        this.env = env;
    }

    @Pointcut("execution(* pe.certus.com.rest.controller.*.*initSecure*(..)) " +
            "|| @annotation(pe.certus.com.enums.ValidationExecution) ")
    public void controller() {
    }

    @Pointcut("execution(* pe.certus.com.rest.controller.*.*initialLoad*(..)) ")
    public void initialLoadConfigurations() {
    }

    @Before("controller()")
    public void logBefore(JoinPoint jp) throws CustomAccessDeniedException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        for (GrantedAuthority grantedAuthority : authentication.getAuthorities()) {

            String sessionID = ((WebAuthenticationDetails) authentication
                    .getDetails())
                    .getSessionId();

            if (isAnonymousRole(grantedAuthority)) {

                if (getElementFromCacheIsNull(sessionID)
                        && (validateUrlRequest(ACCOUNT_PATH) || validateUrlRequest(DASHBOARD_PATH))) {
                    LOGGER.info("Don't have session");
                    throw new CustomAccessDeniedException(properties.getErrorMessageAccessDenied());
                } else {
                    return;
                }

            } else {

                if (getElementFromCacheIsNull(sessionID)
                        && (validateUrlRequest(ACCOUNT_PATH) || validateUrlRequest(DASHBOARD_PATH))) {
                    LOGGER.info("Don't have session");
                    throw new CustomAccessDeniedException(properties.getErrorMessageAccessDenied());

                } else if (!getElementFromCacheIsNull(sessionID)
                        && (validateUrlRequest(REGISTER_PATH) || validateUrlRequest(LOGIN_PATH))) {
                    LOGGER.info("Already have session");
                    throw new SessionExistException(properties.getErrorMessageSessionExists());
                } else {
                    return;
                }
            }
        }
    }

    @After("initialLoadConfigurations()")
    public Object validateMenuSession(JoinPoint joinPoint) throws Throwable {

        try {

            Object result = joinPoint.getArgs()[0];
            Parameters parameters = (Parameters) result;
            parameters.getBody().getResponse().setMenuList(new ArrayList<Menu>());

            List<Menu> menuList = MenuUtil.getMenu(getOptionMenu());
            List<Menu> menuListRefill = new ArrayList<Menu>();

            String sessionID = UserUtil.getSessionIDFromWebAuthenticationDetails();

            boolean notHaveSession = getElementFromCacheIsNull(sessionID);

            refillMenuSession(menuList, menuListRefill, notHaveSession);

            LOGGER.info("menu session aspect : " + Utils.obtenerJson(menuListRefill));

            parameters.getBody().getResponse().setMenuList(menuListRefill);

            return parameters;
        } catch (IllegalArgumentException e) {
            LOGGER.error("Illegal arguments");
            throw e;
        }
    }

    private void refillMenuSession(List<Menu> menuList, List<Menu> menuListRefill, boolean notHaveSession) {
        for (int i = 0; i < menuList.size(); i++) {
            if (Constants.TypeMenu.PUBLIC_SECONDARY.name().equals(menuList.get(i).getTypeMenu())) {
                if (notHaveSession
                        && menuList.get(i).getIsAlive().equals(Constants.FLAG_ACTIVE)) {
                    LOGGER.info("item no added without session: " + Utils.obtenerJson(menuList.get(i)));
                } else if (!notHaveSession
                        && menuList.get(i).getIsAlive().equals(Constants.FLAG_INACTIVE)) {
                    LOGGER.info("item no added with session: " + Utils.obtenerJson(menuList.get(i)));
                } else {
                    menuListRefill.add(menuList.get(i));
                }
            } else {
                menuListRefill.add(menuList.get(i));
            }
        }
    }

    private List<OptionMenu> getOptionMenu() {
        List<String> roles = new ArrayList<String>();
        roles.add(Constants.TypeRole.ROLE_ANONYMO.toString());

        List<String> typeMenus = new ArrayList<String>();
        typeMenus.add(Constants.TypeMenu.PUBLIC.toString());
        typeMenus.add(Constants.TypeMenu.PUBLIC_SECONDARY.toString());
        typeMenus.add(Constants.TypeMenu.FOOTER_CLIENT_SERVICE.toString());
        typeMenus.add(Constants.TypeMenu.FOOTER_PROFILE.toString());
        typeMenus.add(Constants.TypeMenu.DESTINATIONS.toString());
        typeMenus.add(Constants.TypeMenu.SERVICES.toString());

        return userDao.getOptionMenu(roles, typeMenus);
    }

    private boolean validateUrlRequest(String path) {
        return ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest().getRequestURI().contains(path);
    }

    private boolean isAnonymousRole(GrantedAuthority grantedAuthority) {
        return grantedAuthority.getAuthority().equals(RoleEnum.ROLE_ANONYMOUS.toString());
    }

    private boolean getElementFromCacheIsNull(String sessionID) {
        return getElementFromCache(sessionID) == null;
    }


}
