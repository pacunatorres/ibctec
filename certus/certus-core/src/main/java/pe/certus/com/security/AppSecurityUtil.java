package pe.certus.com.security;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * 
 * @author jose.hurtado
 *
 */
public class AppSecurityUtil {

	public static boolean obtenerUsuarioLogueado() {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (!(auth instanceof AnonymousAuthenticationToken)) {
			return true;
		}

		return false;
	}

}
