package pe.certus.com.security;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import pe.certus.com.config.ApplicationProperties;
import pe.certus.com.constants.Constants;
import pe.certus.com.dao.UserDao;
import pe.certus.com.entity.Role;
import pe.certus.com.util.Decrypt;
import pe.certus.com.util.Utils;

import java.util.ArrayList;
import java.util.List;

public class AppAuthenticationProvider implements AuthenticationProvider {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());


    @Autowired
    ApplicationProperties properties;

    @Autowired
    UserDao userDao;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        LOGGER.info("< inicio >");

        // TODO REVISAR

        Long currentTime = null;

        Authentication auth = null;
        String username = authentication.getName();
        String password = authentication.getCredentials().toString();
        LOGGER.info("< Usuario :  " + username + ">");

        String usernameDE = Decrypt.decrypt(username);

        List<GrantedAuthority> grantedAuths;

        try {

            currentTime = System.currentTimeMillis();
            grantedAuths = new ArrayList<GrantedAuthority>();

            if (StringUtils.isNotBlank(usernameDE)) {

                List<Role> profiles = userDao.getProfileByUserName(usernameDE);
                for (Role profile : profiles) {
                    LOGGER.info("< Profile: " + Utils.obtenerJsonFormateado(profile) + " >");
                    grantedAuths.add(new SimpleGrantedAuthority(profile.getRole()));
                    auth = new UsernamePasswordAuthenticationToken(username, usernameDE, grantedAuths);
                }

                return auth;
            }

            password = Decrypt.encrypt(password + Constants.PHRASE_KEY);

            if (userDao.isValidLogin(username, password)) {

                List<Role> profiles = userDao.getProfileByUserName(username);
                for (Role profile : profiles) {
                    LOGGER.info("< Profile: " + Utils.obtenerJsonFormateado(profile) + " >");
                    grantedAuths.add(new SimpleGrantedAuthority(profile.getRole()));
                    auth = new UsernamePasswordAuthenticationToken(username, password, grantedAuths);
                }

                return auth;
            }

        } catch (Exception e) {
            LOGGER.info("Error : " + Utils.obtenerStackTraceFromException(e));
            return auth;
        } finally {
            LOGGER.info("<<< End authentication >>> " + "| tiempo de proceso (HH:MM:SS): " + Utils.obtenerTiempoTranscurrido(System.currentTimeMillis() - currentTime));
        }
        return auth;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

}
