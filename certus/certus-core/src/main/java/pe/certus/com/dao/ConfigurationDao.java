package pe.certus.com.dao;

import pe.certus.com.entity.AcademicConfiguration;
import pe.certus.com.entity.Classroom;
import pe.certus.com.entity.Configuration;
import pe.certus.com.entity.Interval;
import pe.certus.com.exception.SalesGeneralException;

import java.util.List;
import java.util.Map;

/**
 * Created by jose.hurtado on 10/11/2017.
 */
public interface ConfigurationDao {

    List<Configuration> getConfigurationsByParams(Map<String, Object> params) throws SalesGeneralException;

    Integer registerAcademicConfigurations(AcademicConfiguration academicConfiguration) throws SalesGeneralException;

    List<AcademicConfiguration> getAllAcademicConfigurationByStatus(String status) throws SalesGeneralException;

    Integer deleteAcademicConfiguration(Integer code) throws SalesGeneralException;

    Integer registerClassroom(Classroom classroom) throws SalesGeneralException;

    List<Classroom> getAllSitesByStatus(String status) throws SalesGeneralException;

    Integer deleteSite(Integer code) throws SalesGeneralException;

    Integer registerInterval(Interval interval) throws SalesGeneralException;

    List<Interval> getAllIntervalByStatus(String status) throws SalesGeneralException;

    Integer deleteInterval(Integer code) throws SalesGeneralException;

}
