package pe.certus.com.service;

import pe.certus.com.exception.SalesGeneralException;
import pe.certus.com.message.User;
import pe.certus.com.request.Request;
import pe.certus.com.response.Response;

/**
 * Created by jose.hurtado on 8/17/2017.
 */
public interface UserService {

    User userInformation(String username) throws SalesGeneralException;

    String createSession(Request request, String key) throws SalesGeneralException;

    void destroySession(String key);

    pe.certus.com.entity.User getUserByEmail(String email) throws SalesGeneralException;

    Response getSessionDataFromCache(boolean isDashboardMenu) throws SalesGeneralException;

    Response updateUserSelective(String code, Request request) throws SalesGeneralException;

    Response getUserByCode(Integer code) throws SalesGeneralException;

}
