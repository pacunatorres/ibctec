package pe.certus.com.util;

import org.apache.commons.codec.binary.Base64;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by jose.hurtado on 11/2/2017.
 */
public class Util {

    public static String getFullURL(HttpServletRequest request) {
        StringBuffer requestURL = request.getRequestURL();
        String queryString = request.getQueryString();

        if (queryString == null) {
            return requestURL.toString();
        } else {
            return requestURL.append('?').append(queryString).toString();
        }
    }

    public static Map<String, String> getQueryMap(String query) {
        String[] params = query.split("&");
        Map<String, String> map = new HashMap<String, String>();
        for (String param : params) {
            String name = param.split("=")[0];
            String value = param.split("=")[1];
            map.put(name, value);
        }
        return map;
    }

    public static String getPasswordAutoGenerated() {
        return String.valueOf(100000 + new Random().nextInt(99999 + 1));
    }

    public static String encodeString(String str) {
        byte[] bytesEncoded = Base64.encodeBase64(str.getBytes());
        return new String(bytesEncoded);
    }

    public static String decodeString(String str) {
        byte[] valueDecoded = Base64.decodeBase64(str);
        return new String(valueDecoded);
    }

}
