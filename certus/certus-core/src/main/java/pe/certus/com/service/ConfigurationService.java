package pe.certus.com.service;

import pe.certus.com.exception.SalesGeneralException;
import pe.certus.com.request.Request;
import pe.certus.com.response.Response;

public interface ConfigurationService {

    Response getConfigurations(boolean isBackEndRequest) throws SalesGeneralException;

    Response registerAcademicConfiguration(Request request) throws SalesGeneralException;

    Response getAllAcademicConfiguration(Request request) throws SalesGeneralException;

    Response deleteAcademicConfiguration(Integer code) throws SalesGeneralException;

    Response registerSite(Request request, String macAddress) throws SalesGeneralException;

    Response getAllSites(Request request) throws SalesGeneralException;

    Response deleteSite(Integer code) throws SalesGeneralException;

    Response registerInterval(Request request) throws SalesGeneralException;

    Response getAllInterval(Request request) throws SalesGeneralException;

    Response deleteInterval(Integer code) throws SalesGeneralException;

}
