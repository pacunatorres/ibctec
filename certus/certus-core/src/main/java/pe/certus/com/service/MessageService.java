package pe.certus.com.service;

import pe.certus.com.constants.Constants;
import pe.certus.com.exception.SalesGeneralException;
import pe.certus.com.message.MessageMail;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Locale;

/**
 * Created by jose.hurtado on 11/20/2017.
 */
public interface MessageService {

    boolean sendMailFromTemplate(Constants.TypesTransactionMessage typesTransactionMessage,
                                 MessageMail messageMail,
                                 Locale locale) throws SalesGeneralException, MessagingException, IOException;

    void sendMailFromTemplateAsync(Constants.TypesTransactionMessage typesTransactionMessage,
                                      MessageMail messageMail,
                                      Locale locale) throws SalesGeneralException, MessagingException, IOException;

}
