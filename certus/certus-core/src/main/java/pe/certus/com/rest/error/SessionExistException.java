package pe.certus.com.rest.error;

/**
 * Created by jose.hurtado on 7/11/2017.
 */
public class SessionExistException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private String message;

    public SessionExistException(String msg) {
        super(msg);
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}