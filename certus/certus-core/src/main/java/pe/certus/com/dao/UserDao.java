package pe.certus.com.dao;

import pe.certus.com.entity.OptionMenu;
import pe.certus.com.entity.Role;
import pe.certus.com.entity.User;
import pe.certus.com.exception.SalesGeneralException;

import java.util.List;

/**
 * Created by jose.hurtado on 8/17/2017.
 */
public interface UserDao {

    List<OptionMenu> getOptionMenu(List<String> profiles, List<String> typeMenus) throws SalesGeneralException;

    List<Role> getProfileByUserName(String userName) throws SalesGeneralException;

    User getUserByUserName(String userName) throws SalesGeneralException;

    boolean isValidLogin(String userName, String password) throws SalesGeneralException;

    User getUserByEmail(String email) throws SalesGeneralException;

    List<Role> getAllRoles() throws SalesGeneralException;

    Integer updateUserSelective(pe.certus.com.entity.User usuario) throws SalesGeneralException;

    User getUserByCode(Integer code) throws SalesGeneralException;

}
