package pe.certus.com.rest.controller;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pe.certus.com.constants.Constants;
import pe.certus.com.enums.ValidationExecution;
import pe.certus.com.exception.SalesGeneralException;
import pe.certus.com.message.Body;
import pe.certus.com.message.Parameters;
import pe.certus.com.request.Request;
import pe.certus.com.rest.validator.GenericFormValidator;
import pe.certus.com.service.ConfigurationService;
import pe.certus.com.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;

/**
 * Created by jose.hurtado on 11/2/2017.
 */
@Controller
@Scope("request")
@RequestMapping(value = "/secure")
@Transactional(
        propagation = Propagation.REQUIRED,
        rollbackFor = Throwable.class
)
public class ConfigurationController {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    private UserService userService;

    @Autowired
    @Qualifier("globalValidator")
    private GenericFormValidator globalValidator;

    @Autowired
    private ConfigurationService configurationService;

    @ValidationExecution
    @RequestMapping(value = Constants.RESOURCE_PATH.REGISTER_ACADEMIC_CONFIGURATION,
            method = RequestMethod.POST,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters registerAcademicConfiguration(@RequestBody Parameters parameters,
                                                    BindingResult bindingResult) {
        try {

            globalValidator.validate(parameters, bindingResult);

            if (bindingResult.hasErrors()) {
                throw new SalesGeneralException("Ocurrio un error en el registro de configuracion academica,",
                        Constants.ERROR_CODES.ERROR_REGISTER);
            } else {
                parameters.getBody().getRequest().setIpRequest(parameters.getHeader().getDeviceId());
                parameters.getBody().setResponse(
                        configurationService.registerAcademicConfiguration(parameters.getBody().getRequest())
                );

            }

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }

    @ValidationExecution
    @RequestMapping(value = Constants.RESOURCE_PATH.GET_ALL_ACADEMIC_CONFIGURATION,
            method = RequestMethod.POST,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters getAllAcademicConfiguration(@RequestBody Parameters parameters) {
        try {

            parameters.getBody().setRequest(new Request());
            parameters.getBody().getRequest().setIpRequest(parameters.getHeader().getDeviceId());
            parameters.getBody().setResponse(configurationService.getAllAcademicConfiguration(
                    parameters.getBody().getRequest()
            ));

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }

    @ValidationExecution
    @RequestMapping(value = Constants.RESOURCE_PATH.DELETE_ACADEMIC_CONFIGURATION + "/{code}",
            method = RequestMethod.DELETE,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters deleteAcademicConfiguration(@PathVariable("code") Integer code) {
        Parameters parameters = new Parameters();
        try {

            parameters.setBody(new Body());
            parameters.getBody().setResponse(configurationService.deleteAcademicConfiguration(code));

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }

    @ValidationExecution
    @RequestMapping(value = Constants.RESOURCE_PATH.REGISTER_SITE,
            method = RequestMethod.POST,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters registerSite(@RequestBody Parameters parameters,
                                   BindingResult bindingResult,
                                   HttpServletRequest request) {
        try {

            globalValidator.validate(parameters, bindingResult);

            if (bindingResult.hasErrors()) {
                throw new SalesGeneralException("Ocurrio un error en el registro de sedes,",
                        Constants.ERROR_CODES.ERROR_REGISTER);
            } else {

                String ipAddress;
                String macAddress = StringUtils.EMPTY;
                if (request.getHeader("X-FORWARDED-FOR") == null) {
                    ipAddress = request.getRemoteAddr();
                    macAddress = getMACAddress(ipAddress);
                }

                parameters.getBody().getRequest().setIpRequest(parameters.getHeader().getDeviceId());
                parameters.getBody().setResponse(
                        configurationService.registerSite(
                                parameters.getBody().getRequest(),
                                macAddress
                        )
                );

            }

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }

    private String getMACAddress(String ip) {
        String str = "";
        String macAddress = "";
        try {
            Process p = Runtime.getRuntime().exec("nbtstat -A " + ip);
            InputStreamReader ir = new InputStreamReader(p.getInputStream());
            LineNumberReader input = new LineNumberReader(ir);
            for (int i = 1; i < 100; i++) {
                str = input.readLine();
                if (str != null) {
                    if (str.indexOf("MAC Address") > 1) {
                        macAddress = str.substring(str.indexOf("MAC Address") + 14, str.length());
                        break;
                    }
                }
            }
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return macAddress;
    }

    @ValidationExecution
    @RequestMapping(value = Constants.RESOURCE_PATH.GET_ALL_SITES,
            method = RequestMethod.POST,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters getAllSites(@RequestBody Parameters parameters) {
        try {

            parameters.getBody().setRequest(new Request());
            parameters.getBody().getRequest().setIpRequest(parameters.getHeader().getDeviceId());
            parameters.getBody().setResponse(configurationService.getAllSites(
                    parameters.getBody().getRequest()
            ));

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }

    @ValidationExecution
    @RequestMapping(value = Constants.RESOURCE_PATH.DELETE_SITE + "/{code}",
            method = RequestMethod.DELETE,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters deleteSite(@PathVariable("code") Integer code) {
        Parameters parameters = new Parameters();
        try {

            parameters.setBody(new Body());
            parameters.getBody().setResponse(configurationService.deleteSite(code));

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }

    @ValidationExecution
    @RequestMapping(value = Constants.RESOURCE_PATH.REGISTER_INTERVAL,
            method = RequestMethod.POST,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters registerInterval(@RequestBody Parameters parameters,
                                       BindingResult bindingResult) {
        try {

            globalValidator.validate(parameters, bindingResult);

            if (bindingResult.hasErrors()) {
                throw new SalesGeneralException("Ocurrio un error en el registro de periodos,",
                        Constants.ERROR_CODES.ERROR_REGISTER);
            } else {

                parameters.getBody().getRequest().setIpRequest(parameters.getHeader().getDeviceId());
                parameters.getBody().setResponse(
                        configurationService.registerInterval(
                                parameters.getBody().getRequest()
                        )
                );

            }

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }


    @ValidationExecution
    @RequestMapping(value = Constants.RESOURCE_PATH.GET_ALL_INTERVAL,
            method = RequestMethod.POST,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters getAllInteval(@RequestBody Parameters parameters) {
        try {

            parameters.getBody().setRequest(new Request());
            parameters.getBody().getRequest().setIpRequest(parameters.getHeader().getDeviceId());
            parameters.getBody().setResponse(configurationService.getAllInterval(
                    parameters.getBody().getRequest()
            ));

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }

    @ValidationExecution
    @RequestMapping(value = Constants.RESOURCE_PATH.DELETE_INTERVAL + "/{code}",
            method = RequestMethod.DELETE,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters deleteInterval(@PathVariable("code") Integer code) {
        Parameters parameters = new Parameters();
        try {

            parameters.setBody(new Body());
            parameters.getBody().setResponse(configurationService.deleteInterval(code));

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }


}

