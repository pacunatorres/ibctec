package pe.certus.com.rest.validator;

import org.springframework.validation.Validator;

public interface GenericFormValidator extends Validator {

    String PATTERN_RUC = "^[0-9]{11}$";
    String PATTERN_DNI = "^[0-9]{8}$";
    String PATTERN_CARNET_MILITAR = "^[A-Za-z0-9\\s\\.\\-]{3,12}$";
    String PATTERN_PASAPORTE = "^[A-Za-z0-9\\s\\.\\-]{3,12}$";
    String PATTERN_CARNET_EXTRANJERIA = "^[A-Za-z0-9\\s\\.\\-]{3,12}$";
    String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    String PATTERN_LETTERS_SPACES = "^[\\p{L} .'-]+$";
    String PATTERN_LETTERS = "(?<=\\s|^)[a-zA-Z]*(?=[.,;:]?\\s|$)";
    String PATTERN_DIGITS = "\\d+";
    String PATTERN_ALPHANUMERIC = "^\\s*[\\da-zA-Z][\\da-zA-Z\\s]*$";
    String PATTERN_DATE = "^\\d{2}-\\d{2}-\\d{4}$";
    String PATTERN_PASSWORD = "^[a-zA-Z0-9][A-Za-z\\d$@$!%*?+-/&]+$";

}
