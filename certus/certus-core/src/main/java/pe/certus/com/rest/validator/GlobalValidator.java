package pe.certus.com.rest.validator;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import pe.certus.com.message.Parameters;
import pe.certus.com.request.Request;

import java.util.regex.Pattern;

@Component
@Scope("prototype")
public class GlobalValidator implements GenericFormValidator {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    MessageSource messageSource;

    private static boolean isValidPattern(String email, final String regex) {
        return StringUtils.isBlank(email)
                ? false
                : Pattern.compile(regex).matcher(email).matches();
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }

    @Override
    public void validate(Object target, Errors errores) {
        LOGGER.info("validate: init");

        Parameters parameters = (Parameters) target;

        String serviceName = parameters.getHeader().getServiceName();
        Request request = parameters.getBody().getRequest();

        // TODO implementar validaciones por endpoint


        LOGGER.info("validate: fin");
    }

    private void validateEmail(Errors errores, String email) {
        if (!isValidPattern(email, PATTERN_EMAIL)) {
            errores.reject("Email", "Debe ingresar un correo valido.");
        }
    }

    private void validateLetterAndSpaces(Errors errores, String letters) {
        if (!isValidPattern(letters, PATTERN_LETTERS_SPACES)) {
            errores.reject("AlfabeticoConEspacio", "Debe ingresar letras y espacios.");
        }
    }

    private void validateOnlyLetter(Errors errores, String letters) {
        if (!isValidPattern(letters, PATTERN_LETTERS)) {
            errores.reject("Alfabetico", "Debe ingresar solo letras.");
        }
    }

    private void validateDigits(Errors errores, String digits) {
        if (StringUtils.isNotBlank(digits) && !isValidPattern(digits, PATTERN_DIGITS)) {
            errores.reject("Digitos", "Debe ingresar solo digitos.");
        }
    }

    private void validateAlphanumeric(Errors errores, String alphanumeric) {
        if (StringUtils.isNotBlank(alphanumeric) && !isValidPattern(alphanumeric, PATTERN_ALPHANUMERIC)) {
            errores.reject("Alfanumerico", "Debe ingresar solo valores alfanumericos.");
        }
    }

    private void validateDate(Errors errores, String date) {
        if (!isValidPattern(date, PATTERN_DATE)) {
            errores.reject("Fecha", "Debe ingresar una fecha valida.");
        }
    }

    private void validatePassword(Errors errores, String date) {
        if (!isValidPattern(date, PATTERN_PASSWORD)) {
            errores.reject("Contrasenia", "Debe ingresar una contrasenia correcta.");
        }
    }
}
