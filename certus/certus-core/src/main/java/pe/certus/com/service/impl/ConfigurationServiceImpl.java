package pe.certus.com.service.impl;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import pe.certus.com.config.ApplicationProperties;
import pe.certus.com.constants.Constants;
import pe.certus.com.dao.ConfigurationDao;
import pe.certus.com.dao.UserDao;
import pe.certus.com.entity.Classroom;
import pe.certus.com.entity.Interval;
import pe.certus.com.entity.Role;
import pe.certus.com.exception.SalesGeneralException;
import pe.certus.com.message.AcademicConfiguration;
import pe.certus.com.message.Configuration;
import pe.certus.com.message.GenericCombo;
import pe.certus.com.message.IntervalConfiguration;
import pe.certus.com.request.Request;
import pe.certus.com.response.Response;
import pe.certus.com.service.ConfigurationService;
import pe.certus.com.util.UserUtil;
import pe.certus.com.util.Utils;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static pe.certus.com.constants.Constants.INTERVAL_STATUS.CLOSED;
import static pe.certus.com.util.UserUtil.getUserFromJsonFormat;

@Service
public class ConfigurationServiceImpl implements ConfigurationService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ConfigurationDao configurationDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private ApplicationProperties properties;

    @Override
    @Cacheable(value = "cacheConfigurations")
    public Response getConfigurations(boolean isBackEndRequest) throws SalesGeneralException {
        return isBackEndRequest ? loadMinimalConfigurations() : loadConfigurations();
    }

    private Response loadMinimalConfigurations() {
        Response response = new Response();
        try {

            List<String> keywords = new ArrayList<String>();

            keywords.add(Constants.TypeTableConfiguration.COPYRIGHT.toString());
            keywords.add(Constants.TypeTableConfiguration.GENDER.toString());
            keywords.add(Constants.TypeTableConfiguration.LIMIT_AGE.toString());
            keywords.add(Constants.TypeTableConfiguration.EMAIL_CONTACT.toString());
            keywords.add(Constants.TypeTableConfiguration.PHONE_CONTACT.toString());
            keywords.add(Constants.TypeTableConfiguration.PRINCIPAL_ADDRESS.toString());

            Map<String, Object> params = new HashMap<String, Object>();
            params.put("state", "1");
            params.put("keywords", keywords);

            List<pe.certus.com.entity.Configuration> listConfigurations = configurationDao.getConfigurationsByParams(params);

            Map<String, Object> mapConfigurations = getListConfiguration(listConfigurations);

            List<Configuration> configurations = (List<Configuration>) mapConfigurations.get("listConfigurations");
            List<GenericCombo> genderList = (List<GenericCombo>) mapConfigurations.get("genderList");
            List<GenericCombo> documentTypeList = (List<GenericCombo>) mapConfigurations.get("documentTypeList");
            List<GenericCombo> profileList = getFillAvailableProfiles();

            LOGGER.info("< available profiles: " + Utils.obtenerJsonFormateado(profileList) + " >");
            LOGGER.info("< configuration: " + Utils.obtenerJsonFormateado(configurations) + " >");
            LOGGER.info("< genders: " + Utils.obtenerJsonFormateado(genderList) + " >");
            LOGGER.info("< document types: " + Utils.obtenerJsonFormateado(documentTypeList) + " >");


            response.setProfiles(profileList);
            response.setConfigurations(configurations);
            response.setDocumentTypes(documentTypeList);
            response.setGenders(genderList);

            response.setCode(NumberUtils.INTEGER_ONE.toString());
            response.setMessage("Se obtuvieron las configuraciones exitosamente");


        } catch (SalesGeneralException eX) {
            throw new SalesGeneralException(eX.getMessage(), Constants.ERROR_CODES.ERROR_GET_CONFIGURATIONS);
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
        return response;
    }

    private List<GenericCombo> getFillAvailableProfiles() {
        List<GenericCombo> profileList = new ArrayList<GenericCombo>();

        GenericCombo itemBean = new GenericCombo();
        itemBean.setCode(StringUtils.EMPTY);
        itemBean.setDescription(" - Seleccione - ");
        profileList.add(itemBean);

        for (Role perfil : userDao.getAllRoles()) {
            GenericCombo gc = new GenericCombo();
            gc.setCode(perfil.getIdRole().toString());
            gc.setDescription(perfil.getDescription());
            profileList.add(gc);
        }
        return profileList;
    }

    private Response loadConfigurations() {
        Response response = new Response();
        try {

            List<String> keywords = new ArrayList<String>();
            keywords.add(Constants.TypeTableConfiguration.COPYRIGHT.toString());
            keywords.add(Constants.TypeTableConfiguration.GENDER.toString());
            keywords.add(Constants.TypeTableConfiguration.LIMIT_AGE.toString());
            keywords.add(Constants.TypeTableConfiguration.EMAIL_CONTACT.toString());
            keywords.add(Constants.TypeTableConfiguration.PHONE_CONTACT.toString());
            keywords.add(Constants.TypeTableConfiguration.PRINCIPAL_ADDRESS.toString());
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("state", "1");
            params.put("keywords", keywords);

            List<pe.certus.com.entity.Configuration> listConfigurations = configurationDao.getConfigurationsByParams(params);

            Map<String, Object> mapConfigurations = getListConfiguration(listConfigurations);

            List<Configuration> configurations = (List<Configuration>) mapConfigurations.get("listConfigurations");
            List<GenericCombo> genderList = (List<GenericCombo>) mapConfigurations.get("genderList");

            LOGGER.info("< configuration: " + Utils.obtenerJsonFormateado(configurations) + " >");
            LOGGER.info("< genders: " + Utils.obtenerJsonFormateado(genderList) + " >");

            response.setConfigurations(configurations);
            response.setGenders(genderList);

            response.setCode(NumberUtils.INTEGER_ONE.toString());
            response.setMessage("Se obtuvieron las configuraciones exitosamente");

        } catch (SalesGeneralException eX) {
            throw new SalesGeneralException(eX.getMessage(), Constants.ERROR_CODES.ERROR_GET_CONFIGURATIONS);
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
        return response;
    }

    private Map<String, Object> getListConfiguration(List<pe.certus.com.entity.Configuration> listConfigurations) {
        Map<String, Object> params = new HashMap<String, Object>();
        List<Configuration> list = new ArrayList<Configuration>();
        List<GenericCombo> genderList = new ArrayList<GenericCombo>();

        GenericCombo itemBean = new GenericCombo();
        itemBean.setCode(StringUtils.EMPTY);
        itemBean.setDescription(" - Seleccione - ");
        genderList.add(itemBean);

        for (pe.certus.com.entity.Configuration cfg : listConfigurations) {
            if (Constants.TypeTableConfiguration.GENDER.toString().equals(cfg.getCtable())) {
                GenericCombo gc = fillGenericCombo(cfg);
                genderList.add(gc);
            } else {
                setListConfiguration(list, cfg);
            }
        }

        Configuration configuration = new Configuration();
        list.add(configuration);
        configuration = new Configuration();
        list.add(configuration);

        params.put("listConfigurations", list);
        params.put("genderList", genderList);

        return params;
    }

    private void setListConfiguration(List<Configuration> list, pe.certus.com.entity.Configuration cfg) {
        Configuration configuration = fillConfigurations(cfg);
        list.add(configuration);
    }

    private GenericCombo fillGenericCombo(pe.certus.com.entity.Configuration cfg) {
        GenericCombo gc = new GenericCombo();
        gc.setCode(cfg.getCvalue());
        gc.setDescription(cfg.getCvaluedesc());
        return gc;
    }

    private Configuration fillConfigurations(pe.certus.com.entity.Configuration cfg) {
        Configuration configuration = new Configuration();
        configuration.setKeyword(cfg.getCtable());
        configuration.setTitle(cfg.getCvalue());
        configuration.setDescription(cfg.getCvaluedesc());
        configuration.setValue1(cfg.getCtag1());
        configuration.setValue2(cfg.getCtag2());
        configuration.setValue3(cfg.getCtag3());
        configuration.setValue4(cfg.getCtag4() != null ? cfg.getCtag4().longValue() : NumberUtils.LONG_ZERO);
        configuration.setIndex(cfg.getIndex().longValue());
        return configuration;
    }

    @Override
    public Response registerAcademicConfiguration(Request request) throws SalesGeneralException {
        Response response = new Response();

        try {

            AcademicConfiguration academicConfiguration = request.getAcademicConfiguration();
            LOGGER.info("data for register " + Utils.obtenerJsonFormateado(academicConfiguration));
            Integer userID = getUserIdFromSession();
            LOGGER.info("User id from session " + userID);

            pe.certus.com.entity.AcademicConfiguration academicConfigurationEntity
                    = fillAcademicConfiguration(academicConfiguration,
                    new BigDecimal(userID),
                    request.getIpRequest());

            if (NumberUtils.INTEGER_ZERO < configurationDao.registerAcademicConfigurations(academicConfigurationEntity)) {
                response.setCode(NumberUtils.INTEGER_ONE.toString());
                response.setMessage("Registro satisfactorio");
                LOGGER.info("Registro satisfactorio");
            } else {
                throw new SalesGeneralException("No se registro la confguracion academica",
                        Constants.ERROR_CODES.ERROR_REGISTER);
            }

        } catch (SalesGeneralException eX) {
            throw new SalesGeneralException(eX.getMessage(), Constants.ERROR_CODES.ERROR_REGISTER);
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
        return response;
    }

    private Integer getUserIdFromSession() {
        return Integer.parseInt(getUserFromJsonFormat(UserUtil.getSessionIDFromWebAuthenticationDetails()).getCode());
    }

    private pe.certus.com.entity.AcademicConfiguration fillAcademicConfiguration(AcademicConfiguration acaConf,
                                                                                 BigDecimal idUser,
                                                                                 String requestIp) {

        pe.certus.com.entity.AcademicConfiguration acaConfEntity = new pe.certus.com.entity.AcademicConfiguration();

        acaConfEntity.setLevel(acaConf.getLevel());
        acaConfEntity.setIpRegistration(acaConf.getIpMarking());
        acaConfEntity.setAccounting(new BigDecimal(acaConf.getAccounting()));
        acaConfEntity.setIncomeTolerance(new BigDecimal(acaConf.getIncomeToleranceInMinits()));
        acaConfEntity.setExitTolerance(new BigDecimal(acaConf.getOutcomeToleranceInMinits()));
        acaConfEntity.setPrevious(new BigDecimal(acaConf.getBeforeTimeInMinits()));
        acaConfEntity.setAfter(new BigDecimal(acaConf.getAfterTimeInMinits()));
        acaConfEntity.setIdUser(idUser);
        acaConfEntity.setState(Constants.FLAG_ACTIVE);
        acaConfEntity.setCodUserCrea(idUser.toString()); // TODO registrar codigo active directory
        acaConfEntity.setDateUserCrea(new Date());
        acaConfEntity.setDeTermCrea(requestIp);
        return acaConfEntity;
    }

    @Override
    public Response getAllAcademicConfiguration(Request request) throws SalesGeneralException {
        Response response = new Response();
        List<AcademicConfiguration> academicConfigurations = new ArrayList<AcademicConfiguration>();
        try {

            LOGGER.info("request from: " + request.getIpRequest());

            for (pe.certus.com.entity.AcademicConfiguration academicConfiguration
                    : configurationDao.getAllAcademicConfigurationByStatus(Constants.FLAG_ACTIVE)) {
                academicConfigurations.add(fillAcademicConfiguration(academicConfiguration));
            }

            response.setCode(NumberUtils.INTEGER_ONE.toString());
            response.setMessage("Consulta exitosa.");
            response.setAcademicConfigurations(academicConfigurations);


        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.ERROR_GET_ALL_ROWS);
        }
        return response;
    }

    private AcademicConfiguration fillAcademicConfiguration(
            pe.certus.com.entity.AcademicConfiguration academicConfiguration) {
        AcademicConfiguration academicConfigurationMessage = new AcademicConfiguration();
        academicConfigurationMessage.setCode(academicConfiguration.getIdAcaConf().toPlainString());
        academicConfigurationMessage.setLevel(academicConfiguration.getLevel());
        academicConfigurationMessage.setIpMarking(academicConfiguration.getIpRegistration());
        academicConfigurationMessage.setAccounting(academicConfiguration.getAccounting().toString());
        academicConfigurationMessage.setIncomeToleranceInMinits(academicConfiguration.getIncomeTolerance().intValue());
        academicConfigurationMessage.setOutcomeToleranceInMinits(academicConfiguration.getExitTolerance().intValue());
        academicConfigurationMessage.setBeforeTimeInMinits(academicConfiguration.getPrevious().intValue());
        academicConfigurationMessage.setAfterTimeInMinits(academicConfiguration.getAfter().intValue());
        return academicConfigurationMessage;
    }

    @Override
    public Response deleteAcademicConfiguration(Integer code) throws SalesGeneralException {
        Response response = new Response();

        try {

            LOGGER.info("delete academic configuration: " + code);
            Integer deleteResult = configurationDao.deleteAcademicConfiguration(code);
            LOGGER.info("delete result: " + deleteResult);
            if (NumberUtils.INTEGER_ZERO == deleteResult) {
                throw new SalesGeneralException("Ocurrio un error al eliminar registro.",
                        Constants.ERROR_CODES.ERROR_DELETE);
            }

            response.setCode(NumberUtils.INTEGER_ONE.toString());
            response.setMessage("Se elimino registro satisfactoriamente.");

        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.ERROR_DELETE);
        }
        return response;
    }

    @Override
    public Response registerSite(Request request, String macAddress) throws SalesGeneralException {
        Response response = new Response();

        try {

            AcademicConfiguration academicConfiguration = request.getAcademicConfiguration();
            LOGGER.info("data for register " + Utils.obtenerJsonFormateado(academicConfiguration));
            Integer userID = getUserIdFromSession();
            LOGGER.info("User id from session " + userID);

            Classroom classroom
                    = fillClassroom(academicConfiguration,
                    new BigDecimal(userID),
                    request.getIpRequest(),
                    macAddress);

            if (NumberUtils.INTEGER_ZERO < configurationDao.registerClassroom(classroom)) {
                response.setCode(NumberUtils.INTEGER_ONE.toString());
                response.setMessage("Registro satisfactorio");
                LOGGER.info("Registro satisfactorio");
            } else {
                throw new SalesGeneralException("No se registro la sede",
                        Constants.ERROR_CODES.ERROR_REGISTER);
            }

        } catch (SalesGeneralException eX) {
            throw new SalesGeneralException(eX.getMessage(), Constants.ERROR_CODES.ERROR_REGISTER);
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
        return response;
    }

    private Classroom fillClassroom(AcademicConfiguration classroom,
                                    BigDecimal idUser,
                                    String requestIp,
                                    String macAddress) {
        Classroom classroomEntity = new Classroom();
        classroomEntity.setCampus(classroom.getCampus());
        classroomEntity.setBuilding(classroom.getBuilding());
        classroomEntity.setClassroom(classroom.getClassroom());
        classroomEntity.setIpAddress(requestIp);
        classroomEntity.setMacAddress(macAddress);
        classroomEntity.setIdUser(idUser);
        classroomEntity.setState(Constants.FLAG_ACTIVE);
        classroomEntity.setCodUserCrea(idUser.toString()); // TODO registrar codigo active directory
        classroomEntity.setDateUserCrea(new Date());
        classroomEntity.setDeTermCrea(requestIp);
        return classroomEntity;
    }


    @Override
    public Response getAllSites(Request request) throws SalesGeneralException {
        Response response = new Response();
        List<AcademicConfiguration> academicConfigurations = new ArrayList<AcademicConfiguration>();
        try {

            LOGGER.info("request from: " + request.getIpRequest());

            for (Classroom classroom
                    : configurationDao.getAllSitesByStatus(Constants.FLAG_ACTIVE)) {
                academicConfigurations.add(fillSite(classroom));
            }

            response.setCode(NumberUtils.INTEGER_ONE.toString());
            response.setMessage("Consulta exitosa.");
            response.setAcademicConfigurations(academicConfigurations);


        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.ERROR_GET_ALL_ROWS);
        }
        return response;
    }

    private AcademicConfiguration fillSite(Classroom classroom) {
        AcademicConfiguration academicConfigurationMessage = new AcademicConfiguration();
        academicConfigurationMessage.setCode(classroom.getIdClassroom().toPlainString());
        academicConfigurationMessage.setCampus(classroom.getCampus());
        academicConfigurationMessage.setBuilding(classroom.getBuilding());
        academicConfigurationMessage.setClassroom(classroom.getClassroom());
        academicConfigurationMessage.setIpAddress(classroom.getIpAddress());
        return academicConfigurationMessage;
    }

    @Override
    public Response deleteSite(Integer code) throws SalesGeneralException {
        Response response = new Response();

        try {

            LOGGER.info("delete site: " + code);
            Integer deleteResult = configurationDao.deleteSite(code);
            LOGGER.info("delete result: " + deleteResult);
            if (NumberUtils.INTEGER_ZERO == deleteResult) {
                throw new SalesGeneralException("Ocurrio un error al eliminar registro.",
                        Constants.ERROR_CODES.ERROR_DELETE);
            }

            response.setCode(NumberUtils.INTEGER_ONE.toString());
            response.setMessage("Se elimino registro satisfactoriamente.");

        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.ERROR_DELETE);
        }
        return response;
    }

    @Override
    public Response registerInterval(Request request) throws SalesGeneralException {
        Response response = new Response();

        try {

            IntervalConfiguration intervalConfiguration = request.getIntervalConfiguration();
            LOGGER.info("data for register " + Utils.obtenerJsonFormateado(intervalConfiguration));
            Integer userID = getUserIdFromSession();
            LOGGER.info("User id from session " + userID);

            Interval intervalEntity
                    = fillIntervalEntity(intervalConfiguration,
                    new BigDecimal(userID),
                    request.getIpRequest());

            if (NumberUtils.INTEGER_ZERO < configurationDao.registerInterval(intervalEntity)) {
                response.setCode(NumberUtils.INTEGER_ONE.toString());
                response.setMessage("Registro satisfactorio");
                LOGGER.info("Registro satisfactorio");
            } else {
                throw new SalesGeneralException("No se registro la confguracion academica",
                        Constants.ERROR_CODES.ERROR_REGISTER);
            }

        } catch (SalesGeneralException eX) {
            throw new SalesGeneralException(eX.getMessage(), Constants.ERROR_CODES.ERROR_REGISTER);
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
        return response;
    }

    private Interval fillIntervalEntity(IntervalConfiguration intervalConfiguration,
                                        BigDecimal idUser,
                                        String requestIp) throws Exception {
        Interval interval = new Interval();
        interval.setYear(new BigDecimal(intervalConfiguration.getYear()));
        interval.setMonth(intervalConfiguration.getMonth());
        interval.setStart(Utils.parseStringToDate(intervalConfiguration.getStartDate(), Constants.PATTERN_DATE));
        interval.setEnd(Utils.parseStringToDate(intervalConfiguration.getEndDate(), Constants.PATTERN_DATE));
        interval.setDescState(CLOSED);
        interval.setIdUser(idUser);
        interval.setState(Constants.FLAG_ACTIVE);
        interval.setCodUserCrea(idUser.toString()); // TODO registrar codigo active directory
        interval.setDateUserCrea(new Date());
        interval.setDeTermCrea(requestIp);
        return interval;
    }

    @Override
    public Response getAllInterval(Request request) throws SalesGeneralException {
        Response response = new Response();
        List<IntervalConfiguration> intervalConfigurations = new ArrayList<IntervalConfiguration>();
        try {

            LOGGER.info("request from: " + request.getIpRequest());

            for (Interval interval
                    : configurationDao.getAllIntervalByStatus(Constants.FLAG_ACTIVE)) {
                intervalConfigurations.add(fillIntervalMessage(interval));
            }

            response.setCode(NumberUtils.INTEGER_ONE.toString());
            response.setMessage("Consulta exitosa.");
            response.setIntervalConfigurations(intervalConfigurations);


        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.ERROR_GET_ALL_ROWS);
        }
        return response;
    }

    private IntervalConfiguration fillIntervalMessage(Interval interval) {
        IntervalConfiguration intervalConfiguration = new IntervalConfiguration();
        intervalConfiguration.setCode(interval.getIdInterval().toPlainString());
        intervalConfiguration.setYear(interval.getYear().toPlainString());
        intervalConfiguration.setMonth(interval.getMonth());
        intervalConfiguration.setStartDate(getReadableUserDates(Constants.PATTERN_DATE, interval.getStart()));
        intervalConfiguration.setEndDate(getReadableUserDates(Constants.PATTERN_DATE, interval.getEnd()));
        intervalConfiguration.setDescriptionStatus(interval.getDescState());
        return intervalConfiguration;
    }

    private String getReadableUserDates(String patternUserDate, Date date) {
        DateFormat df = new SimpleDateFormat(patternUserDate);
        return df.format(date);
    }

    @Override
    public Response deleteInterval(Integer code) throws SalesGeneralException {
        Response response = new Response();

        try {

            LOGGER.info("delete site: " + code);
            Integer deleteResult = configurationDao.deleteInterval(code);
            LOGGER.info("delete result: " + deleteResult);
            if (NumberUtils.INTEGER_ZERO == deleteResult) {
                throw new SalesGeneralException("Ocurrio un error al eliminar registro.",
                        Constants.ERROR_CODES.ERROR_DELETE);
            }

            response.setCode(NumberUtils.INTEGER_ONE.toString());
            response.setMessage("Se elimino registro satisfactoriamente.");

        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.ERROR_DELETE);
        }
        return response;
    }

}