package pe.certus.com.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import pe.certus.com.constants.Constants;
import pe.certus.com.message.Menu;
import pe.certus.com.message.MessageMail;
import pe.certus.com.message.User;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static pe.certus.com.constants.Constants.KEYWORD_ESTIMADO_CLIENTE;
import static pe.certus.com.constants.Constants.NULL_KEYWORD;

/**
 * Created by jose.hurtado on 10/27/2017.
 */
public class UserUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserUtil.class);

    public static pe.certus.com.entity.User fillUserDataForRegister(User user, Constants.TypesUser typesUser) throws Exception {
        pe.certus.com.entity.User userEntity = new pe.certus.com.entity.User();
        userEntity.setCodeAd(user.getCode());
        userEntity.setName(user.getName());
        userEntity.setLastName(user.getFirstLastName());
        userEntity.setEmail(user.getEmail());
        userEntity.setGender(user.getGender());
        userEntity.setPassword(user.getPassword());
        userEntity.setPhone(new BigDecimal(user.getPhone()));
        userEntity.setState(Constants.FLAG_ACTIVE);
        userEntity.setCodUserCrea(Constants.USER_GENERIC_PUBLIC);
        userEntity.setDateUserCrea(new Date());
        userEntity.setDeTermCrea("127.0.0.1");
        return userEntity;
    }

    public static pe.certus.com.entity.User fillUserDataForUpdateSelective(String idUsuario, User user) throws Exception {
        pe.certus.com.entity.User userEntity = new pe.certus.com.entity.User();
        System.out.println("idUsuario: "+idUsuario);
        userEntity.setIdUser(new BigDecimal(idUsuario));

        userEntity.setName(StringUtils.isBlank(user.getName()) ? null : user.getName());
        userEntity.setLastName(StringUtils.isBlank(user.getFirstLastName()) ? null : user.getFirstLastName());

        userEntity.setGender(StringUtils.isBlank(user.getGender()) ? null : user.getGender());
        System.out.println(user);
        System.out.println(user.getUsername());
        System.out.println(user.getEmail());
        System.out.println(user.getPhone());
        userEntity.setPhone(new BigDecimal(StringUtils.isBlank(user.getPhone()) ? null : user.getPhone()));

        userEntity.setState(StringUtils.isBlank(user.getState())
                ? null
                : user.getState());
        // TODO Agregar ultima sesion
        //userEntity.setLastSesion(new Date());

        userEntity.setCodUserModi(Constants.USER_GENERIC_PUBLIC);
        userEntity.setDateUserModi(new Date());
        userEntity.setDeTermModi("127.0.0.1");
        return userEntity;
    }

    public static MessageMail getMessageMail(User user) {
        MessageMail messageMail = new MessageMail();

        messageMail.setPassword(getPassword(user));
        messageMail.setUserName(user.getEmail());

        String userFullName = user.getName() + StringUtils.SPACE + user.getFirstLastName();

        userFullName = StringUtils.countMatches(userFullName, "null") == 1
                ? userFullName.replace(NULL_KEYWORD, StringUtils.EMPTY).trim().toUpperCase()
                : StringUtils.countMatches(userFullName, "null") == 2
                ? KEYWORD_ESTIMADO_CLIENTE
                : userFullName;

        messageMail.setFullName(userFullName);
        messageMail.setEmail(user.getEmail());
        LOGGER.info("sending email whit parameters: " + Utils.obtenerJsonFormateado(messageMail));
        return messageMail;
    }

    private static String getPassword(User user) {
        return StringUtils.isBlank(user.getPassword()) ? user.getNewPassword() : user.getPassword();
    }

    public static User getUserFromJsonFormat(String sessionID) {
        Element element = getElementFromCache(sessionID);
        if (element == null) {
            return null;
        }
        String payloadCache = element.getObjectValue().toString();
        JSONObject jsonObj = new JSONObject(payloadCache);
        JSONObject jsonUserObj = jsonObj.getJSONObject(Constants.KEYWORKD_SESSION_USER);
        return new Gson().fromJson(jsonUserObj.toString(), User.class);
    }

    public static List<Menu> getMenuFromJsonFormat(String sessionID) {
        Element element = getElementFromCache(sessionID);
        if (element == null) {
            return null;
        }
        String payloadCache = element.getObjectValue().toString();
        JSONObject jsonObj = new JSONObject(payloadCache);
        JSONArray jsonArray = jsonObj.getJSONArray(Constants.KEYWORKD_SESSION_MENU);
        Type listType = new TypeToken<ArrayList<Menu>>() {
        }.getType();
        return new Gson().fromJson(jsonArray.toString(), listType);
    }

    public static Element getElementFromCache(String sessionID) {
        CacheManager manager = CacheManager.create();
        Cache cache = manager.getCache(Constants.CACHE_USER);
        return cache.get(sessionID);
    }

    public static String getSessionIDFromWebAuthenticationDetails() {
        return ((WebAuthenticationDetails) SecurityContextHolder.getContext()
                .getAuthentication()
                .getDetails())
                .getSessionId();
    }

    public static boolean getElementFromCacheIsNull(String sessionID) {
        return getElementFromCache(sessionID) == null;
    }

}
