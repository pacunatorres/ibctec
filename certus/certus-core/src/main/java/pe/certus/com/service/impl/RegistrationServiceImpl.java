package pe.certus.com.service.impl;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.certus.com.bean.RegistryBean;
import pe.certus.com.constants.Constants;
import pe.certus.com.dao.RegistrationDao;
import pe.certus.com.exception.SalesGeneralException;
import pe.certus.com.message.Registration;
import pe.certus.com.request.Request;
import pe.certus.com.response.Response;
import pe.certus.com.service.RegistrationService;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Avantica on 6/1/2018.
 */
@Service
public class RegistrationServiceImpl implements RegistrationService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RegistrationDao registrationDao;

    @Override
    public Response getAllRegistry(Request request) throws SalesGeneralException {
        Response response = new Response();
        List<Registration> registrations = new ArrayList<Registration>();
        try {

            LOGGER.info("request from: " + request.getIpRequest());

            for (RegistryBean registryBean
                    : registrationDao.getAllRegistry()) {
                registrations.add(fillDepartmentMessage(registryBean));
            }

            response.setCode(NumberUtils.INTEGER_ONE.toString());
            response.setMessage("Consulta exitosa.");
            response.setRegistrations(registrations);


        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.ERROR_GET_ALL_ROWS);
        }
        return response;
    }

    private Registration fillDepartmentMessage(RegistryBean registryBean) {
        Registration registration = new Registration();
        registration.setStart(getReadableUserDates(Constants.PATTERN_DATE, registryBean.getStart()));
        registration.setEnd(getReadableUserDates(Constants.PATTERN_DATE, registryBean.getEnd()));
        registration.setIdExploitation(registryBean.getIdExploitation().toString());
        registration.setDescProg(registryBean.getDescProg());
        registration.setCodProd(registryBean.getCodProd().toString());
        registration.setDescProd(registryBean.getDescProd());
        registration.setDescCampus(registryBean.getDescCampus());
        registration.setClassroom(registryBean.getClassroom());
        registration.setDescUnitExploitation(registryBean.getDescUnitExploitation());
        registration.setToday(getReadableUserDates(Constants.PATTERN_DATE, registryBean.getToday()));
        registration.setIncome(registryBean.getIncome());
        registration.setOutcome(registryBean.getOutcome());
        registration.setReunionType(registryBean.getReunionType());
        return registration;
    }

    private String getReadableUserDates(String patternUserDate, Date date) {
        DateFormat df = new SimpleDateFormat(patternUserDate);
        return df.format(date);
    }
}
