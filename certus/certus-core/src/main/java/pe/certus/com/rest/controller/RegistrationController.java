package pe.certus.com.rest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.certus.com.constants.Constants;
import pe.certus.com.enums.ValidationExecution;
import pe.certus.com.exception.SalesGeneralException;
import pe.certus.com.message.Parameters;
import pe.certus.com.request.Request;
import pe.certus.com.service.RegistrationService;

import static pe.certus.com.constants.Constants.RESOURCE_PATH.GET_ALL_REGISTRY;

/**
 * Created by jose.hurtado on 11/2/2017.
 */
@Controller
@Scope("request")
@RequestMapping(value = "/secure")
@Transactional(
        propagation = Propagation.REQUIRED,
        rollbackFor = Throwable.class
)
public class RegistrationController {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    private RegistrationService registrationService;

    @ValidationExecution
    @RequestMapping(value = GET_ALL_REGISTRY,
            method = RequestMethod.POST,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters getAllRegistry(@RequestBody Parameters parameters) {
        try {

            parameters.getBody().setRequest(new Request());
            parameters.getBody().getRequest().setIpRequest(parameters.getHeader().getDeviceId());
            parameters.getBody().setResponse(registrationService.getAllRegistry(
                    parameters.getBody().getRequest()
            ));

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }
}

