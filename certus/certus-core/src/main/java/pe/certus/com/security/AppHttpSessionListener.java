package pe.certus.com.security;

import pe.certus.com.config.ApplicationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Created by jose.hurtado on 3/27/2018.
 */
@Component
public class AppHttpSessionListener implements HttpSessionListener {

    @Autowired
    ApplicationProperties properties;

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        se.getSession().setMaxInactiveInterval(properties.getSessionTimeout() * 60);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        se.getSession().invalidate();
    }
}
