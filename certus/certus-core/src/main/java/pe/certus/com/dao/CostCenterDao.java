package pe.certus.com.dao;

import pe.certus.com.entity.Department;
import pe.certus.com.entity.ExploitationUnit;
import pe.certus.com.entity.Product;
import pe.certus.com.exception.SalesGeneralException;

import java.util.List;

/**
 * Created by jose.hurtado on 8/17/2017.
 */
public interface CostCenterDao {

    Integer registerExploitationUnit(ExploitationUnit exploitationUnit) throws SalesGeneralException;

    List<ExploitationUnit> getAllExploitationUnitByStatus(String status) throws SalesGeneralException;

    Integer deleteExploitationUnit(Integer code) throws SalesGeneralException;

    Integer registerProduct(Product product) throws SalesGeneralException;

    List<Product> getAllProductsByStatus(String status) throws SalesGeneralException;

    Integer deleteProduct(Integer code) throws SalesGeneralException;

    Integer registerDepartment(Department department) throws SalesGeneralException;

    List<Department> getAllDepartmentsByStatus(String status) throws SalesGeneralException;

    Integer deleteDepartment(Integer code) throws SalesGeneralException;

}
