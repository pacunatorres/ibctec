package pe.certus.com.service.impl;

import pe.certus.com.builder.HtmlMessageBuilder.MailParametersChangePassword;
import pe.certus.com.builder.HtmlMessageBuilder.MailParametersRegisterUser;
import pe.certus.com.builder.HtmlMessageBuilder.MailParametersRememberPassword;
import pe.certus.com.config.ApplicationProperties;
import pe.certus.com.constants.Constants;
import pe.certus.com.exception.SalesGeneralException;
import pe.certus.com.message.MailParameters;
import pe.certus.com.message.MessageMail;
import pe.certus.com.service.MessageService;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import static pe.certus.com.constants.Constants.PNG_MIME;

/**
 * Created by jose.hurtado on 11/20/2017.
 */
@Service
public class MessageServiceImpl implements MessageService {

    private static final String EMAIL_EDITABLE_TEMPLATE_CLASSPATH_RES = "classpath:mail/templates/";

    private static final String LOGO_IMAGE = "mail/images/sales-logo.png";
    private static final String BANNER_IMAGE = "mail/images/banner.png";
    private static final String CHECK_IMAGE = "mail/images/check.png";
    private static final String FACEBOOK_ICON_IMAGE = "mail/images/face-icon.png";
    private static final String TWITTER_ICON_IMAGE = "mail/images/twitter-icon.png";
    private static final String YOUTUBE_ICON_IMAGE = "mail/images/youtube-icon.png";

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());
    @Autowired
    ApplicationProperties properties;

    @Autowired
    private TemplateEngine stringTemplateEngine;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private JavaMailSender mailSender;

    @Override
    public boolean sendMailFromTemplate(Constants.TypesTransactionMessage typesTransactionMessage,
                                        MessageMail messageMail,
                                        Locale locale) throws SalesGeneralException, MessagingException, IOException {

        MailParameters mailParameters = getHtmlMailRequest(typesTransactionMessage,
                messageMail,
                locale);
        mailParameters.setFromEmail(properties.getMailFrom());
        mailParameters.setToEmail(messageMail.getEmail());

        sendMailMessage(mailParameters);

        return true;
    }

    @Override
    @Async
    public void sendMailFromTemplateAsync(Constants.TypesTransactionMessage typesTransactionMessage,
                                          MessageMail messageMail,
                                          Locale locale) throws SalesGeneralException, MessagingException, IOException {

        MailParameters mailParameters = getHtmlMailRequest(typesTransactionMessage,
                messageMail,
                locale);
        mailParameters.setFromEmail(properties.getMailFrom());
        mailParameters.setToEmail(messageMail.getEmail());

        sendMailMessage(mailParameters);
    }


    private MailParameters getHtmlMailRequest(Constants.TypesTransactionMessage typesMessage,
                                              MessageMail message,
                                              Locale locale) throws MessagingException, IOException {

        MailParameters mailRequest = null;
        Context ctx = new Context(locale);

        switch (typesMessage) {
            case REGISTERUSER:
                MailParametersRegisterUser mailParametersRegisterUser
                        = new MailParametersRegisterUser(properties);
                mailRequest = mailParametersRegisterUser.mailParametersBuilder(
                        getTemplateForRegisterUserMessage(message, ctx));
                break;
            case CHANGEPASSWORD:
                MailParametersChangePassword mailParametersChangePassword
                        = new MailParametersChangePassword(properties);
                mailRequest = mailParametersChangePassword.mailParametersBuilder(
                        getTemplateForRegisterUserMessage(message, ctx)); // FIXME CAMBIAR PARA CAMBIO DE PASSWORD
                break;
            case REMEMBERPASSWORD:
                MailParametersRememberPassword mailParametersRememberPassword
                        = new MailParametersRememberPassword(properties);
                mailRequest = mailParametersRememberPassword.mailParametersBuilder(
                        getTemplateForRegisterUserMessage(message, ctx)); // FIXME CAMBIAR PARA RECORDAR PASSWORD
                break;
            default:
                break;
        }
        return mailRequest;
    }


    private String getTemplateForRegisterUserMessage(MessageMail messageMail,
                                                     Context ctx) throws MessagingException, IOException {

        ctx.setVariable("fullNameClient", messageMail.getFullName());
        ctx.setVariable("userName", messageMail.getUserName());
        ctx.setVariable("password", messageMail.getPassword());

        return stringTemplateEngine.process(getMailTemplateGeneric(properties.getMailTemplateRegisterUser()), ctx);
    }


    // FIXME colocar en un servicio cacheable
    public String getMailTemplateGeneric(String templateName) throws IOException {
        final Resource templateResource = this.applicationContext.getResource(EMAIL_EDITABLE_TEMPLATE_CLASSPATH_RES
                + templateName);
        final InputStream inputStream = templateResource.getInputStream();
        return IOUtils.toString(inputStream, Constants.EMAIL_TEMPLATE_ENCODING);
    }

    public void sendMailMessage(MailParameters mailParameters) throws MessagingException {
        LOGGER.info("preparing for sending email");
        MimeMessage mimeMessage = this.mailSender.createMimeMessage();
        MimeMessageHelper message = new MimeMessageHelper(
                mimeMessage,
                properties.isMailIsMultipart(),
                properties.getMailTemplateEncoding());

        message.setSubject(mailParameters.getSubject());
        message.setFrom(mailParameters.getFromEmail());
        message.setTo(mailParameters.getToEmail());
        message.setText(mailParameters.getTemplateHtml(), properties.isMailIsHtml());

        message.addInline("sales-logo", new ClassPathResource(LOGO_IMAGE), PNG_MIME);
        message.addInline("banner", new ClassPathResource(BANNER_IMAGE), PNG_MIME);
        message.addInline("check", new ClassPathResource(CHECK_IMAGE), PNG_MIME);
        message.addInline("face-icon", new ClassPathResource(FACEBOOK_ICON_IMAGE), PNG_MIME);
        message.addInline("twitter-icon", new ClassPathResource(TWITTER_ICON_IMAGE), PNG_MIME);
        message.addInline("youtube-icon", new ClassPathResource(YOUTUBE_ICON_IMAGE), PNG_MIME);

//        mailSender.send(mimeMessage); pacuna
        LOGGER.info("email send");
    }


}
