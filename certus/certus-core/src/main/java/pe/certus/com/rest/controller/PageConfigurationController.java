package pe.certus.com.rest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pe.certus.com.constants.Constants;
import pe.certus.com.exception.SalesGeneralException;
import pe.certus.com.message.Parameters;
import pe.certus.com.service.ConfigurationService;

@Controller
@Scope("request")
@RequestMapping(value = "/secure")
public class PageConfigurationController {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());
    @Autowired
    private ConfigurationService configurationService;


    @RequestMapping(value = Constants.RESOURCE_PATH.LOAD_CONFIGURATION,
            method = RequestMethod.POST,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters initialLoad(@RequestBody Parameters parameters,
                                  @RequestParam(value = "origin", required = false) boolean isBackEndResquest) {

        try {

            parameters.getBody().setResponse(configurationService.getConfigurations(isBackEndResquest));

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }

}
