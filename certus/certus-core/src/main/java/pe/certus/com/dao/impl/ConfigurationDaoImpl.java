package pe.certus.com.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pe.certus.com.constants.Constants;
import pe.certus.com.dao.ConfigurationDao;
import pe.certus.com.entity.AcademicConfiguration;
import pe.certus.com.entity.Classroom;
import pe.certus.com.entity.Configuration;
import pe.certus.com.entity.Interval;
import pe.certus.com.exception.SalesDBException;
import pe.certus.com.exception.SalesGeneralException;
import pe.certus.com.repository.SalesRepository;

import java.util.List;
import java.util.Map;

/**
 * Created by jose.hurtado on 10/11/2017.
 */
@Repository
public class ConfigurationDaoImpl implements ConfigurationDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigurationDaoImpl.class);

    @Autowired
    SalesRepository salesRepository;

    @Override
    public List<Configuration> getConfigurationsByParams(Map<String, Object> params) throws SalesGeneralException {
        try {

            return salesRepository.getConfigurationByParams(params);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }

    @Override
    public Integer registerAcademicConfigurations(AcademicConfiguration academicConfiguration) {
        try {

            return salesRepository.registerAcademicConfigurations(academicConfiguration);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }

    @Override
    public List<AcademicConfiguration> getAllAcademicConfigurationByStatus(String status) throws SalesGeneralException {
        try {

            return salesRepository.getAllAcademicConfigurationByStatus(status);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }

    @Override
    public Integer deleteAcademicConfiguration(Integer code) throws SalesGeneralException {
        try {

            return salesRepository.deleteAcademicConfiguration(code);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }

    @Override
    public Integer registerClassroom(Classroom classroom) throws SalesGeneralException {
        try {

            return salesRepository.registerClassroom(classroom);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }

    @Override
    public List<Classroom> getAllSitesByStatus(String status) throws SalesGeneralException {
        try {

            return salesRepository.getAllSitesByStatus(status);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }

    @Override
    public Integer deleteSite(Integer code) throws SalesGeneralException {
        try {

            return salesRepository.deleteSite(code);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }

    @Override
    public Integer registerInterval(Interval interval) throws SalesGeneralException {
        try {

            return salesRepository.registerInterval(interval);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }

    @Override
    public List<Interval> getAllIntervalByStatus(String status) throws SalesGeneralException {
        try {

            return salesRepository.getAllIntervalByStatus(status);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }

    @Override
    public Integer deleteInterval(Integer code) throws SalesGeneralException {
        try {

            return salesRepository.deleteInterval(code);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }


}
