package pe.certus.com.rest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by jose.hurtado on 11/13/2017.
 */
@Controller
@Scope("request")
@RequestMapping(value = "/error")
public class ErrorController {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    @RequestMapping(method = RequestMethod.GET)
    public String error() {
        LOGGER.info("< redirect to error page >");
        return "layout.public-error-detail";
    }

    @RequestMapping(value = "/notFound", method = RequestMethod.GET)
    public String notFound() {
        LOGGER.info("< redirect to 404 page >");
        return "layout.public-404-detail";
    }

}
