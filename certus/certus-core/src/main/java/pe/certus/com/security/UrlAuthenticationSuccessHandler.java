package pe.certus.com.security;


import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import pe.certus.com.config.ApplicationProperties;
import pe.certus.com.constants.Constants;
import pe.certus.com.dao.UserDao;
import pe.certus.com.enums.RoleEnum;
import pe.certus.com.message.Menu;
import pe.certus.com.message.User;
import pe.certus.com.request.Request;
import pe.certus.com.service.UserService;
import pe.certus.com.util.Decrypt;
import pe.certus.com.util.MenuUtil;
import pe.certus.com.util.Utils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class UrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {


    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    UserDao userDao;

    @Autowired
    private ApplicationProperties properties;

    @Autowired
    private UserService userService;

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();


    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {

        handle(request, response, authentication);
        clearAuthenticationAttributes(request);
    }

    protected void handle(HttpServletRequest request,
                          HttpServletResponse response,
                          Authentication authentication) throws IOException {

        LOGGER.info("< inicio >");
        Request objRequest = new Request();
        String targetUrl;
        String role;

        try {

            role = getRoleByAuthentication(authentication);

            targetUrl = properties.getPathSuccessfulLoginAdmin();

            List<String> listRole = new ArrayList<String>();
            listRole.add(role);

            List<String> typeMenus = new ArrayList<String>();
            typeMenus.add(Constants.TypeMenu.PRIVATE.toString());

            User user = getUserDataToSaveCache(authentication);
            List<Menu> listMenu = setMenuListToSaveCache(listRole, typeMenus);

            objRequest.setUser(user);
            objRequest.setMenuList(listMenu);

            LOGGER.info("< creating data cache user: " + Utils.obtenerJsonFormateado(objRequest) + " >");
            LOGGER.info("< password encrypted: " + user.getPassword() + " >");

            String sessionId = ((WebAuthenticationDetails) authentication.getDetails()).getSessionId();
            userService.createSession(objRequest, sessionId);
            session().setAttribute("isLogged", true);

            LOGGER.info("< redirect url: " + targetUrl + " >");
            if (response.isCommitted()) {
                LOGGER.info("< has send redirect: " + targetUrl + ">");
                return;
            }

            redirectStrategy.sendRedirect(request, response, targetUrl);

        } catch (Exception e) {
            LOGGER.info("< Error creating data cache: " + Utils.obtenerStackTraceFromException(e) + " >");
            clearAuthenticationAttributes(request);
            redirectStrategy.sendRedirect(request, response, properties.getUrlLoginPrivate());
        }
        LOGGER.info("< fin >");
    }

    private List<Menu> setMenuListToSaveCache(List<String> listRole, List<String> typeMenus) {
        return MenuUtil.getMenu(userDao.getOptionMenu(listRole, typeMenus));
    }

    private User getUserDataToSaveCache(Authentication authentication) throws Exception {
        String userString = Decrypt.decrypt((String) authentication.getPrincipal());
        return StringUtils.isNotBlank(userString)
                ? userService.userInformation(userString)
                : userService.userInformation((String) authentication.getPrincipal());
    }

    protected String getRoleByAuthentication(Authentication authentication) {
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();

        for (GrantedAuthority grantedAuthority : authorities) {

            if (grantedAuthority.getAuthority().equals(RoleEnum.ROLE_ADMIN.toString())) {
                LOGGER.info("< redirect admin page ||ADMIN|| >");
                return grantedAuthority.getAuthority();
            } else if (grantedAuthority.getAuthority().equals(RoleEnum.ROLE_CLIENT.toString())) {
                LOGGER.info("< redirect admin page ||CLIENT|| >");
                return grantedAuthority.getAuthority();
            }

        }
        LOGGER.info("< login redirect >");
        return null;
    }

    private Map<String, String> getAuthorityData(Map<String, String> map,
                                                 String authority) {
        map.put("role", authority);
        return map;
    }


    protected void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }

    private HttpSession session() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getSession(true);
    }

    protected RedirectStrategy getRedirectStrategy() {
        return redirectStrategy;
    }

    public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }

}
