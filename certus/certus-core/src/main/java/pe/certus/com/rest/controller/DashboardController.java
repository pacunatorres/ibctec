package pe.certus.com.rest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Avantica on 4/12/2018.
 */
@Controller
@Scope("request")
@RequestMapping(value = "/secure/admin")
public class DashboardController {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String initSecureLogin() {
        LOGGER.info("< inicio >");
        return "layout.secure-admin-login-detail";
    }

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public String initSecureDashboard() {
        LOGGER.info("< inicio >");
        return "layout.secure-admin-dashboard-detail";
    }

    @RequestMapping(value = "/dashboard/academic", method = RequestMethod.GET)
    public String initSecureAcademicConfiguration() {
        LOGGER.info("< inicio >");
        return "layout.secure-academic-detail";
    }

    @RequestMapping(value = "/dashboard/site", method = RequestMethod.GET)
    public String initSecureSiteConfiguration() {
        LOGGER.info("< inicio >");
        return "layout.secure-site-detail";
    }

    @RequestMapping(value = "/dashboard/interval", method = RequestMethod.GET)
    public String initSecureIntervalConfiguration() {
        LOGGER.info("< inicio >");
        return "layout.secure-interval-detail";
    }

    @RequestMapping(value = "/dashboard/unitexplot", method = RequestMethod.GET)
    public String initSecureUnitExploitation() {
        LOGGER.info("< inicio >");
        return "layout.secure-exploitation-detail";
    }

    @RequestMapping(value = "/dashboard/product", method = RequestMethod.GET)
    public String initSecureProduct() {
        LOGGER.info("< inicio >");
        return "layout.secure-product-detail";
    }

    @RequestMapping(value = "/dashboard/department", method = RequestMethod.GET)
    public String initSecureDepartment() {
        LOGGER.info("< inicio >");
        return "layout.secure-department-detail";
    }

    @RequestMapping(value = "/dashboard/registry", method = RequestMethod.GET)
    public String initSecureRegitry() {
        LOGGER.info("< inicio >");
        return "layout.secure-registry-detail";
    }

}
