package pe.certus.com.rest.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by jose.hurtado on 7/11/2017.
 */
@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class CustomAccessDeniedException extends RuntimeException {

    private static final long serialVersionUID = 1L;
    private String message;

    public CustomAccessDeniedException(String msg) {
        super(msg);
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}