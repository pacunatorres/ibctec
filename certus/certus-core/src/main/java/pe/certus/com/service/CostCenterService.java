package pe.certus.com.service;

import pe.certus.com.exception.SalesGeneralException;
import pe.certus.com.request.Request;
import pe.certus.com.response.Response;

public interface CostCenterService {

    Response registerExploitationUnit(Request request) throws SalesGeneralException;

    Response getAllExploitationUnit(Request request) throws SalesGeneralException;

    Response deleteExploitationUnit(Integer code) throws SalesGeneralException;

    Response registerProduct(Request request) throws SalesGeneralException;

    Response getAllProducts(Request request) throws SalesGeneralException;

    Response deleteProduct(Integer code) throws SalesGeneralException;

    Response registerDepartment(Request request) throws SalesGeneralException;

    Response getAllDepartments(Request request) throws SalesGeneralException;

    Response deleteDepartment(Integer code) throws SalesGeneralException;

}
