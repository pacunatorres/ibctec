package pe.certus.com.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pe.certus.com.constants.Constants;
import pe.certus.com.dao.CostCenterDao;
import pe.certus.com.entity.Department;
import pe.certus.com.entity.ExploitationUnit;
import pe.certus.com.entity.Product;
import pe.certus.com.exception.SalesDBException;
import pe.certus.com.exception.SalesGeneralException;
import pe.certus.com.repository.SalesRepository;

import java.util.List;

/**
 * Created by jose.hurtado on 8/17/2017.
 */
@Repository
public class CostCenterDaoImpl implements CostCenterDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(CostCenterDaoImpl.class);

    @Autowired
    SalesRepository salesRepository;

    @Override
    public Integer registerExploitationUnit(ExploitationUnit exploitationUnit) throws SalesGeneralException {
        try {

            return salesRepository.registerExploitationUnit(exploitationUnit);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }

    @Override
    public List<ExploitationUnit> getAllExploitationUnitByStatus(String status) throws SalesGeneralException {
        try {

            return salesRepository.getAllExploitationUnitByStatus(status);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }

    @Override
    public Integer deleteExploitationUnit(Integer code) throws SalesGeneralException {
        try {

            return salesRepository.deleteExploitationUnit(code);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }

    @Override
    public Integer registerProduct(Product product) throws SalesGeneralException {
        try {

            return salesRepository.registerProduct(product);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }

    @Override
    public List<Product> getAllProductsByStatus(String status) throws SalesGeneralException {
        try {

            return salesRepository.getAllProductsByStatus(status);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }

    @Override
    public Integer deleteProduct(Integer code) throws SalesGeneralException {
        try {

            return salesRepository.deleteProduct(code);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }

    @Override
    public Integer registerDepartment(Department department) throws SalesGeneralException {
        try {

            return salesRepository.registerDepartment(department);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }

    @Override
    public List<Department> getAllDepartmentsByStatus(String status) throws SalesGeneralException {
        try {

            return salesRepository.getAllDepartmentsByStatus(status);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }

    @Override
    public Integer deleteDepartment(Integer code) throws SalesGeneralException {
        try {

            return salesRepository.deleteDepartment(code);

        } catch (SalesDBException e) {
            LOGGER.error(e.getMessage(), e);
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }
}
