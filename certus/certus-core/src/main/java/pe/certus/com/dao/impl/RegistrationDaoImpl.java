package pe.certus.com.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pe.certus.com.bean.RegistryBean;
import pe.certus.com.constants.Constants;
import pe.certus.com.dao.RegistrationDao;
import pe.certus.com.exception.SalesDBException;
import pe.certus.com.exception.SalesGeneralException;
import pe.certus.com.repository.SalesRepository;

import java.util.List;

/**
 * Created by Avantica on 6/1/2018.
 */
@Repository
public class RegistrationDaoImpl implements RegistrationDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationDaoImpl.class);

    @Autowired
    private SalesRepository salesRepository;

    @Override
    public List<RegistryBean> getAllRegistry() throws SalesGeneralException {
        try {

            return salesRepository.getAllRegistry();

        } catch (SalesDBException e) {
        	e.printStackTrace();
            LOGGER.error(e.getMessage(), e);
            
            throw new SalesGeneralException(e.getMessage(), e.getErrorCode());
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
    }
}
