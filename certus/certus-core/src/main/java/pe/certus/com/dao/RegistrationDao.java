package pe.certus.com.dao;

import pe.certus.com.bean.RegistryBean;
import pe.certus.com.exception.SalesGeneralException;

import java.util.List;

/**
 * Created by jose.hurtado on 10/11/2017.
 */
public interface RegistrationDao {

    List<RegistryBean> getAllRegistry() throws SalesGeneralException;

}
