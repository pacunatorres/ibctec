package pe.certus.com.rest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import pe.certus.com.constants.Constants;
import pe.certus.com.enums.ValidationExecution;
import pe.certus.com.exception.SalesGeneralException;
import pe.certus.com.message.Body;
import pe.certus.com.message.Parameters;
import pe.certus.com.request.Request;
import pe.certus.com.rest.validator.GenericFormValidator;
import pe.certus.com.service.CostCenterService;

/**
 * Created by jose.hurtado on 11/2/2017.
 */
@Controller
@Scope("request")
@RequestMapping(value = "/secure")
@Transactional(
        propagation = Propagation.REQUIRED,
        rollbackFor = Throwable.class
)
public class CostCenterController {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    private CostCenterService costCenterService;

    @Autowired
    @Qualifier("globalValidator")
    private GenericFormValidator globalValidator;

    @ValidationExecution
    @RequestMapping(value = Constants.RESOURCE_PATH.REGISTER_EXPLOITATION_UNIT,
            method = RequestMethod.POST,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters registerExploitationUnit(@RequestBody Parameters parameters,
                                               BindingResult bindingResult) {
        try {

            globalValidator.validate(parameters, bindingResult);

            if (bindingResult.hasErrors()) {
                throw new SalesGeneralException("Ocurri&oacute; un error en el registro de unidades de explotaci&oacute;n.",
                        Constants.ERROR_CODES.ERROR_REGISTER);
            } else {

                parameters.getBody().getRequest().setIpRequest(parameters.getHeader().getDeviceId());
                parameters.getBody().setResponse(
                        costCenterService.registerExploitationUnit(
                                parameters.getBody().getRequest()
                        )
                );

            }

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }


    @ValidationExecution
    @RequestMapping(value = Constants.RESOURCE_PATH.GET_ALL_EXPLOITATION_UNITS,
            method = RequestMethod.POST,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters getAllExploitation(@RequestBody Parameters parameters) {
        try {

            parameters.getBody().setRequest(new Request());
            parameters.getBody().getRequest().setIpRequest(parameters.getHeader().getDeviceId());
            parameters.getBody().setResponse(costCenterService.getAllExploitationUnit(
                    parameters.getBody().getRequest()
            ));

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }

    @ValidationExecution
    @RequestMapping(value = Constants.RESOURCE_PATH.DELETE_EXPLOITATION_UNIT + "/{code}",
            method = RequestMethod.DELETE,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters deleteExploitation(@PathVariable("code") Integer code) {
        Parameters parameters = new Parameters();
        try {

            parameters.setBody(new Body());
            parameters.getBody().setResponse(costCenterService.deleteExploitationUnit(code));

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }

    @ValidationExecution
    @RequestMapping(value = Constants.RESOURCE_PATH.REGISTER_PRODUCT,
            method = RequestMethod.POST,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters registerProduct(@RequestBody Parameters parameters,
                                      BindingResult bindingResult) {
        try {

            globalValidator.validate(parameters, bindingResult);

            if (bindingResult.hasErrors()) {
                throw new SalesGeneralException("Ocurri&oacute; un error en el registro del producto.",
                        Constants.ERROR_CODES.ERROR_REGISTER);
            } else {

                parameters.getBody().getRequest().setIpRequest(parameters.getHeader().getDeviceId());
                parameters.getBody().setResponse(
                        costCenterService.registerProduct(
                                parameters.getBody().getRequest()
                        )
                );

            }

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }


    @ValidationExecution
    @RequestMapping(value = Constants.RESOURCE_PATH.GET_ALL_PRODUCTS,
            method = RequestMethod.POST,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters getAllProducts(@RequestBody Parameters parameters) {
        try {

            parameters.getBody().setRequest(new Request());
            parameters.getBody().getRequest().setIpRequest(parameters.getHeader().getDeviceId());
            parameters.getBody().setResponse(costCenterService.getAllProducts(
                    parameters.getBody().getRequest()
            ));

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }

    @ValidationExecution
    @RequestMapping(value = Constants.RESOURCE_PATH.DELETE_PRODUCT + "/{code}",
            method = RequestMethod.DELETE,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters deleteProduct(@PathVariable("code") Integer code) {
        Parameters parameters = new Parameters();
        try {

            parameters.setBody(new Body());
            parameters.getBody().setResponse(costCenterService.deleteProduct(code));

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }

    @ValidationExecution
    @RequestMapping(value = Constants.RESOURCE_PATH.REGISTER_DEPARTMENT,
            method = RequestMethod.POST,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters registerDepartment(@RequestBody Parameters parameters,
                                         BindingResult bindingResult) {
        try {

            globalValidator.validate(parameters, bindingResult);

            if (bindingResult.hasErrors()) {
                throw new SalesGeneralException("Ocurri&oacute; un error en el registro del departamento.",
                        Constants.ERROR_CODES.ERROR_REGISTER);
            } else {

                parameters.getBody().getRequest().setIpRequest(parameters.getHeader().getDeviceId());
                parameters.getBody().setResponse(
                        costCenterService.registerDepartment(
                                parameters.getBody().getRequest()
                        )
                );

            }

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }


    @ValidationExecution
    @RequestMapping(value = Constants.RESOURCE_PATH.GET_ALL_DEPARTMENT,
            method = RequestMethod.POST,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters getAllDepartments(@RequestBody Parameters parameters) {
        try {

            parameters.getBody().setRequest(new Request());
            parameters.getBody().getRequest().setIpRequest(parameters.getHeader().getDeviceId());
            parameters.getBody().setResponse(costCenterService.getAllDepartments(
                    parameters.getBody().getRequest()
            ));

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }

    @ValidationExecution
    @RequestMapping(value = Constants.RESOURCE_PATH.DELETE_DEPARTMENT + "/{code}",
            method = RequestMethod.DELETE,
            headers = Constants.ACCEPT_APPLICATION_JSON,
            produces = Constants.PRODUCES_APPLICATION_JSON_CHARSET_UTF_8)
    @ResponseBody
    public Parameters deleteDepartment(@PathVariable("code") Integer code) {
        Parameters parameters = new Parameters();
        try {

            parameters.setBody(new Body());
            parameters.getBody().setResponse(costCenterService.deleteDepartment(code));

        } catch (SalesGeneralException ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    ex.getErrorCode());
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            throw new SalesGeneralException(ex.getMessage(),
                    Constants.ERROR_CODES.GENERIC_ERROR,
                    parameters.getHeader());
        }
        return parameters;
    }

}

