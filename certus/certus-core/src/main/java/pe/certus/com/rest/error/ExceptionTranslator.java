package pe.certus.com.rest.error;

import pe.certus.com.config.ApplicationProperties;
import pe.certus.com.exception.SalesGeneralException;
import pe.certus.com.message.Error;
import pe.certus.com.message.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;

import static pe.certus.com.constants.Constants.ERROR_CODES.GENERIC_ERROR;
import static pe.certus.com.constants.Constants.PUBLIC_KEYWORD;
import static pe.certus.com.util.ParameterUtil.buildResponseError;
import static pe.certus.com.util.Util.encodeString;


@ControllerAdvice
public class ExceptionTranslator {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ApplicationProperties properties;

    @ExceptionHandler(CustomAccessDeniedException.class)
    public ModelAndView handleCustomAccessDeniedException(HttpServletRequest request) {
        LOGGER.info("Se denego el acceso a URL: " + request.getRequestURL());
        ModelAndView model;
        if (request.getRequestURL().toString().contains(PUBLIC_KEYWORD)) {
            model = new ModelAndView("layout.public-login-detail");
            model.addObject("error", Boolean.TRUE);
            model.addObject("msg", encodeString(properties.getErrorMessageAccessDenied()));

            // TODO Mejorar
            RedirectView redirectView = new RedirectView("http://localhost:8086/secure/admin/login");
            model.setView(redirectView);
        } else {
            model = new ModelAndView("layout.secure-admin-login-detail");
            model.addObject("error", Boolean.TRUE);
            model.addObject("msg", encodeString(properties.getErrorMessageAccessDenied()));

            // TODO Mejorar
            RedirectView redirectView = new RedirectView("http://localhost:8086/secure/admin/login");
            model.setView(redirectView);
        }

        return model;
    }

    @ExceptionHandler(SessionExistException.class)
    public ModelAndView handleCustomSessionExistException(HttpServletRequest request) {
        ModelAndView model = new ModelAndView("layout.public-account-detail");
        model.addObject("message", properties.getErrorMessageSessionExists());
        return model;
    }

    @ExceptionHandler({SalesGeneralException.class})
    @ResponseBody
    public Parameters processAppMovilException(SalesGeneralException exception) {
        String errorCode = exception.getErrorCode();
        String errorCodeReplace = replaceErrorCode(errorCode);
        return buildResponseError(exception.getHeader(), new Error(errorCodeReplace,
                properties.getMapErrors().get(errorCodeReplace)));
    }

    private String replaceErrorCode(String errorCode) {
        return StringUtils.isEmpty(errorCode) ? GENERIC_ERROR : errorCode;
    }

}
