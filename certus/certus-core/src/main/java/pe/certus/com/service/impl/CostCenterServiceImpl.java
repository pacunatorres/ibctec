package pe.certus.com.service.impl;

import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.certus.com.constants.Constants;
import pe.certus.com.dao.CostCenterDao;
import pe.certus.com.entity.Department;
import pe.certus.com.entity.ExploitationUnit;
import pe.certus.com.entity.Product;
import pe.certus.com.exception.SalesGeneralException;
import pe.certus.com.message.CostCenterConfiguration;
import pe.certus.com.request.Request;
import pe.certus.com.response.Response;
import pe.certus.com.service.CostCenterService;
import pe.certus.com.util.UserUtil;
import pe.certus.com.util.Utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static pe.certus.com.util.UserUtil.getUserFromJsonFormat;

@Service
public class CostCenterServiceImpl implements CostCenterService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CostCenterDao costCenterDao;

    @Override
    public Response registerExploitationUnit(Request request) throws SalesGeneralException {
        Response response = new Response();

        try {

            CostCenterConfiguration costCenterConfiguration = request.getCostCenterConfiguration();
            LOGGER.info("data for register exploitation unit" + Utils.obtenerJsonFormateado(costCenterConfiguration));
            Integer userID = getUserIdFromSession();
            LOGGER.info("User id from session " + userID);

            ExploitationUnit exploitationUnit
                    = fillExploitationUnitEntity(costCenterConfiguration,
                    new BigDecimal(userID),
                    request.getIpRequest());

            if (NumberUtils.INTEGER_ZERO < costCenterDao.registerExploitationUnit(exploitationUnit)) {
                response.setCode(NumberUtils.INTEGER_ONE.toString());
                response.setMessage("Registro satisfactorio");
                LOGGER.info("Registro satisfactorio");
            } else {
                throw new SalesGeneralException("No se registro la unidad de explotacion.",
                        Constants.ERROR_CODES.ERROR_REGISTER);
            }

        } catch (SalesGeneralException eX) {
            throw new SalesGeneralException(eX.getMessage(), Constants.ERROR_CODES.ERROR_REGISTER);
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
        return response;
    }

    @Override
    public Response getAllExploitationUnit(Request request) throws SalesGeneralException {
        Response response = new Response();
        List<CostCenterConfiguration> costCenterConfigurations = new ArrayList<CostCenterConfiguration>();
        try {

            LOGGER.info("request from: " + request.getIpRequest());

            for (ExploitationUnit exploitationUnit
                    : costCenterDao.getAllExploitationUnitByStatus(Constants.FLAG_ACTIVE)) {
                costCenterConfigurations.add(fillExploitationUnitMessage(exploitationUnit));
            }

            response.setCode(NumberUtils.INTEGER_ONE.toString());
            response.setMessage("Consulta exitosa.");
            response.setCostCenterConfigurations(costCenterConfigurations);


        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.ERROR_GET_ALL_ROWS);
        }
        return response;
    }

    @Override
    public Response deleteExploitationUnit(Integer code) throws SalesGeneralException {
        Response response = new Response();

        try {

            LOGGER.info("delete exploitation unit: " + code);
            Integer deleteResult = costCenterDao.deleteExploitationUnit(code);
            LOGGER.info("delete result: " + deleteResult);
            if (NumberUtils.INTEGER_ZERO == deleteResult) {
                throw new SalesGeneralException("Ocurrio un error al eliminar registro.",
                        Constants.ERROR_CODES.ERROR_DELETE);
            }

            response.setCode(NumberUtils.INTEGER_ONE.toString());
            response.setMessage("Se elimino registro satisfactoriamente.");

        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.ERROR_DELETE);
        }
        return response;
    }

    private ExploitationUnit fillExploitationUnitEntity(CostCenterConfiguration costCenterConfiguration,
                                                        BigDecimal idUser,
                                                        String requestIp) throws Exception {
        ExploitationUnit exploitationUnit = new ExploitationUnit();
        exploitationUnit.setCodCampus(costCenterConfiguration.getCode());
        exploitationUnit.setDescCampus(costCenterConfiguration.getDescription());
        exploitationUnit.setUnitExp(costCenterConfiguration.getCodeSelector());
        exploitationUnit.setDescUnitExp(costCenterConfiguration.getDescriptionSelector());
        exploitationUnit.setIdUser(idUser);
        exploitationUnit.setState(Constants.FLAG_ACTIVE);
        exploitationUnit.setCodUserCrea(idUser.toString()); // TODO registrar codigo active directory
        exploitationUnit.setDateUserCrea(new Date());
        exploitationUnit.setDeTermCrea(requestIp);
        return exploitationUnit;
    }

    private CostCenterConfiguration fillExploitationUnitMessage(ExploitationUnit exploitationUnit) {
        CostCenterConfiguration costCenterConfiguration = new CostCenterConfiguration();
        costCenterConfiguration.setIdCostCenter(exploitationUnit.getIdExpUnit().toPlainString());
        costCenterConfiguration.setCode(exploitationUnit.getCodCampus());
        costCenterConfiguration.setDescription(exploitationUnit.getDescCampus());
        costCenterConfiguration.setCodeSelector(exploitationUnit.getUnitExp());
        costCenterConfiguration.setDescriptionSelector(exploitationUnit.getDescUnitExp());
        return costCenterConfiguration;
    }

    @Override
    public Response registerProduct(Request request) throws SalesGeneralException {
        Response response = new Response();

        try {

            CostCenterConfiguration costCenterConfiguration = request.getCostCenterConfiguration();
            LOGGER.info("data for register product " + Utils.obtenerJsonFormateado(costCenterConfiguration));
            Integer userID = getUserIdFromSession();
            LOGGER.info("User id from session " + userID);

            Product product
                    = fillProductEntity(costCenterConfiguration,
                    new BigDecimal(userID),
                    request.getIpRequest());

            if (NumberUtils.INTEGER_ZERO < costCenterDao.registerProduct(product)) {
                response.setCode(NumberUtils.INTEGER_ONE.toString());
                response.setMessage("Registro satisfactorio");
                LOGGER.info("Registro satisfactorio");
            } else {
                throw new SalesGeneralException("No se registro el producto.",
                        Constants.ERROR_CODES.ERROR_REGISTER);
            }

        } catch (SalesGeneralException eX) {
            throw new SalesGeneralException(eX.getMessage(), Constants.ERROR_CODES.ERROR_REGISTER);
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
        return response;
    }

    @Override
    public Response getAllProducts(Request request) throws SalesGeneralException {
        Response response = new Response();
        List<CostCenterConfiguration> costCenterConfigurations = new ArrayList<CostCenterConfiguration>();
        try {

            LOGGER.info("request from: " + request.getIpRequest());

            for (Product product
                    : costCenterDao.getAllProductsByStatus(Constants.FLAG_ACTIVE)) {
                costCenterConfigurations.add(fillProductMessage(product));
            }

            response.setCode(NumberUtils.INTEGER_ONE.toString());
            response.setMessage("Consulta exitosa.");
            response.setCostCenterConfigurations(costCenterConfigurations);


        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.ERROR_GET_ALL_ROWS);
        }
        return response;
    }

    @Override
    public Response deleteProduct(Integer code) throws SalesGeneralException {
        Response response = new Response();

        try {

            LOGGER.info("delete product: " + code);
            Integer deleteResult = costCenterDao.deleteProduct(code);
            LOGGER.info("delete result: " + deleteResult);
            if (NumberUtils.INTEGER_ZERO == deleteResult) {
                throw new SalesGeneralException("Ocurrio un error al eliminar registro.",
                        Constants.ERROR_CODES.ERROR_DELETE);
            }

            response.setCode(NumberUtils.INTEGER_ONE.toString());
            response.setMessage("Se elimino registro satisfactoriamente.");

        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.ERROR_DELETE);
        }
        return response;
    }

    private Product fillProductEntity(CostCenterConfiguration costCenterConfiguration,
                                      BigDecimal idUser,
                                      String requestIp) throws Exception {
        Product product = new Product();
        product.setCodProg(costCenterConfiguration.getCode());
        product.setDescProg(costCenterConfiguration.getDescription());
        product.setCodProd(costCenterConfiguration.getCodeSelector());
        product.setDescProd(costCenterConfiguration.getDescriptionSelector());
        product.setIdUser(idUser);
        product.setState(Constants.FLAG_ACTIVE);
        product.setCodUserCrea(idUser.toString()); // TODO registrar codigo active directory
        product.setDateUserCrea(new Date());
        product.setDeTermCrea(requestIp);
        return product;
    }

    private CostCenterConfiguration fillProductMessage(Product product) {
        CostCenterConfiguration costCenterConfiguration = new CostCenterConfiguration();
        costCenterConfiguration.setIdCostCenter(product.getIdProduct().toPlainString());
        costCenterConfiguration.setCode(product.getCodProg());
        costCenterConfiguration.setDescription(product.getDescProg());
        costCenterConfiguration.setCodeSelector(product.getCodProd());
        costCenterConfiguration.setDescriptionSelector(product.getDescProd());
        return costCenterConfiguration;
    }

    @Override
    public Response registerDepartment(Request request) throws SalesGeneralException {
        Response response = new Response();

        try {

            CostCenterConfiguration costCenterConfiguration = request.getCostCenterConfiguration();
            LOGGER.info("data for register department " + Utils.obtenerJsonFormateado(costCenterConfiguration));
            Integer userID = getUserIdFromSession();
            LOGGER.info("User id from session " + userID);

            Department departmentEntity
                    = fillDepartmentEntity(costCenterConfiguration,
                    new BigDecimal(userID),
                    request.getIpRequest());

            if (NumberUtils.INTEGER_ZERO < costCenterDao.registerDepartment(departmentEntity)) {
                response.setCode(NumberUtils.INTEGER_ONE.toString());
                response.setMessage("Registro satisfactorio");
                LOGGER.info("Registro satisfactorio");
            } else {
                throw new SalesGeneralException("No se registro el departamento.",
                        Constants.ERROR_CODES.ERROR_REGISTER);
            }

        } catch (SalesGeneralException eX) {
            throw new SalesGeneralException(eX.getMessage(), Constants.ERROR_CODES.ERROR_REGISTER);
        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.GENERIC_ERROR);
        }
        return response;
    }

    @Override
    public Response getAllDepartments(Request request) throws SalesGeneralException {
        Response response = new Response();
        List<CostCenterConfiguration> costCenterConfigurations = new ArrayList<CostCenterConfiguration>();
        try {

            LOGGER.info("request from: " + request.getIpRequest());

            for (Department department
                    : costCenterDao.getAllDepartmentsByStatus(Constants.FLAG_ACTIVE)) {
                costCenterConfigurations.add(fillDepartmentMessage(department));
            }

            response.setCode(NumberUtils.INTEGER_ONE.toString());
            response.setMessage("Consulta exitosa.");
            response.setCostCenterConfigurations(costCenterConfigurations);


        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.ERROR_GET_ALL_ROWS);
        }
        return response;
    }

    @Override
    public Response deleteDepartment(Integer code) throws SalesGeneralException {
        Response response = new Response();

        try {

            LOGGER.info("delete department: " + code);
            Integer deleteResult = costCenterDao.deleteDepartment(code);
            LOGGER.info("delete result: " + deleteResult);
            if (NumberUtils.INTEGER_ZERO == deleteResult) {
                throw new SalesGeneralException("Ocurrio un error al eliminar registro.",
                        Constants.ERROR_CODES.ERROR_DELETE);
            }

            response.setCode(NumberUtils.INTEGER_ONE.toString());
            response.setMessage("Se elimino registro satisfactoriamente.");

        } catch (Exception e) {
            throw new SalesGeneralException(e.getMessage(), Constants.ERROR_CODES.ERROR_DELETE);
        }
        return response;
    }

    private Integer getUserIdFromSession() {
        return Integer.parseInt(getUserFromJsonFormat(UserUtil.getSessionIDFromWebAuthenticationDetails()).getCode());
    }

    private Department fillDepartmentEntity(CostCenterConfiguration costCenterConfiguration,
                                            BigDecimal idUser,
                                            String requestIp) throws Exception {
        Department department = new Department();
        department.setCodLevel(costCenterConfiguration.getCode());
        department.setDescLevel(costCenterConfiguration.getDescription());
        department.setCodDept(costCenterConfiguration.getCodeSelector());
        department.setDescDept(costCenterConfiguration.getDescriptionSelector());
        department.setIdUser(idUser);
        department.setState(Constants.FLAG_ACTIVE);
        department.setCodUserCrea(idUser.toString()); // TODO registrar codigo active directory
        department.setDateUserCrea(new Date());
        department.setDeTermCrea(requestIp);
        return department;
    }

    private CostCenterConfiguration fillDepartmentMessage(Department department) {
        CostCenterConfiguration costCenterConfiguration = new CostCenterConfiguration();
        costCenterConfiguration.setIdCostCenter(department.getIdDepartment().toPlainString());
        costCenterConfiguration.setCode(department.getCodLevel());
        costCenterConfiguration.setDescription(department.getDescLevel());
        costCenterConfiguration.setCodeSelector(department.getCodDept());
        costCenterConfiguration.setDescriptionSelector(department.getDescDept());
        return costCenterConfiguration;
    }

}