package pe.certus.com.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import pe.certus.com.config.ApplicationProperties;
import pe.certus.com.service.UserService;
import pe.certus.com.util.AuthenticationUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AppLogoutSuccessHandler extends
        SimpleUrlLogoutSuccessHandler implements LogoutSuccessHandler {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    @Autowired
    ApplicationProperties properties;

    @Autowired
    UserService userService;

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onLogoutSuccess(
            HttpServletRequest request,
            HttpServletResponse response,
            Authentication authentication)
            throws IOException, ServletException {

        try {

            String refererUrl = request.getHeader("Referer");
            LOGGER.info("logout url request: " + refererUrl);

            String targetUrl = AuthenticationUtil.getTargetRedirect(properties, refererUrl);
            LOGGER.info("logout url redirect: " + targetUrl);

            if (authentication != null) {
                destroySession(authentication);
            }

            session().setAttribute("isLogged", false);
            session().invalidate();
            redirectStrategy.sendRedirect(request, response, targetUrl);

        } catch (Exception e) {
            LOGGER.error("Error: " + e.getMessage(), e);
            redirectStrategy.sendRedirect(request, response, properties.getUrlLoginPrivate());
        }
    }

    private void destroySession(Authentication authentication) {
        String sessionId = ((WebAuthenticationDetails) authentication.getDetails()).getSessionId();
        userService.destroySession(sessionId);
    }

    private HttpSession session() {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        return attr.getRequest().getSession(true);
    }

}
