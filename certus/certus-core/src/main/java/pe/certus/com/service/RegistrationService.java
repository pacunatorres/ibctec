package pe.certus.com.service;

import pe.certus.com.exception.SalesGeneralException;
import pe.certus.com.request.Request;
import pe.certus.com.response.Response;

public interface RegistrationService {

    Response getAllRegistry(Request request) throws SalesGeneralException;

}
